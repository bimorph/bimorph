<?

header("Access-Control-Allow-Origin:*");

if(!$BMF_CLUMP = rb("bmf-clump", "hide", "id", 0)){ //mpre("Нет видимых скоплений в админке");
}elseif(1 < count($BMF_CLUMP)){ mpre("Подключаемых скопления в админке больше одного", $BMF_CLUMP);
}elseif(!$bmf_clump = first($BMF_CLUMP)){ mpre("ОШИБКА выборки первого скопления из списка");
}elseif(!$path = "modules/bmf/sh/cpp/{$bmf_clump["id"]}"){ mpre("ОШИБКА получения пути к подключаемой базе данных");
}elseif(!$database = mpql(mpqw("PRAGMA database_list"))){ mpre("ОШИБКА получения списка баз данных");
//}else if(!is_string($realpath = (realpath($path) ?: ""))){ mpre("ОШИБКА получения полного пути до файла", $path);
}elseif(!$_database = array_column($database, "file", "name")){ mpre("ОШИБКА получения баз данных поименно");
}else if(!is_string($now = (get($_database, "clump") ?: ""))){ mpre("ОШИБКА получения текущей подключенной БД");
}else if(!$_database = call_user_func(function($_database) use($now){ // Отключение другой подключенной БД
		if(!$now){ //mpre("Других БД не подключено");
		//}elseif($realpath == $now){ //mpre("Подключена нужная БД ($now)");
		}elseif(!qw("DETACH DATABASE clump")){ mpre("ОШИБКА подключения таблицы скопления", $_database);
		}elseif(!$database = mpql(mpqw("PRAGMA database_list"))){ mpre("ОШИБКА получения списка баз данных");
		}elseif(!$_database = array_column($database, "file", "name")){ mpre("ОШИБКА получения баз данных поименно");
		}else{ //mpre("Отключаем БД {$now}");
		} return $_database;
	}, $_database)){ mpre("ОШИБКА отключения БД");
}elseif($f = get($_database, "clump")){ mpre("База данных уже подключена `{$f}`", $_database);
}elseif(!qw("ATTACH DATABASE `{$path}` AS clump")){ mpre("ОШИБКА подключения таблицы скопления", $_database);
}else{ //mpre($bmf_clump, $path);
}
