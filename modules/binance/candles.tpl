<? if(!$BINANCE_CANDLES = rb("binance-candles", 90)): mpre("ОШИБКА выборки списка свечей"); ?>
<? elseif(!$_BINANCE_CANDLES = array_values($BINANCE_CANDLES)): mpre("ОШИБКА получения значений по порядку") ?>
<? elseif(!$max = max(array_column($BINANCE_CANDLES, "id"))): mpre("ОШИБКА расчета максимального идентификатора") ?>
<? elseif(!$BINANCE_CANDLES = array_map(function($nn, $binance_candles) use($_BINANCE_CANDLES){ // Расчет прибыли
		if(0 > ($_nn = $nn+1)){ mpre("ОШИБКА расчета предыдущего значения");
		}else if(!$_binance_candles = get($_BINANCE_CANDLES, $_nn)){ //mpre("ОШИБКА получения свечи прогноза");
		}else if(!$shift = $binance_candles["open"]-$binance_candles["close"]){ mpre("ОШИБКА расчета отклонения");
		//}else if(!is_numeric($diff_min = min(abs($_binance_candles["prediction"]), abs($shift)))){ mpre("ОШИБКА нахождения минимальной прибыли");
		//}else if(!is_numeric($diff_max = max(abs($_binance_candles["prediction"]), abs($shift)))){ mpre("ОШИБКА нахождения минимальной прибыли");
		//}else if(!is_numeric($diff = (abs($_binance_candles["prediction"]) > abs($shift) ? $diff_min : $diff_max))){ mpre("ОШИБКА расчета разницы");
		}else if(!is_numeric($diff = (abs($_binance_candles["prediction"]) > abs($shift) ? abs($shift) : abs($shift)))){ mpre("ОШИБКА расчета разницы");
		}else if(!$binance_candles["commission"] = number_format($binance_candles["close"]*0.001, 2, '.', '')){ mpre("Расчет комиссии");
		}else if(!$profit = number_format(0 > $_binance_candles["prediction"]*$shift ? $diff : -$diff, 2, ".", "")){ mpre("ОШИБКА расчета прибыли свечи");
		//}else if(true){ mpre($binance_candles, $commission, $profit);
		}else if(!$binance_candles["profit"] = $profit-$binance_candles["commission"]){ mpre("ОШИБКА расчета прибыли свечи с учетом комиссии");
		}else{ //mpre("Свеча {$binance_candles["id"]} прибыль свечи {$profit}");
		} return $binance_candles;
	}, array_keys($_BINANCE_CANDLES), $_BINANCE_CANDLES)): mpre("ОШИБКА расчета прибыли свечей") ?>
<? elseif(!$_BINANCE_CANDLES = array_values($BINANCE_CANDLES)): mpre("ОШИБКА получения значений по порядку") ?>
<? elseif(!$BINANCE_CANDLES = array_map(function($nn, $binance_candles) use($_BINANCE_CANDLES){ // Итоговая прибыль
		if(!$_BINANCE_CANDLES_ = array_slice($_BINANCE_CANDLES, $nn)){ mpre("ОШИБКА получения списка за свечей");
		}else if(!$profits = array_column($_BINANCE_CANDLES_, "profit")){ //mpre("ОШИБКА получения списка прибылей свечей");
		}else if(!is_numeric($binance_candles["itog"] = array_sum($profits))){ mpre("ОШИБКА расчета прибыли свечи");
		}else{ //mpre($binance_candles, $itog);
		} return $binance_candles;
	}, array_keys($_BINANCE_CANDLES), array_values($_BINANCE_CANDLES))): mpre("ОШИБКА расчета прибыли") ?>
<? else: //mpre($BINANCE_CANDLES) ?>
	<h2>Предсказание цены биткоин</h2>
	<div class="table candles">
		<style>
			.table.candles > div > span { border-collapse:collapse; border:1px solid #bbb; white-space:nowrap; }
		</style>
		<div class="th">
			<span>Свеча</span>
			<span>Время предсказания</span>
			<span>Время открытия</span>
			<span>Время закрытия</span>
			<span>Открытие</span>
			<span>Закрытие</span>
			<span>Объем торгов</span>
			<span>Изменение</span>
			<span>Прогноз</span>
			<span>Комиссия</span>
			<span>Позиция</span>
			<span>Прибыль</span>
		</div>
		<? foreach($BINANCE_CANDLES as $binance_candles): ?>
			<? //mpre("Время", date("YdHG"), date("YdHG", $binance_candles["openTime"]/1e3+60*60)) ?>
			<? if(!is_numeric($price = number_format($binance_candles["close"]-$binance_candles["open"], 2, '.', ''))): mpre("ОШИБКА расчета изменения", $binance_candles, $price) ?>
			<? elseif(!is_numeric($close = $binance_candles["close"])): mpre("ОШИБКА расчета изменения") ?>
			<? elseif(!$closeTime = date("Y.m.d H:i", $binance_candles["closeTime"]/1e3)): mpre("ОШИБКА расчета времени") ?>
			<? elseif(!$openTime = date("Y.m.d H:i", $binance_candles["openTime"]/1e3)): mpre("ОШИБКА расчета времени") ?>
			<? elseif(!is_numeric($prediction = (float)get($binance_candles["prediction"]))): mpre("ОШИБКА получения прогноза") ?>
			<? elseif(!$color_profit = (0 > get($binance_candles, "profit") ? "red" : "green")): mpre("ОШИБКА расчета подсветки") ?>
			<? elseif(!$color_itog = (0 > get($binance_candles, "itog") ? "red" : "green")): mpre("ОШИБКА расчета подсветки") ?>
			<?// elseif(!is_numeric($commission = get($binance_candles, "close")*0.001)): mpre("ОШИБКА расчета комиссии за ставку") ?>
			<? //elseif(!is_numeric($itog = (isset($itog) ? $itog : 0)+ (get($binance_candles, "profit") ?: 0))): mpre("ОШИБКА получения итоговой прибыли") ?>
			<? else: //mpre($binance_candles) ?>
				<div>
					<span title="Номер свечи"><?=$binance_candles["id"]?></span>
					<span title="Время прогноза"><?=date("Y.m.d H:i", $binance_candles["time"])?></span>
					<span title="Время начала свечи"><?=$openTime?></span>
					<span title="Время окончания свечи"><?=$closeTime?></span>
					<span title="Цена открытия свечи"><?=$binance_candles["open"]?></span>
					<span title="Цена закрытия свечи"><?=($close ?: "")?></span>
					<span title="Объем торгов свечи"><?=$binance_candles["assetBuyVolume"]?></span>
					<span title="Изменения цены свечи"><?=($price ?: "")?></span>
					<span style="font-weight:bold;" title="Прогноз цены на следующую свечу">&#8657;<?=$prediction?></span>
					<span title="Комиссия за ставку"><?=get($binance_candles, "commission")?></span>
					<span style="color:<?=$color_profit?>;" title="Прибыль за ставку"><?=get($binance_candles, "profit")?></span>
					<span style="color:<?=$color_itog?>" title="Итоговая прибыль за весь период"><?=get($binance_candles, "itog")?></span>
				</div>
			<? endif; ?>
		<? endforeach; ?>
	</div>
<? endif; ?>
