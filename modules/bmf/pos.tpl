<h1>Список результатов</h1>
<? if(!$CALC = rb("calc")): mpre("ОШИБКА выборки расчетов") ?>
<? elseif(!$CALC_POS = rb("calc_pos")): mpre("ОШИБКА выборки списка позиций") ?>
<? elseif(!$CALC_VAL = rb("calc_val")): mpre("ОШИБКА выборки списка значений") ?>
<? else: //mpre($CALC_POS) ?>
	<? foreach($CALC_POS as $calc_pos): ?>
		<div class="table calc" style="text-align:center;">
			<style>
				.table.calc > div > span { border:1px solid #888; }
				.table > div > span.bold { background-color:#ddd; font-weight:bold; }
				.table.calc > div > span.red { background-color:lime; }
				
				.row .col-md-3 {display:none;}
				.col-md-9 { width: auto;}
				.container {max-width:100%;}
			</style>
			<div class="th">
				<span><?=$calc_pos["name"]?></span>
				<span>Стр.</span>
				<? foreach($CALC_POS as $_calc_pos): ?>
					<span><?=$_calc_pos["name"]?></span>
				<? endforeach; ?>
				<span>Млд.</span>
				<? foreach($CALC_POS as $_calc_pos): ?>
					<span><?=$_calc_pos["name"]?></span>
				<? endforeach; ?>
			</div>
			<? foreach([""=>[]]+$CALC_POS as $_calc_pos): ?>
				<div>
					<span class="th"><?=get($_calc_pos, "name")?></span>
					<? foreach(range(0, 1) as $val): ?>
						<? if(!is_numeric($_val = ("1" == $val ? "0" : "1"))): mpre("ОШИБКА получения противозначения"); ?>
						<? elseif(!$POS = array_map(function($pos, $v = ["-", "Ошибка"]) use($calc_pos, $_calc_pos, $val, $_val, $CALC, $CALC_VAL){ // Расчет левого поля
								if(!$calc_val = rb($CALC_VAL, "v1", "v0", $pos["v{$_val}"], $pos["v{$_val}"])){ mpre("ОШИБКА получения расчета");
								}else if(!$calc = rb($CALC, "calc_pos_id", "calc_val_id", $calc_pos["id"], $calc_val["id"])){ mpre("ОШИБКА получения расчета");
								}else if(!$_calc_pos){ $v = [$calc["val"], "[{$pos["v{$_val}"]} {$calc_pos["description"]} {$pos["v{$_val}"]}]"];
								}else if(!$_calc_val = rb($CALC_VAL, "v1", "v0", $pos["v{$val}"], $pos["v{$val}"])){ mpre("ОШИБКА получения левого нижестоящего");
								}else if(!$_calc = rb($CALC, "calc_pos_id", "calc_val_id", $_calc_pos["id"], $_calc_val["id"])){ mpre("ОШИБКА получения левого расчета");
								}else if(!$calc_val = rb($CALC_VAL, "v1", "v0", $_calc["val"], $pos["v{$_val}"])){ mpre("ОШИБКА получения левого нижестоящего");
								}else if(!$calc = rb($CALC, "calc_pos_id", "calc_val_id", $calc_pos["id"], $calc_val["id"])){ mpre("ОШИБКА получения левого расчета");
								}else if(!$_desc = "{$pos["v{$val}"]} {$_calc_pos["description"]} {$pos["v{$val}"]}"){ mpre("ОШИБКА формирования левого описания");
								}else if(!$desc = "{$_desc} [{$_calc["val"]} {$calc_pos["description"]} {$pos["v{$_val}"]}]"){ mpre("ОШИБКА формирования описания");
								}else if(!$v = [$calc["val"], $desc]){ mpre("ОШИБКА установки результата расчета");
								}else{ //mpre("Расчет левого результата", $calc);
								} return $v;
							}, $CALC_POS)): mpre("ОШИБКА получения результата") ?>
						<? elseif(!$pos = array_map(function($pos){ // Формат знаков
								return "<span title=\"{$pos[1]}\">{$pos[0]}</span>";
							}, $POS)): mpre("ОШИБКА форматирования знаков") ?>
						<? else: ?>
							<span class="bold" style="vertical-align:middle;"><?=implode(null, $pos)?></span>
							<? foreach($CALC_POS as $_calc_pos_): ?>
								<? if(!$Calc = function($vl1, $vl0, $vr1, $vr0, $v = ["-", "Неизвестно"]) use($calc_pos, $_calc_pos, $val, $_val, $_calc_pos_, $CALC, $CALC_VAL){ // Расчет значения входящих сигналов
										if(!$_calc_val = rb($CALC_VAL, "v1", "v0", $vr1, $vr0)){ mpre("ОШИБКА получения правого морфа");
										}else if(!$_calc = rb($CALC, "calc_pos_id", "calc_val_id", $_calc_pos_["id"], $_calc_val["id"])){ mpre("ОШИБКА получения расчета правого морфа");
										}else if(!$calc_val = rb($CALC_VAL, "v1", "v0", $vl1, $_calc["val"])){ mpre("ОШИБКА получения расчета");
										}else if(!$calc = rb($CALC, "calc_pos_id", "calc_val_id", $calc_pos["id"], $calc_val["id"])){ mpre("ОШИБКА получения расчета");
										}else if(!$_calc_pos){ $v = [$calc["val"], "[{$vl1} {$calc_pos["description"]} {$_calc["val"]}] {$vr1} {$_calc_pos_["description"]} {$vr0}"];
										}else if(!$_calc_val = rb($CALC_VAL, "v1", "v0", $vl1, $vl0)){ mpre("ОШИБКА получения левого нижестоящего");
										}else if(!$_calc = rb($CALC, "calc_pos_id", "calc_val_id", $_calc_pos["id"], $_calc_val["id"])){ mpre("ОШИБКА получения левого расчета");
										}else if(!$_calc_val_ = rb($CALC_VAL, "v1", "v0", $vr1, $vr0)){ mpre("ОШИБКА получения левого нижестоящего");
										}else if(!$_calc_ = rb($CALC, "calc_pos_id", "calc_val_id", $_calc_pos_["id"], $_calc_val_["id"])){ mpre("ОШИБКА получения левого расчета");
										}else if(!$calc_val = rb($CALC_VAL, "v1", "v0", $_calc["val"], $_calc_["val"])){ mpre("ОШИБКА получения левого нижестоящего");
										}else if(!$calc = rb($CALC, "calc_pos_id", "calc_val_id", $calc_pos["id"], $calc_val["id"])){ mpre("ОШИБКА получения левого расчета");
										}else if(!$_desc = "{$vl1} {$_calc_pos["description"]} {$vl0}"){ mpre("ОШИБКА формирования левого описания");
										}else if(!$_desc_ = "{$vr1} {$_calc_pos_["description"]} {$vr0}"){ mpre("ОШИБКА формирования правого описания");
										}else if(!$desc = "{$_desc} [{$_calc["val"]} {$calc_pos["description"]} {$_calc_["val"]}] {$_desc_}"){ mpre("ОШИБКА формирования описания");
										}else if(!$v = [$calc["val"], $desc]){ mpre("ОШИБКА установки результата расчета");
										}else{ //mpre("Расчет левого результата");
										} return $v;
									}): mpre("ОШИБКА расчета значения входящих сигналов") ?>
								<? elseif(!$_POS = array_map(function($pos) use($Calc, $calc_pos, $_calc_pos, $val, $_val, $_calc_pos_, $CALC, $CALC_VAL){ // Расчет левого поля
										if(!$v = $Calc($pos["v{$_val}"], $pos["v{$_val}"], $pos["v{$val}"], $pos["v{$val}"])){ mpre("ОШИБКА получения правого морфа");
										}else{ //mpre("Расчет левого результата");
										} return $v;
									}, $CALC_POS)): mpre("ОШИБКА получения результата") ?>
								<? elseif(!$ps = array_map(function($_pos){ // Формат знаков
										return "<span title=\"{$_pos[1]}\">{$_pos[0]}</span>";
									}, $_POS)): mpre("ОШИБКА форматирования знаков") ?>
								<? elseif(!is_array($class = call_user_func(function($class = []) use($_calc_pos, $_calc_pos_){ // Класс значения
										if(!is_array($class += ($_calc_pos ? [] : ["bold"]))){ mpre("ОШИБКА получения класса жирности");
										}else if(!is_array($class += (("01" == get($_calc_pos, "name")) && ("11" == get($_calc_pos_, "name")) ? ["red"] : []))){ mpre("ОШИБКА получения подсветки красного");
										}else if(!is_array($class += (("11" == get($_calc_pos, "name")) && ("01" == get($_calc_pos_, "name")) ? ["red"] : []))){ mpre("ОШИБКА получения подсветки красного");
										}else if(!is_array($class += (("00" == get($_calc_pos, "name")) && ("10" == get($_calc_pos_, "name")) ? ["red"] : []))){ mpre("ОШИБКА получения подсветки красного");
										}else if(!is_array($class += (("10" == get($_calc_pos, "name")) && ("00" == get($_calc_pos_, "name")) ? ["red"] : []))){ mpre("ОШИБКА получения подсветки красного");
										}else{ //mpre($_calc_pos, $_calc_pos_);
										} return $class;
									}))): mpre("ОШИБКА получения класса значения") ?>
								<? else: // mpre($_pos); ?>
									<span class="<?=implode(" ", $class)?>">
										<b><?=implode(null, $ps)?></b>
										<? if(!$_calc_pos): //mpre("Заголовок") ?>
										<? else: ?>
											<div class="table">
												<div class="th">
													<span></span>
													<span></span>
													<? foreach($CALC_POS as $_pos): ?>
														<span><?=$_pos["name"]?></span>
													<? endforeach; ?>
												</div>
												<div>
													<span class="th"></span>
													<span class="th"></span>
													<? foreach($CALC_POS as $_pos): ?>
														<? if(!$POS = array_map(function($p) use($Calc, $_pos, $calc_pos, $_calc_pos, $val, $_val, $_calc_pos_, $CALC, $CALC_VAL){ // Расчет левого поля
																if(!$_calc_val = rb($CALC_VAL, "v1", "v0", $p["v{$val}"], $p["v{$val}"])){ mpre("ОШИБКА получения левого нижестоящего");
																}else if(!$_calc = rb($CALC, "calc_pos_id", "calc_val_id", $_pos["id"], $_calc_val["id"])){ mpre("ОШИБКА получения левого расчета");
																}else if(!$v = $Calc($p["v{$_val}"], $p["v{$_val}"], $p["v{$_val}"], $_calc["val"])){ mpre("ОШИБКА получения правого морфа");
																}else{ //mpre("Расчет левого результата");
																} return $v;
															}, $CALC_POS)): mpre("ОШИБКА получения результата") ?>
														<? elseif(!$ps = array_map(function($_pos){ // Формат знаков
																return "<span title=\"{$_pos[1]}\">{$_pos[0]}</span>";
															}, $POS)): mpre("ОШИБКА форматирования знаков") ?>
														<? else: ?>
															<span class="bold"><?=implode(null, $ps)?></span>
														<? endif; ?>
													<? endforeach; ?>
												</div>
												<? foreach($CALC_POS as $pos): ?>
													<div>
														<? if(!$POS = array_map(function($p) use($Calc, $pos, $calc_pos, $_calc_pos, $val, $_val, $_calc_pos_, $CALC, $CALC_VAL){ // Расчет левого поля
																if(!$_calc_val = rb($CALC_VAL, "v1", "v0", $p["v{$val}"], $p["v{$val}"])){ mpre("ОШИБКА получения левого нижестоящего");
																}else if(!$_calc = rb($CALC, "calc_pos_id", "calc_val_id", $pos["id"], $_calc_val["id"])){ mpre("ОШИБКА получения левого расчета");
																}else if(!$v = $Calc($_calc["val"], $p["v{$_val}"], $p["v{$_val}"], $p["v{$_val}"])){ mpre("ОШИБКА получения правого морфа");
																}else{ //mpre("Расчет левого результата");
																} return $v;
															}, $CALC_POS)): mpre("ОШИБКА получения результата") ?>
														<? elseif(!$ps = array_map(function($_pos){ // Формат знаков
																return "<span title=\"{$_pos[1]}\">{$_pos[0]}</span>";
															}, $POS)): mpre("ОШИБКА форматирования знаков") ?>
														<? else: ?>
															<span class="th"><?=$pos["name"]?></span>
															<span class="bold"><?=implode(null, $ps)?></span>
															<? foreach($CALC_POS as $_pos): ?>
																<? if(!$POS = array_map(function($p) use($Calc, $pos, $_pos, $calc_pos, $_calc_pos, $val, $_val, $_calc_pos_, $CALC, $CALC_VAL){ // Расчет левого поля
																		if(!$_calc_val = rb($CALC_VAL, "v1", "v0", $p["v{$val}"], $p["v{$val}"])){ mpre("ОШИБКА получения левого нижестоящего");
																		}else if(!$_calc = rb($CALC, "calc_pos_id", "calc_val_id", $pos["id"], $_calc_val["id"])){ mpre("ОШИБКА получения левого расчета");
																		}else if(!$_calc_val_ = rb($CALC_VAL, "v1", "v0", $p["v{$val}"], $p["v{$val}"])){ mpre("ОШИБКА получения левого нижестоящего");
																		}else if(!$_calc_ = rb($CALC, "calc_pos_id", "calc_val_id", $_pos["id"], $_calc_val_["id"])){ mpre("ОШИБКА получения левого расчета");
																		}else if(!$v = $Calc($_calc["val"], $p["v{$_val}"], $p["v{$_val}"], $_calc_["val"])){ mpre("ОШИБКА получения правого морфа");
																		}else{ //mpre("Расчет левого результата");
																		} return $v;
																	}, $CALC_POS)): mpre("ОШИБКА получения результата") ?>
																<? elseif(!$ps = array_map(function($pos){ // Формат знаков
																		return "<span title=\"{$pos[1]}\">{$pos[0]}</span>";
																	}, $POS)): mpre("ОШИБКА форматирования знаков") ?>
																<? else: ?>
																	<span><?=implode(null, $ps)?></span>
																<? endif; ?>
															<? endforeach; ?>
														<? endif; ?>
													</div>
												<? endforeach; ?>
											</div>
										<? endif; ?>
									</span>
								<? endif; ?>
							<? endforeach; ?>
						<? endif; ?>
					<? endforeach; ?>
				</div>
			<? endforeach; ?>
		</div>
	<? endforeach; ?>
<? endif; ?>
