<? if(!$TEST_GROUP = rb("test_group")): mpre("ОШИБКА получения списка групп") ?>
<? //elseif(!$colorl = [1=>"00010010", "00010011", "00011010", "00011011", "00110000"]): mpre("Подсвечиваем правую половину") ?>
<? //elseif(!$colorr = [1=>"01010010", "01010011", "10101010", "11001011"]): mpre("Подсвечиваем правую половину") ?>
<? elseif(!$_test_group = rb($TEST_GROUP, "name", "[00000000]")): mpre("ОШИБКА получения нулевой группы") ?>
<? else: //mpre($test) ?>
	<div class="table test_group" style="width:auto; margin:10px;">
		<style>
			.table.test_group > div > span { padding:2px; text-align:center; }
		</style>
		<div class="th">
			<span>Порядок</span>
			<span>Группа</span>
			<span>loop</span>
			<span>index</span>
			<span>Количество</span>
			<span>100</span>
			<span>Лучш</span>
			<span>100</span>
			<span>Количество</span>
			<span>loop</span>
			<span>index</span>
			<span>Группа</span>
			<span>Элемент</span>
		</div>
		<? foreach($TEST_GROUP as $test_group): ?>
			<? if(!is_array($TEST_BIN = rb("test_bin", "test_group_id", "id", $test_group["id"]))): mpre("ОШИБКА получения среднего количества итераций") ?>
			<? elseif(!$testl = ql("SELECT AVG(`loop`) AS loop, AVG(`index`) AS `index` FROM `mp_bmf_test` WHERE `test_bin_id` IN (". in($TEST_BIN). ") AND loop<>100", 0)): mpre("ОШИБКА получения среднего количества итераций") ?>
			<? elseif(!is_numeric($testl["count"] = ql("SELECT COUNT(*) AS `count` FROM `mp_bmf_test` WHERE `test_bin_id` IN (". in($TEST_BIN). ")", 0, 'count'))): mpre("ОШИБКА получения количества значений") ?>
			<? elseif(!is_numeric($testl["max"] = ql("SELECT COUNT(loop) AS `max` FROM `mp_bmf_test` WHERE `test_bin_id` IN (". in($TEST_BIN). ") AND loop=100", 0, 'max'))): mpre("ОШИБКА получения количества максимальных значений") ?>
			<? elseif(!$_TEST_BIN = rb("test_bin", "bmf-test_group", "id", $test_group["id"])): mpre("ОШИБКА получения среднего количества итераций") ?>
			<? elseif(!$testr = ql("SELECT AVG(`loop`) AS loop, AVG(`index`) AS `index` FROM `mp_bmf_test` WHERE `test_bin_id` IN (". in($_TEST_BIN). ") AND loop<>100", 0)): mpre("ОШИБКА получения среднего количества итераций") ?>
			<? elseif(!is_numeric($testr["count"] = ql("SELECT COUNT(*) AS `count` FROM `mp_bmf_test` WHERE `test_bin_id` IN (". in($_TEST_BIN). ")", 0, 'count'))): mpre("ОШИБКА получения количества значений") ?>
			<? elseif(!is_numeric($testr["max"] = ql("SELECT COUNT(loop) AS `max` FROM `mp_bmf_test` WHERE `test_bin_id` IN (". in($_TEST_BIN). ") AND loop=100", 0, 'max'))): mpre("ОШИБКА получения количества максимальных значений") ?>
			<? elseif(!is_array($test_bin = rb("test_bin", "test_group_id", "bmf-test_group", $test_group["id"], $_test_group["id"]))): mpre("ОШИБКА получения номера группы") ?>
			<? elseif(!is_array($_test_bin = rb("test_bin", "test_group_id", "bmf-test_group", $_test_group["id"], $test_group["id"]))): mpre("ОШИБКА получения номера группы") ?>
			<? else: //mpre($_test_group, $test_group); ?>
				<div>
					<span><?=get($test_bin, "calc_num")?></span>
					<span style="font-weight:bold; background-color:<?=(min(max($l = get($testl, 'loop'), 1), 5) == $l ? "yellow" : "inherit")?>;"><?=$test_group["name"]?></span>
					<span title="Среднее количество морфов в сборке левой части"><?=number_format(get($testl, 'loop'), 4)?></span>
					<span title="Среднее количество итераций левой части"><?=number_format(get($testl, 'index'), 4)?></span>
					<span style="font-weight:bold;" title="Количество расчетов в левой части"><?=get($testl, 'count')?></span>
					<span title="Количество расчетов в левой части с максимальным значением"><?=get($testl, 'max')?></span>
					<span title="Сумма результатов"><?=(get($testl, 'max')+ get($testr, 'max'))?></span>
					<span title="Количество расчетов в правой части с максимальным значением"><?=get($testr, 'max')?></span>
					<span style="font-weight:bold;" title="Количество расчетов в левой части"><?=get($testr, 'count')?></span>
					<span title="Среднее количество итераций правой части"><?=number_format(get($testr, 'loop'), 4)?></span>
					<span title="Среднее количество морфов в сборке правой части"><?=number_format(get($testr, 'index'), 4)?></span>
					<span style="font-weight:bold; background-color:<?=(min(max($l = get($testr, 'loop'), 1), 5) == $l ? "yellow" : "inherit")?>;"><?=$test_group["name"]?></span>
					<span><?=get($_test_bin, "calc_num")?></span>
				</div>
			<? endif; ?>
		<? endforeach; ?>
	</div>
<? endif; ?>
