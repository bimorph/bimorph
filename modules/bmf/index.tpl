<? if(!$cut = ["m"]): mpre("ОШИБКА установки вырезаемых значений"); ?>
<? elseif(!$data = ['dano', 'itog', 'дано', 'итог']): mpre("ОШИБКА установки неиспользуемых имен"); ?>
<? //elseif(!is_array($get = array_diff_key(array_diff_key($_GET, array_flip($cut)), array_flip($data)))): mpre("Убираем информацию о модуле"); ?>
<? //elseif(!is_array($flip = array_flip($get))): mpre("ОШИБКА переварачивания входных значений"); ?>
<? elseif(!$db = (array_key_exists("null", $_GET) ? get($_GET, "") : array_search("", $_GET))): mpre("ОШИБКА сеть не указана <a href='/bmf/test'>/bmf/test</a>", $flip); ?>
<? //elseif(true): mpre($_GET, $db) ?>
<? elseif(!call_user_func(function($alias) use($db, $conf){ // Получения идентификатора соединения с БД
		if(!$database = mpql(mpqw("PRAGMA database_list"))){ mpre("ОШИБКА получения списка баз данных");
		}else if(!$dbname = "{$db}.sqlite"){ mpre("ОШИБКА получения файла БД {$db}");
		}else if(!$path = "modules/bmf/sh/cpp/clump/{$dbname}"){ mpre("ОШИБКА получения пути до файла");
		}else if(!file_exists($path)){ //mpre("ОШИБКА файл с БД не найден (возможно он еще небыл создан)", $filename);
		}elseif(!$_database = array_column($database, "file", "name")){ mpre("ОШИБКА получения баз данных поименно");
		}else if(!is_string($now = (get($_database, $alias) ?: ""))){ mpre("ОШИБКА получения текущей подключенной БД");
		}else if(!$_database = call_user_func(function($_database) use($now, $alias){ // Отключение другой подключенной БД
				if(!$now){ //mpre("Других БД не подключено");
				//}elseif($realpath == $now){ //mpre("Подключена нужная БД ($now)");
				}elseif(!qw("DETACH DATABASE {$alias}")){ mpre("ОШИБКА подключения таблицы скопления", $_database);
				}elseif(!$database = mpql(mpqw("PRAGMA database_list"))){ mpre("ОШИБКА получения списка баз данных");
				}elseif(!$_database = array_column($database, "file", "name")){ mpre("ОШИБКА получения баз данных поименно");
				}else{ //mpre("Отключаем БД {$now}");
				} return $_database;
			}, $_database)){ mpre("ОШИБКА отключения БД");
		}elseif($f = get($_database, $alias)){ mpre("База данных уже подключена `{$f}`", $_database);
		}elseif(!qw("ATTACH DATABASE `{$path}` AS {$alias}")){ mpre("ОШИБКА подключения таблицы скопления", $_database);
		}elseif(!$database = mpql(mpqw("PRAGMA database_list"))){ mpre("ОШИБКА получения списка баз данных");
		}else{ //mpre($database);
		} return $database;
	}, 'test')): mpre("<<< Загрузите данные для расчетов в форму слева") ?>
<? elseif(!$BMF_DANO_VALUES = mpqn(mpqw("SELECT * FROM test.mp_bmf_dano_values"))): mpre("ОШИБКА получения списка дано") ?>
<? elseif(!$BMF_ITOG_VALUES = mpqn(mpqw("SELECT * FROM test.mp_bmf_itog_values"))): mpre("ОШИБКА получения списка итогов") ?>
<? else: //mpre($BMF_INDEX) ?>
	<div>
		<style>
			.itog.update input {background-image:url(/img/loading6.gif); background-position:95% 50%; background-repeat:no-repeat; /*background-size: 80px 20px;*/}
		</style>
		<script sync>
			(function($, script){
				$(script).parent()/*.on("change", ".dano input", function(e){
					$(e.delegateTarget).find("form").trigger("submit");
					$(e.delegateTarget).find(".itog").addClass("update");
					$(e.delegateTarget).find(".itog").find("input").attr("value", "");
				})*/.one("init", function(e){
					$(FORMS = $(e.currentTarget).is("form") ? e.currentTarget : $(e.currentTarget).find("form")).on("submit", function(e){
						$(e.delegateTarget).find(".itog").addClass("update");
						$(e.delegateTarget).find("button[type=submit]").prop("disabled", true);
						$.ajax({
							type: 'POST',
							url: $(e.currentTarget).attr('action'),
							data: $(e.currentTarget).serialize(),
							dataType: 'json',
						}).done(function(json){
							console.log("json", json);
						$(e.delegateTarget).find("button[type=submit]").prop("disabled", false);
							Object.keys(json.itog).map(function(key){
								var val, input, selector;
								if(!(selector = ""+ key+ "")){ mpre("ОШИБКА установки селектора");
								}else if(!(input = document.getElementsByName(selector))){ console.error("ОШИБКА список полей итога не найден name="+ selector);
								}else if(!(val = json.itog[key])){ console.error("ОШИБКА получения значения итога");
								}else if(!(input[0].value = val)){
								}else{ //console.log("key:", key, "val:", val, "input:", input);
									$(e.delegateTarget).find(".itog").removeClass("update");
								}
							});
						}).fail(function(error){
							alert(error.responseText);
							$(e.delegateTarget).find("button[type=submit]").prop("disabled", false);
						}); return false;
					}).attr("target", "response_"+(timeStamp = e.timeStamp));

					$("<"+"iframe>").attr("name", "response_"+timeStamp).appendTo(FORMS).load(function(){
						var response = $(this).contents().find("body").html();
						if(json = $.parseJSON(response)){
							console.log("json:", json);
							alert("Информация добавлена в кабинет");
						}else{ alert(response); }
					}).hide();
					
					/*setTimeout(function(){
						$(FORMS).trigger("submit");
					}, 100);*/
				}).ready(function(e){ $(script).parent().trigger("init"); })
			})(jQuery, document.currentScript)
		</script>
		<div class="table">
			<div>
				<span style="width:50%;">
					<form method="post" action="/bmf/null/<?=$db?>">
						<div class="table">
							<div class="th">
								<span style="width:30%;">Имя поля</span>
								<span>Значение</span>
							</div>
							<? foreach($BMF_DANO_VALUES as $bmf_dano_values): ?>
								<div class="dano">
									<span style="text-align:right;"><?=$bmf_dano_values["name"]?></span>
									<span><input type="text" placeholder="Значение" name="dano[<?=$bmf_dano_values["name"]?>]" value="<?=$bmf_dano_values["value"]?>"></span>
								</div>
							<? endforeach; ?>
							<div class="th">
								<span>Имя поля</span>
								<span>Результат</span>
							</div>
							<? foreach($BMF_ITOG_VALUES as $bmf_itog_values): ?>
								<div class="itog">
									<span style="text-align:right;"><?=$bmf_itog_values["name"]?></span>
									<span><input disabled type="text" placeholder="Результат" value="<?=$bmf_itog_values["value"]?>" name="<?=$bmf_itog_values["name"]?>"></span>
								</div>
							<? endforeach; ?>
							<div>
								<span></span>
								<span><button type="submit">Пересчитать</button></span>
							</div>
						</div>
					</form>
				</span>
			</div>
		</div>
	</div>
<? endif; ?>

