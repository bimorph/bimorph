
<? if(!$CALC = rb("calc")): mpre("ОШИБКА выборки расчетов") ?>
<? elseif(!$fields = array_map(function($n){ return $n; }, range(0, 15))): mpre("ОШИБКА получения списка полей"); ?>
<? else: //mpre($fields); ?>
	<div class="tab table" style="width:auto;">
		<style>
			.tab.table > div > span { padding:3px; }
		</style>
		<div class="th">
			<span>f</span>
			<span>calc</span>
			<span>loop0</span>
			<span>loop1</span>
			<span>index0</span>
			<span>index1</span>
		</div>
		<? foreach($fields as $field): ?>
			<? if(!$sql = "SELECT AVG(`loop`) AS `loop`, AVG(`index`) AS `index` FROM {$conf["db"]["prefix"]}bmf_test WHERE `f{$field}`=0 AND `loop`!=100 -- 4486 AND `f0`=0"): mpre("ОШИБКА составления запроса среднего нуля для поля {$field}") ?>
			<? elseif(!$avg = ql($sql, 0)): mpre("ОШИБКА получения результата") ?>
			<? elseif(!$loop0 = number_format(get($avg, 'loop'), 3)): mpre("ОШИБКА получения среднего нуля поля f{$field}") ?>
			<? elseif(!$index0 = number_format(get($avg, 'index'), 3)): mpre("ОШИБКА получения среднего нуля поля f{$field}") ?>
			<? elseif(!$sql = "SELECT AVG(`loop`) AS `loop`, AVG(`index`) AS `index` FROM {$conf["db"]["prefix"]}bmf_test WHERE `f{$field}`=1 AND `loop`!=100 -- 4486 AND `f0`=0"): mpre("ОШИБКА составления запроса среднего нуля для поля {$field}") ?>
			<? elseif(!$avg = ql($sql, 0)): mpre("ОШИБКА получения результата") ?>
			<? elseif(!$loop1 = number_format(get($avg, 'loop'), 3)): mpre("ОШИБКА получения среднего нуля поля f{$field}") ?>
			<? elseif(!$index1 = number_format(get($avg, 'index'), 3)): mpre("ОШИБКА получения среднего нуля поля f{$field}") ?>
			<? elseif(!$calc = rb($CALC, "value", $field)): mpre("ОШИБКА выборки расчета") ?>
			<? else: //mpre(ql($sql)); ?>
				<div>
					<span>f<?=$field?></span>
					<span><?=$calc["name"]?></span>
					<span style="color:<?=($loop0 < $loop1 ? "green" : "red")?>;"><?=$loop0?></span>
					<span style="color:<?=($loop0 > $loop1 ? "green" : "red")?>;"><?=$loop1?></span>
					<span style="color:<?=($index0 < $index1 ? "green" : "red")?>;"><?=$index0?></span>
					<span style="color:<?=($index0 > $index1 ? "green" : "red")?>;"><?=$index1?></span>
				</div>
			<? endif; ?>
		<? endforeach; ?>
	</div>
<? endif; ?>
