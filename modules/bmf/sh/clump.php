<?

if(!file_exists($func_file = "phar://../../../index.phar/include/func.php")){ mpre("ОШИБКА подключения файла функций {$func_file}");
}else if(!require_once($func_file)){ mpre("ОШИБКА подключения файла функций {$func_file}");
}elseif(!$conf['user']['gid'][1] = "Администратор"){ mpre("ОШИБКА уставоки пользователя администратором");
}elseif(!file_exists($conf_file = "/var/www/192.168.1.6/include/config.php")){ mpre("ОШИБКА конфигурационный файл не найден {$conf_file}");
}elseif(!require_once($conf_file)){ mpre("ОШИБКА подключения конфигурационного файла {$conf_file}");
}else if(!file_exists($htdb = "/var/www/192.168.1.6/.htdb")){ mpre("ОШИБКА файла базы данных не найден {$htdb}");
}elseif(!$conf['db']['conn'] = conn("{$conf["db"]["type"]}:{$htdb}")){ mpre("Ошибка подключения БД попробуйте установить `apt install php-sqlite3`");
}else if(!$clump_id = get($argv, 1)){ mpre("ОШИБКА номер скопления не задан");
}else if(!$bmf_clump = rb("bmf-clump", "id", $clump_id)){ mpre("ОШИБКА скопление не найдено {$w}");
}else if(!qw("UPDATE mp_bmf_clump SET hide=1 WHERE id<>". (int)$bmf_clump["id"])){ mpre("ОШИБКА скрытия других скоплений");
}else if(!qw("UPDATE mp_bmf_clump SET hide=0 WHERE id=". (int)$bmf_clump["id"])){ mpre("ОШИБКА скрытия других скоплений");
}else if(!$clump_db = "/var/www/192.168.1.6/modules/bmf/db/{$bmf_clump["id"]}.sqlite"){ mpre("ОШИБКА файла данных скопления не найден");
}else if(!qw($s = "ATTACH DATABASE '{$clump_db}' AS clump")){ mpre("ОШИБКА подключения файла базы скопления {$s}");
}else if(!$CLUMP_HISTORY = rb("bmf-clump_history", "clump_id", "id", $bmf_clump["id"])){ mpre("ОШИБКА данные для скопления не заданы");
}else if(!$CLUMP_HISTORY_DANO = rb("bmf-clump_history_dano", "clump_history_id", "id", $CLUMP_HISTORY)){ mpre("ОШИБКА получения списка исходных значений");
}else if(!$CLUMP_HISTORY_ITOG = rb("bmf-clump_history_itog", "clump_history_id", "id", $CLUMP_HISTORY)){ mpre("ОШИБКА получения списка исходных значений");
}else if(!$DANO_VALUES = rb("bmf-dano_values", "clump_id", "id", $bmf_clump["id"])){ mpre("ОШИБКА получения значений дано", $bmf_clump);
}else if(!$ITOG_VALUES = rb("bmf-itog_values", "clump_id", "id", $bmf_clump["id"])){ mpre("ОШИБКА получения значений итог", $bmf_clump);
}else if(!$DATA = array_values(array_map(function($clump_history) use($CLUMP_HISTORY_DANO, $CLUMP_HISTORY_ITOG, $DANO_VALUES, $ITOG_VALUES){ // Составление параметров
		if(!$_CLUMP_HISTORY_DANO = rb($CLUMP_HISTORY_DANO, "clump_history_id", "id", $clump_history["id"])){ mpre("ОШИБКА выборки исходных данных истории");
		}else if(!$_DANO_VALUES = rb($DANO_VALUES, "id", "name", rb($_CLUMP_HISTORY_DANO, "dano_values_id"))){ mpre("ОШИБКА получения списка значений");
		}else if(!$history_dano = array_map(function($_dano_values) use($_CLUMP_HISTORY_DANO){
				if(!$_clump_history_dano = rb($_CLUMP_HISTORY_DANO, "dano_values_id", $_dano_values["id"])){ mpre("ОШИБКА значения не найден");
				}else{ return $_clump_history_dano["value"]; }
			}, $_DANO_VALUES)){ mpre("ОШИБКА составления истории дано");
		}else if(!$_CLUMP_HISTORY_ITOG = rb($CLUMP_HISTORY_ITOG, "clump_history_id", "id", $clump_history["id"])){ mpre("ОШИБКА выборки итоговых данных истории");
		}else if(!$_ITOG_VALUES = rb($ITOG_VALUES, "id", "name", rb($_CLUMP_HISTORY_ITOG, "itog_values_id"))){ mpre("ОШИБКА получения списка значений");
		}else if(!$history_itog = array_map(function($_itog_values) use($_CLUMP_HISTORY_ITOG){
				if(!$_clump_history_itog = rb($_CLUMP_HISTORY_ITOG, "itog_values_id", $_itog_values["id"])){ mpre("ОШИБКА значения не найден");
				}else{ return $_clump_history_itog["value"]; }
			}, $_ITOG_VALUES)){ mpre("ОШИБКА составления истории итога");
		}else if(!$dano = ["dano"=>$history_dano, "itog"=>$history_itog]){ mpre("ОШИБКА получения даты истории");
		}else{ return $dano; }
	}, $CLUMP_HISTORY))){ mpre("ОШИБКА составления списка параметров");
//}else if(true){ mpre($DATA);
}else if(!$json = json_encode($DATA, 3|256/*Русские символы*/)){ mpre("ОШИБКА формирования строки данных");
}else if(!$cmd = "echo '{$json}' | cpp/bmf {$bmf_clump["id"]}"){ mpre("ОШИБКА формирования запуска обучения");
}else{ //mpre($json, $cmd);
	system($cmd);
}
