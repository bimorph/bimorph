#include <stdio.h>
#include <iostream>
#include <utility>
//#include <iostream>
#include <string>
#include <cctype>
#include <cstring> // strstr
#include <map> /* Ассоциативные массивы */
//#include <iomanip>
//#include <typeinfo>
//#include <new>
//#include <locale>
#include <array>
#include <ctime>
//#include <fstream>
#include <algorithm> // Количество символов в строке
#include <bitset> // Перевод десятичных чисел к двоичному виду
#include <cmath> // Возведение в степень
#include <unistd.h> // sleep
//#include <algorithm> // replace_if
//#include <stdlib.h>

#include <sqlite3.h>
//#include "MathExpr/MathExpr.h"
//#include "cparse/shunting-yard.h"

//#include "tinyexpr/tinyexpr.c"
//#include "tinyexpr/tinyexpr.h"
//#include "exprtk/exprtk.hpp"

#include <sys/file.h> // umask
#include <cstdlib>

//#include <vector> // CSVRow
//#include <sstream> //CSVRow
#include "csv.h"

#include "json.hpp"
#include <random>
#include <sys/time.h>
#include "md5.h"
//#include "crc32/crc32.c"
//#include <ctime> // timestamp
//#include <thread>
#include <regex> // Регулярные выражения замена символов во входящей строке

#include <iomanip>
#include <ctime> // std::put_time Функции времени
#include <sys/time.h> // timeval Функции времени
//#include <fstream>
#include <experimental/filesystem> // Права доступа к файлу
#include <string.h> // strpos
#include <thread>
#include <mutex>
#include <future> // Асинхноррное выполение потоков

using json = nlohmann::json;
json in;

using namespace std; //setlocale(LC_ALL, "ru_RU");
#include "bmf.map"

typedef std::map<string, string> TMs;
typedef std::map<string, TMs> TMMs;
typedef std::map<int, TMs> TMMi;
typedef std::map<string, TMMi> TM3i;

TM3i BMF_INDEX_EX, BMF_DANO_EX, BMF_ITOG_EX;
TMMi BMF_CLUMP, DATABASES;//, BMF_INDEX, BMF_DANO, _BMF_DANO, _BMF_ITOG, BMF_ITOG;
TM3i BMF_DANO_VALUES_EX, BMF_ITOG_VALUES_EX; // Значения позиций морфов
TM3i BMF_DANO_TITLES_EX, BMF_ITOG_TITLES_EX; // Списки справочников
TMs databases; // Расчетный итог

//std::mutex mu;a
//std::lock_guard<std::mutex> lock(mu);

TMs bmf_clump; // Текущее скопление
string mask = "+000000000"; // Маска нового морфа в списке
int loop = 0, loop_max; // Количетсо итераций скрипта
string cmd; // Строка запуска


string e; // ОШИБКА для отображения

int mpre(TMs row, int line, string comment = "", string prefix = "", string key = ""){
	string num;
	if([&](){ // Получение ключа
			if("" != key){ num = key;
			}else if(row.end() != row.find("id")){ num = row["id"];
			}else{ //mpre("Ни ключ ни идентификатор не задан", __LINE__);
			} return false;
		}()){ //mpre("ОШИБКА получения ключа", __LINE__);
	}else{
		std::cerr << prefix << num << " => " << "( // __" << to_string(line) << "__ " << comment << "\n";
		for(TMs::iterator itr = row.begin(); itr != row.end(); itr++){
			string field = (string) itr->first;
			string val = (string) itr->second;
			std::cerr << prefix << "\t[" << field << "]=>" << val << "," << endl;
		}; std::cerr << prefix << ")\n";
	} return 1;
}

int mpre(TMMi TAB, int line, string comment = ""){
	std::cerr << "Array";
	if(line > 0){
		std::cerr << "__" << std::to_string(line) << "__";
	}; std::cerr << "( // " << comment << "\n";
	for(auto itr = TAB.begin(); itr != TAB.end(); itr++){
		int key = itr->first;
		TMs row = itr->second;
		mpre(row, line, comment, "\t", to_string(key));
	} std::cerr << ")\n";
	return 1;
}


int mpre(TMMs TAB, int line, string comment = ""){
	std::cerr << "Array";
	if(line > 0){
		std::cerr << "__" << std::to_string(line) << "__";
	}; std::cerr << "( // " << comment << "\n";
	for(auto itr = TAB.begin(); itr != TAB.end(); itr++){
		string key = itr->first;
		TMs row = itr->second;
		mpre(row, line, comment, "\t", key);
	} std::cerr << ")\n";
	return 1;
}


int mpre(string mess, int line, string comment = ""){
	if([&](){ std::cerr << line << "." << mess << (comment.empty() ? "" : " `") << comment << (comment.empty() ? "" : "`") << endl; return false; }()){ mpre("ОШИБКА вывода уведомления", __LINE__);
	}else if([&](){ int npos = mess.find("ОШИБКА"); return (-1 == npos); }()){ //mpre("Вхождение ключевого слова", __LINE__);
	}else if([&](){ mpre("^^^ Критическое сообщение ^^^", __LINE__); return true; }()){ mpre("Отображения информации о критической ситуации", __LINE__);
	}else if([&](){ int response = system("sleep 3"); return (0 != response); }()){ std::cerr << __LINE__ << " Остановка программы" << endl; exit(1);
	}else{ //mpre("Возвращенное значение"+ to_string(response), __LINE__);
	} return 1;
}

int strtol(string str){
	char * endptr;
	mpre("Преобразование числа "+ str, __LINE__);
	int num = strtol(str.c_str(), &endptr, 10);
	mpre("Преобразование значение "+ to_string(num), __LINE__);
	return num;
}

int mpre(TM3i TABS, int line, string comment = ""){
	std::cerr << "Array";
	if(line > 0){
		std::cerr << "__" << std::to_string(line) << "__";
	}; std::cerr << "( // " << comment << "\n";
	//for_each(TABS.begin(), TABS.end(), [&](auto &tab_itr){
	for(auto &tab_itr:TABS){
		string keys;
		if([&](){ keys = tab_itr.first; return false; }()){ mpre("ОШИБКА получения ключа очередного элемента", __LINE__);
		}else{
			std::cerr << "\tArray";
			if(line > 0){
				std::cerr << "\t" << keys << " => " ;
			}; std::cerr << "( // " << comment << "\n";
			for(auto itr = tab_itr.second.begin(); itr != tab_itr.second.end(); itr++){
				int key = itr->first;
				TMs row = itr->second;
				mpre(row, line, comment, "\t\t", to_string(key));
			} std::cerr << "\t)\n";
		}
	}; std::cerr << ")\n";
	return 1;
}



TMMi rb(TMMi &TAB, TMs values, bool debug = false){
	//std::lock_guard<std::mutex> lock(mu);
	TMMi LIST;
	//for_each(TAB.begin(), TAB.end(), [&](auto &itr){
	for(auto &tab_itr:TAB){
		bool keep = true; int id = tab_itr.first;
		TMs row = tab_itr.second;
		//for_each(values.begin(), values.end(), [&](auto &it){
		for(auto &values_itr:values){
			string key, val;
			if([&](){ key = values_itr.first; return false; }()){ std::cerr << __LINE__ << " Ключ условия выборки << " << key << endl;
			}else if([&](){ val = values_itr.second; return false; }()){ std::cerr << __LINE__ << " Значения условия выборки << " << val << endl;
			}else if(row.find(key) == row.end()){ std::cerr << __LINE__ << " Поле в таблице не найдено [" << key << "]=" << val << endl; mpre(values, __LINE__); mpre(row, __LINE__); keep = false;
			}else if(val == row.find(key)->second){// std::cerr << __LINE__ << " Значение не соответствует уcловию << " << " " << it->second << ":" << values.find(key)->second << endl;
			}else{ keep = false; // std::cerr << "i=" << i << ";" << endl;
			}// std::cerr << __LINE__ << " i[" << i++ << "]={" << keys[i] << ":" << vals[i] << "}" << endl;
		};
		if(keep){// mpre(row, __LINE__);
			LIST.insert(make_pair(id, row));
		}// std::cerr << __LINE__ << " Количество элементов в выборке:" << LIST.size() << endl;
	}; return LIST;
}

int Crc32(const char *message) {
	int i, j;
	unsigned int byte, crc;
	i = 0;
	crc = 0xFFFFFFFF;
	while (message[i] != 0) {
		byte = message[i];    // Get next byte.
		crc = crc ^ byte;
		for(j = 7; j>=0; j--){
			crc = (crc >> 1) ^ (crc & 1 ? 0xEDB88320 : 0);
		} i = i + 1;
	}
	return ~crc;
}

TMs erb(TMMi TAB, TMs values, bool debug = false){ // Поиск по неиднексированной таблице
	TMMi LIST; TMs row;
	if([&](){ LIST = rb(TAB, values); return false; }()){ std::cerr << __LINE__ << " Получение списка подходящих под условия элементов" << endl;
	}else if(LIST.size() > 1){ mpre(LIST, __LINE__, "Список выборки"); std::cerr << __LINE__ << " Количество элементов в выборке "+ to_string(LIST.size())+ " более одного" << endl; mpre(values, __LINE__); // data(LIST, __LINE__); return row;
	}else if(LIST.empty()){// std::cerr << __LINE__ << " Список выборки пуст" << endl; mpre(values, __LINE__); return row;
	}else if(LIST.begin() == LIST.end()){ std::cerr << __LINE__ << " ОШИБКА выборки первого элемента из списка выборки" << endl;
	}else{ row = LIST.begin()->second;
	} return row;
}

TMs erb_insert(TM3i &TABS, string id, TMs index){ //mpre("Добавление индекса "+ id, __LINE__);
	//std::lock_guard<std::mutex> lock(mu);
	if([&](){ // Удаление старого значения из списка
			TMs _index;
			if(TABS.at("").find(stoi(id)) == TABS.at("").end()){ //mpre("Добавляемое значение списке не найдено", __LINE__);
			}else if([&](){ _index = TABS.at("").at(stoi(id)); return _index.empty(); }()){ mpre("ОШИБКА выборки удаляемой записи", __LINE__);
			}else if([&](){ // Удаляем все ключи
					//for_each(TABS.begin(), TABS.end(), [&](auto &tab_itr){ // Список созданных ранее индексов
					for(auto &tab_itr:TABS){
						string _field, _value, _md5; int _crc32;
						if([&](){ _field = tab_itr.first; return (0 >= _field.length()); }()){ //mpre("Не индексируем ключи с нулевой длинной", __LINE__);
						}else if(_index.end() == _index.find(_field)){ mpre("ОШИБКА при удалении индекса его уже нет", __LINE__);
						}else if([&](){ _value = _index.at(_field); return false; }()){ mpre("ОШИБКА получения значения морфа", __LINE__);
						}else if([&](){ _crc32 = Crc32(_value.c_str()); return false; }()){ mpre("ОШИБКА получения crc32 значения поля", __LINE__);
						}else if([&](){ _md5 = md5(_value); return (0 >= _md5.length()); }()){ mpre("ОШИБКА получения md5 значения поля", __LINE__);
						}else if(tab_itr.second.end() == tab_itr.second.find(_crc32)){ //mpre("Cписок crc32 удаления не найден "+ to_string(_crc32)+ " "+ _md5, __LINE__);
						}else if([&](){ tab_itr.second.at(_crc32).erase(id); return false; }()){ mpre("ОШИБКА удаления индекса", __LINE__);
						}else{ //mpre(_INDEX_, __LINE__, "Список md5 "+ to_string(_crc32)+ " "+ id); mpre("Удаления индекса "+ _field+ " "+ id, __LINE__);
						}
					}; return false;
				}()){ mpre("ОШИБКА удаления ключей", __LINE__);
			}else if([&](){ TABS.at("").erase(stoi(id)); return TABS.empty(); }()){ mpre("ОШИБКА удаления значения из справочника", __LINE__);
			}else if(TABS.at("").find(stoi(id)) != TABS.at("").end()){ mpre("ОШИБКА Удаляемое значение все еще в списке", __LINE__);
			}else{ //return true;
			} return false;
		}()){ mpre("ОШИБКА удаления старого значения из таблицы", __LINE__);
	}else if([&](){ for(auto &tabs_itr:TABS){ //for_each(TABS.begin(), TABS.end(), [&](auto &tabs_itr){ // Проверка наличия индексов и обновление
			string field, _value, _md5; int crc32;
			if([&](){ field = tabs_itr.first; return (0 >= field.length()); }()){ //mpre("Только поля с ненулевой длинной", __LINE__);
			}else if(index.end() == index.find(field)){ mpre(index, __LINE__, "ОШИБКА индексное поле "+ field+ " не найдено в списке параметров");
			}else if([&](){ _value = index.at(field); return false; }()){ mpre("ОШИБКА получения значения поля", __LINE__);
			}else if([&](){ crc32 = Crc32(_value.c_str()); return false; }()){ mpre("ОШИБКА получения crc32 хеша значения", __LINE__);
			}else if([&](){ _md5 = md5(_value); return (0 >= _md5.length()); }()){ mpre("ОШИБКА расчета md5 строки значения", __LINE__);
			}else if([&](){ // Если массив не существует то добавляем пустое значение
					if(tabs_itr.second.find(crc32) != tabs_itr.second.end()){ //mpre("Массив уже существует не добавляем пустой", __LINE__);
					}else if([&](){ TMs _INDEX_; tabs_itr.second[crc32] = _INDEX_; return false; }()){ mpre("ОШИБКА добавления пустого значени crc32", __LINE__);
					}else{ //mpre("Добавление нового списка crc32", __LINE__);
					} return false;
				}()){ mpre("ОШИБКА добавления значения crc32 массиву", __LINE__);
			}else if([&](){ // Добавление хеша
					if(tabs_itr.second.at(crc32).find(id) != tabs_itr.second.at(crc32).end()){ mpre("Значение md5 уже создано и не требует добавления", __LINE__);
					}else if([&](){ tabs_itr.second.at(crc32).insert(make_pair(id, _md5)); return tabs_itr.second.at(crc32).empty(); }()){ mpre("ОШИБКА добавления нового значения md5", __LINE__);
					}else{ //mpre("Добавление нового значения md5 в crc32", __LINE__);
					} return false;
				}()){ mpre("ОШИБКА добавления хеша значения", __LINE__);
			}else{ //mpre("Добавления индекса полю "+ field, __LINE__);
			}
		}; return false;	}()){ mpre("ОШИБКА добавления новой записи в таблицу", __LINE__);
	}else if([&](){ TABS.at("").insert(make_pair(stoi(id), index)); return TABS.empty(); }()){ mpre("ОШИБКА добавления нового значения к списку таблицы", __LINE__);
	}else{ //mpre(TABS, __LINE__, "Добавление записи в таблицу"); //mpre("ОШИБКА добавления строки к индексной таблице", __LINE__);
	} return index;
}

TMs erb(TM3i &TABS, TMs values, bool debug = false){ // Поиск по индексированной таблице
	TMs line;
	if([&](){ // Проверка наличия индексных полей
			//for_each(values.begin(), values.end(), [&](auto value_itr){
			for(auto &value_itr:values){
				string field, value;
				if([&](){ field = value_itr.first; return (0 >= field.length()); }()){ mpre("ОШИБКА поле нулевой длинны", __LINE__);
				}else if("id" == field){ //mpre("Не добавляем индекс на поле id", __LINE__);
				}else if(TABS.find(field) != TABS.end()){ //mpre("Индекс поля в таблице уже создан `"+ field+ "`", __LINE__);
				}else if([&](){ value = value_itr.second; return false; }()){ mpre("ОШИБКА получения значения", __LINE__);
				}else if([&](){ TABS.insert(make_pair(field, TMMi({}))); return (TABS.end() == TABS.find(field)); }()){ mpre("ОШИБКА добавления поля `"+ field+ "` к индексу", __LINE__);
				}else if([&](){ for(auto &tabs_itr:TABS.at("")){ //for_each(TABS.at("").begin(), TABS.at("").end(), [&](auto tabs_itr){ // Добавляем индексы
						TMs tabs; string value, _md5; int crc32;
						if([&](){ tabs = tabs_itr.second; return tabs.empty(); }()){ mpre("ОШИБКА выборки элемента для индекса", __LINE__);
						}else if(tabs.end() == tabs.find(field)){ mpre("ОШИБКА поле `"+ field+ "` не указано у элемента", __LINE__);
						}else if([&](){ value = tabs.at(field); return false; }()){ mpre("ОШИБКА получения значения поля", __LINE__);
						}else if([&](){ crc32 = Crc32(value.c_str()); return false; }()){ mpre("ОШИБКА нахожения crc32 значения "+ to_string(crc32), __LINE__);
						}else if([&](){ _md5 = md5(value); return (0 >= _md5.length()); }()){ mpre("ОШИБКА нахождения хеша значения", __LINE__);
						//}else if([&](){ TMs _tabs = erb_insert(TABS, tabs["id"], tabs); return _tabs.empty(); }()){ mpre("ОШИБКА добавления элемента в индекс", __LINE__);
						}else if([&](){
								if(TABS.at(field).end() != TABS.at(field).find(crc32)){ //mpre("Список crc32 уже установлен", __LINE__);
								}else if([&](){ TABS.at(field).insert(make_pair(crc32, TMs({}))); return (TABS.at(field).end() == TABS.at(field).find(crc32)); }()){ mpre("ОШИБКА добавления новго списка crc32", __LINE__);
								}else{ //mpre("Добавление нового списка crc32 "+ to_string(crc32), __LINE__);
								}
							return false; }()){ mpre("ОШИБКА добавления списка идентификаторов", __LINE__);
						}else if([&](){ TABS.at(field).at(crc32).insert(make_pair(tabs["id"], _md5)); return (TABS.at(field).at(crc32).end() == TABS.at(field).at(crc32).find(tabs["id"])); }()){ mpre("ОШИБКА добавления идентификатора списку crc32", __LINE__);
						}else{ //mpre(tabs, __LINE__, "Добавляем элемент в интекс");
						}
					}; return false; }()){ mpre("ОШИБКА устанвоки списка crc32 md5 значений", __LINE__);
				}else{ //mpre("Добавление хешей поля "+ field, __LINE__);
					//mpre("ОШИБКА Добавление значения к индексному полю `"+ field+ "`", __LINE__);
				}
			}; return false;
		}()){ mpre("ОШИБКА проверки наличия ключей", __LINE__);
	}else if([&](){ // Получение значения выборки по идентификатору
			string id;
			if(1 != values.size()){ mpre("ОШИБКА полей для поиска больше одного", __LINE__);
			}else if(values.end() == values.find("id")){ //mpre("Выборка только по id", __LINE__);
			}else if([&](){ id = values.at("id"); return (0 >= id.length()); }()){ //mpre("Значение id в выборке не установлено", __LINE__);
			}else if(TABS.at("").end() == TABS.at("").find(stoi(id))){ /*mpre(TABS.at(""), __LINE__, "Список");*/ mpre("ОШИБКА Запись по id не найдена "+ id, __LINE__);
			}else if([&](){ line = TABS.at("").at(stoi(id)); return line.empty(); }()){ mpre("ОШИБКА выборка по id пуста", __LINE__);
			}else{ //mpre("ОШИБКА Выборка по идентификатору "+ id, __LINE__);
			} return false;
		}()){ mpre("ОШИБКА получения значения выборки по id", __LINE__);
	}else if([&](){ // Выборка значения по другим полям (не id)
			TMMi _TABS; string field, value, _md5; int crc32;
			if(1 < values.size()){ mpre(TABS, __LINE__, "TABS"); mpre("ОШИБКА полей для поиска более одного", __LINE__);
			}else if(values.find("id") != values.end()){ //mpre("Выборка по id", __LINE__);
			}else if([&](){ field = values.begin()->first; return (0 >= field.length()); }()){ mpre("ОШИБКА получения поля для поиска", __LINE__);
			}else if(TABS.end() == TABS.find(field)){ mpre("ОШИБКА Индекс поля не создан "+ field, __LINE__);
			}else if([&](){ value = values.begin()->second; return false; }()){ mpre("ОШИБКА получения значения поля", __LINE__);
			}else if([&](){ crc32 = Crc32(value.c_str()); return (0 == crc32); }()){ mpre("ОШИБКА нахожения crc32 значения "+ to_string(crc32), __LINE__);
			}else if(TABS.at(field).end() == TABS.at(field).find(crc32)){ //mpre("Список идентификаторов по crc32 не найден", __LINE__);
			}else if([&](){ _md5 = md5(value); return (0 >= _md5.length()); }()){ mpre("ОШИБКА нахождения хеша значения", __LINE__);
			}else if([&](){ for(auto &list_itr:TABS.at(field).at(crc32)){ //for_each(TABS.at(field).at(crc32).begin(), TABS.at(field).at(crc32).end(), [&](auto list_itr){ // Получение значений по crc32/md5
					TMs _tab; string _id, _md5_;
					if([&](){ _md5_ = list_itr.second; return (0 >= _md5_.length()); }()){ mpre("ОШИБКА пустой md5", __LINE__);
					}else if(_md5_ != _md5){ mpre("Значение md5 не совпадает с поиском", __LINE__);
					}else if([&](){ _id = list_itr.first; return (0 >= _id.length()); }()){ mpre("ОШИБКА получения идентификатора", __LINE__);
					}else if(0 > _id.find_last_not_of("1.0")){ mpre("ОШИБКА не подходящий формат идентификатора "+ _id, __LINE__);
					}else if(TABS.at("").end() == TABS.at("").find(stoi(_id))){ mpre("ОШИБКА элемент `"+ _id+ "` не найден в списке", __LINE__);
					}else if([&](){ _tab = TABS.at("").at(stoi(_id)); return _tab.empty(); }()){ mpre("ОШИБКА элемент по id не найден", __LINE__);
					}else if([&](){ _TABS.insert(make_pair(stoi(_id), _tab)); return _TABS.empty(); }()){ mpre("ОШИБКА добавления значения к списку", __LINE__);
					}else{ //mpre("ОШИБКА выборки по значению", __LINE__);
					}
				}; return false; }()){ mpre("ОШИБКА выборки по списку crc32/md5", __LINE__);
			}else if(1 < _TABS.size()){ mpre(values, __LINE__, "Список полей для выборки"); mpre("ОШИБКА выбранных значений более одного", __LINE__);
			}else if(_TABS.begin() == _TABS.end()){ mpre("Пустое значение выборки по crc32/md5", __LINE__);
			//}else if([&](){ mpre(_TABS, __LINE__, "Выборка"); return false; }()){ mpre("ОШИБКА уведомления о найденном идентификаторе", __LINE__);
			}else if([&](){ line = _TABS.begin()->second; return line.empty(); }()){ mpre("ОШИБКА выборки найденного значения из списка md5", __LINE__);
			}else{ //mpre(_TABS, __LINE__, "Список выборки"); mpre(line, __LINE__, "Результат выборки"); mpre(values, __LINE__, "Список полей для выборки"); mpre("ОШИБКА выбираем по индексным полям", __LINE__);
			} return false;
		}()){ mpre("ОШИБКА получения значения выборки по другим полям (не id)", __LINE__);
	}else{ //mpre("ОШИБКА выборка из индексированной таблицы", __LINE__);
	} return line;
}

TMMi rb(TM3i &TABS, TMs values, bool debug = false){
	//std::lock_guard<std::mutex> lock(mu);
	TMMi TAB, LIST; TMMs INDEXES; int _min = 0; string _min_field = "";
	if([&](){ //mpre(TAB, __LINE__, "Таб"); // Проверка наличия индексов
			//for_each(values.begin(), values.end(), [&](auto value_itr){
			for(auto &value_itr:values){
				string field, value;
				if([&](){ field = value_itr.first; return (0 >= field.length()); }()){ mpre("ОШИБКА поле нулевой длинны", __LINE__);
				}else if("id" == field){ //mpre("Не добавляем индекс на поле id", __LINE__);
				}else if(TABS.find(field) != TABS.end()){ //mpre("Поле индекса найденов списке", __LINE__);
				}else if([&](){ TABS.insert(make_pair(field, TMMi({}))); return TABS.empty(); }()){ mpre("ОШИБКА добавления поля индекса к интексной таблице", __LINE__);
				}else if([&](){ // Список индексов значений полей
						TMs _index; int _id; TMMi TAB = TABS.at("");
						//for_each(TAB.begin(), TAB.end(), [&](auto &tab_itr){ // Добавление в индекс каждого из значений
						for(auto &tab_itr:TAB){
							if([&](){ _index = tab_itr.second; return _index.empty(); }()){ mpre("ОШИБКА пустой элемент в списке таблицы", __LINE__);
							}else if([&](){ _id = tab_itr.first; return (0 == _id); }()){ mpre("ОШИБКА получения ключа записи (не может быть ноль)", __LINE__);
							}else if([&](){ erb_insert(TABS, to_string(_id), _index); return _index.empty(); }()){ mpre("ОШИБКА добавления индекса", __LINE__);
							}else{ //mpre("Добавление индекса "+ field+ " "+ to_string(_id), __LINE__); //mpre("Добавляем индекс "+ id+ " "+ value+ " "+ to_string(crc32), __LINE__);
							}
						}; return false;
					}()){ mpre("Создание индекса всех значений полей", __LINE__);
				//}else if([&](){ TABS[field] = _INDEX; return TABS.empty(); }()){ mpre("ОШИБКА доабвления вновь сформированного индекса к индексной таблице", __LINE__);
				}else{ //mpre(_INDEX, __LINE__, "Список crc32 индексов");// mpre("ОШИБКА Добавление индексного поля "+ field, __LINE__);
				}
			}; return false;
		}()){ mpre("ОШИБКА формирования списка индекса", __LINE__);
	}else if([&](){ // Получения списка индексов запроса
			//for_each(values.begin(), values.end(), [&](auto value_itr){ // Перебор условий выборки и получения их индексов
			for(auto &value_itr:values){
				string _field, _value, _md5, _id; int crc32;
				if([&](){ _field = value_itr.first; return (0 >= _field.length()); }()){ mpre("ОШИБКА получения имени поля выборки", __LINE__);
				}else if([&](){ INDEXES.insert(make_pair(_field, TMs({}))); return INDEXES.empty(); }()){ mpre("ОШИБКА добавления пустого индекса", __LINE__);
				}else if([&](){ _value = value_itr.second; return false; }()){ mpre("ОШИБКА получения значения условия выборки", __LINE__);
				}else if(TABS.find(_field) == TABS.end()){ mpre("ОШИБКА поля индекса в таблице не найдено", __LINE__);
				}else if(TABS.at(_field).empty()){ //mpre(TABS, __LINE__, "Индекс таблица"); mpre("Полученный список пуст", __LINE__);
				}else if([&](){ crc32 = Crc32(_value.c_str()); return false; }()){ mpre("ОШИБКА получения числового хеш значения", __LINE__);
				}else if(TABS.at(_field).find(crc32) == TABS.at(_field).end()){ //mpre("Список crc32 не найден", __LINE__);
				}else if([&](){ _md5 = md5(_value); return (0 >= _md5.length()); }()){ mpre("ОШИБКА формирования md5 хеша строки", __LINE__);
				}else if([&](){ // Проверка списка md5 на совпадение со значением
						//for_each(TABS.at(_field).at(crc32).begin(), TABS.at(_field).at(crc32).end(), [&](auto indexes_itr){ // Проверяем md5 сумму в списке индексов значениях
						for(auto &indexes_itr:TABS.at(_field).at(crc32)){
							if([&](){ string _md5_ = indexes_itr.second; return (_md5 == _md5_); }()){ //mpre("ОШИБКА выборки значения md5", __LINE__);
							}else if([&](){ _id = indexes_itr.first; return (0 >= _id.length()); }()){ mpre("ОШИБКА получения идентификатора записи", __LINE__);
							}else if([&](){ TABS.at(_field).at(crc32).erase(_id); return false; }()){ mpre("ОШИБКА удаления не подходящего по условиям md5", __LINE__);
							}else{ mpre("ОШИБКА коллизия md5", __LINE__);
							}
						}; return false;
					}()){ mpre("ОШИБКА проверки значений на хеш", __LINE__);
				}else if([&](){ // Получение минимального поля
						int size;
						if([&](){ size = TABS.at(_field).at(crc32).size(); return false; }()){ mpre("ОШИБКА получения размера очередного индекса", __LINE__);
						}else if((_min != 0) && (_min < size)){ mpre("ОШИБКА Текущее значение больше минимального (не пустого)", __LINE__);
						}else if([&](){ _min = size; return false; }()){ mpre("ОШИБКА сохранения минимального значения", __LINE__);
						}else if([&](){ _min_field = _field; return (0 >= _min_field.length()); }()){ mpre("ОШИБКА сохраенния минимального поля", __LINE__);
						}else{ //mpre("Минимальное поле "+ _min_field+ " "+ to_string(_min), __LINE__);
						} return (0 >= _min_field.length());
					}()){ mpre("ОШИБКА расчета минимального поля", __LINE__);
				}else if([&](){ INDEXES[_field] = TABS.at(_field).at(crc32); return INDEXES.empty(); }()){ mpre("ОШИБКА добавления значений в индекс", __LINE__);
				}else{ //mpre("Размер min "+ to_string(_min)+ " "+ _min_field, __LINE__);
				}
			}; return INDEXES.empty();
		}()){ mpre("ОШИБКА получения индексов запроса", __LINE__);
	}else if([&](){ //Удаляем ключи не со всех индексов
			TMs _INDEXES; TMMs::iterator TMMs_itr; string _id;
			//mpre(TABS.at(""), __LINE__, "Список");
			//mpre(INDEXES, __LINE__, "Индексы");
			if(INDEXES.find(_min_field) == INDEXES.end()){ //mpre(INDEXES, __LINE__, "Список индексов"); mpre("ОШИБКА индекс по минимальному полю в списке индексов не найден "+ _min_field, __LINE__);
			}else if([&](){ // Проверяем значения ключа с минимальным количеством значений
					//for_each(INDEXES.at(_min_field).begin(), INDEXES.at(_min_field).end(), [&](auto &_indexes_itr){ // Перебор всех значений минимального поля ключа
					for(auto &_indexes_itr:INDEXES.at(_min_field)){
						if([&](){ _id = _indexes_itr.first; return (0 >= _id.length()); }()){ mpre("ОШИБКА получения значения индекса", __LINE__);
						}else if([&](){ //mpre("Проверка ключа "+ _id, __LINE__); // Только если все ключи совпадают
								//for_each(INDEXES.begin(), INDEXES.end(), [&](auto &indexes_itr){ // Перебор всех ключей и проверка на наличия значения
								for(auto &indexes_itr:INDEXES){
									TMs _index;
									if(indexes_itr.second.empty()){ mpre("ОШИБКА получения списка ключей для проверки", __LINE__);
									}else if(indexes_itr.second.find(_id) == indexes_itr.second.end()){ mpre("Значение в списке ключей не найдено", __LINE__);
									}else if(TABS.at("").find(stoi(_id)) == TABS.at("").end()){ mpre("ОШИБКА получения совпадающего по ключам значения", __LINE__);
									}else if([&](){ _index = TABS.at("").at(stoi(_id)); return _index.empty(); }()){ mpre("ОШИБКА получения значения по ключу", __LINE__);
									}else if([&](){ LIST.insert(make_pair(stoi(_id), _index)); return LIST.empty(); }()){ mpre("ОШИБКА добавления значения в итоговый список", __LINE__);
									}else{ //mpre(_INDEXES_, __LINE__, "Ключ на проверку");
									}
								}; return false;
							}()){ mpre("ОШИБКА проверки совпадения всех ключей", __LINE__);
						}else{ //mpre("Идентификатор минимального индекса "+ _id, __LINE__);
						}
					}; return false;
				}()){ mpre("ОШИБКА получения списка значений из ключей", __LINE__);
			}else{ //mpre(INDEXES, __LINE__, "Минимальное значение "+ _min_field+ " "+ to_string(_min));
			} return false;
		}()){ mpre("ОШИБКА совмещения всех индексов по ключам (только ключи во всех индексах)", __LINE__);
	}else if((0 != _min) && LIST.empty()){ mpre(TABS, __LINE__, "TABS"); mpre(INDEXES, __LINE__, "Индексы"); mpre("ОШИБКА результат выборки пуст", __LINE__);
	}else{ //mpre("ОШИБКА формирования списка индексов", __LINE__);
	} return LIST;
}


int main(int argc, char **argv){
	sqlite3* db = 0;
	string dbname;

	TMMs data[16];// int data_size;
	int timestamp = time(0), values_length = 128;
	TMMi DANO, ITOG; // Отличающиеся от расчетных дано
	TMs calc_rand; // Случайный расчет для подстановки в результат
	string clump_id; // Скопление если не указано то данные из php кода
	//int num = -1; // Номер данных в массиве входящих данных
	int change = -1, // Начальное значение количества расчетов
	size_max = 1e9, // Максимально допустимое количество морфов в расчете
	index_clean = 0, // Очистка результатов
	index_save = 1, // Запись результатов расчета морфов в базу
	calc_value = 2, // 9 Значение для обучения скопления 2
	loop_max = 1, // Количество повторов
	system_count = 30; // Количество итераций

	int calc_num = -1; // Число вариант развития системы 0 - 65535
	float perc = 0, pips_perc = 0; // Процент правильных ответов
	//string calc_bin = "-1"; // Двоичное представление числа развития
	int divider; // Номер итога для расчета
	TM3i _VAL, _CALC;


	std::function<sqlite3_stmt*(string)> exec; // Таблица с интервалами времени
	std::function<sqlite3_stmt*(string)> prepare; // Таблица с интервалами времени
	std::function<TMMi(string)> Tab; // Выборка таблицы из БД
	std::function<TMs(string,TMs,TMs,TMs)> fk; // Сохранение информации в базу
	std::function<time_t(string,string,int)> Timer; TM3i TIMER; // Таблица с интервалами времени
	std::function<TMMi(string,TMMi)> Save; // Сохранение информации в базу
	std::function<int(TMs,string,TM3i&,string)> Values; // Проверка наличия значений в БД
	std::function<int(TMs,string,TM3i&,TM3i&,TM3i&,string)> Vals; // Обучение
	std::function<int(json,int,string)> LearningAll; // Обучение
	std::function<int(TMs)> Learning; // Обучение
	std::function<string(double)> Dec2bin; // Перерасчет размерности размерности из десятеричной в двоичную
	std::function<TMs(TMs,TMs)> Learn; // Рерасчет морфа
	std::function<double(string)> Bin2dec; // Перерасчет размерности из двоичной в десятеричную
	std::function<string(TMs,string)> Val; // Расчет истории
	std::function<TMs(TMs,bool)> Calc; // Расчет истории
	std::function<TMs(TMs)> CalcAll; // Расчет истории
	std::function<TMs(TMs)> Calc_new; // Расчет истории
	std::function<TMs(TMs)> Choice; // Расчет истории
	std::function<string(TMMi&)> Id; // Расчет локального идентификатора
	std::function<TMMi(TMMi&,string,string,string)> Dataset; // Выгрузка данных из базы с проверкой равенства
	std::function<bool(TMMi,int,string,string)> Data; // Отображение списка данных в коде
	std::function<bool(int,int)> Do; // Непосредственно расчет

	if([&](){ clump_id = (argc > 1 ? argv[1] : clump_id); return (0 >= clump_id.length()); }()){ mpre("Параметр адресной строки с номером скопления не задан", __LINE__);
	}else if([&](){ divider = (3 < argc ? stoi(argv[3]) : 0); return (0 > divider); }()){ mpre("ОШИБКА получения номера итога", __LINE__);
	//}else if(!system("sleep 1")){ mpre("Сон", __LINE__);
	}else if([&](){ loop_max = (argc > 2 ? stoi(argv[2]) : loop_max); return (0 >= loop_max); }()){ mpre("ОШИБКА получения количества повторений", __LINE__);
	//}else if([&](){ std::locale::global(std::locale("ru_RU.utf8")); return false; }()){ mpre("ОШИБКА установки локали", __LINE__);
	}else if([&](){ // Составление списка значений быстрого доступа
			if(!_VAL.empty()){ //mpre("Список быстрого доступа не пуст", __LINE__);
			}else{ for(auto &val_itr:BMF_CALC_VAL){ //for_each(BMF_CALC_VAL.begin(), BMF_CALC_VAL.end(), [&](auto val_itr){ // Познакам добавляем в список быстрого доступа
				TMs val;
				if([&](){ val = val_itr.second; return val.empty(); }()){ mpre("ОШИБКА получения очередного знака", __LINE__);
				}else if([&](){ // Добавление списка второго уровня v0
						if([&](){ return (_VAL.end() != _VAL.find(val["v1"])); }()){ //mpre("Второй уровень v1 уже установлен", __LINE__);
						}else if([&](){ _VAL.insert(make_pair(val["v1"], TMMi({}))); return _VAL.empty(); }()){ mpre("ОШИБКА добавления нового уровня v0", __LINE__);
						}else{ //mpre("Добавление нового уровня v1", __LINE__);
						}
					return (false); }()){ mpre("ОШИБКА добавление списка v1", __LINE__);
				}else if([&](){ _VAL.at(val["v1"]).insert(make_pair(stoi(val["v0"]), val)); return (_VAL.at(val["v1"]).end() == _VAL.at(val["v1"]).find(stoi(val["v0"]))); }()){ mpre("ОШИБКА добавления второго уровня", __LINE__);
				}else{ //mpre("Добавление нового знака списку быстрого доступа", __LINE__);
				}
			}; }
		return false; }()){ mpre("ОШИБКА составления списка быстрого доступа", __LINE__);
	}else if([&](){ // Составление списка расчетов быстрого доступа
			if(!_CALC.empty()){ //mpre("Список быстрого доступа не пуст", __LINE__);
			}else{ for(auto &calc_itr:BMF_CALC){ //for_each(BMF_CALC.begin(), BMF_CALC.end(), [&](auto calc_itr){ // По расчетам добавляем в список быстрого доступа
				TMs calc;
				if([&](){ calc = calc_itr.second; return calc.empty(); }()){ mpre("ОШИБКА получения очередного расчета", __LINE__);
				}else if([&](){ // Добавление списка второго уровня v0
						if([&](){ return (_CALC.end() != _CALC.find(calc["calc_pos_id"])); }()){ //mpre("Уровень calc_pos_id уже установлен", __LINE__);
						}else if([&](){ _CALC.insert(make_pair(calc["calc_pos_id"], TMMi({}))); return _CALC.empty(); }()){ mpre("ОШИБКА добавления нового уровня calc_pos_id", __LINE__);
						}else{ //mpre("Добавление нового уровня calc_pos_id", __LINE__);
						}
					return false; }()){ mpre("ОШИБКА добавление списка calc_pos_id", __LINE__);
				}else if([&](){ _CALC.at(calc["calc_pos_id"]).insert(make_pair(stoi(calc["calc_val_id"]), calc)); return (_CALC.at(calc["calc_pos_id"]).end() == _CALC.at(calc["calc_pos_id"]).find(stoi(calc["calc_val_id"]))); }()){ mpre("ОШИБКА добавления второго уровня", __LINE__);
				}else{ //mpre("Добавление нового знака списку быстрого доступа", __LINE__);
				}
			}; }
		return false; }()){ mpre("ОШИБКА составления списка быстрого доступа", __LINE__);
	}else if([&](){ // Установка функций
			if([&](){ // Запрос к БД
					exec = ([&](string sql){ // Запрос к БД
						int result, sqlite_result, sleep = 30;
						sqlite3_stmt* stmt; // Запрос к базе данныхa
							do{
								if([&](){ sqlite_result = sqlite3_exec(db, sql.c_str(), 0, &stmt, 0); return (SQLITE_OK == sqlite_result); }()){ //mpre("Запрос выполнен без ошибок");
								}else if([&](){ mpre("Повторный запрос к БД через "+ to_string(sleep *= 2), __LINE__); mpre("Запрос "+ sql, __LINE__); return false; }()){ mpre("ОШИБКА уведомления", __LINE__);
								}else if([&](){ int request = system(("sleep "+ to_string(sleep)).c_str()); return (0 != request); }()){ mpre("Выход из запроса", __LINE__); exit(1);
								}else{ //mpre("Повторный запрос к БД", __LINE__);
								}
							}while(SQLITE_OK != sqlite_result);
						return stmt;//sqlite3_last_insert_rowid(db);
					});
				return false; }()){ mpre("ОШИБКА установки функции запроса к БД", __LINE__);
			}else if([&](){ // Запрос к БД
					prepare = ([&](string sql){ // Запрос к БД
						int result, sqlite_result, sleep = 30;
						sqlite3_stmt* stmt; // Запрос к базе данныхa
							do{
								if([&](){ sqlite_result = sqlite3_prepare(db, sql.c_str(), -1, &stmt, 0); return (SQLITE_OK == sqlite_result); }()){ //mpre("Запрос выполнен без ошибок");
								}else if([&](){ mpre("Повторная выбрка из БД через "+ to_string(sleep *= 2), __LINE__); mpre("Запрос "+ sql, __LINE__); return false; }()){ mpre("ОШИБКА уведомления", __LINE__);
								}else if([&](){ int request = system(("sleep "+ to_string(sleep)).c_str()); return (0 != request); }()){ mpre("Выход из запроса", __LINE__); exit(1);
								}else{ //mpre("Повторный запрос к БД", __LINE__);
								}
							}while(SQLITE_OK != sqlite_result);
						return stmt;//sqlite3_last_insert_rowid(db);
					});
				return false; }()){ mpre("ОШИБКА установки функции запроса к БД", __LINE__);
			}else if([&](){ // Выборка таблицы
					Tab = ([&](string sql){ // Запрос к бд
						char *val; int count; TMMi TAB;
						sqlite3_stmt* stmt;
						//if(SQLITE_OK != sqlite3_prepare(db, sql.c_str(), -1, &stmt, 0)){ std::cerr << __LINE__ << " ОШИБКА выполнения запроса к базе данных: " << sql << endl;
						if([&](){ stmt = prepare(sql); return false; }()){ mpre("ОШИБКА запроса к БД", __LINE__);
						}else if([&](){ count = sqlite3_column_count(stmt); return false; }()){ std::cerr << " ОШИБКА расчета количества записей в таблице";
						}else{ int id = 0;
							while(SQLITE_ROW == sqlite3_step(stmt)){
								TMs row; string row_id;
								for(int num = 0; num < count; num++){
									string field = (string) sqlite3_column_name(stmt, num);
									val = (char*) sqlite3_column_text(stmt, num);
									string value = (val == NULL ? "" : val);
									row.insert(std::make_pair(field, value));
								} //exit(mpre(mask, __LINE__, "Маска"));
								int next_id = (row.end() == row.find("id") ? ++id : stoi(row["id"]));
								TAB.insert(std::make_pair(next_id, row));
							}// std::cerr << __LINE__ << " Запрос к БД " << sql << " размер: " << TAB.size() << endl;
						} return TAB;
					});
				return false; }()){ mpre("ОШИБКА установки функции выборки списка таблциы", __LINE__);
			}else if([&](){ // Обновление записей БД
					fk = ([&](string table, TMs where, TMs insert, TMs update){ // Обновление
						TMs row; string sql;
						sqlite3_stmt* stmt;
						if([&](){
					  		if(where.empty()){// std::cerr << __LINE__ << " Условие на выборку записей не указано" << endl;
					  		}else if([&](){
					  				string values;
					  				sql = "SELECT * FROM `"+ table+ "`";
					  				for(TMs::iterator itr = where.begin(); itr != where.end(); itr++){
					  					string key = itr->first;
					  					string val = itr->second;
					  					string separ = (values.size() ? " AND " : "");
					  					values += separ+ "`"+ key+ "`='"+ val+ "'";
					  				} sql += " WHERE "+ values;
					  				return false;
					  			}()){ std::cerr << __LINE__ << " Запрос добавления выборку записи из БД" << endl;
					  		}else if([&](){ TMMi TAB;
					  				if([&](){ TAB = Tab(sql); return false; }()){ // Выполнение запроса к базе дыннх
					  				}else if([&](){ return TAB.size() < 1; }()){// std::cerr << __LINE__ << " Размер выборки меньше нуля << " << sql << endl;
					  				}else if([&](){ row = TAB.begin()->second; return false; }()){ std::cerr << __LINE__ << "Список измененных данных" << endl;
					  				}else{// mpre(row, __LINE__); // std::cerr << __LINE__ << " Запрос к БД:" << sql << endl; // mpre(row);
					  				} return false;
					  			}()){ std::cerr << " Запрос на получения вновь установленной записи" << endl;
					//		}else if([&](){ std::cerr << __LINE__ << " Условие выборки " << sql << endl; mpre(row); return false; }()){ // Уведомление
					  		}else{// std::cerr << __LINE__ << " Запрос на выборку значений: " << sql << endl; // mpre(where, __LINE__);
					  		} return false;
					  	}()){ std::cerr << __LINE__ << " Выборка записи по условию" << endl;
					  }else if([&](){ int rowid; // Добавление
					  		if(insert.empty()){// std::cerr << __LINE__ << " Условия добавления новой записи не указаны" << endl;
					  		}else if(!row.empty()){// std::cerr << __LINE__ << " Запись уже найдена по условиям << " << row["id"] << endl; mpre(where, __LINE__); mpre(insert, __LINE__); mpre(update, __LINE__);
					  		}else if([&](){ insert.erase("id"); return insert.empty(); }()){ mpre("Устанавливаем нулевой идентификатор", __LINE__);
					  		}else if([&](){
					  				string fields, values;
					  				sql = "INSERT INTO `"+ table+ "`";
					  				for(TMs::iterator itr = where.begin(); itr != where.end(); itr++){
					  					string key = itr->first;
					  					string val = itr->second;
					  					string separ = (fields.size() ? ", " : "");
					  					fields += separ+ "`"+ key+ "`";
					  					values += separ+ "'"+ val+ "'";
					  				} for(TMs::iterator itr = insert.begin(); itr != insert.end(); itr++){
					  					string key = itr->first;
					  					string val = itr->second;
					  					string separ = (fields.size() ? ", " : "");
					  					fields += separ+ "`"+ key+ "`";
					  					values += separ+ "'"+ val+ "'";
					  				} sql += "("+ fields+ ") VALUES ("+ values+ ")";
					//				std::cerr << __LINE__ << " (Запрос) Добавление новой записи: " << sql << endl; mpre(where, __LINE__); mpre(insert, __LINE__);
					  				return false;
					  			}()){ std::cerr << __LINE__ << " Запрос добавления новой записи в БД" << endl;
					  		//}else if(SQLITE_OK != sqlite3_exec(db, sql.c_str(), 0, 0, 0)){ std::cerr << __LINE__ << " ОШИБКА выполнения запроса к базе данных: " << sql << " >> " << sqlite3_errmsg(db) << endl;// return false;
					  		}else if([&](){ stmt = exec(sql); return false; }()){ mpre("ОШИБКА выполнения запроса к БД", __LINE__); //std::cerr << __LINE__ << " ОШИБКА выполнения запроса к базе данных: " << sql << " >> " << sqlite3_errmsg(db) << endl;// return false;
					  		}else if([&](){ rowid = sqlite3_last_insert_rowid(db); return false; }()){ mpre("ОШИБКА получения идентификатора последней установленной записи", __LINE__); //std::cerr << __LINE__ << " Идентификатор последней установленной записи << " << rowid << endl;
					  		}else if([&](){
					  				if([&](){ sql = "SELECT * FROM `"+ table+ "` WHERE id="+ to_string(rowid); return false; }()){ std::cerr << __LINE__ << " Запрос на выборку внось установленной записи" << endl;
					  				}else if([&](){ TMMi TAB = Tab(sql); row = erb(TAB, {{"id", to_string(rowid)}}); return false; }()){ std::cerr << __LINE__ << "Список измененных данных" << endl;
					  				}else{// std::cerr << __LINE__ << " Запрос к БД:" << sql << endl; // mpre(row);
					  				} return false;
					  			}()){ std::cerr << " Запрос на получения вновь установленной записи" << endl;
					  		}else{// std::cerr << __LINE__ << " Добавление новой записи к базе `"+ table+ "` << " << rowid << " " << endl;
					  		} return false;
					  	}()){ std::cerr << __LINE__ << "ОШИБКА добавление запроса если не найден по условиям" << endl;
					  }else if([&](){ // Обновление
					  		if(update.empty()){ //std::cerr << __LINE__ << " Условия обновления не указаны" << endl;
					  		}else if(row.empty()){ std::cerr << __LINE__ << " Запись для обновления не найдена " << sql << endl;
					  		}else if([&](){
					  				string fields, values;
					  				sql = "UPDATE `"+ table+ "`";
					  				for(TMs::iterator itr = update.begin(); itr != update.end(); itr++){
					  					string key = itr->first;
					  					if(key == "id"){ // Сохраняем идентификатор записи
					  					}else if(key.substr(0, 1) == "_"){ // Технические поля
					  					}else{
					  						string val = itr->second;
					  						string separ = (values.size() ? ", " : "");
					  						values += separ+ "`"+ key+ "`='"+ val+ "'";
					  					}
					  				} sql += " SET "+ values+ " WHERE id="+ row["id"];
					  				return false;
					  			}()){ std::cerr << __LINE__ << " Запрос добавления новой записи в БД" << endl;
					  		}else if([&](){ exec(sql); return false; }()){ mpre("ОШИБКА обновления записи", __LINE__); //std::cerr << __LINE__ << " ОШИБКА обновления записи" << endl;
					  		}else if([&](){ sql = "SELECT * FROM `"+ table+ "` WHERE id="+ row["id"]; return false; }()){ mpre("ОШИБКА запроса на выборку вновь установленных данных", __LINE__); //std::cerr << __LINE__ << " Запрос на выборку внось установленной записи" << endl;
					  		}else if([&](){ TMMi TAB = Tab(sql); row = erb(TAB, {{"id", row["id"]}}); return false; }()){ mpre("ОШИБКА полуения списка измененных данных", __LINE__); //std::cerr << __LINE__ << "Список измененных данных" << endl;
					  		}else{ //std::cerr << __LINE__ << " Обновляем запись: " << sql << endl;// mpre(update, __LINE__);
					  		} return false;
					  	}()){ std::cerr << __LINE__ << " Обновение найденной по условию записи" << endl;
					  }else{// mpre(where, __LINE__); mpre(insert, __LINE__); mpre(update, __LINE__);
					  } return row;
					});
				return false; }()){ mpre("ОШИБКА обьявления функции Crc32", __LINE__);
			}else if([&TIMER, &Timer](){ Timer = ([&](string name, string title, int line){ // Получаем число возвращаем двойное числоa
					//}else if([&](){ time_t t = Timer("Сохранение данных", "Запускаем", __LINE__); return false; }()){ mpre("ОШИБКА засечения времени", __LINE__);
					TMMi timer; TMs tmr; time_t t; struct timeval tv; double microsec, duration; int next;
					if([&](){ gettimeofday(&tv, NULL); return false; }()){ mpre("Структура содержащая время и микровремя", __LINE__);
					}else if([&](){ microsec = tv.tv_usec/1e6+tv.tv_sec; return (0 >= microsec); }()){ mpre("ОШИБКА получения микросекунд", __LINE__);
					}else if([&](){ timer = (TIMER.end() == TIMER.find(name) ? timer : TIMER.at(name)); return false; }()){ mpre("ОШИБКА получения ввремени события", __LINE__);
					}else if([&](){ tmr = (timer.end() == timer.find(1) ? tmr : timer.at(1)); return false; }()){ mpre("ОШИБКА получения первого элемента списка", __LINE__);
					}else if([&](){ duration = (tmr.end() == tmr.find("microsec") ? 0 : microsec-stod(tmr.at("microsec"))); return (0 > t); }()){ mpre("ОШИБКА получения времени исполнения", __LINE__);
					}else if([&](){ next = timer.size()+1; return (0 >= next); }()){ mpre("ОШИБКА определения следующего идентификатора", __LINE__);
					}else if([&](){ tmr = {{"id", to_string(next)}, {"title", title}, {"microsec", to_string(microsec)}, {"duration", to_string(duration)}}; return tmr.empty(); }()){ mpre("ОШИБКА установки значений очередного тайа", __LINE__);
					}else if([&](){ timer.insert(make_pair(next, tmr)); return timer.empty(); }()){ mpre("ОШИБКА добавления нового изменения событию", __LINE__);
					}else if([&](){ // Обновление таблицы таймера
						if(TIMER.end() != TIMER.find(name)){ TIMER.at(name) = timer;
						}else if([&](){ TIMER.insert(make_pair(name, timer)); return TIMER.empty(); }()){ mpre("ОШИБКА добавления нового события в таймер", __LINE__);
						}else{ //mpre(TIMER, __LINE__, "Таймер");
						} return TIMER.empty(); }()){ mpre("ОШИБКА обновления таблицы таймера", __LINE__);
					}else{ std::cerr << line << "." << "Таймер `" << name << "` " << title << " " << to_string(duration) << endl;
						//mpre(timer, __LINE__);
					} return t;
				}); return false; }()){ mpre("ОШИБКА установки функции засечения времени", __LINE__);
			}else if([&Id](){ // Генерация локального идентификатора
					Id = ([&](TMMi& ROWS){
						int id, id_max = 0, id_min = 0, id_next; TMs row_min, row_max;
						if([&](){ id_max = (ROWS.empty() ? 0 : ROWS.rbegin()->first); return false; }()){ exit(mpre("ОШИБКА получения идентификатора последней записи: "+ to_string(id_max), __LINE__));
						}else if([&](){ id_min = (ROWS.empty() ? 0 : ROWS.begin()->first); return false; }()){ exit(mpre("ОШИБКА получения идентификатора первой записи", __LINE__));
						}else if([&](){ id_next = max(abs(id_min), abs(id_max))+1; return (0 >= id_next); }()){ mpre("ОШИБКА получения максимального значения", __LINE__);
						}else if([&](){ id = (id_next+1)*-1; return (0 <= id); }()){ mpre("ОШИБКА устанвоки следующего id", __LINE__);
						}else if(ROWS.end() != ROWS.find(id)){ mpre("ОШИБКА Дублирование идентификаторов ["+ to_string(id_min)+ ":"+ to_string(id_max)+ "] "+ to_string(id), __LINE__);// exit(mpre(BMF_INDEX, __LINE__));
						}else{// mpre("Максимальный id_min: "+ to_string(id_min)+ " id_max: " + to_string(id_max) + " id_next: "+ to_string(id_next)+ " id:"+ id, __LINE__);
							return to_string(id); // mpre("Возвращаемое значение: "+ id, __LINE__);
						} (mpre(ROWS, __LINE__, "ОШИБКА получения очередного идентификатора"));
					}); return false;
				}()){ mpre("ОШИБКА создания функции локального идентификатора", __LINE__);
			}else if([&](){ // Формат данных в коде
					//bool data(TMMi TAB, int line, string tab = "", string comment = ""){
					Data = ([&](TMMi TAB, int line, string tab, string comment){
						string sepor;
						std::cerr << line << " " << (tab == "" ? "" : "TMMi "+ tab+ " = ") << "{" << (comment == "" ? "" : " // "+comment) << endl;
						for(TMMi::iterator itr = TAB.begin(); itr != TAB.end(); itr++){
							int id = itr->first;
							TMs row = itr->second;
							std::cerr << "\t{"+ to_string(id)+ ", {";
							for(TMs::iterator it = row.begin(); it != row.end(); it++){
								string key = it->first;
								string val = it->second;
								sepor = (it == row.begin() ? "" : ", ");
								std::cerr << sepor+ "{\""+ key+ "\", \""+ val+ "\"}";
							} std::cerr << "}}," << endl;
						} std::cerr << "};" <<  endl;
						return false;
					}); return false;
				}()){ mpre("ОШИБКА установки функции отображения данных в коде", __LINE__);
			}else if([&](){ // Восстановление из БД с проверкой
					Dataset = ([&](TMMi& TAB, string name, string table, string comment){ // Функция восстановления из БД с проверкой
						TMMi _TAB; string master, sql; bool check = true;
						if([&](){ // Проверка наличия таблицы в базах
								if(DATABASES.empty()){ mpre("ОШИБКА не установлен список баз данных", __LINE__);
								}else if([&](){ master = string(databases.empty() ? "" : "bmf.")+ "sqlite_master"; return (0 >= master.length()); }()){ mpre("ОШИБКА формирования имени таблицы проверки", __LINE__);
								//}else if([&](){ sql = "SELECT 0 AS id, count(*) AS cnt FROM sqlite_master WHERE type='table' AND name='"+ table+ "'"; return (0 >= sql.length()); }()){ mpre("ОШИБКА составления запроса проверки таблицы скоплений", __LINE__);
								//}else if([&](){ TMMi CLUMP; CLUMP = Tab(sql); return ("0" != CLUMP[0]["cnt"]); }()){ return false; //mpre("ОШИБКА Таблица `"+ table+ "` отсутсвует в базе", __LINE__);
								}else if([&](){ sql = "SELECT 0 AS id, count(*) AS cnt FROM "+ master+ " WHERE type='table' AND name='"+ table+ "'"; return (0 >= sql.length()); }()){ mpre("ОШИБКА составления запроса проверки таблицы скоплений", __LINE__);
								}else if([&](){ TMMi CLUMP; CLUMP = Tab(sql); return ("0" != CLUMP[0]["cnt"]); }()){ return false; //mpre("ОШИБКА Таблица `"+ table+ "` отсутсвует в базе", __LINE__);
								}else{ mpre("Таблица `"+ table+ "` не найдена в (База скопления) и bmf (База сайта)", __LINE__);
								} return true;
							}()){ mpre("Данные справочника "+ name+ " с "+ to_string(TAB.size())+ " позициями не перезагружается", __LINE__);
						}else if([&](){ sql = "SELECT * FROM `"+ table+ "`"; return false; }()){ // Запрос на выбору данных
						}else if([&](){ _TAB = Tab(sql); return false; }()){ std::cerr << __LINE__ << " ОШИБКА выборки таблицы из базы данных";
						}else if(!check){ return TAB;
						}else if(_TAB.size() != TAB.size()){ mpre("Не совпадение размера данных в справочниках", __LINE__); //data(_TAB, __LINE__, name, comment); //std::cerr << " Не совпадение размера данных в справочниках" << endl; data(_TAB, __LINE__, name, comment);
						}else if([&](){ bool check = false; TMs index, index_, row;
								for(auto itr = _TAB.begin(); itr != _TAB.end(); itr++){
									TMs _index = itr->second;
									if([&](){ index = erb(TAB, _index); row = erb(TAB, {{"id", _index["id"]}}); return index.empty(); }()){ std::cerr << __LINE__ << " Данные не совпадают" << endl; mpre(row, __LINE__); mpre(_index, __LINE__); check = true;
									}else{// std::cerr << __LINE__ << " Данные совпали << " << index["id"] << endl;
										//mpre(index, __LINE__); mpre(_index, __LINE__);
									}
								} return check;
							}()){ std::cerr << __LINE__ << " ОШИБКА Даныне в справочниках не совпадают" << endl; Data(_TAB, __LINE__, name, comment); system("sleep 1"); exit(1);
						}else{// std::cerr << __LINE__ << " (Выборка данных) Запрос: " << sql << endl;
						} return TAB;
					}); return false;
				}()){ mpre("ОШИБКА установки функции выборки из БД", __LINE__);
			}else if([&](){ // Преобразование десятеричной системы счисления в двоичную
					Dec2bin = ([&Dec2bin](double decimal){ // Получаем число возвращаем двойное число
						string binary, _decimal = to_string(decimal), dec, _dec, bin, _bin; 
						std::function<string(unsigned long long)> dec2bin; // Перерасчет размерности размерности из десятеричной в двоичную
						//mpre("Десятичное "+ _decimal, __LINE__);
			
						if([&](){ // Перевод целого числа в двоичную форму
								dec2bin = ([&](long long n){ // Получаем число возвращаем двойное число
									std::string r; long long i = n;
									do{ r=(i%2==0 ?"0":"1")+r;
									}while(0<(i/=2)); return (0 == n ? "0" : r);
								}); return false;
							}()){ mpre("ОШИБКА создания функции перевода из целого числа в двоичное", __LINE__);
						}else if([&](){ _decimal = ("-" == _decimal.substr(0, 1) ? _decimal.substr(1, _decimal.length()) : _decimal); return (0 >= _decimal.length()); }()){ mpre("ОШИБКА удаления знака отрицания", __LINE__);
						}else if([&](){ // Получение целой части числа
								int pos; dec = _decimal;
								if([&](){ pos = _decimal.rfind("."); return (0 > pos); }()){ mpre("Число без дробной части", __LINE__);
								}else if([&](){ dec = _decimal.substr(0, pos); return (0 >= dec.length()); }()){ mpre("ОШИБКА нахождения целой части числа", __LINE__);
								}else if([&](){ _dec = _decimal.substr(pos+1, _decimal.length()); return (0 >= _dec.length()); }()){ mpre("ОШИБКА нахождения дробной части числа", __LINE__);
								}else if([&](){ pos = _dec.find_last_not_of('0'); return false; }()){ mpre("ОШИБКА Действительных чисел в дробной части не найдено", __LINE__);
								}else if([&](){ _dec = (0 > pos ? "0" : _dec.substr(0, pos+1)) ; return (0 >= _dec.length()); }()){ mpre("ОШИБКА удаления лишних пробелов в дробной части", __LINE__);
								}else{ //mpre("Число с дробной частью в позиции "+ to_string(pos), __LINE__);
								} return false;
							}()){ mpre("ОШИБКА получения целой части числа "+ dec, __LINE__);
						}else if([&](){ bin = dec2bin(stod(dec)); return (0 >= bin.length()); }()){ mpre("ОШИБКА перевода в двоичную форму целой части числа", __LINE__);
						}else if([&](){ std::reverse(_dec.begin(), _dec.end()); return false; }()){ mpre("ОШИБКА переворачивания дробной части", __LINE__);
						}else if([&](){ _bin = dec2bin(stod(_dec)); return (0 >= _bin.length()); }()){ mpre("ОШИБКА перевода в двоичную форму дробной части числа", __LINE__);
						}else if([&](){ std::reverse(_bin.begin(), _bin.end()); return false; }()){ mpre("ОШИБКА переворачивания дробной части", __LINE__);
						}else if([&](){ binary = bin+ ("0" == _bin ? "" : "."+_bin); return (0 >= binary.length()); }()){ mpre("ОШИБКА получения итогового двоичного числа", __LINE__);
						}else if([&](){ binary = (0 > decimal ? "-" : "")+ binary; return (0 >= binary.length()); }()){ mpre("ОШИБКА установки знака отрицания", __LINE__);
						}else{ //mpre("Исходное число "+ to_string(decimal)+ " "+ dec+ "."+ _dec, __LINE__);
							//mpre("Результат "+ bin+ "."+ _bin+ " => "+ binary, __LINE__);
						} return binary;
					}); return false;
				}()){ mpre("ОШИБКА формирования функции пересчета разрядности чисел", __LINE__);
			}else if([&](){ // Перевод из двоичной системы в десятичную
				Bin2dec = ([](string binary){ // Получаем число возвращаем двойное число
					double decimal; unsigned long long dec; string bin, _bin, _dec, _binary = binary;
					std::function<long long(string)> bin2dec; // Перерасчет размерности размерности из десятеричной в двоичную
						if([&](){ // Перевод целого числа в двоичную форму
						bin2dec = ([&](string binary){ // Получаем число возвращаем двойное число
							//mpre("Двоичный перевод "+ binary, __LINE__);
							unsigned long long decimal = 0;
							for(int i = 0; i < binary.length(); i++){
								int pos, _pow; string val;
								if([&](){ pos = binary.length()-i-1; return (0 > pos); }()){ mpre("ОШИБКА получения позиции символа", __LINE__);
								}else if([&](){ _pow = (unsigned long long)pow(2, i); return (0 > _pow); }()){ mpre("ОШИБКА получения степени числа", __LINE__);
								}else if([&](){ val = binary.substr(pos, 1); return (1 != val.length()); }()){ mpre("ОШИБКА получения символа", __LINE__);
								}else if([&](){ decimal += ("1" == val ? _pow : 0); return (0 > decimal); }()){ mpre("ОШИБКА получения результата", __LINE__);
								}else{ //mpre("Смещение/степень i="+ to_string(i)+ " pos="+ to_string(pos)+ " val="+ val+ " _pow="+ to_string(_pow)+ " >> "+ to_string(decimal), __LINE__);
								}
							} //mpre("Значения функций "+ to_string(decimal)+ "bin2dec("+ binary+ ")", __LINE__);
							return decimal;
						}); return false; }()){ mpre("ОШИБКА создания функции перевода из двоичного числа в десятичное", __LINE__);
					}else if([&](){ _binary = ("-" == _binary.substr(0, 1) ? _binary.substr(1, _binary.length()) : _binary); return (0 >= _binary.length()); }()){ mpre("ОШИБКА удаления символа отрицания", __LINE__);
					}else if([&](){ // Получение целой части числа
							int pos; 
							if([&](){ pos = _binary.rfind("."); return false; }()){ mpre("Число без дробной части", __LINE__);
							//}else if(true){ mpre("Позиция "+ to_string(pos), __LINE__);
							}else if([&](){ bin = (-1 == pos ? _binary.substr(0, _binary.length()) : _binary.substr(0, pos)); return (0 >= bin.length()); }()){ mpre("ОШИБКА нахождения целой части числа "+ binary+ " "+ _binary+ " "+ to_string(pos), __LINE__);
							}else if(0 > pos){ //mpre("Не расчитываем дробную часть", __LINE__);
							}else if([&](){ _bin = _binary.substr(pos+1, _binary.length()); return (0 >= _bin.length()); }()){ //mpre("Дробная часть не указана "+ _binary, __LINE__);
							}else if([&](){ pos = _bin.find_last_not_of('0'); return false; }()){ mpre("ОШИБКА Действительных чисел в дробной части не найдено", __LINE__);
							}else{ //mpre("Число с дробной частью в позиции "+ to_string(pos)+ " >> "+ _bin, __LINE__);
							} return false;
						}()){ mpre("ОШИБКА получения целой части числа "+ dec, __LINE__);
					}else if([&](){ dec = bin2dec(bin); return (0 > dec); }()){ mpre("ОШИБКА перевода в двоичную форму целой части числа", __LINE__);
					}else if([&](){ std::reverse(_bin.begin(), _bin.end()); return false; }()){ mpre("ОШИБКА переворачивания дробной части", __LINE__);
					}else if([&](){ _dec = to_string(bin2dec(_bin)); return (0 > _dec.length()); }()){ mpre("ОШИБКА перевода в двоичную форму дробной части числа", __LINE__);
					}else if([&](){ std::reverse(_dec.begin(), _dec.end()); return false; }()){ mpre("ОШИБКА переворачивания дробной части", __LINE__);
					}else if([&](){ decimal = stod(to_string(dec)+ "."+_dec); return false; }()){ mpre("ОШИБКА получения итогового двоичного числа", __LINE__);
					}else if([&](){ decimal *= ("-" == binary.substr(0, 1) ? -1 : 1); return false; }()){ mpre("ОШИБКА установки отрицательного значения", __LINE__);
					}else{ //mpre("Результат расчетов "+ binary+ " "+ bin+ "("+ to_string(dec)+ ")."+ _bin+ "("+ _dec+ ") >> "+ to_string(decimal), __LINE__);
					} return decimal;
				}); return false; }()){ mpre("ОШИБКА Функция перевода из двоичной в десятичную систему", __LINE__);
			}else if([&Learn, &Val, &Calc, &Choice, &Id](){ // Обучение посигнально
					Learn = ([&](TMs index, TMs bmf_index){ //mpre("Учеба", __LINE__);  //system("sleep 1"); // mpre(BMF_INDEX, __LINE__, "Список"); // if(remove){ mpre(row, __LINE__, "Ученик"); }
						TMMs INDEX; TMs dano, _calc_, index_back;
						if([&](){ dano = erb(BMF_DANO_EX, {{"id", index["dano_id"]}}); return dano.empty(); }()){ mpre("ОШИБКА установки дано морфа", __LINE__);
						}else if([&](){ // Отображение информации об обучении
								if(!bmf_index.empty()){ // Отображаем только для корневого морфа
								}else{ std::cerr << __LINE__ << " Индекс " << index["id"] << " источник " << dano["id"] << " Расчет " << index["f1"].length() << "/" << index["f0"].length() << endl;
								} return false;
							}()){ mpre("ОШИБКА отображения информации", __LINE__);
						}else if(index.empty()){ std::cerr << __LINE__ << " Пустой морф на обучении" << endl;
						//}else if([&](){ mpre("Начало расчета для обучения "+ index["id"], __LINE__); return false; }()){ mpre("ОШИБКА уведомления", __LINE__);
						}else if([&](){ index_back = index /*= Calc(index, true)*/; return index.empty(); }()){ mpre("ОШИБКА расчета значений морфа", __LINE__);
						//}else if([&](){ index_back = index; return index_back.empty(); }()){ mpre("ОШИБКА сохранения старых значений морфа", __LINE__);
						//}else if([&](){ calc_pos = erb(BMF_CALC_POS, {{"id", index["calc_pos_id"]}}); return calc_pos.empty(); }()){ mpre("ОШИБКА выборки позиции морфа", __LINE__);
						}else if([&](){ // Расчет списка изменяемых значений
								TMs calc, _calc_val, calc_val, _index_1, _index_0; string _field_;
								//if([&]{ std::random_device rd; std::mt19937 g(rd()); _field_ = (0 == rd()%2 ? "bmf-calc" : "bmf_calc"); return (0 >= _field_.length()); }()){ mpre("ОШИБКА получения случайного поля для обновления", __LINE__);
								//if([&](){ _field_ = (dano["val"] == index["val"] ? "bmf-calc" : "bmf_calc"); return (0 >= _field_.length()); }()){ mpre("ОШИБКА установки направления развития по разнице результатов", __LINE__);
								//if([&](){ _field_ = (index["f1"].length() <= index["f0"].length() ? "bmf-calc" : "bmf_calc"); return (0 >= _field_.length()); }()){ mpre("ОШИБКА выбора направления развития", __LINE__);
								if([&](){ _field_ = "bmf-calc"; return (0 >= _field_.length()); }()){ mpre("ОШИБКА установки поля дальнейшего перехода", __LINE__);
								}else if([&](){ calc_val = erb(BMF_CALC_VAL, {{"id", index["calc_val_id"]}}); return calc_val.empty(); }()){ mpre(index, __LINE__, "Морф"); mpre("ОШИБКА выборки значения морфа", __LINE__);
								}else if([&](){ calc = erb(BMF_CALC, {{"calc_pos_id", index["calc_pos_id"]}, {"calc_val_id", index["calc_val_id"]}}); return calc.empty(); }()){ mpre("ОШИБКА получения расчета морфа", __LINE__);
								}else if([&](){ _index_1 = erb(BMF_INDEX_EX, {{"id", index["index_id"]}}); return false; }()){ mpre("ОШИБКА выборки старшего потомка", __LINE__);
								}else if([&](){ _index_0 = erb(BMF_INDEX_EX, {{"id", index["bmf-index"]}}); return false; }()){ mpre("ОШИБКА выборки младшего потомка", __LINE__);
								}else if([&](){ _calc_ = erb(BMF_CALC, {{"id", calc[_field_]}}); return _calc_.empty(); }()){ mpre("ОШИБКА получения необходимого расчета", __LINE__);
								//}else if([&](){ mpre(index, __LINE__, "Морф до изменения позиции"); return false; }()){
								}else if([&](){ index["calc_pos_id"] = _calc_["calc_pos_id"]; return index.empty(); }()){ mpre("ОШИБКА установки обновленной расчетной позиции", __LINE__);
								}else if([&](){ _calc_val = erb(BMF_CALC_VAL, {{"id", _calc_["calc_val_id"]}}); return _calc_val.empty(); }()){ mpre(calc, __LINE__, "Расчет"); mpre("ОШИБКА выборки обновленного значения", __LINE__);
								}else if([&](){ //mpre(_calc_, __LINE__, "Новое значение позиции"); // Расчет изменений старшего потомка
										if(calc_val["v1"] == _calc_val["v1"]){ //mpre("Старшего потомка не изменяем", __LINE__);
										}else if([&](){ INDEX.insert(make_pair("index_id", _index_1)); return INDEX.empty(); }()){ mpre("ОШИБКА добавления в список изменяемых старшего потомка", __LINE__);
										}else{ //mpre("Требуется изменение старшего потомка", __LINE__);
										} return false;
									}()){ mpre("ОШИБКА расчета изменений старшего потомка", __LINE__);
								}else if([&](){ // Расчет изменений младшего потомка
										if(calc_val["v0"] == _calc_val["v0"]){ //mpre("Младшего потомка не изменяем", __LINE__);
										}else if([&](){ INDEX.insert(make_pair("bmf-index", _index_0)); return INDEX.empty(); }()){ mpre("ОШИБКА добавления в список изменяемых мледшего потомка", __LINE__);
										}else{ //mpre("Требуется изменение младшего потомка", __LINE__);
										} return false;
									}()){ mpre("ОШИБКА расчета изменений младшего потомка", __LINE__);
								}else{ //mpre(index, __LINE__, "Изменяемый морф"); mpre(calc, __LINE__, "Текущая позиция"); mpre(_calc_, __LINE__, "Новая позиция");
								} return false;
							}()){ mpre("ОШИБКА расчета необходимых значений", __LINE__);
						}else if([&](){ // Переход к нижестоящему
								if(index.empty()){ mpre("Морф уже удален. Не переходим к нижестоящему", __LINE__);
								}else if(INDEX.empty()){ mpre("Нижестоящие не заданы. Не переходим", __LINE__);
								}else if([&](){ // Переход к нижестоящим по списку
										TMs _index, _index_res, _calc, _calc_val; string field; bool _del;
										//for_each(INDEX.begin(), INDEX.end(), [&](auto &index_itr){ // Перебор всех изменяемых морфов
										for(auto &index_itr:INDEX){
											if([&](){ field = index_itr.first; return (0 >= field.length()); }()){ mpre("ОШИБКА получения ключа нижестоящего", __LINE__);
											}else if([&](){ _index = index_itr.second; return _index.empty(); }()){ //mpre("Нижестоящий не задан", __LINE__);
											//}else if([&](){ _index = Calc(_index, true); return _index.empty(); }()){ mpre("ОШИБКА расчета значений морфа", __LINE__);
											}else if(index["id"] == _index["id"]){ exit(mpre(index, __LINE__, "ОШИБКА Нижестоящий сам же"));
											//}else if([&](){ _calc_val = erb(BMF_CALC_VAL, {{"v1", dano["val"]}, {"v0", dano["val"]}}); return _calc_val.empty(); }()){ mpre("ОШИБКА получения управляющего значения", __LINE__);
											//}else if([&](){ _calc = erb(BMF_CALC, {{"calc_pos_id", index["calc_pos_id"]}, {"calc_val_id", _calc_val["id"]}}); return calc.empty(); }()){ mpre("ОШИБКА получения управляющего значения", __LINE__);
											//}else if([&](){ _del = (_calc["val"] == index_back["val"]) ? true : del; return false; }()){ mpre("ОШИБКА получения обновленного сигнала удаления", __LINE__);
											}else if([&](){ _index_res = Learn(_index, index); return false; }()){ mpre("ОШИБКА статуса ответа перехода на нижестоящий", __LINE__);
											}else if(!_index_res.empty()){// mpre(BMF_INDEX, __LINE__, "Список"); exit(mpre(index_res, __LINE__, "Возвращенный морф не пуст"));
											}else if(_index["val"] == _index_res["val"]){ mpre(_index, __LINE__, "Первоначальный морф"); mpre(_index_res, __LINE__, "Измененный морф"); mpre("ОШИБКА возвращенное значение не изменилось", __LINE__);
											}else if(dano["val"] == _index["val"]){ mpre(dano, __LINE__, "Дано"); mpre(index_back, __LINE__, "Изначальный морф"); mpre("ОШИБКА после удаления знак совпадает с изначальным", __LINE__);
											}else if([&](){ index[field] = ""; return index.empty(); }()){ mpre("ОШИБКА скидываения связи родителя при удалении", __LINE__);
											}else{ //mpre(_index, __LINE__, "Переход к нижестоящему");
											}
										}; return INDEX.empty();
									}()){ mpre("ОШИБКА перебора изменяемых данных", __LINE__);
								}else{ //mpre("Переход к нижестоящим", __LINE__);
								} return false;
							}()){ mpre("ОШИБКА перехода к нижестоящему", __LINE__);
						}else if([&](){// mpre("Расширение добавление нижестоящего морфа", __LINE__); // Расширение текущего морфа добавлением нижестоящего
								TMs _dano, _calc, _calc_, _calc_pos, _calc_val, _calc_val_, _index, _index_1, _index_0; string field, _val, _val_, _fm, _values, _values_; long depth /* История вновь созданного морфа */;
								//for_each(INDEX.begin(), INDEX.end(), [&](auto &index_itr){ // Перебор всех изменяемых морфов
								for(auto &index_itr:INDEX){
									if(index.empty()){ mpre("Морф уже удален не расширяем ", __LINE__);
									}else if([&](){ // Направление развития нового морфа
											if([&](){ field = index_itr.first; return (0 >= field.length()); }()){ mpre("ОШИБКА получения ключа нижестоящего", __LINE__);
											}else if([&](){ _index_1 = erb(BMF_INDEX_EX, {{"id", index["index_id"]}}); return false; }()){ //mpre("Старший смежный морф пуст", __LINE__);
											}else if([&](){ _index_0 = erb(BMF_INDEX_EX, {{"id", index["bmf-index"]}}); return false; }()){ //mpre("Младший смежный морф пуст", __LINE__);
											}else if(!_index_1.empty() && !_index_0.empty()){ //mpre("ОБА потомка установлены переход по установленным в расчетах значениям", __LINE__);
											}else if([&](){ field = (dano["val"] == index["val"] ? "index_id" : "bmf-index"); return (0 >= field.length()); }()){ mpre("ОШИБКА получения направления развития если оба пусты", __LINE__);
											}else if(_index_1.empty() && _index_0.empty()){ //mpre("Потомки пусты устанавливаем по отрицанию", __LINE__);
											}else if([&](){ field = "index_id"; return (0 >= field.length()); }()){ mpre("ОШИБКА установки старшего неполного перехода", __LINE__);
											}else if(_index_1.empty()){ //mpre("Осуществляем старший неполный переход", __LINE__);
											}else if([&](){ field = "bmf-index"; return (0 >= field.length()); }()){ mpre("ОШИБКА установки старшего неполного перехода", __LINE__);
											}else if(_index_0.empty()){ //mpre("Осуществляем младший неполный переход", __LINE__);
											}else{ mpre(index, __LINE__, "Индекс"); mpre(_index_1, __LINE__, "_index_1"); mpre(_index_0, __LINE__, "_index_0"); mpre("ОШИБКА выбора направления развития", __LINE__);
											} return (0 >= field.length());
										}()){ mpre("ОШИБКА получения направления направления развития нового морфа", __LINE__);
									}else if([&](){ _index = index_itr.second; return !_index.empty(); }()){ //mpre("Нижестоящий не задан", __LINE__);
									}else if([&](){ _dano = Choice(index); return _dano.empty(); }()){ mpre("ОШИБКА получения подходящего по изменениям исходного значения", __LINE__);
									//}else if(true){ mpre(_dano, __LINE__, "Исходник для расширения");
									}else if(_dano["id"] == dano["id"]){ mpre("ОШИБКА выбор текущего исходного значения", __LINE__);
									}else if([&](){ _val = (dano["val"] == "1" ? "0" : "1"); return false; }()){ mpre("Нахождение сигнала противоположного сигналу текущего морфа", __LINE__);
									}else if([&](){ _calc_pos = erb(BMF_CALC_POS, {{"dano", _dano["val"]}, {"itog", _val}}); return _calc_pos.empty(); }()){ mpre(_dano, __LINE__, "ОШИБКА нахождения позиции нового морфа "+ _val);
									}else if([&](){ _calc_val = erb(BMF_CALC_VAL, {{"v1", _dano["val"]}, {"v0", _dano["val"]}}); return _calc_val.empty(); }()){ mpre("ОШИБКА расчета значений нового морфа", __LINE__);
									}else if([&](){ _calc = erb(BMF_CALC, {{"calc_pos_id", _calc_pos["id"]}, {"calc_val_id", _calc_val["id"]}}); return _calc.empty(); }()){ mpre("ОШИБКА получения расчета морфа", __LINE__);
									}else if([&](){ depth = stod(index["depth"])+1; return (0 >= depth); }()){ mpre("ОШИБКА расчета глубены морфа", __LINE__);
									}else if([&](){ // Добавление нового морфа
											if([&](){ _index = {{"id", Id(BMF_INDEX_EX.at(""))}, {"clump_id", index["clump_id"]}, {"itog_values_id", index["itog_values_id"]}, {"depth", to_string(depth)}, {"dano_id", _dano["id"]}, {"itog_id", index["itog_id"]}, {"val", _val}, {"calc_pos_id", _calc_pos["id"]}, {"calc_val_id", _calc_val["id"]}, {"index_id", ""}, {"bmf-index", ""}, {"f1", _dano["id"]}, {"f0", _dano["id"]}}; return _index.empty(); }()){ mpre("ОШИБКА создания нового морфа", __LINE__);
											}else if([&](){ erb_insert(BMF_INDEX_EX, _index["id"], _index); return _index.empty(); }()){ mpre("ОШИБКА добавления вновь созданного морфа в справочник", __LINE__);
											}else{
											}
										return _index.empty(); }()){ mpre("ОШИБКА добавлния нового морфа", __LINE__);
									}else if(dano["val"] == _index["val"]){ mpre("ОШИБКА Вновь добавленное значение не изменило знак", __LINE__);
									}else if([&](){ index[field] = _index["id"]; return index.empty(); }()){ mpre("ОШИБКА устанвоки связи в родительский морф", __LINE__);
									}else{ mpre("Расширение "+ index["id"]+ " => "+ _index["id"], __LINE__); // mpre(index, __LINE__, "Родитель"); mpre(_index, __LINE__, "Потомок");
										return _index.empty();
									}
								}; return false;
							}()){ mpre("ОШИБКА расширения нижестоящего морфа", __LINE__);
						/*}else if([&](){// mpre("Расчет значения обучения морфа", __LINE__);
								TMs index_1, index_0, calc, calc_pos, calc_val; string v1, v0;
								if(index.empty()){// mpre("Морф уже удален", __LINE__);
								}else if([&](){ dano = erb(BMF_DANO_EX, {{"id", index["dano_id"]}}); return dano.empty(); }()){ mpre("ОШИБКА установки дано морфа", __LINE__);
								}else if([&](){ index_1 = erb(BMF_INDEX_EX, {{"id", index["index_id"]}}); return false; }()){ mpre("ОШИБКА выборки старшего элемента", __LINE__);
								}else if([&](){ index_0 = erb(BMF_INDEX_EX, {{"id", index["bmf-index"]}}); return false; }()){ mpre("ОШИБКА выборки младшего морфа", __LINE__);
								}else if([&](){ v1 = (index_1.empty() ? dano["val"] : index_1["val"]); return (1 != v1.length()); }()){ mpre("ОШИБКА получения старшего знака морфа", __LINE__);
								}else if([&](){ v0 = (index_0.empty() ? dano["val"] : index_0["val"]); return (1 != v0.length()); }()){ mpre("ОШИБКА получения младшего знака морфа", __LINE__);
								}else if([&](){ calc_val = erb(BMF_CALC_VAL, {{"v1", v1}, {"v0", v0}}); return calc_val.empty(); }()){ mpre("ОШИБКА получения занчения морфа", __LINE__);
								}else if([&](){ calc = erb(BMF_CALC, {{"calc_pos_id", index["calc_pos_id"]}, {"calc_val_id", calc_val["id"]}}); return calc.empty(); }()){ mpre("ОШИБКА выборки расчета морфа", __LINE__);
								}else if([&](){ index["calc_val_id"] = calc_val["id"]; return calc_val.empty(); }()){ mpre("ОШИБКА установки значения морфа", __LINE__);
								}else if([&](){ index["val"] = calc["val"]; return index.empty(); }()){ mpre("ОШИБКА установки знака морфу", __LINE__);
								}else{ //mpre("Индекс "+ index["id"]+ " Обучение "+ to_string(index["f1"].length())+ "/"+ to_string(index["f0"].length()), __LINE__); // mpre("Расчет обучения "+ index["id"]+ " => "+ index, __LINE__);
									//mpre("Расчет "+ v1+ ":"+ v0+ " обучения "+ index["id"], __LINE__);
								} return index.empty();
							}()){ mpre("ОШИБКА расчета значения морфа", __LINE__);*/
						}else if([&](){ index["calc_val_id"] = _calc_["calc_val_id"]; return index.empty(); }()){ mpre("ОШИБКА установки обновленной расчетной позиции", __LINE__);
						}else if([&](){ index["val"] = _calc_["val"]; return index.empty(); }()){ mpre("ОШИБКА установки обновленной расчетной позиции", __LINE__);
						}else if([&](){ erb_insert(BMF_INDEX_EX, index["id"], index); return index.empty(); }()){ mpre("ОШИБКА обновления значений морфа в справочнике", __LINE__);
						}else if(index_back["val"] == index["val"]){
							mpre(index_back, __LINE__, "Старое значение");
							mpre(index, __LINE__, "Новое значение");
							mpre(BMF_DANO_EX.at(""), __LINE__, "Список дано");
							mpre(BMF_INDEX_EX.at(""), __LINE__, "Список морфов");
							mpre("ОШИБКА изменения значения морфа", __LINE__);
						}else{// mpre(BMF_INDEX, __LINE__, "Список морфов"); 
						} return index;
					}); return false;
				}()){ mpre("ОШИБКА установки обучения посигнально", __LINE__);
			}else if([&](){ // Установка значений
					Values = ([&](TMs value, string alias, TM3i &BMF_VALUES, string clump_id){ // Проверка наличия значений в БД и установка новых
						TMs values, vals; string _values, _val;
						//for_each(value.begin(), value.end(), [&](pair<string, string> value_itr){ // Устанавливаем значения
						for(auto &value_itr:value){
							if([&](){ _values = value_itr.first; return (0 >= _values.length()); }()){ mpre("ОШИБКА получения имени значения", __LINE__);
							}else if([&](){ _val = value_itr.second; return false; }()){ mpre("ОШИБКА получения значения из данных", __LINE__);
							}else if([&](){ values = erb(BMF_VALUES, {{"name", _values}}); return !values.empty(); }()){ //mpre("Значение уже создано `"+ _values+ "`", __LINE__);
							}else if([&](){ // Добавление значения
									if([&](){ vals = {{"id", Id(BMF_VALUES.at(""))}, {"name", _values}, {"clump_id", clump_id}, {"value", _val}}; return vals.empty(); }()){ mpre("ОШИБКА фонмирования нового значения", __LINE__);
									}else if([&](){ erb_insert(BMF_VALUES, vals["id"], vals); return BMF_VALUES.empty(); }()){ mpre("ОШИБКА добавления нового значения в справочник", __LINE__);
									}else{ //mpre("Добавление значения", __LINE__);
									}
								return vals.empty(); }()){ mpre("ОШИБКА добавления значения", __LINE__);
							}else{ mpre("Добавляем новое значение `"+ _values+ "` ("+ alias+ ")", __LINE__);
							}
						}; return false;
					});
				return false; }()){ mpre("ОШИБКА установки функции установки значений", __LINE__);
			}else if([&](){ // Установка значений
					Vals = ([&](TMs VALUE, string alias, TM3i &BMF_VALS, TM3i &BMF_VALUES, TM3i &BMF_TITLES, string clump_id){ // Установка входных значений
						int large, small, bmf_vals_size = BMF_VALS.at("").size();
						TM3i SIGN; string dec, _dec; // Список знаков
						for(auto &value_itr:VALUE){
							TMMi VALS; TMs values, bmf_titles; string val, _value, value; long long bin; int pos;
							if([&](){ value = value_itr.second; return false; }()){ mpre("ОШИБКА получения значения итератора", __LINE__);
							}else if([&](){ // Выборка значения и если не найден добавляем
									if([&](){ values = erb(BMF_VALUES, {{"name", value_itr.first}}); return !values.empty(); }()){ //mpre("Значение уже создано `"+ _values+ "`", __LINE__);
									}else{ mpre("ОШИБКА значение не найдено в списке `"+ value_itr.first+ "`", __LINE__);
									} return values.empty();
								}()){ mpre("ОШИБКА выборки значения", __LINE__);
							}else if([&](){ // Получение заголовка значения если нет то его создание
									TMMi TITLES; int nn = 0;
									if([&](){ regex b("^[0-9|.]+$"); return regex_match(value, b); }()){ //mpre("Формат значения `"+ value_itr.first+ "` верен `"+ value_itr.second+ "`", __LINE__);
									}else if([&](){ TITLES = rb(BMF_TITLES, {{alias+ "_values_id", values["id"]}}); return false; }()){ mpre("ОШИБКА выборки элементов заголовка с указанным номером", __LINE__);
									}else if([&](){ bmf_titles = erb(TITLES, {{"name", value}}); return !bmf_titles.empty(); }()){ //mpre("Заголовок найден в справочнике "+ value, __LINE__);
									}else if([&](){ //mpre(bmf_titles, __LINE__, "Заголовок"); // Получение номера следующего заголовка
											TMMi NN; string _nn; TMs titles;
											if([&](){ for(auto &titles_itr:TITLES){ //for_each(TITLES.begin(), TITLES.end(), [&](auto &titles_itr){
													if([&](){ titles = titles_itr.second; return titles.empty(); }()){ mpre("ОШИБКА получения заголовка из списка", __LINE__);
													}else if(titles.end() == titles.find("value")){ mpre("ОШИБКА поле value не установлено в заголовоке", __LINE__);
													}else if([&](){ _nn = titles.at("value"); return (0 >= _nn.length()); }()){ mpre("ОШИБКА значение номера заголовока не задано", __LINE__);
													}else if([&](){ NN.insert(make_pair(stoi(_nn), titles)); return NN.empty(); }()){ mpre("ОШИБКА добавления значения в список", __LINE__);
													}else{ //mpre(NN, __LINE__, "NN");
													}
												}; return false; }()){ mpre("ОШИБКА получени списка по номерам", __LINE__);
											}else if([&](){ nn = (NN.empty() ? -1 : NN.begin()->first-1); return (0 <= nn); }()){ mpre("ОШИБКА нахождения следующего номера "+ to_string(nn), __LINE__);
											}else if([&](){ bmf_titles = {{"id", Id(BMF_TITLES.at(""))}, {"clump_id", clump_id}, {alias+ "_values_id", values["id"]}, {"value", to_string(nn)}, {"name", value}}; return bmf_titles.empty(); }()){ mpre("ОШИБКА получения нового заголовка", __LINE__);
											}else if([&](){ erb_insert(BMF_TITLES, bmf_titles["id"], bmf_titles); return BMF_TITLES.empty(); }()){ mpre("ОШИБКА добавления заголовка в справочник", __LINE__);
											}else{ //mpre("Значение следующего заголовка `"+ value+ "` "+ to_string(nn), __LINE__);
											}
										return (0 < nn); }()){ mpre("ОШИБКА определения номера заголовока `"+ value+ "`", __LINE__);
									}else{ //mpre(bmf_titles, __LINE__, "Заголовок"); mpre("ОШИБКА Установлено наличие заголовка", __LINE__);
									} return false;
								}()){ mpre("ОШИБКА получения заголовка", __LINE__);
							//}else if([&](){ mpre("Значение "+ values["name"]+ " "+ value, __LINE__); return false; }()){ mpre("ОШИБКА уведомления", __LINE__);
							}else if([&](){ // Расчитываем и устанавливаем значение
									if([&](){ value = (bmf_titles.empty() ? value : bmf_titles.at("value")); return (0 >= value.length()); }()){ mpre("ОШИБКА Значение не определено", __LINE__);
									}else if([&](){ values["value"] = value; return values.empty(); }()){ mpre("ОШИБКА установки результата значению", __LINE__);
									}else if([&](){ BMF_VALUES.at("").at(stoi(values["id"])) = values; return (BMF_VALUES.find("values") != BMF_VALUES.end()); }()){ mpre(BMF_VALUES, __LINE__, "Справочник"); mpre("ОШИБКА сохранения результата в справочнике", __LINE__);
									}else{ //mpre(values, __LINE__, "Устанавливаем значние"); //mpre("Установка результата значению `"+ values["name"]+ "` "+ value, __LINE__);
									} return values.empty();
								}()){ mpre("ОШИБКА установки результата значению", __LINE__);
							//}else if([&](){ mpre("Максимальное число "+ to_string(std::numeric_limits<double>::max()), __LINE__); return false; }()){ mpre("ОШИБКА получения ограничений расчетов", __LINE__);
							}else if([&](){ _value = Dec2bin(stod(value)); return (0 >= _value.length()); }()){ mpre("ОШИБКА получения строки сиволов двоичного розультата "+ value, __LINE__);
							//}else if([&](){ mpre("Значение "+ values["name"]+ " "+ value+ " 0x"+ _value, __LINE__); return false; }()){ mpre("ОШИБКА уведомления", __LINE__);
							}else if([&](){ _value = ("-" == _value.substr(0, 1) ? _value.substr(1, _value.length()) : _value); return (0 >= _value.length()); }()){ mpre("ОШИБКА получения значения без знака", __LINE__);
							}else if([&](){ pos = _value.rfind("."); return false; }()){ mpre("ОШИБКА получения позиции точки", __LINE__);
							/*}else if([&](){ // Список знаков
									if((0 >= bmf_vals_size) && (BMF_VALS.end() != BMF_VALS.find(alias+ "_values_id"))){ mpre(BMF_VALS, __LINE__, "Список знаков"); mpre("ОШИБКА ключ связи с значением уже создан "+ to_string(bmf_vals_size)+ " `"+ alias+ "_values_id`", __LINE__); exit(1);
									}else if((0 >= bmf_vals_size)){ //mpre("Список знаков пуст "+ to_string(bmf_vals_size), __LINE__);
									}else if([&](){ VALS = rb(BMF_VALS, {{alias+ "_values_id", values["id"]}}); return false; }()){ mpre("ОШИБКА получения всех знаков", __LINE__);
									}else{ //mpre("Выборка знаков по ключам", __LINE__);
									}
								return false; }()){ mpre("ОШИБКА получения списка знаков", __LINE__);*/
							}else if([&](){ VALS = rb(BMF_VALS, {{alias+ "_values_id", values["id"]}}); return false; }()){ mpre("ОШИБКА получения всех знаков", __LINE__);
							}else if([&](){ SIGN = {{"-1", TMMi({})}, {"0", TMMi({})}, {"1", TMMi({})}}; return (3 != SIGN.size()); }()){ mpre("ОШИБКА добавления списка сигналов в справочник", __LINE__);
							}else if([&](){ for(auto &vals_itr:VALS){ //for_each(VALS.begin(), VALS.end(), [&](auto &vals_itr){ // Разбиваем спиок на две группы
									TMs vals; string name, sign, _value_; int val;
									if([&](){ vals = vals_itr.second; return vals.empty(); }()){ mpre("ОШИБКА получения знака", __LINE__);
									}else if([&](){ name = vals.at("name"); return (0 >= name.length()); }()){ mpre("ОШИБКА получения позиции символа", __LINE__);
									}else if([&](){ val = stoi(name); return false; }()){ mpre("ОШИБКА получения признака знака", __LINE__);
									}else if([&](){ sign = to_string((val > 0) - (val < 0)); return false; }()){ mpre("Нулевой сигнал не учитываем", __LINE__);
									}else if([&](){ SIGN[sign].insert(make_pair(stoi(vals["id"]), vals)); return SIGN.at(sign).empty(); }()){ mpre("ОШИБКА добавления сигнала в список", __LINE__);
									//}else if([&](){ vals_itr.second.at("val") = "0"; return false; }()){ mpre("ОШИБКА установки значения", __LINE__);
									//}else if([&](){ mpre("Начало" + value+ " "+ to_string(pos), __LINE__); return false; }()){ mpre("Останов", __LINE__);
									}else if([&](){ _value_ = ("." == value.substr(0, 1) ? "0"+ value : value); return (0 >= _value_.length()); }()){ mpre("ОШИБКА обработки значений начинающихся с точки", __LINE__);
									}else if([&](){ vals["val"] = ((vals["name"] == "0" && 0 > stoi(_value_)) ? "1" : "0"); return vals.empty(); }()){ mpre("ОШИБКА расчета значения знака", __LINE__);
									//}else if([&](){ mpre("Конец", __LINE__); return false; }()){ mpre("Останов", __LINE__);
									}else if([&](){ BMF_VALS.at("").at(stoi(vals["id"])) = vals; return false; }()){ mpre("ОШИБКА установки значения знакам", __LINE__);
									}else{ //mpre("Позиция "+ name+ " sign="+ sign, __LINE__);
									}
								}; return false; }()){ mpre("ОШИБКА получения списка целых и дробных бит", __LINE__);
							}else if([&](){ // Добавление бита отрицания
									TMs _vals;
									if((0 <= stod(value)) || (0 < SIGN.at("0").size())){ //mpre("Требуется знак отрицания `"+ values["name"]+ "` "+ value, __LINE__);
									}else if([&](){ val = (0 > stod(value) ? "1" : "0"); return (1 != val.length()); }()){ mpre("ОШИБКА расчета занчения знака", __LINE__);
									}else if([&](){ // Добавление бита трицания
											if([&](){ _vals = {{"id", Id(BMF_VALS.at(""))}, {"clump_id", clump_id}, {alias+ "_values_id", values["id"]}, {"name", "0"}, {"val", val}, {"values", val}}; return _vals.empty(); }()){ mpre("ОШИБКА создания нового знака", __LINE__);
											}else if([&](){ erb_insert(BMF_VALS, _vals["id"], _vals); return _vals.empty(); }()){ mpre("ОШИБКА обновления справочника знаков", __LINE__);
											}else{
											}
										return false; }()){ mpre("ОШИБКА добавления бита отрицания", __LINE__);
									}else{ mpre("Добавление знака отрицания "+ val, __LINE__);
									}
								return false; }()){ mpre("ОШИБКА добавление нулевого бита", __LINE__);
							}else if([&](){ // Проверка необходимости добавлять знаки
									if([&](){ large = (0 > pos ? _value.length() : pos); return false; }()){ mpre("ОШИБКА подсчета количества символов в целой части", __LINE__);
									}else if([&](){ small = (0 > pos ? 0 : _value.length()-pos-1); return false; }()){ mpre("ОШИБКА подсчета количества символов в целой части", __LINE__);
									}else{ //mpre("В числе "+ _value+ " целая часть "+ to_string(large)+ " дробная "+ to_string(small), __LINE__);
									} return false;
								}()){ mpre("ОШИБКА расчета необходимости добавления новых знаков", __LINE__);
							}else if([&](){ // Увдомление о новых знаках
									if(large > SIGN.at("1").size()){ //mpre("Необходимо добавить знаков целой части "+ _value+ " "+ to_string(SIGN.at("1").size()), __LINE__);
									}else if(small > SIGN.at("-1").size()){ //mpre("Необходимо добавить знаков дробной части "+ _value+ " "+ to_string(SIGN.at("-1").size()), __LINE__);
									}else{ //mpre("Добавление новых знаков не требуется", __LINE__);
										return false;
									} std::cerr << __LINE__ << " Добавляем новый знак `"+ values["name"]+ "` 0x"+ _value;
									//mpre(TMs({{"pos", to_string(pos)}, {"small",to_string(small)}, {"large", to_string(large)}, {"_value", _value}}), __LINE__, "Парметры");
									//mpre(SIGN, __LINE__, "Сигналы");
								return false; }()){ mpre("ОШИБКА отображения уведомления о добавлениии новых знаков", __LINE__);
							}else if([&](){ // //mpre("Добавляем новый знак "+ values["name"]+ " "+ to_string(VALS.size())+ "=>"+ to_string(_value.length())+ " "+ _value+ " ("+ to_string(value)+ ")" , __LINE__); //system("sleep 1");
									for(int i = 0; i < _value.length(); i++){ // Добавление знаков значению
										TMs _vals; int nn;
										if([&](){ // Расчет значения знака
												if([&](){ val = _value.substr(i, 1); return (1 != val.length()); }()){ mpre("ОШИБКА получения символа "+ to_string(i), __LINE__);
												}else if("." != val){ //mpre("Не знак числа "+ val, __LINE__);
												}else if([&](){ val = ("-" == _value.substr(0, 1) ? "1" : "0"); return (1 != val.length()); }()){ mpre("ОШИБКА расчета знака числа", __LINE__);
												}else{ //mpre("Расчет знака "+ value, __LINE__);
												}
											return (1 != val.length()); }()){ mpre("ОШИБКА расчета значения знака", __LINE__);
										}else if("-" == val){ mpre("ОШИБКА Символ отрицания не должен попадаться", __LINE__);
										}else if([&](){ // Расчет номера в списке
												if(0 > pos){ nn = _value.length()-i; //mpre("Число без точки "+ to_string(nn), __LINE__);
												}else if(i == pos){ nn = 0; //mpre("Точка "+ to_string(nn), __LINE__);
												}else if(i < pos){ nn = pos-i; //mpre("Целая часть "+ to_string(nn), __LINE__);
												}else if(i > pos){ nn = -i+pos; //mpre("Дробная часть "+ to_string(nn), __LINE__);
												}else{ mpre("ОШИБКА получение значения источника/результата", __LINE__);
												} //mpre("Значение "+ _value+ " позиция pos="+ to_string(pos)+ " смещение i="+ to_string(i)+ " номер nn="+ to_string(nn)+ " символ val="+ val, __LINE__);
											return false; }()){ mpre("ОШИБКА результат точка", __LINE__);
										}else if([&](){ // Выборка уже созданных значений
												string _values;
												/*if(0 >= bmf_vals_size){ //mpre("Пустой список не ищем", __LINE__);
												}else*/ if([&](){ _vals = erb(VALS, {{"clump_id", clump_id}, {alias+ "_values_id", values["id"]}, {"name", to_string(nn)}}); return _vals.empty(); }()){ //mpre("Знак `"+ values["name"]+ "` уже добавлен "+ to_string(nn), __LINE__);
												}else if([&](){ _vals["val"] = val; return (1 != val.length()); }()){ mpre("ОШИБКА установки знака знака", __LINE__);
												}else if([&](){ _values = _vals["values"]+ val; return (0 >= _values.length()); }()){ mpre("ОШИБКА инкремента знака к истории", __LINE__);
												}else if([&](){ _vals["values"] = (values_length < _values.length() ? _values.substr(_values.length()-values_length, _values.length()) : _values); return (0 >= _values.length()); }()){ mpre("ОШИБКА приведения истории к нужной длинне", __LINE__);
												}else{ //mpre("Установка знака и истории знаку", __LINE__);
												}
											return false; }()){ mpre("ОШИБКА определения знака и установка его значений/истории", __LINE__);
										}else if([&](){ // Добавление нового знака
												if(false){ mpre("ОШИБКА уведомления", __LINE__);
												//}else if(true){ mpre("Првоерка "+ value, __LINE__);
												}else if(!_vals.empty()){ //mpre("Знак уже создан не добавляем", __LINE__);
												}else if([&](){ _vals = {{"id", Id(BMF_VALS.at(""))}, {"clump_id", clump_id}, {alias+ "_values_id", values["id"]}, {"name", to_string(nn)}, {"val", val}, {"values", val}}; return _vals.empty(); }()){ mpre("ОШИБКА создания нового знака", __LINE__);
												}else if([&](){ erb_insert(BMF_VALS, _vals["id"], _vals); return _vals.empty(); }()){ mpre("ОШИБКА обновления справочника знаков", __LINE__);
												}else if([&](){ /*std::lock_guard<std::mutex> lock(mu);*/ std::cerr << " " << to_string(nn); return false; }()){ mpre("ОШИБКА уведомления о создании нового знака", __LINE__);
												}else{  //mpre("Добавление нового знака `"+ values["name"]+ "` "+ to_string(nn), __LINE__);
												}
											return false; }()){ mpre("ОШИБКА добавления нового знака", __LINE__);
										//}else if([&](){ mpre(_vals, __LINE__, "Проверяем знак "+ to_string(i)); return false; }()){ mpre("ОШИБКА уведомления", __LINE__);
										/*}else if([&](){ // Добавление морфа в справочник если его там нет
												if(BMF_VALS.at("").end() != BMF_VALS.at("").find(stoi(_vals["id"]))){ //mpre("Значение уже в справочнике", __LINE__);
												}else{ //mpre("Добавление нового знака", __LINE__);
												}
											return _vals.empty(); }()){ mpre("ОШИБКА добавления морфа в справочник если нет", __LINE__);*/
										}else if(_vals.empty()){ mpre("ОШИБКА пустое значение знака", __LINE__);
										}else if(BMF_VALS.at("").end() == BMF_VALS.at("").find(stoi(_vals["id"]))){ mpre("ОШИБКА знак не найден в списке", __LINE__);
										}else if([&](){ BMF_VALS.at("").at(stoi(_vals["id"])).at("val") = _vals["val"]; return (BMF_VALS.end() != BMF_VALS.find("val")); }()){ mpre("ОШИБКА установки значения создан индекс `vals`", __LINE__);
										}else if([&](){ BMF_VALS.at("").at(stoi(_vals["id"])).at("values") = _vals["values"]; return (BMF_VALS.end() != BMF_VALS.find("values")); }()){ mpre("ОШИБКА установки значения создан индекс `values`", __LINE__);
										}else{ //mpre("Установка знака `"+ values["name"]+ "` "+ to_string(nn), __LINE__); //mpre(_vals, __LINE__, "Добавление нового знака "+ to_string(i));
										}
									}  return false;
								}()){ mpre("ОШИБКА добавления знаков значению", __LINE__);
							}else if([&](){ // Увдомление о новых знаках
									if(large > SIGN.at("1").size()){ //mpre("Необходимо добавить знаков целой части", __LINE__);
									}else if(small > SIGN.at("-1").size()){ //mpre("Необходимо добавить знаков дробной части", __LINE__);
									}else{ //mpre("Добавление новых знаков не требуется", __LINE__);
										return false;
									} std::cerr << endl;
								return false; }()){ mpre("ОШИБКА отображения уведомления о добавлениии новых знаков", __LINE__);
							}else{ //mpre("Знак ", __LINE__); //mpre(values, __LINE__, "Значение"); mpre(VALS, __LINE__, "Значения");
							};
						}; return false;
					}); return false;
				}()){ mpre("ОШИБКА установки значений", __LINE__);
			/*}else if([&](){ // Расчет морфа по формуле
					//TMs Calc(TMs index, true){ // Расчет значения двоичной формулы
					Calc = ([&](TMs index){ // Обучение
						std::function<string(string)> Val = ([&](string formula){ //в этой функции рассмотрим все возможные операции, что могут встретиться в строке
							string val;
							std::function<bool(char)> operation = ([&](char c){ //в этой функции рассмотрим все возможные операции, что могут встретиться в строке
								return c=='+' || c=='-' || c=='*' ||  c=='/' || c=='^' || c=='&' || c=='|' || c=='!';
							});
							std::function<int(int)> prioritet = ([&](int op){ //данная функция будет возвращать приоритет поступившей операции
								if(0 > op){ return 4;
						      }else if(op == '&' || op == '|' || op == '!'){ return 1;
						   	}else if(op == '+' || op == '-'){ return 2;
				      		}else if(op == '*' || op == '/' ){ return 3;
			   			   }else if(op == '^'){ return 5;
					   	   }else{ return -1; }
							});
							std::function<vector<long>(vector<long>, char)> action = ([&](vector<long> value, char op){ //следующая функция описывает принцип работы каждого оператора
								if(0 > op){ //для унарных операций
					      		long unitar=value.back();
				   		   	value.pop_back();
			
			   					if(-op=='-'){ value.push_back(-unitar);
									}else if((unitar != 0) && (unitar != 1)){ mpre("ОШИБКА значение "+ to_string(unitar)+ " не верное ожидается 0 или 1", __LINE__);
									}else if(-op=='!'){ value.push_back(unitar == 0 ? 1 : 0);
									}else{ mpre("ОШИБКА не опознанный оператор "+ op, __LINE__);
									}
								}else{ //для бинарных операций
			      				long right = value.back();
			      				value.pop_back();
				   	   		long left = value.back();
			   					value.pop_back();
			
			   					if('+' == op){ value.push_back(left+right);
						      	}else if('-' == op){ value.push_back(left-right);
							   	}else if('*' == op){ value.push_back(left*right);
			      				}else if('/' == op){ value.push_back(left/right);
				      			}else if('^' == op){ value.push_back(pow(left,right));
			   		   		}else if('&' == op){ value.push_back((left == 0 ? false : true)&(right == 0 ? false : true) ? 1 : 0);
				   				}else if('|' == op){ value.push_back((left == 0 ? false : true)|(right == 0 ? false : true) ? 1 : 0);
									}else{ mpre("ОШИБКА оператор не определен "+ to_string(op), __LINE__);
									}
								} return value;
							});
							std::function<int(string)> calculus = ([&](string formula){ // Расчет значения формулы
								bool unary=true;        //создадим булевскую переменную, для распределения операторов на унарные и бинарные
				   	 		vector<long>value;        //заведем массив для целых чисел
							    vector<char>op;           //и соответственно для самых операторов
							    for(int i=0; i<formula.size(); i++){
			      			      if(formula[i]=='('){    //если текущий элемент — открывающая скобка, то положим её в стек
				         	   	    op.push_back('(');  
			   		      		    unary=true;
			         			   }else if(formula[i]==')'){
				      	      		 while(op.back()!='('){  //если закрывающая скобка - выполняем все операции, находящиеся внутри этой скобки
				   	   	              value = action(value, op.back());
			   			   	           op.pop_back();
				         			    } op.pop_back();
			                			unary=false;
					            	}else if(operation(formula[i])){ //если данный элемент строки является одни из выше перечисленных операндов,то
						   		       char zn=formula[i];
			   	   			       if(unary==true) zn=-zn; //придает отрицательное значение, для распознавания функции унарности оператора 
			      		      		 while(!op.empty() && prioritet(op.back())>=prioritet(zn)){
					            	        value = action(value, op.back());   //выполняем сами алгебраические вычисления, где все уже операции упорядочены  
				   		         	     op.pop_back();              //в одном из стеков по строгому убыванию приоритета, если двигаться от вершины
				         		       } op.push_back(zn);
			   	         			 unary=true;
				         		   }else{
			         				    string number;      //заведем строку для найденных числовых операндов
			   			         	 while(i<formula.size() && isdigit(formula[i])){
												number+=formula[i++];//распознаем их с помощью библиотечной функции строк
			      	         		 } i--;
			
											 TMs _dano; string dano_values, val;
											 if(false){
											 //}else if(BMF_DANO_EX.at("").end() == BMF_DANO_EX.at("").find(stoi(number))){ mpre("ОШИБКА параметр отсутствует в исходных данных "+ number, __LINE__);
											 //}else if([&](){ TMs::iterator itr = BMF_DANO_EX.at("").find(stoi(number)); _dano = ((BMF_DANO_EX.at("").end() == itr) ? TMs({}) : itr->second); return _dano.empty(); }()){ mpre("ОШИБКА элемент не найден", __LINE__);
											 }else if([&](){ _dano = BMF_DANO_EX.at("").at(stoi(number)); return _dano.empty(); }()){ mpre("ОШИБКА получения дано знака", __LINE__);
											 }else if([&](){ dano_values = _dano.at("values"); return (0 >= dano_values.length()); }()){ mpre("ОШИБКА пустая история дано", __LINE__);
											 }else if([&](){ val = dano_values.substr(dano_values.length()-1, 1); return (1 != val.length()); }()){ mpre("ОШИБКА формата знака истории", __LINE__);
											 }else{ //mpre("Дано "+ number+ " смещение "+ to_string(std::abs(offset))+ " равно "+ val, __LINE__);
												value.push_back(stol(val)); //поместим в наш стек с числовыми выражениями
											 } unary=false;
			      				   }
					     		}
							 	while(!op.empty()){     //выполним еще не использованные операции в стеке 
			  			   		value = action(value, op.back());
					     	   	op.pop_back();
				   		 	} return value.back(); //получим на выходе значение выражения
							}); return to_string(calculus(formula));
						});
			
						TMs _calc_val, _calc; string _f1, _f0, _v1, _v0;
						if([&](){ _f1 = index.at("f1"); return (0 >= _f1.length()); }()){ mpre("ОШИБКА получения формулы старшего значения", __LINE__);
						}else if([&](){ _f0 = index.at("f0"); return (0 >= _f0.length()); }()){ mpre("ОШИБКА получения формулы младшего значения", __LINE__);
						}else if([&](){ _v1 = Val(_f1); return (1 != _v1.length()); }()){ mpre("ОШИБКА расчета старшего значения формулы", __LINE__);
						}else if([&](){ _v0 = Val(_f0); return (1 != _v0.length()); }()){ mpre("ОШИБКА расчета младшего значения формулы", __LINE__);
						}else if([&](){ _calc_val = erb(BMF_CALC_VAL, {{"v1", _v1}, {"v0", _v0}}); return _calc_val.empty(); }()){ mpre("ОШИБКА получения значения "+ _v1+ " "+ _v0, __LINE__);
						}else if([&](){ _calc = erb(BMF_CALC, {{"calc_pos_id", index["calc_pos_id"]}, {"calc_val_id", _calc_val["id"]}}); return _calc.empty(); }()){ mpre("ОШИБКА получения расчета морфа", __LINE__);
						}else if([&](){ index["calc_val_id"] = _calc_val["id"]; return index.empty(); }()){ mpre("ОШИБКА сохранения расчета", __LINE__);
						}else if([&](){ index["val"] = _calc["val"]; return index.empty(); }()){ mpre("ОШИБКА установки расчетного значения морфа", __LINE__);
						}else{ //mpre("Расчетная история", __LINE__);
						} return index;
					}); return false;
				}()){ mpre("ОШИБКА установки функции расчета", __LINE__);
			}else if([&](){ // Расчет сигнала
					Calc = ([&](TMs index){ // Обучение
						typedef std::map<int, TMMi> TMii;
						TMii CALC; TMMi INDEX; TMs _index; int depth;
						if(index["depth"] != "0"){ mpre(index, __LINE__, "Расчетный морф"); mpre("ОШИБКА расчеты производятся только над корневыми морфами", __LINE__);
						}else if([&](){ INDEX = rb(BMF_INDEX_EX, {{"itog_id", index["itog_id"]}}); return INDEX.empty(); }()){ mpre("ОШИБКА у итога не найдено связанных морфов", __LINE__);
						}else if([&](){ for_each(INDEX.begin(), INDEX.end(), [&](auto &index_itr){ // Составляем лестницу расчетов
								TMs index, index_1, index_0, dano; int depth;
								if([&](){ index = index_itr.second; return index.empty(); }()){ mpre("ОШИБКА получения морфа для расчета", __LINE__);
								}else if(index.end() == index.find("dano_id")){ mpre("ОШИБКА у морфа не найдено поле dano_id", __LINE__);
								}else if(BMF_DANO_EX.at("").end() == BMF_DANO_EX.at("").find(stoi(index["dano_id"]))){ mpre("ОШИБКА в исходном справочнике не нашлось связанного с морфа элемента dano_id="+ index["dano_id"], __LINE__);
								}else if([&](){ dano = BMF_DANO_EX.at("").at(stoi(index.at("dano_id"))); return dano.empty(); }()){ mpre("ОШИБКА не найден исходный элемент для морфа", __LINE__);
								}else if([&](){ depth = stoi(index["depth"]); return (0 > depth); }()){ mpre("ОШИБКА получения глубины морфа", __LINE__);
								}else if([&](){ // Получение старшего потомка
										if(index.end() == index.find("index_id")){ mpre("ОШИБКА поле index_id не задано у морфа", __LINE__);
										}else if("" == index.at("index_id")){ //mpre("Старший потомок у морфа не задан "+ index["id"], __LINE__);
										}else if([&](){ regex b("^-?[0-9|.]+$"); return !regex_match(index.at("index_id"), b); }()){ mpre("ОШИБКА Формат значения `"+ index["index_id"]+ "` не верен", __LINE__);
										}else if(BMF_INDEX_EX.at("").end() == BMF_INDEX_EX.at("").find(stoi(index["index_id"]))){ mpre("ОШИБКА указанный у морфа нижестоящий потомок не найден в справочнике", __LINE__);
										}else if([&](){ index_1 = BMF_INDEX_EX.at("").at(stoi(index["index_id"])); return index_1.empty(); }()){ mpre("ОШИБКА получения старшего потомка", __LINE__);
										}else{ //mpre(index_1, __LINE__, "Старший нижестоящий потомок");
										}
									return false; }()){ mpre("ОШИБКА получения старшего потомка", __LINE__);
								}else if([&](){ // Получение старшего потомка
										if(index.end() == index.find("bmf-index")){ mpre("ОШИБКА поле index_id не задано у морфа", __LINE__);
										}else if("" == index.at("bmf-index")){ //mpre("Младший потомок у морфа не задан "+ index["id"], __LINE__);
										}else if([&](){ regex b("^-?[0-9|.]+$"); return !regex_match(index.at("bmf-index"), b); }()){ mpre("ОШИБКА Формат значения `"+ index["bmf-index"]+ "` не верен", __LINE__);
										}else if(BMF_INDEX_EX.at("").end() == BMF_INDEX_EX.at("").find(stoi(index["bmf-index"]))){ mpre("ОШИБКА указанный у морфа нижестоящий потомок не найден в справочнике", __LINE__);
										}else if([&](){ index_0 = BMF_INDEX_EX.at("").at(stoi(index["bmf-index"])); return index_0.empty(); }()){ mpre("ОШИБКА получения старшего потомка", __LINE__);
										}else{ //mpre(index_0, __LINE__, "Младший нижестоящий потомок");
										}
									return false; }()){ mpre("ОШИБКА получения старшего потомка", __LINE__);
								}else if(!index_1.empty() && !index_0.empty()){ //mpre("Морф не является висячим "+ index["id"], __LINE__);
								}else if([&](){ // Добавление пустого уровня
										if(CALC.find(depth) != CALC.end()){ //mpre("Уровень уже добавлен", __LINE__);
										}else if([&](){ CALC.insert(make_pair(depth, TMMi({}))); return (CALC.end() == CALC.find(depth)); }()){ mpre("ОШИБКА создания пустого уровня списку расчетов", __LINE__);
										}else{ //mpre("Добавляем новый уровень списку расчетов "+ to_string(depth), __LINE__);
										}
									return false; }()){ mpre("ОШИБКА добавления пустого списка уровню морфа", __LINE__);
								}else if([&](){ CALC.at(depth).insert(make_pair(stoi(index["id"]), index)); return CALC.at(depth).empty(); }()){ mpre("ОШИБКА добавления морфа в список расчета", __LINE__);
								}else{ //mpre("Добавляем морф к списку расчетов "+ index["id"], __LINE__); //mpre(index, __LINE__, "Морф");
								}
							}); return false; }()){ mpre("ОШИБКА распределения входящих данных по списку расчета", __LINE__);
						}else if([&](){ depth = CALC.rbegin()->first; return (0 > depth); }()){ mpre("ОШИБКА получения максимального уровня расчетов", __LINE__);
						}else if([&](){ do{ //mpre("РАСЧЕТ Уровень "+ to_string(depth), __LINE__); // for_each(CALC.rbegin(), CALC.rend(), [&](auto &calc_itr){ // Расчет списка по уровням
								int _depth; TMii::iterator calc_itr;
								if([&](){ calc_itr = CALC.find(depth); return (CALC.end() == calc_itr); }()){ mpre("ОШИБКА уровень не найден "+ to_string(depth), __LINE__);
								}else if(calc_itr->second.empty()){ mpre("ОШИБКА список морфов уровня расчетов пуст", __LINE__);
								}else if([&](){ depth = calc_itr->first; return (0 > depth); }()){ mpre("ОШИБКА получения глубины списка морфов", __LINE__);
								}else if([&](){ _depth = depth-1; return (-1 > _depth); }()){ mpre("ОШИБКА расчета глубины вышестоящего морфа", __LINE__);
								}else if([&](){ for_each(calc_itr->second.begin(), calc_itr->second.end(), [&](auto index_itr){ // Расчет морфов глубины
										TMs index, _index_, calc_val, calc; string field, _field;
										if([&](){ index = index_itr.second; return index.empty(); }()){ mpre("ОШИБКА получения морфа глубины", __LINE__);
										}else if([&](){ //mpre("Расчет значения обучения морфа", __LINE__);
												TMs _calc, dano, calc_pos, _calc_val, index_1, index_0; string v1, v0;
												if(index.empty()){ mpre("ОШИБКА морф пуст", __LINE__);
												}else if([&](){ dano = erb(BMF_DANO_EX, {{"id", index["dano_id"]}}); return dano.empty(); }()){ mpre("ОШИБКА установки дано морфа", __LINE__);
												}else if([&](){ index_1 = erb(BMF_INDEX_EX, {{"id", index["index_id"]}}); return false; }()){ mpre("ОШИБКА выборки старшего элемента", __LINE__);
												}else if([&](){ index_0 = erb(BMF_INDEX_EX, {{"id", index["bmf-index"]}}); return false; }()){ mpre("ОШИБКА выборки младшего морфа", __LINE__);
												}else if([&](){ v1 = (index_1.empty() ? dano["val"] : index_1["val"]); return (1 != v1.length()); }()){ mpre("ОШИБКА получения старшего знака морфа", __LINE__);
												}else if([&](){ v0 = (index_0.empty() ? dano["val"] : index_0["val"]); return (1 != v0.length()); }()){ mpre("ОШИБКА получения младшего знака морфа", __LINE__);
												}else if([&](){ _calc_val = erb(BMF_CALC_VAL, {{"v1", v1}, {"v0", v0}}); return _calc_val.empty(); }()){ mpre("ОШИБКА получения занчения морфа", __LINE__);
												}else if([&](){ _calc = erb(BMF_CALC, {{"calc_pos_id", index["calc_pos_id"]}, {"calc_val_id", _calc_val["id"]}}); return _calc.empty(); }()){ mpre("ОШИБКА выборки расчета морфа", __LINE__);
												}else if([&](){ index["calc_val_id"] = _calc_val["id"]; return _calc_val.empty(); }()){ mpre("ОШИБКА установки значения морфа", __LINE__);
												}else if(index["val"] == _calc["val"]){ return true;
												}else if([&](){ index["val"] = _calc["val"]; return index.empty(); }()){ mpre("ОШИБКА установки знака морфу", __LINE__);
												}else{ //mpre("Расчеты "+ index["id"]+ " значения "+ v1+ ","+ v0, __LINE__); // mpre("Расчет обучения "+ index["id"]+ " => "+ index, __LINE__);
												} return index.empty();
											}()){ mpre("Расчета значения морфа не изменился", __LINE__);
										}else if([&](){ erb_insert(BMF_INDEX_EX, index["id"], index); return index.empty(); }()){ mpre("ОШИБКА сохранения результата расчета в справочник", __LINE__);
										}else if([&](){ // Получение поля связи c вышестоящим морфом
												if([&](){ _index_ = erb(BMF_INDEX_EX, {{field = "index_id", index["id"]}}); return !_index_.empty(); }()){ //mpre("Текущий морф связан с вышестоящим по полю index_id", __LINE__);
												}else if([&](){ _index_ = erb(BMF_INDEX_EX, {{field = "bmf-index", index["id"]}}); return !_index_.empty(); }()){ //mpre("Текущий морф связан с вышестоящим по полю bmf-index", __LINE__);
												}else if(!_index_.empty() && (_depth != stoi(_index_["depth"]))){ mpre("ОШИБКА уровемнь корневого морфа не совпадает с вышестоящим", __LINE__);
												}else if("0" != index["depth"]){ mpre(index, __LINE__, "Расчетный морф"); mpre("ОШИБКА не корневой морф не связан с вышестоящими", __LINE__);
												}else{ //mpre(index, __LINE__, "Корневой морф"); //mpre("ОШИБКА текущий морф не связан с вышестоящим", __LINE__);
													_index = index;
												}
											return _index_.empty(); }()){ //mpre("Корневой морф ["+ index["id"]+ "] val="+ index["val"], __LINE__);
										}else if([&](){ // Добавление списка вышестоящего морфа
												if(CALC.find(_depth) != CALC.end()){ //mpre("Список вышестоящего морфа уже создан", __LINE__);
												}else if([&](){ CALC.insert(make_pair(_depth, TMMi({}))); return (CALC.end() == CALC.find(_depth)); }()){ mpre("ОШИБКА добавления списка вышестоящего морфа", __LINE__);
												}else{ //mpre("Добавление нового уровня списку расчетов "+ to_string(_depth), __LINE__);
												}
											return CALC.empty(); }()){ mpre("ОШИБКА добавления списка вышестоящего морфа", __LINE__);
										}else if([&](){ // Установка значения родителю
												if(CALC.at(_depth).end() != CALC.at(_depth).find(stoi(_index_["id"]))){ //mpre("Родительский морф уже есть в списке "+ _index_["id"], __LINE__);
												}else if([&](){ CALC.at(_depth).insert(make_pair(stoi(_index_["id"]), _index_)); return CALC.at(_depth).empty(); }()){ mpre("ОШИБКА добавления родительского морфа списку расчетов", __LINE__);
												}else{ //mpre("Добавление родительского морфа в список расчетов "+ _field+ " "+ _index_["id"], __LINE__);
												}
											return false; }()){ mpre("ОШИБКА ошибка получения/добавления родительского морфа в/из списока расчетов", __LINE__);
										}else if([&](){ CALC.at(_depth).at(stoi(_index_["id"])) = _index_; return _index_.empty(); }()){ mpre("ОШИБКА сохранения расчетов родительского морфа", __LINE__);
										}else{ //mpre(index, __LINE__, "Морф глубины "+ to_string(depth));
										}
									}); return false; }()){ mpre("ОШИБКА перебора всех морфов глубины", __LINE__);
								}else{ //mpre("Глубина "+ to_string(depth), __LINE__);
								}
							}while(0 <= --depth); return false; }()){ mpre("ОШИБКА расчета списка по уровням", __LINE__);
						}else if(_index.empty()){ mpre("ОШИБКА расчета корневого морфа", __LINE__);
						}else{ //mpre(_index, __LINE__, "ОШИБКА расчета");
						} return _index;
					});
				return false; }()){ mpre("ОШИБКА создания фукнции подсчета сигнала", __LINE__);*/
			}else if([&](){ // Расчет сигнала
					CalcAll = ([&](TMs index){ //mpre("Расчет общий", __LINE__); // Обучение
						TMMi CALC, INDEX; TMs _index = index; int depth;
						if(index["depth"] != "0"){ mpre(index, __LINE__, "Расчетный морф"); mpre("ОШИБКА расчеты производятся только над корневыми морфами", __LINE__);
						}else if([&](){ INDEX = rb(BMF_INDEX_EX, {{"itog_id", index["itog_id"]}}); return INDEX.empty(); }()){ mpre("ОШИБКА у итога не найдено связанных морфов", __LINE__);
						}else if([&](){ for(auto &index_itr:INDEX){ // for_each(INDEX.begin(), INDEX.end(), [&](auto &index_itr){ // Составляем лестницу расчетов
								TMs index, index_1, index_0, dano; int depth, next;
								if([&](){ index = index_itr.second; return index.empty(); }()){ mpre("ОШИБКА получения морфа для расчета", __LINE__);
								}else if(index.end() == index.find("dano_id")){ mpre("ОШИБКА у морфа не найдено поле dano_id", __LINE__);
								}else if([&](){ return (BMF_DANO_EX.at("").end() == BMF_DANO_EX.at("").find(stoi(index["dano_id"]))); }()){ mpre("ОШИБКА в исходном справочнике не нашлось связанного с морфа элемента dano_id="+ index["dano_id"], __LINE__);
								}else if([&](){ dano = BMF_DANO_EX.at("").at(stoi(index.at("dano_id"))); return dano.empty(); }()){ mpre("ОШИБКА не найден исходный элемент для морфа", __LINE__);
								}else if([&](){ depth = stoi(index["depth"]); return (0 > depth); }()){ mpre("ОШИБКА получения глубины морфа", __LINE__);
								}else if([&](){ // Получение старшего потомка
										if(index.end() == index.find("index_id")){ mpre("ОШИБКА поле index_id не задано у морфа", __LINE__);
										}else if("" == index.at("index_id")){ //mpre("Старший потомок у морфа не задан "+ index["id"], __LINE__);
										//}else if([&](){ regex b("^-?[0-9|.]+$"); return !regex_match(index.at("index_id"), b); }()){ mpre("ОШИБКА Формат значения `"+ index["index_id"]+ "` не верен", __LINE__);
										}else if(0 > index.at("index_id").find_last_not_of("1.0")){ mpre("ОШИБКА не подходящий формат идентификатора "+ index.at("index_id"), __LINE__);
										}else if([&](){ return (BMF_INDEX_EX.at("").end() == BMF_INDEX_EX.at("").find(stoi(index["index_id"]))); }()){ mpre("ОШИБКА указанный у морфа нижестоящий потомок не найден в справочнике", __LINE__);
										}else if([&](){ index_1 = BMF_INDEX_EX.at("").at(stoi(index["index_id"])); return index_1.empty(); }()){ mpre("ОШИБКА получения старшего потомка", __LINE__);
										}else{ //mpre(index_1, __LINE__, "Старший нижестоящий потомок");
										}
									return false; }()){ mpre("ОШИБКА получения старшего потомка", __LINE__);
								}else if([&](){ // Получение старшего потомка
										if(index.end() == index.find("bmf-index")){ mpre("ОШИБКА поле index_id не задано у морфа", __LINE__);
										}else if("" == index.at("bmf-index")){ //mpre("Младший потомок у морфа не задан "+ index["id"], __LINE__);
										//}else if([&](){ regex b("^-?[0-9|.]+$"); return !regex_match(index.at("bmf-index"), b); }()){ mpre("ОШИБКА Формат значения `"+ index["bmf-index"]+ "` не верен", __LINE__);
										}else if(0 > index.at("bmf-index").find_last_not_of("1.0")){ mpre("ОШИБКА не подходящий формат идентификатора "+ index.at("index_id"), __LINE__);
										}else if([&](){ return (BMF_INDEX_EX.at("").end() == BMF_INDEX_EX.at("").find(stoi(index["bmf-index"]))); }()){ mpre(index, __LINE__, "Морф"); mpre("ОШИБКА указанный у морфа нижестоящий потомок не найден в справочнике", __LINE__);
										}else if([&](){ index_0 = BMF_INDEX_EX.at("").at(stoi(index["bmf-index"])); return index_0.empty(); }()){ mpre("ОШИБКА получения старшего потомка", __LINE__);
										}else{ //mpre(index_0, __LINE__, "Младший нижестоящий потомок");
										}
									return false; }()){ mpre("ОШИБКА получения старшего потомка", __LINE__);
								}else if(!index_1.empty() && !index_0.empty()){ //mpre("Морф не является висячим "+ index["id"], __LINE__);
								}else if([&](){ // Добавление пустого уровня
										if(CALC.find(depth) != CALC.end()){ //mpre("Уровень уже добавлен", __LINE__);
										}else if([&](){ CALC.insert(make_pair(depth, TMs({}))); return (CALC.end() == CALC.find(depth)); }()){ mpre("ОШИБКА создания пустого уровня списку расчетов", __LINE__);
										}else{ //mpre("Добавляем новый уровень списку расчетов "+ to_string(depth), __LINE__);
										}
									return false; }()){ mpre("ОШИБКА добавления пустого списка уровню морфа", __LINE__);
								}else if([&](){ CALC.at(depth).insert(make_pair(index["id"], index["id"])); return CALC.at(depth).empty(); }()){ mpre("ОШИБКА добавления морфа в список расчета", __LINE__);
								}else{ //mpre("Добавляем морф к списку расчетов "+ to_string(next)+ " "+ index["id"], __LINE__); //mpre(index, __LINE__, "Морф");
								}
							}; return false; }()){ mpre("ОШИБКА распределения входящих данных по списку расчета", __LINE__);
						}else if([&](){ depth = CALC.rbegin()->first; return (0 > depth); }()){ mpre("ОШИБКА получения максимального уровня расчетов", __LINE__);
						}else if([&](){ do{ //mpre("РАСЧЕТ Уровень "+ to_string(depth), __LINE__); // for_each(CALC.rbegin(), CALC.rend(), [&](auto &calc_itr){ // Расчет списка по уровням
								int _depth; TMMi::iterator calc_itr;
								if([&](){ calc_itr = CALC.find(depth); return (CALC.end() == calc_itr); }()){ //mpre("ОШИБКА Уровень не найден "+ to_string(depth), __LINE__);
								}else if(calc_itr->second.empty()){ mpre("ОШИБКА список морфов уровня расчетов пуст", __LINE__);
								}else if([&](){ _depth = depth-1; return (-1 > _depth); }()){ mpre("ОШИБКА расчета глубины вышестоящего морфа", __LINE__);
								}else if([&](){ for(auto &index_itr:calc_itr->second){ //for_each(calc_itr->second.begin(), calc_itr->second.end(), [&](auto index_itr){ // Расчет морфов глубины
										TMs index, _index_, calc_val, calc, _calc; string field, _field, index_id;
										if([&](){ index = BMF_INDEX_EX.at("").at(stoi(index_itr.second)); return index.empty(); }()){ mpre("ОШИБКА получения морфа глубины", __LINE__);
										}else if([&](){ //mpre("Расчет значения обучения морфа", __LINE__);
												TMs dano, calc_pos, _calc_val, index_1, index_0; string v1, v0;
												if(index.empty()){ mpre("ОШИБКА морф пуст", __LINE__);
												}else if([&](){ dano = erb(BMF_DANO_EX, {{"id", index["dano_id"]}}); return dano.empty(); }()){ mpre("ОШИБКА установки дано морфа", __LINE__);
												//}else if([&](){ index_1 = erb(BMF_INDEX_EX, {{"id", index["index_id"]}}); return false; }()){ mpre("ОШИБКА выборки старшего элемента", __LINE__);
												}else if([&](){ // Старший потомок
														if("" == index["index_id"]){ //mpre("Связь со старшим потомком не указана", __LINE__);
														}else if([&](){ return (BMF_INDEX_EX.at("").end() == BMF_INDEX_EX.at("").find(stoi(index["index_id"]))); }()){ //mpre("Морф не найден", __LINE__);
														}else if([&](){ index_1 = BMF_INDEX_EX.at("").at(stoi(index["index_id"])); return index_1.empty(); }()){ mpre("ОШИБКА выборки старшего потомка", __LINE__);
														}else{
														}
													return false; }()){ mpre("ОШИБКА получения старшено потомка", __LINE__);
												//}else if([&](){ index_0 = erb(BMF_INDEX_EX, {{"id", index["bmf-index"]}}); return false; }()){ mpre("ОШИБКА выборки младшего морфа", __LINE__);
												}else if([&](){ // Старший потомок
														if("" == index["bmf-index"]){ //mpre("Связь со младшим потомком не указана", __LINE__);
														}else if([&](){ return (BMF_INDEX_EX.at("").end() == BMF_INDEX_EX.at("").find(stoi(index["bmf-index"]))); }()){ //mpre("Морф не найден", __LINE__);
														}else if([&](){ index_0 = BMF_INDEX_EX.at("").at(stoi(index["bmf-index"])); return index_0.empty(); }()){ mpre("ОШИБКА выборки младшего потомка", __LINE__);
														}else{
														}
													return false; }()){ mpre("ОШИБКА получения старшено потомка", __LINE__);
												}else if([&](){ v1 = (index_1.empty() ? dano["val"] : index_1["val"]); return (1 != v1.length()); }()){ mpre("ОШИБКА получения старшего знака морфа", __LINE__);
												}else if([&](){ v0 = (index_0.empty() ? dano["val"] : index_0["val"]); return (1 != v0.length()); }()){ mpre("ОШИБКА получения младшего знака морфа", __LINE__);
												}else if([&](){ _calc_val = _VAL.at(v1).at(stoi(v0)); return _calc_val.empty(); }()){ mpre("ОШИБКА получения значения морфа", __LINE__);
												}else if([&](){ _calc = _CALC.at(index["calc_pos_id"]).at(stoi(_calc_val["id"])); return _calc.empty(); }()){ mpre("ОШИБКА выборки расчета", __LINE__);
												}else if([&](){ index["calc_val_id"] = _calc_val["id"]; return _calc_val.empty(); }()){ mpre("ОШИБКА установки значения морфа", __LINE__);
												}else if([&](){ BMF_INDEX_EX.at("").at(stoi(index["id"])) = index; return index.empty(); }()){ mpre("ОШИБКА сохранения значения в справочнике", __LINE__);
												}else{ //mpre("Расчеты "+ index["id"]+ " значения "+ v1+ ","+ v0, __LINE__); // mpre("Расчет обучения "+ index["id"]+ " => "+ index, __LINE__);
												} return index.empty();
											}()){ mpre("ОШИБКА расчета значения морфа", __LINE__);
										}else if([&](){ // Установка корневого морфа
												if("0" != index["depth"]){ //mpre(index, __LINE__, "Расчетный морф"); //mpre("Не корневой морф", __LINE__);
												}else if([&](){ index["val"] = _calc["val"]; return index.empty(); }()){ mpre("ОШИБКА установки значения корневому морфу", __LINE__);
												}else if([&](){ BMF_INDEX_EX.at("").at(stoi(index["id"])) = index; return (BMF_INDEX_EX.end() != BMF_INDEX_EX.find("val")); }()){ mpre("ОШИБКА установки расчетного значения морфа", __LINE__);
												}else{ _index = index; }
											return _index.empty(); }()){ mpre("Корневой морф", __LINE__);
										}else if(index["val"] == _calc["val"]){ //mpre("Значения морфа не изменилось можно дальше не вычислять", __LINE__);
										}else if([&](){ index["val"] = _calc["val"]; return index.empty(); }()){ mpre("ОШИБКА установки знака морфу", __LINE__);
										}else if([&](){ // Получение поля связи c вышестоящим морфом
												if([&](){ _index_ = erb(BMF_INDEX_EX, {{field = "index_id", index["id"]}}); return !_index_.empty(); }()){ //mpre("Текущий морф связан с вышестоящим по полю index_id", __LINE__);
												}else if([&](){ _index_ = erb(BMF_INDEX_EX, {{field = "bmf-index", index["id"]}}); return !_index_.empty(); }()){ //mpre("Текущий морф связан с вышестоящим по полю bmf-index", __LINE__);
												}else if(!_index_.empty() && (_depth != stoi(_index_["depth"]))){ mpre("ОШИБКА уровень корневого морфа не совпадает с вышестоящим", __LINE__);
												}else{ //mpre(index, __LINE__, "Корневой морф"); //mpre("ОШИБКА текущий морф не связан с вышестоящим", __LINE__);
												}
											return _index_.empty(); }()){ //mpre("Корневой морф ["+ index["id"]+ "] val="+ index["val"], __LINE__);
										}else if([&](){ // Добавление списка вышестоящего морфа
												if(CALC.find(_depth) != CALC.end()){ //mpre("Список вышестоящего морфа уже создан", __LINE__);
												}else if([&](){ CALC.insert(make_pair(_depth, TMs({}))); return (CALC.end() == CALC.find(_depth)); }()){ mpre("ОШИБКА добавления списка вышестоящего морфа", __LINE__);
												}else{ //mpre("Добавление нового уровня списку расчетов "+ to_string(_depth), __LINE__);
												}
											return CALC.empty(); }()){ mpre("ОШИБКА добавления списка вышестоящего морфа", __LINE__);
										}else if([&](){ // Добавление вышестоящего морфа
												if(CALC.at(_depth).end() != CALC.at(_depth).find(_index_["id"])){ //mpre("Родительский морф уже есть в списке "+ _index_["id"], __LINE__);
												}else if([&](){ CALC.at(_depth).insert(make_pair(_index_["id"], _index_["id"])); return CALC.at(_depth).empty(); }()){ mpre("ОШИБКА добавления родительского морфа списку расчетов", __LINE__);
												}else{
												}
											return CALC.at(_depth).empty(); }()){ mpre("ОШИБКА добавления вышестоящего морфа", __LINE__);
										}else if([&](){ BMF_INDEX_EX.at("").at(stoi(index["id"])) = index; return (BMF_INDEX_EX.end() != BMF_INDEX_EX.find("val")); }()){ mpre("ОШИБКА установки расчетного значения морфа", __LINE__);
										}else{ //mpre(index, __LINE__, "Морф глубины "+ to_string(depth));
										}
									}; return false; }()){ mpre("ОШИБКА перебора всех морфов глубины", __LINE__);
								}else{ //mpre("Глубина "+ to_string(depth), __LINE__);
								}
							}while(0 <= --depth); return false; }()){ mpre("ОШИБКА расчета списка по уровням", __LINE__);
						}else if(_index.empty()){ mpre("ОШИБКА расчета корневого морфа", __LINE__);
						}else{ //mpre(_index, __LINE__, "ОШИБКА расчета");
						} return _index;
					});
				return false; }()){ mpre("ОШИБКА создания фукнции подсчета сигнала", __LINE__);
			}else if([&](){ // Расчет сигнала
					Calc = ([&](TMs index, bool both){ //mpre(index, __LINE__, "расчет"); //mpre("Расчет "+ to_string(both)+ " "+ index["id"], __LINE__); // Обучение с учетом оптимизации расчетов сигнала
						TMs dano, calc, index_1, index_0, index_first, index_second, calc_pos, calc_val, calc_top, calc_val_top; string v1, v0;
						if(index.empty()){ mpre(index, __LINE__, "ОШИБКА морф для расчета пуст");
						}else if([&](){ // Уведомление корневого уровня
								if(!both){ //mpre("Не корневой уровень", __LINE__);
								}else{ //mpre("Расчет "+ index["id"]+ " "+ index["depth"], __LINE__);
								}
							return false; }()){ mpre("ОШИБКА уведомления о расчета коренвого уровня", __LINE__);
						}else if(index.end() == index.find("dano_id")){ mpre(index, __LINE__, "Морф"); mpre("ОШИБКА исключительная ситуация", __LINE__);
						}else if([&](){ index_1 = index_first = erb(BMF_INDEX_EX, {{"id", index["index_id"]}}); return false; }()){ mpre("ОШИБКА получения старшего потомка", __LINE__);
						}else if([&](){ index_0 = index_second = erb(BMF_INDEX_EX, {{"id", index["bmf-index"]}}); return false; }()){ mpre("ОШИБКА получения младшего потомка", __LINE__);
						}else if([&](){ dano = erb(BMF_DANO_EX, {{"id", index["dano_id"]}}); return dano.empty(); }()){ mpre(index, __LINE__, "Морф"); mpre("ОШИБКА выборки дано морфа", __LINE__);
						}else if([&](){ calc_pos = erb(BMF_CALC_POS, {{"id", index["calc_pos_id"]}}); return calc_pos.empty(); }()){ mpre(index, __LINE__, "ОШИБКА получения позиции морфа");
						}else if([&](){ // Выбор нарпавление расчета
								int s1, s0;
								if("" == index["s1"]){ //mpre("Статистика старшего морфа пуста", __LINE__);
								}else if("" == index["s0"]){ //mpre("Статистика младшего морфа пуста", __LINE__);
								}else if([&](){ s1 = stoi(index["s1"]); return false; }()){ mpre("ОШИБКА расчета старшей статистики", __LINE__);
								}else if([&](){ s0 = stoi(index["s0"]); return false; }()){ mpre("ОШИБКА расчета младшей статистики", __LINE__);
								}else if(s1 == s0){ //mpre("Статистика морфов равна", __LINE__);
								}else if([&](){ // Сверхпозиция на единицах
										if(calc_pos["v1"] == calc_pos["v0"]){ //mpre("Сверхпозиция не на единицах", __LINE__);
										}else if(s1 > s0){ index_first = index_1; index_second = index_0;
										}else{ index_first = index_0; index_second = index_1;
										}
									return false; }()){ mpre("Равное значение статистики", __LINE__);
								}else if([&](){ // Сверхпозиция на нулях
										if(calc_pos["v1"] != calc_pos["v0"]){ //mpre("Сверхпозиция не на нулях", __LINE__);
										}else if(s1 > s0){ index_first = index_0; index_second = index_1;
										}else{ index_first = index_1; index_second = index_0;
										}
									return false; }()){ mpre("Равное значение статистики", __LINE__);
								}else{
								}
							return false; }()){ mpre("ОШИБКА направление расчета", __LINE__);
						}else if([&](){ //mpre(index_first, __LINE__, "index_first"); mpre(index_second, __LINE__, "index_second"); // Расчет старшего потомка
								if(index_first.empty()){ //mpre("Старший потомок "+ index_val_1+ " пуст "+ index["id"], __LINE__);
								}else if([&](){ index_first = Calc(index_first, false); return index_first.empty(); }()){ mpre("ОШИБКА расчета первого морфа", __LINE__);
								}else if([&](){ calc_top = erb(BMF_CALC, {{"id", calc_pos["calc_id"]}}); return calc_top.empty(); }()){ mpre("ОШИБКА получения верхней расчетной позиции", __LINE__);
								}else if([&](){ calc_val_top = erb(BMF_CALC_VAL, {{"id", calc_top["calc_val_id"]}}); return calc_val_top.empty(); }()){ mpre("ОШИБКА получения верхнего значения", __LINE__);
								}else if([&](){ calc_val = (index_first["val"] == calc_val_top["v1"] ? calc_val : calc_val_top); return false; }()){ mpre("ОШИБКА расчета результата", __LINE__);
								}else{ //mpre("Частичный расчет морфа "+ index["id"], __LINE__);
								}
							return false; }()){ mpre("ОШИБКА расчета первого потомка", __LINE__);
						}else if([&](){ // Расчет второго потомка
								if(!calc_val.empty() && !both){ //mpre("Одного значений достаточно "+ index["id"], __LINE__);
								}else if(index_second.empty()){ //mpre("Младший потомок "+ index_val_0+ " пуст "+ index["id"], __LINE__);
								}else if([&](){ index_second = Calc(index_second, false); return index_second.empty(); }()){ mpre("ОШИБКА расчета младшего морфа", __LINE__);
								}else{ //mpre("Полный расчет "+ index_val_0+ " младшего морфа "+ index["id"], __LINE__);
								}
							return false; }()){ mpre("ОШИБКА расчета второго потомка", __LINE__);
						}else if([&](){ //Расчет старшего результата
								TMs _index;
								if([&](){ v1 = dano["val"]; return (1 != v1.length()); }()){ mpre("ОШИБКА установки значения дано", __LINE__);
								}else if(index_1.empty()){ //mpre("Старший потомок пуст", __LINE__);
								}else if(!(_index = index_first).empty() && (index_1["id"] == _index["id"])){ v1 = _index["val"];
								}else if(!(_index = index_second).empty() && (index_1["id"] == _index["id"])){ v1 = _index["val"];
								}else{ mpre("ОШИБКА соответствия расчетов морфу", __LINE__);
								}
							return (1 != v1.length()); }()){ mpre("ОШИБКА расчета старшего результата", __LINE__);
						}else if([&](){ //Расчет младшего результата
								TMs _index;
								if([&](){ v0 = dano["val"]; return (1 != v0.length()); }()){ mpre("ОШИБКА установки значения дано", __LINE__);
								}else if(index_0.empty()){ //mpre("Старший потомок пуст", __LINE__);
								}else if(!(_index = index_first).empty() && (index_0["id"] == _index["id"])){ v0 = _index["val"];
								}else if(!(_index = index_second).empty() && (index_0["id"] == _index["id"])){ v0 = _index["val"];
								}else{ mpre("ОШИБКА соответствия расчетов морфу", __LINE__);
								}
							return (1 != v0.length()); }()){ mpre("ОШИБКА расчета мледшего результата", __LINE__);
						}else if([&](){ // Расчет итогового морфа
								if([&](){ calc_val = _VAL.at(v1).at(stoi(v0)); return calc_val.empty(); }()){ mpre("ОШИБКА выборки итогового значения морфа "+ v1+ ":"+ v0, __LINE__);
								}else if([&](){ calc = _CALC.at(index["calc_pos_id"]).at(stoi(calc_val["id"])); return calc.empty(); }()){ mpre("ОШИБКА выборки расчета", __LINE__);
								}else{ //mpre(calc_val, __LINE__, "Значения"); mpre("Итоговый расчет "+ calc["val"]+ " морфа "+ index["id"], __LINE__);
								}
							return calc.empty(); }()){ mpre(index, __LINE__, "Морф"); mpre(calc_val, __LINE__, "Значение"); mpre("ОШИБКА расчета значения морфа", __LINE__);
						}else if([&](){ // Сохранение статистики результатов
								int s1, s0;
								if([&](){ s1 = ("" == index["s1"] ? 0 : stoi(index["s1"])); index["s1"] = to_string(s1+ ("1" == calc_val["v1"] ? 10 : -10)+ (0 > s1 ? 9 : -9)); return false; }()){ mpre("ОШИБКА получения статистики старшего сигнала", __LINE__);
								}else if([&](){ BMF_INDEX_EX.at("").at(stoi(index["id"]))["s1"] = index["s1"]; return (BMF_INDEX_EX.end() != BMF_INDEX_EX.find("s1")); }()){ mpre("ОШИБКА сохранения в справочник статистики старшего сигнала", __LINE__);
								}else if([&](){ s0 = ("" == index["s0"] ? 0 : stoi(index["s0"])); index["s0"] = to_string(s0+ ("1" == calc_val["v0"] ? 10 : -10)+ (0 > s0 ? 9 : -9)); return false; }()){ mpre("ОШИБКА получения статистики младшего сигнала", __LINE__);
								}else if([&](){ BMF_INDEX_EX.at("").at(stoi(index["id"]))["s0"] = index["s0"]; return (BMF_INDEX_EX.end() != BMF_INDEX_EX.find("s0")); }()){ mpre("ОШИБКА сохранения в справочник статистики младшего сигнала", __LINE__);
								//}else if([&](){ BMF_INDEX_EX.at("").at(stoi(index["id"])) = index; return index.empty(); }()){ mpre("ОШИБКА сохранения статистики в справочник", __LINE__);
								}else{ //mpre(index, __LINE__, "Статистика"); //mpre("s1 "+ to_string(s1)+ " s0 "+ to_string(s0), __LINE__);
								}
							return index.empty(); }()){ mpre("ОШИБКА сохранения статистики результатов", __LINE__);
						}else if([&](){ // Установка значений расчета
								if([&](){ index["calc_val_id"] = calc_val["id"]; return index.empty(); }()){ mpre("ОШИБКА установки значения морфа", __LINE__);
								}else if([&](){ index["val"] = calc["val"]; return index.empty(); }()){ mpre("ОШИБКА установки значения морфа", __LINE__);
								}else{
								}
							return false; }()){ mpre("ОШИБКА установки значений морфа", __LINE__);
						//}else if([&](){ erb_insert(BMF_INDEX_EX, index["id"], index); return index.empty(); }()){ mpre("ОШИБКА сохранения данных в справочнике", __LINE__);
						}else{ //mpre(index, __LINE__, "Расчет");// mpre(index_0, __LINE__, "Младший");
						} return index;
					});
				return false; }()){ mpre("ОШИБКА создания фукнции подсчета сигнала", __LINE__);
			}else if([&](){ // Обучение итога
					Learning = ([&](TMs itog){ //mpre("Обновление", __LINE__); // Обучение
						TMs index, _index, values; int change = 0;
						if([&](){ //mpre("Проверка морфа", __LINE__);// Выборка морфа итога
								if("" == itog["index_id"]){ //mpre("ОШИБКА пустая ссылка на морф в итоге", __LINE__);
								}else if(BMF_INDEX_EX.at("").end() == BMF_INDEX_EX.at("").find(stoi(itog["index_id"]))){ mpre("Морф не найден (возможно не выборан для итога)", __LINE__);
								}else if([&](){ index = erb(BMF_INDEX_EX, {{"id", itog["index_id"]}}); return false; }()){ mpre("ОШИБКА выборки связанного морфа", __LINE__);
								}else{ //mpre(index, __LINE__ "Указанный морф");
								}
							return false; }()){ mpre("ОШИБКА выборки морфа итога", __LINE__);
						}else if([&](){// mpre("Добавляем исходный морф", __LINE__);// mpre(itog, __LINE__, "Итог");// mpre(BMF_INDEX, __LINE__, "Список морфов");
								TMMi DANO; TMs dano, calc_balance, calc_val, _calc_val_, _calc_; string _values_, _val_;
								if(!index.empty()){// mpre("Итог "+ itog["id"]+ " первоначальный морф уже добавлен", __LINE__);
								}else if([&](){ return (0 >= BMF_DANO_EX.at("").size()); }()){ mpre("ОШИБКА список исходных данных пуст", __LINE__);
								}else if([&](){ dano = BMF_DANO_EX.at("").begin()->second; return dano.empty(); }()){ mpre("ОШИБКА выборки первого дано", __LINE__);
								}else if([&](){ calc_val = erb(BMF_CALC_VAL, {{"v1", dano["val"]}, {"v0", dano["val"]}}); return calc_val.empty(); }()){ mpre("ОШИБКА выборки значения", __LINE__);
								}else if([&](){ calc_balance = erb(BMF_CALC_BALANCE, {{"calc_val_id", calc_val["id"]}, {"val", itog["val"]}}); return calc_balance.empty(); }()){ mpre(calc_val, __LINE__, "ОШИБКА выборки подходящего значения "+ itog["val"]);
								}else if([&](){ index = {{"id", Id(BMF_INDEX_EX.at(""))}, {"clump_id", clump_id}, {"itog_values_id", itog["itog_values_id"]}, {"depth","0"}, {"dano_id", dano["id"]}, {"itog_id", itog["id"]}, {"val", itog["val"]}, {"calc_pos_id", calc_balance["calc_pos_id"]}, {"calc_val_id", calc_val["id"]}, {"index_id", ""}, {"bmf-index", ""}, {"f1", dano["id"]}, {"f0", dano["id"]}}; return index.empty(); }()){ mpre("ОШИБКА формирования свойст нового морфа", __LINE__);
								}else if([&](){ erb_insert(BMF_INDEX_EX, index["id"], index); return index.empty(); }()){ mpre("ОШИБКА добавления морфа в справочник", __LINE__);
								}else if([&](){ itog["index_id"] = index["id"]; return itog.empty(); }()){ mpre("ОШИБКА установки свойства связи итога с морфом", __LINE__);
								}else if([&](){ erb_insert(BMF_ITOG_EX, itog["id"], itog); return itog.empty(); }()){ mpre("ОШИБКА индексирования итога", __LINE__);
								}else{ //exit(mpre(BMF_INDEX_EX, __LINE__, "Добаляем исходный морф"));
								} return index.empty();
							}()){ exit(mpre("ОШИБКА создания первоначального морфа для дано", __LINE__));
						}else if([&](){ _index = Calc(index, false); return _index.empty(); }()){ mpre("ОШИБКА расчета значений морфа", __LINE__);
						}else if([&](){ // Значение итога
								if(BMF_ITOG_VALUES_EX.at("").end() == BMF_ITOG_VALUES_EX.at("").find(stoi(itog["itog_values_id"]))){ mpre("ОШИБКА значение итога не установлено", __LINE__);
								}else if([&](){ values = BMF_ITOG_VALUES_EX.at("").at(stoi(itog["itog_values_id"])); return values.empty(); }()){ mpre("ОШИБКА получения значения итога", __LINE__);
								}else{ //mpre(values, __LINE__, "Значение итога");
								}
							return false; }()){ mpre("ОШИБКА выборки значения итога", __LINE__);
						}else if([&](){ // Уведомление о результатах проверки
								TMs _calc_pos;
								if([&](){ _calc_pos = erb(BMF_CALC_POS, {{"id", _index["calc_pos_id"]}}); return _calc_pos.empty(); }()){ mpre("ОШИБКА выборки позиции морфа", __LINE__);
								}else if(itog["val"] == _index["val"]){ mpre("Расчет `"+ values["name"]+ "` "+ itog["name"]+ " ["+ itog["id"]+ "] "+ itog["val"]+ " совпадение ["+ _index["id"]+ "] "+ _index["val"], __LINE__);//+ _index["f1"]+ " "+ _calc_pos["fm"]+ " "+ _index["f0"]
								}else{ mpre("Расчет `"+ values["name"]+ "` "+ itog["name"]+ " ["+ itog["id"]+ "] "+ itog["val"]+ " отличие ["+ _index["id"]+ "] !"+ _index["val"], __LINE__);//+ _index["f1"]+ " "+ _calc_pos["fm"]+ " "+ _index["f0"]
									return false;
								} return true;
							}()){ //mpre("Обучение не требуется", __LINE__);
						}else if([&](){ // Обучение если не режим расчета
								if((3 < argc) && (0 == divider)){ //mpre("Пропускаем обучение итог "+ to_string(divider), __LINE__);
								}else if([&](){ index = CalcAll(index); return (index["val"] != _index["val"]); }()){ mpre("ОШИБКА методы расчета дали разный результат", __LINE__);
								}else if([&](){ TMs _index_ = Learn(index, {}); return (_index["val"] == _index_["val"]); }()){ mpre("ОШИБКА обучения морфа", __LINE__);
								}else{
								}
							return false; }()){ mpre("ОШИБКА обучения морфа", __LINE__);
						}else if([&](){ change = 1; return false; }()){ mpre("ОШИБКА установки изменения", __LINE__);
						}else{ //mpre("Обучение морфа "+ itog["name"], __LINE__); //system("sleep 0.5");
						} return change;
					});
				return false; }()){ mpre("ОШИБКА устанвоки функции расчета итога", __LINE__);
			}else if([&](){ // ОБучение
					//bool Learning(auto js, string clump_id){ //std::cerr << endl << __LINE__ << " ОБУЧЕНИЕ " << js.dump() << endl << endl;// mpre(BMF_INDEX, __LINE__, "Список морфов"); // mpre(endl+ "ОБУЧЕНИЕ", __LINE__);
					LearningAll = ([&](json js, int divider, string clump_id){ //mpre("ОбновлениеОбщее", __LINE__); // Обучение
						int change = 0; TMs index;
						static std::vector<std::thread> threads;
						TMMi INDEX;
						if([&](){ //for(auto itog_itr = BMF_ITOG.begin(); itog_itr != BMF_ITOG.end(); itog_itr++){// mpre("Итог", __LINE__);
							for(auto &itog_itr:BMF_ITOG_EX.at("")){
								TMs itog;
								if([&](){ itog = itog_itr.second; return itog.empty(); }()){ mpre("ОШИБКА получения итогового знака из итератора", __LINE__);
								}else if([&](){ // Пропускаем расчет
										if(3 >= argc){ //mpre("Параметр расчета итога не установлен", __LINE__);
										}else if(0 >= divider){ //mpre("Нулевой параметр итога", __LINE__);
										}else if(to_string(divider) == itog["id"]){ //mpre("Указанный итог равен текущему "+ itog["id"], __LINE__);
										}else{ return true;
										} return false;
									}()){ //mpre("Пропускаем ["+ itog["id"]+ "] != "+ to_string(divider), __LINE__);
								}else if([&](){ change += (std::async(/*std::launch::async,*/ std::bind(Learning, itog))).get(); return false; }()){ mpre("ОШИБКА асинхронного запуска", __LINE__);
								}else{// mpre("Перебор связанных морфов окончен", __LINE__);
								}
							}; return false; }()){ mpre("ОШИБКА перебора знаков для обучения", __LINE__);
						}else{// mpre("Обучение", __LINE__);
						} return change;
					});
				return false; }()){ mpre("ОШИБКА установки функции обучения", __LINE__);
			}else if([&Choice, &Calc, &values_length](){ // Выбор подходящего знака
					//TMs Choice(TMs index){ //mpre(index, __LINE__, "Выбор"); mpre(calc_amend, __LINE__, "Направление"); // Выбор подходящего по сигналам входящего значения
					Choice = ([&](TMs index){ //mpre("Выбор итога для расширения", __LINE__);
						TMs dano, _dano, _dano_, _index, index_1, index_0; string values_1, values_0, index_values, dano_values, index_val, dano_val; int _level = 0;
						/*if([&](){ dano = erb(BMF_DANO, {{"id", index["dano_id"]}}); return dano.empty(); }()){ mpre("ОШИБКА получения исходного сигнала морфа", __LINE__);
						}else if([&](){ _index_values = index["values"]; return (0 >= _index_values.length()); }()){ mpre("ОШИБКА получения истории изменения морфа", __LINE__);
						}else if(0 >= _index_values.length()){ mpre("ОШИБКА нулевой размер истории морфа", __LINE__);*/
						/*if([&](){ _index = Calc(index, true); return _index.empty(); }()){ mpre("ОШИБКА расчета морфа", __LINE__);
						}else*/ if([&](){ index_val = index["val"]; return (1 != index_val.length()); }()){ mpre(index, __LINE__, "ОШИБКА получения знака морфа");
						}else if([&](){ // Выборка нижестоящих морфов
								if([&](){ index_1 = erb(BMF_INDEX_EX, {{"id", index.at("index_id")}}); return false; }()){ mpre("Найден старший морф", __LINE__);
								}else if([&](){ index_0 = erb(BMF_INDEX_EX, {{"id", index.at("bmf-index")}}); return false; }()){ mpre("Найден младший морф", __LINE__);
								}else if(!index_1.empty() && !index_0.empty()){ mpre("ОШИБКА подбор осуществляется только если один из потомков пуск", __LINE__);
								}else{ return false;
								} return true;
							}()){ mpre("ОШИБКА получения истории", __LINE__);
						}else if([&](){ _dano_ = erb(BMF_DANO_EX, {{"id", index["dano_id"]}}); return _dano_.empty(); }()){ mpre("ОШИБКА выборки основного источника", __LINE__);
						}else if([&](){ values_0 = _dano_.at("values"); return (0 >= values_0.length()); }()){ mpre("ОШИБКА получения истории основного дано", __LINE__);
						}else if([&](){ values_1 = _dano_.at("values"); return (0 >= values_1.length()); }()){ mpre("ОШИБКА получения истории основного дано", __LINE__);
						}else if([&](){ // Получение старшего значения
								TMs _dano_1, _calc_pos;
								if(index_1.empty()){ //mpre("История старшего морфа не найдена оставляем историю основного", __LINE__);
								}else if([&](){ _dano_1 = erb(BMF_DANO_EX, {{"id", index_1.at("dano_id")}}); return _dano_1.empty(); }()){ mpre("ОШИБКА выборки дано старшего морфа", __LINE__);
								}else if([&](){ values_1 = _dano_1.at("values"); return (0 >= values_1.length()); }()){ mpre("ОШИБКА получения истории старшего морфа", __LINE__);
								}else if([&](){ _calc_pos = erb(BMF_CALC_POS, {{"id", index_1["calc_pos_id"]}}); return _calc_pos.empty(); }()){ mpre("ОШИБКА выборки позиции страшего морфа", __LINE__);
								}else if("0" == _calc_pos["v1"]){ mpre("Прямой морф не переворачиваем", __LINE__);
								}else if([&](){ values_1 = std::regex_replace(values_1, regex("1"), "2"); return (0 >= values_1.length()); }()){ mpre("ОШИБКА замены вхождений", __LINE__);
								}else if([&](){ values_1 = std::regex_replace(values_1, regex("0"), "1"); return (0 >= values_1.length()); }()){ mpre("ОШИБКА замены вхождений", __LINE__);
								}else if([&](){ values_1 = std::regex_replace(values_1, regex("2"), "0"); return (0 >= values_1.length()); }()){ mpre("ОШИБКА замены вхождений", __LINE__);
								}else{ //mpre(" Было "+ _dano_1.at("values"), __LINE__); mpre("Стало "+ values_1, __LINE__);
								} return (0 >= values_1.length());
							}()){ mpre("ОШИБКА получения старшего значения", __LINE__);
						}else if([&](){ // Получение старшего значения
								TMs _dano_0, _calc_pos;
								if(index_0.empty()){ //mpre("История старшего морфа не найдена оставляем историю основного", __LINE__);
								}else if([&](){ _dano_0 = erb(BMF_DANO_EX, {{"id", index_0.at("dano_id")}}); return _dano_0.empty(); }()){ mpre("ОШИБКА выборки дано старшего морфа", __LINE__);
								}else if([&](){ values_0 = _dano_0.at("values"); return (0 >= values_0.length()); }()){ mpre("ОШИБКА получения истории старшего морфа", __LINE__);
								}else if([&](){ _calc_pos = erb(BMF_CALC_POS, {{"id", index_0["calc_pos_id"]}}); return _calc_pos.empty(); }()){ mpre("ОШИБКА выборки позиции младшего морфа", __LINE__);
								}else if("0" == _calc_pos["v1"]){ mpre("Прямой морф не переворачиваем", __LINE__);
								}else if([&](){ values_0 = std::regex_replace(values_0, regex("1"), "2"); return (0 >= values_0.length()); }()){ mpre("ОШИБКА замены вхождений", __LINE__);
								}else if([&](){ values_0 = std::regex_replace(values_0, regex("0"), "1"); return (0 >= values_0.length()); }()){ mpre("ОШИБКА замены вхождений", __LINE__);
								}else if([&](){ values_0 = std::regex_replace(values_0, regex("2"), "0"); return (0 >= values_0.length()); }()){ mpre("ОШИБКА замены вхождений", __LINE__);
								}else{ //mpre(" Было "+ _dano_0.at("values"), __LINE__); mpre("Стало "+ values_0, __LINE__);
								} return (0 >= values_1.length());
							}()){ mpre("ОШИБКА получения старшего значения", __LINE__);
						}else if([&](){ // Расчет основной истории
								TMs _calc_val, _calc; int offset = 1; string val_1, val_0;
								do{ //mpre("Смещение "+ to_string(offset), __LINE__);
									if(values_0.length() < offset){ //mpre("ОШИБКА размер младшей истории больше смещения", __LINE__);
									}else if(values_1.length() < offset){ //mpre("ОШИБКА размер старшей истории больше смещения", __LINE__);
									}else if([&](){ val_1 = values_1.substr(values_1.length()-offset, 1); return (1 != val_1.length()); }()){ mpre("ОШИБКА получения старшего знака истории", __LINE__);
									}else if([&](){ val_0 = values_0.substr(values_0.length()-offset, 1); return (1 != val_0.length()); }()){ mpre("ОШИБКА получения младшего знака истории", __LINE__);
									}else if([&](){ _calc_val = erb(BMF_CALC_VAL, {{"v1", val_1}, {"v0", val_0}}); return _calc_val.empty(); }()){ mpre("ОШИБКА получения значения сигналов", __LINE__);
									}else if([&](){ _calc = erb(BMF_CALC, {{"calc_pos_id", index["calc_pos_id"]}, {"calc_val_id", _calc_val["id"]}}); return _calc.empty(); }()){ mpre("ОШИБКА выборки расчета морфа", __LINE__);
									}else if([&](){ index_values = _calc["val"]+ index_values; return (0 >= index_values.length()); }()){ mpre("ОШИБКА инкремента знака истории", __LINE__);
									}else{ //mpre("Старший "+ val_1+ " младший "+ val_0+ " история "+ index_values, __LINE__);
									}
								}while(++offset <= values_length);
								//mpre(index, __LINE__, "Морф");
								//mpre("Старшая история "+ values_1, __LINE__);
								//mpre("Младшая история "+ values_0, __LINE__);
								//mpre("ОШИБКА расчета морфа "+ index_values, __LINE__);
								return (0 >= index_values.length());
							}()){ mpre("ОШИБКА расчета основной истории", __LINE__);
						}else if(0 >= index_values.length()){ mpre("ОШИБКА история морфа пуста values_index", __LINE__);
						}else if([&](){ // Зеркалируем последний знак морфа
								string _val, _shift;
								if([&](){ _val = index_values.substr(index_values.length()-1, 1); return (1 != _val.length()); }()){ mpre("ОШИБКА получения последнего перевернутого сигнала", __LINE__);
								}else if([&](){ _shift = ("0" == _val ? "1" : "0"); return (1 != _shift.length()); }()){ mpre("ОШИБКА получения перевернутого последнего значения", __LINE__);
								}else if([&](){ index_values = index_values.substr(0, index_values.length()-1)+ _shift; return (0 >= index_values.length()); }()){ mpre("ОШИБКА длинна расчетного значения не совпадает с исходным", __LINE__);
								}else{ //mpre(index, __LINE__, "Зеркальный последний сигнал морфа "+ values); system("sleep 1");
								} return (0 >= index_values.length());
							}()){ mpre(index, __LINE__, "ОШИБКА переворота последнего сигнала истории морфа");
						}else if([&](){ // Совпадение длинн историй
								//std::lock_guard<std::mutex> lock(mu);
								//for_each(BMF_DANO_EX.at("").begin(), BMF_DANO_EX.at("").end(), [&](auto _dano_itr){ // Перебор всех исходников
								for(auto &_dano_itr:BMF_DANO_EX.at("")){
									TMs _dano_; string _dano_values, _dano_val; bool _dano_equal; int _min, _level_ = -1;
									if([&](){ _dano_ = _dano_itr.second; return _dano_.empty(); }()){ mpre("ОШИБКА получения исходника из итератора", __LINE__);
									}else if(_dano_["id"] == index["dano_id"]){ //mpre("Пропускаем исходник морфа", __LINE__);
									}else if([&](){ _dano_values = _dano_["values"]; return (0 >= _dano_values.length()); }()){ mpre("ОШИБКА получения истории очереного исходника", __LINE__);
									}else if(0 >= _dano_values.length()){ mpre("ОШИБКА нулевая длинна истории исходника", __LINE__);
									}else if([&](){ _dano_val = _dano_values.substr(_dano_values.length()-1, 1); return (1 != _dano_val.length()); }()){ mpre("ОШИБКА получения знака очередного исходника", __LINE__);
									}else if([&](){ _dano_equal = (_dano_val != index_val); return false; }()){ mpre("ОШИБКА нахождения эквивалентов значений исходников", __LINE__);
									}else if([&](){ _min = min(index_values.length(), _dano_values.length()); return (0 >= _min); }()){ mpre("ОШИБКА нахождения максимальной длинны истории", __LINE__);
									}else if([&](){ // Перебор значений история до момента несовпадения
											string _index_val; bool _equal;
											do{
												if([&](){ _level_ += 1; return (0 > _level_); }()){ mpre("ОШИБКА инкремента смещения", __LINE__);
												}else if(_level_ >= _min){ //mpre("Уровень больше длинны минимального значения", __LINE__);
												}else if([&](){ _index_val = index_values.substr(index_values.length()-_level_-1, 1); return (1 != _index_val.length()); }()){ mpre("ОШИБКА получения знака морфа", __LINE__);
												}else if([&](){ _dano_val = _dano_values.substr(_dano_values.length()-_level_-1, 1); return (1 != _dano_val.length()); }()){ mpre("ОШИБКА получения знака исходника", __LINE__);
												}else if([&](){ _equal = (_index_val == _dano_val); return false; }()){ mpre("ОШИБКА расчета эквивалентности значений", __LINE__);
												}else{ //mpre("Смещение "+ to_string(_level_)+ " "+ _index_val+ ":"+ _dano_val+ " _dano_equal="+ (_dano_equal ? "1" : "0")+ " _equal="+ (_equal ? "1" : "0"), __LINE__);
												}
											}while((_min > _level_) && (_dano_equal == _equal)); return (0 > _level_);
										}()){ mpre("ОШИБКА получения уровня совпадения", __LINE__);
									}else if(_level > _level_){ //mpre("Уровень "+ to_string(_level_)+ " меньше или равен предыдущему "+ to_string(_level), __LINE__);
									}else if([&](){ _level = _level_; return (0 > _level); }()){ mpre("ОШИБКА сохранения максимального уровня", __LINE__);
									}else if([&](){ _dano = _dano_; return _dano.empty(); }()){ mpre("ОШИБКА установки подходящего исходника", __LINE__);
									}else{ //mpre("Уровень "+ to_string(_level_)+ " "+ _dano_values+ " min="+ to_string(_min), __LINE__);
									}
								}; return _dano.empty();
							}()){ mpre("ОШИБКА нахождения длинн совпадения история", __LINE__);
						}else if(_dano.empty()){ mpre("ОШИБКА нахождения подходящего результата", __LINE__);
						}else{ mpre("Максимальный уровень "+ to_string(_level)+ " номер источника "+ _dano["id"], __LINE__);
							//mpre(BMF_DANO_EX.at(""), __LINE__, "Дано");
							//mpre("История "+ index_values, __LINE__);
							//mpre(_dano, __LINE__, "Подходящий морф");
						} return _dano;
					}); return false;
				}()){ mpre("ОШИБКА установки функции выбора знака", __LINE__);
			}else if([&](){ // Проверка работы функций конвертации
					string bin, _bin; float dec, _dec, rand;
					if([&]{ std::random_device rd; std::mt19937 g(rd()); rand = (float(rd()%2000000)-1000000)/1000; return (0 == rand); }()){ mpre("ОШИБКА получения случайного значения", __LINE__);
					}else if([&](){ bin = Dec2bin(dec = rand); return (dec != (_dec = Bin2dec(bin))); }()){ mpre("ОШИБКА пересчета "+ to_string(rand)+ " "+ to_string(dec)+ " >> "+ bin+ ">>"+ to_string(_dec) , __LINE__);
					}else{ //mpre("Сравнение 206.6 "+ Dec2bin(206.6)+ " 206.7 "+ Dec2bin(206.7)+ " 206.8 "+ Dec2bin(206.8), __LINE__);
						mpre("Тесты перевода оснований "+ to_string(rand)+ " пройдены корректно", __LINE__);
						return false;
					} return true;
				}()){ mpre("ОШИБКА проверки функиции перевода из двоичной в десятичную", __LINE__);
			}else{ mpre("Функции успешно установлены", __LINE__);
				return false;
			}					
		return true; }()){ mpre("ОШИБКА установки списка функций", __LINE__);
	//}else if([&](){ data_size = sizeof(data)/sizeof(*data); return (0 >= data_size); }()){ mpre("ОШИБКА расчета массива данных", __LINE__);
	}else if([&](){ // Подключение базы данных
			if([&](){ // Создание файла БД если его нет
					if([&](){ dbname = clump_id; return (0 >= dbname.length()); }()){ mpre("ОШИБКА установки имени файла БД "+ clump_id, __LINE__);
					}else if(access(dbname.c_str(), F_OK) == 0){ //mpre("Файл БД уже создан "+ clump_id, __LINE__); system("sleep 10");
					}else if(system(("touch "+ dbname).c_str())){ mpre("ОШИБКА создания файла БД", __LINE__); system("pwd;");
					}else if(system(("chmod o+w "+ dbname).c_str())){ mpre("ОШИБКА назначения доступа файла БД", __LINE__);
					//}else if(system(("chgrp www-data "+ dbname).c_str())){ mpre("ОШИБКА назначения владельца файлу БД", __LINE__);
					}else{ mpre("Создание файла БД "+ dbname, __LINE__);
					}
				return false; }()){ mpre("ОШИБКА создания файла БД", __LINE__);
			}else if([&](){ // Установка соединения с БД
					string attach_database;
					if(SQLITE_OK != sqlite3_open(dbname.c_str(), &db)){ std::cerr << __LINE__ << " ОШИБКА открытия базы данных << " << dbname << endl;
					}else if([&](){ attach_database = "../../../../.htdb"; return (0 >= attach_database.length()); }()){ mpre("ОШИБКА имя доп БД не задано", __LINE__);
					}else if(access(attach_database.c_str(), F_OK) == -1){ mpre("Файл БД не найден "+ attach_database, __LINE__);
					}else if([&](){ exec("ATTACH DATABASE '../../../../.htdb' AS bmf"); return false; }()){ mpre("ОШИБКА Подключение основной базы с данными", __LINE__);
					}else{ std::cerr << __LINE__ << " Подключение родительской БД " << attach_database << endl;
					} return false;
				}()){ mpre("ОШИБКА подключения баз данных", __LINE__);
			}else if([&](){ // Получение пути до файла БД
					int pos;
					if([&](){ pos = dbname.rfind("/"); return (0 > pos); }()){ mpre("Слешей в пути до скопления не найдено", __LINE__);
					}else if([&](){ clump_id = dbname.substr(pos+1, dbname.length()); return (0 >= clump_id.length()); }()){ mpre("ОШИБКА сокращения пути до файла", __LINE__);
					}else{ mpre("Путь до БД сокращен "+ clump_id, __LINE__);
					} return (0 >= clump_id.length());
				}()){ mpre("ОШИБКА получения скопления", __LINE__);
			}else if([&](){ // Получение текущего скопления
					string sql;
					if("0" == clump_id){ mpre("Скопление не указано", __LINE__);
					}else if([&](){ DATABASES = Tab("PRAGMA database_list"); return DATABASES.empty(); }()){ mpre("ОШИБКА получения списка подключенных таблиц", __LINE__);
					}else if([&](){ databases = erb(DATABASES, {{"name", "bmf"}}); return databases.empty(); }()){ mpre("Общая таблица не найдена", __LINE__);
					}else if([&](){ sql = "SELECT 0 AS id, COUNT(*) AS cnt FROM bmf.sqlite_master WHERE type='table' AND name='mp_bmf_clump'"; return (0 >= sql.length()); }()){ mpre("ОШИБКА составления запроса проверки таблицы скоплений", __LINE__);
					}else if([&](){ TMMi CLUMP = Tab(sql); return ("0" == CLUMP.find(0)->second.find("cnt")->second); }()){ mpre("Таблица со скоплениями отсутсвует в базе", __LINE__);
					}else if([&](){ sql = "SELECT * FROM mp_bmf_clump WHERE id='"+ clump_id+ "'"; return false; }()){ mpre("Задайте номер скопления", __LINE__);
					}else if([&](){ BMF_CLUMP = Tab(sql); bmf_clump = (0 >= BMF_CLUMP.size() ? TMs({}) : BMF_CLUMP.begin()->second); return bmf_clump.empty(); }()){ mpre("Информация о скоплении не установлена "+ sql, __LINE__);
					}else if([&](){ bmf_clump = fk("mp_bmf_clump", {{"id", clump_id}}, {}, {{"hide", "0"}}); return bmf_clump.empty(); }()){ mpre("ОШИБКА обновления видимости скопления", __LINE__);
					}else if([&](){ exec("UPDATE mp_bmf_clump SET hide=1 WHERE id<>"+ clump_id); return false; }()){ mpre("ОШИБКА скрытия не активных скопления", __LINE__);
					}else{ mpre("Скопление: `"+ bmf_clump["name"]+ "`", __LINE__);
					} return (0 >= clump_id.length());
				}()){ mpre("ОШИБКА получения скопления", __LINE__);
			}else if([&](){ // Добавление таблиц в БД если они не созданы
					string path;
					if([&](){ exec("CREATE TABLE IF NOT EXISTS main.mp_bmf_index (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,`clump_id` INTEGER,`itog_values_id` INTEGER, `depth` INTEGER,`dano_id` INTEGER,`itog_id` INTEGER,`val` INTEGER,`calc_pos_id` INTEGER,`calc_val_id` INTEGER,`index_id` INTEGER, `bmf-index` INTEGER, `v1` INTEGER, `v0` INTEGER, `s1` INTEGER, `s0` INTEGER, `f1` TEXT, `f0` TEXT)"); return false; }()){ mpre("ОШИБКА создания таблицы морфов", __LINE__);
					}else if([&](){ exec("CREATE TABLE IF NOT EXISTS main.mp_bmf_dano (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,`time` INTEGER,`uid` INTEGER,`clump_id` INTEGER,`dano_values_id` INTEGER,`name` TEXT,`val` INTEGER,`values` TEXT)"); return false; }()){ mpre("ОШИБКА создания таблицы морфов", __LINE__);
					}else if([&](){ exec("CREATE TABLE IF NOT EXISTS main.mp_bmf_itog (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,`time` INTEGER,`uid` INTEGER,`clump_id` INTEGER,`index_id` INTEGER,`itog_values_id` INTEGER,`name` TEXT,`val` INTEGER,`values` TEXT,`shift` INTEGER,`shifting` TEXT)"); return false; }()){ mpre("ОШИБКА создания таблицы морфов", __LINE__);
					}else if([&](){ exec("CREATE TABLE IF NOT EXISTS main.mp_bmf_dano_values (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,`time` INTEGER ,`uid` INTEGER ,`clump_id` INTEGER ,`index_type_id` INTEGER ,`name` TEXT ,`value` TEXT ,`old` INTEGER ,`shift` INTEGER ,`dano_values_option_id` INTEGER ,`bin` TEXT)"); return false; }()){ mpre("ОШИБКА создания значений дано", __LINE__);
					}else if([&](){ exec("CREATE TABLE IF NOT EXISTS main.mp_bmf_itog_values (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,`time` INTEGER,`index_type_id` INTEGER,`clump_id` INTEGER,`name` TEXT,`value` INTEGER,`itog_values_option_id` INTEGER,`bin` INTEGER)"); return false; }()){ mpre("ОШИБКА создания значения итога", __LINE__);
					}else if([&](){ exec("CREATE TABLE IF NOT EXISTS main.mp_bmf_itog_titles (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,`time` INTEGER, `clump_id` INTEGER, `itog_values_id` INTEGER, `value` INTEGER, `name` INTEGER, `text` TEXT)"); return false; }()){ mpre("ОШИБКА создания значения итога", __LINE__);
					}else if([&](){ exec("CREATE TABLE IF NOT EXISTS main.mp_bmf_dano_titles (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,`time` INTEGER, `clump_id` INTEGER, `dano_values_id` INTEGER, `value` INTEGER, `name` INTEGER, `text` TEXT)"); return false; }()){ mpre("ОШИБКА создания значения итога", __LINE__);
					//}else if([&](){ path = "/var/www/192.168.1.6/.htdb"; return (0 >= path.length()); }()){ mpre("ОШИБКА установки пути до файла БД", __LINE__);
					//}else if(exec("ATTACH DATABASE `"+ path+"` AS clump")){ mpre("ОШИБКА при подключении базы скопления "+ path, __LINE__);
					}else{ return false;
					} return true;
				}()){ mpre("ОШИБКА подключения БД созвездия и создания таблиц", __LINE__);
			}else if([&](){ // Очищение списка морфов
					if(!index_clean){ mpre("Сохраняем результат предыдущих расчетов", __LINE__);
					}else if([&](){ exec("DELETE FROM main.mp_bmf_index WHERE 1"); return false; }()){ mpre("ОШИБКА выполнения запроса на очистку морфорв", __LINE__);
					}else if([&](){ BMF_INDEX_EX[""] = Tab("SELECT * FROM `mp_bmf_index` -- WHERE `clump_id`='"+ clump_id+ "'"); return false; }()){ mpre("ОШИБКА выборки списка морфов из базы", __LINE__);
					}else if(0 < BMF_INDEX_EX.size()){ mpre("ОШИБКА после удаления всех морфов в базе все еще остаются данные", __LINE__);
					}else{ mpre("Очистка списка морфов index_clean="+ to_string(index_clean), __LINE__); system("sleep 1");
					} return false;
				}()){ mpre("ОШИБКА выборки списка морфов", __LINE__);
			}else if([&](){ BMF_CALC_POS = Dataset(BMF_CALC_POS, "BMF_CALC_POS", "mp_bmf_calc_pos", "Позиции"); return BMF_CALC_POS.empty(); }()){ mpre("ОШИБКА выборки позиций расчетов", __LINE__);
			}else if([&](){ BMF_CALC_VAL = Dataset(BMF_CALC_VAL, "BMF_CALC_VAL", "mp_bmf_calc_val", "Знаки"); return BMF_CALC_VAL.empty(); }()){ mpre("ОШИБКА выборки значений расчетов", __LINE__);
			}else if([&](){ BMF_INDEX_TYPE = Dataset(BMF_INDEX_TYPE, "BMF_INDEX_TYPE", "mp_bmf_index_type", "Тип значения"); return BMF_INDEX_TYPE.empty(); }()){ mpre("ОШИБКА выборки типа значений", __LINE__);
			}else if([&](){ TMMi DANO = Tab("SELECT * FROM `mp_bmf_dano` -- WHERE `clump_id`='"+ clump_id+ "'"); BMF_DANO_EX[""] = DANO; return false; }()){ mpre("ОШИБКА получения дано скопления", __LINE__);
			}else if([&](){ exec("CREATE INDEX IF NOT EXISTS `bmf-dano_clump_id_dano_values_id` ON mp_bmf_dano(clump_id,dano_values_id);"); return false; }()){ mpre("ОШИБКА создания индекса", __LINE__);
			}else if([&](){ TMMi DANO_VALUES = Tab("SELECT * FROM `mp_bmf_dano_values` -- WHERE `clump_id`='"+ clump_id+ "'"); BMF_DANO_VALUES_EX[""] = DANO_VALUES; return false; }()){ mpre("ОШИБКА получения дано значений", __LINE__);
			}else if([&](){ TMMi DANO_TITLES = Tab("SELECT * FROM `mp_bmf_dano_titles` -- WHERE `clump_id`='"+ clump_id+ "'"); BMF_DANO_TITLES_EX[""] = DANO_TITLES; return false; }()){ mpre("ОШИБКА получения дано справочника", __LINE__);
			}else if([&](){ exec("CREATE INDEX IF NOT EXISTS `bmf-itog_clump_id_itog_values_id` ON mp_bmf_itog(clump_id,itog_values_id);"); return false; }()){ mpre("ОШИБКА создания индекса", __LINE__);
			}else if([&](){ TMMi ITOG_VALUES = Tab("SELECT * FROM `mp_bmf_itog_values` -- WHERE `clump_id`='"+ clump_id+ "'"); BMF_ITOG_VALUES_EX[""] = ITOG_VALUES; return false; }()){ mpre("ОШИБКА получения итогов значений", __LINE__);
			}else if([&](){ TMMi ITOG_TITLES = Tab("SELECT * FROM `mp_bmf_itog_titles` -- WHERE `clump_id`='"+ clump_id+ "'"); BMF_ITOG_TITLES_EX[""] = ITOG_TITLES; return false; }()){ mpre("ОШИБКА получения итогов справочника", __LINE__);
			}else if([&](){ TMMi ITOG = Tab("SELECT * FROM `mp_bmf_itog` -- WHERE `clump_id`='"+ clump_id+ "'"); BMF_ITOG_EX[""] = ITOG; return false; }()){ mpre("ОШИБКА получения итогов скопления", __LINE__);
			}else if([&](){ // Получение списка итогов
					if(3 > argc){ mpre("Итог не указан", __LINE__);
					}else if(0 >= divider){ mpre("Режим расчета указан итог "+ to_string(divider), __LINE__);
					}else if(BMF_ITOG_EX.at("").end() != BMF_ITOG_EX.at("").find(divider)){ mpre("Указан итог для расчета "+ to_string(divider), __LINE__);
					}else{ mpre("ОШИБКА указанный итог не найден "+ to_string(divider), __LINE__); exit(0);
					}
				return false; }()){ mpre("ОШИБКА получения списка итогов", __LINE__);
			//}else if(true){ mpre(ITOG, __LINE__, "Итоги"); mpre("ОШИБКА выборки итога", __LINE__);
			//}else if([&](){ BMF_INDEX_EX[""] = Tab("SELECT * FROM `mp_bmf_index` -- WHERE `clump_id`='"+ clump_id+ "'"); return false; }()){ mpre("ОШИБКА выборки списка морфов из базы", __LINE__);
			}else if([&](){ // Выборка морфов
					string sql;
					if([&](){ sql = "SELECT * FROM `mp_bmf_index` WHERE "+ (0 >= divider ? "1" : "itog_id='"+ to_string(divider)+ "'"); return (0 >= sql.length()); }()){ mpre("ОШИБКА составления запроса на выборку морфов", __LINE__);
					}else if([&](){ BMF_INDEX_EX[""] = Tab(sql); return false; }()){ mpre("ОШИБКА выборки списка морфов из базы", __LINE__);
					}else{ //mpre(sql, __LINE__); mpre(BMF_INDEX_EX, __LINE__, "Список морфов");
					}
				return false; }()){ mpre("ОШИБКА выборки списка морфов", __LINE__);
			//}else if(true){ mpre(BMF_INDEX_EX.at(""), __LINE__, "Список морфов"); exit(0);
			}else{ //mpre(bmf_clump, __LINE__, "Скопление");
			}
		return false; }()){ mpre("ОШИБКА подключения базы данных", __LINE__);
	}else if([&](){ // Получение текстовой строки из консоли
			string in_string;
			if(1 >= argc){ mpre("Номер скопления не задан", __LINE__);
			}else if([&](){ string str = ""; while(std::cin >> str){ in_string += str; }; return (0 >= in_string.length()); }()){ mpre("ОШИБКА входящий параметр не задан", __LINE__);
			}else if([&](){ in = json::parse(in_string); return false; }()){ mpre("ОШИБКА Входящий параметр с обучающими данными пуст", __LINE__);
			}else{ //mpre("Входные данные "+ in.dump(), __LINE__);
			} return false;
		}()){ mpre("ОШИБКА получения данных", __LINE__);
	}else if([&](){ // Многократное дублирование запуска
			string cmd; int _change;
			if(0 >= system_count){ exit(mpre("Множественный перезапуск отключен system_count="+ to_string(system_count), __LINE__));
			}else if(argc > 1){ mpre("Пропускаем множественный запуск", __LINE__);
			}else if([&](){ for(int i = 0; i < system_count; i++){ // Множественный запуск скрипта
					string cmd; int response;
					if([&](){ cmd = "cd modules/bmf/sh/; php "+ ("9" == clump_id ? "iris.php" : "clump.php "+ clump_id); return (0 >= cmd.length()); }()){ mpre("ОШИБКА установки строки запуска php кода", __LINE__);
					}else if([&](){ mpre("Запуск скрипта "+ cmd, __LINE__); return false; }()){ mpre("ОШИБКА отображения команды запуска скрипта", __LINE__);
					}else if([&](){ response = system(cmd.c_str()); return false; }()){ mpre("ОШИБКА запуска скрипта на исполнение", __LINE__);
					}else if(0 != response){ exit(mpre("Прерывание исполнения скрипта "+ cmd, __LINE__));
					}else{ mpre("Значение возврата `"+ cmd+ "` равно "+ to_string(response)+ "\n", __LINE__);
					}
				} return true; }()){ exit(mpre("Завершение перезапуска php скрипта", __LINE__));
			}else{ exit(mpre("Множественный запуск", __LINE__));
			} return false;
		}()){ mpre("ОШИБКА множественного запуска", __LINE__);
	}else if([&](){ // Непосредственно расчет
			Do = ([&](int divider, int remainder){ // Цикл повторений расчета
				int pips_change = 0, pips_sum = 0; float _perc, _pips_perc;
				do{ // Расчет и обучение входящих параметров
					if([&](){ loop += 1; change = pips_sum = pips_change = 0; return false; }()){ mpre("ОШИБКА скидывания флага изменений", __LINE__);
					}else if([&](){ std::random_device rd; std::mt19937 g(rd()); shuffle(in.begin(), in.end(), g); std::cerr << endl << __LINE__ << " ПЕРЕМЕШИВАНИЕ " << loop << endl; return in.empty(); }()){ mpre("ОШИБКА перемешивания массива входящих значений", __LINE__);
					}else if([&](){ // Расчет каждого из входных значений
						TMMi _BMF_INDEX; TMs _index, dano, itog; int count = 0;
						for(auto &js:in){ // Расчет исходных данных
							if([&](){ count += 1; return (0 >= count); }()){ mpre("ОШИБКА увеличения примера", __LINE__);
							}else if([&](){ pips_sum += BMF_ITOG_EX.at("").size(); return false; }()){ mpre("ОШИБКА расчета количества итогов", __LINE__);
							}else if([&](){ TMs _dano = js["dano"]; dano = _dano; return dano.empty(); }()){ mpre("ОШИБКА получения входных знаков", __LINE__);
							}else if([&](){ _perc = (0 >= in.size() ? : float(in.size()-change)*100.0/in.size()); return false; }()){ mpre("ОШИБКА расчета процента", __LINE__);
							}else if([&](){ _pips_perc = (0 >= pips_sum ? 0 : float(pips_sum-pips_change)*100.0/pips_sum); return false; }()){ mpre("ОШИБКА расчета процента совпадения сигнала", __LINE__);
							}else if([&](){ std::cerr << endl << __LINE__ << " РАСЧЕТ " << js["dano"].dump() << endl;  return false; }()){ mpre("ОШИБКА отображения информации", __LINE__);
							}else if([&](){ std::cerr << endl << __LINE__ << " Время: " << (time(0)-timestamp) << " Выборка: " << loop << "/" << in.size() << "/" << count << " Морфы: " << BMF_INDEX_EX.at("").size() << "/" << change << " Процент: " << perc << "/" << _perc << "% Итоги: " << BMF_ITOG_EX.at("").size() << "/" << pips_sum << "/" << pips_change << " Процент: " << pips_perc << "/" << _pips_perc << "%" << endl; return false; }()){ mpre("ОШИБКА вывода информации о данных", __LINE__);
							}else if([&](){ Values(dano, "dano", BMF_DANO_VALUES_EX, clump_id); return BMF_DANO_VALUES_EX.empty(); }()){ mpre("ОШИБКА установки входящих значений", __LINE__);
							//}else if([&](){ mpre(BMF_DANO_EX.at(""), __LINE__, "Знаки dano"); return false; }()){ mpre("ОШИБКА уведомления", __LINE__);
							}else if([&](){ Vals(dano, "dano", BMF_DANO_EX, BMF_DANO_VALUES_EX, BMF_DANO_TITLES_EX, clump_id); return BMF_DANO_EX.empty(); }()){ mpre("ОШИБКА установки входящих значений", __LINE__);
							//}else if([&](){ mpre(BMF_DANO_EX.at(""), __LINE__, "Знаки dano"); return false; }()){ mpre("ОШИБКА уведомления", __LINE__);
							}else if(js["itog"].empty()){ std::cerr << __LINE__ << ".ОБУЧЕНИЕ не задано" << endl;
							}else if([&](){ std::cerr << endl << __LINE__ << " ОБУЧЕНИЕ " << js["itog"].dump() << endl << endl; return false; }()){ mpre("ОШИБКА вывода уведомления", __LINE__);
							}else if([&](){ TMs _itog = js["itog"]; itog = _itog; return dano.empty(); }()){ mpre("ОШИБКА получения входных знаков", __LINE__);
							//}else if([&](){ mpre(BMF_ITOG_EX.at(""), __LINE__, "Знаки itog"); return false; }()){ mpre("ОШИБКА уведомления", __LINE__);
							}else if([&](){ Values(itog, "itog", BMF_ITOG_VALUES_EX, clump_id); return BMF_ITOG_VALUES_EX.empty(); }()){ mpre("ОШИБКА установки входящих значений", __LINE__);
							}else if([&](){ Vals(itog, "itog", BMF_ITOG_EX, BMF_ITOG_VALUES_EX, BMF_ITOG_TITLES_EX, clump_id); return BMF_ITOG_EX.empty(); }()){ mpre("ОШИБКА установки входящих значений", __LINE__);
							}else if([&](){ int _change; pips_change += _change = LearningAll(js, divider, clump_id); return (0 == _change); }()){ mpre("Полное совпадение", __LINE__);
							}else if([&](){ return (0 >= ++change); }()){ mpre("ОШИБКА установки флага изменения", __LINE__);
							}else{ //mpre(BMF_ITOG_VALUES_EX, __LINE__, "Список итогов");
							}
						}; return false; }()){ mpre("ОШИБКА перебора перетасованных значений входящих данных", __LINE__);
					}else if((0 < size_max) && (size_max < BMF_INDEX_EX.at("").size())){ mpre("Максимальное количество морфов "+ to_string(BMF_INDEX_EX.at("").size())+ " больше допустимого значения size_max = "+ to_string(size_max)+ ". Расчет прерван "+ to_string(change), __LINE__); exit(EXIT_FAILURE /* 256 */);
					}else if([&](){ perc = float(in.size()-change)*100.0/in.size(); return false; }()){ mpre("ОШИБКА расчета процента", __LINE__);
					}else if([&](){ pips_perc = float(pips_sum-pips_change)*100.0/pips_sum; return false; }()){ mpre("ОШИБКА расчета процента совпадения сигнала", __LINE__);
					}else{ //std::cerr << endl << __LINE__ << " ЦИКЛ ОБУЧЕНИЯ " << to_string(change) << " из " << to_string(in.size()) << " Процент " << perc << "%";
						//std::cerr << " Итогов " << to_string(pips_change) << " Изменений сигналов " << to_string(pips_change) << " Процент " << to_string(pips_perc) << endl;
					}
				}while(change != 0 && ((0 == loop_max) || (loop < loop_max)));
			return false; });
		return false; }()){ mpre("ОШИБКА создания функции непосредственно расчета", __LINE__);
	}else if([&](){ //std::cerr << __LINE__ << " Входящая строка: " << in_string << endl;
			int remainder;
			//if([&](){ divider = (argc > 3 ? stoi(argv[3]) : 0); return (0 > divider); }()){ mpre("ОШИБКА получения номера итога", __LINE__);
			if([&](){ remainder = 1; return false; }()){ mpre("ОШИБКА расчета остатка", __LINE__);
			}else{ //mpre("Запуск расета с делителем "+ to_string(divider)+ " повторения "+ to_string(remainder), __LINE__);
				Do(divider, remainder);
			}
		return false; }()){ mpre("ОШИБКА перебора всех входных значений", __LINE__);
	}else if([&](){ for(auto &itog_values_itr:BMF_ITOG_VALUES_EX.at("")){ //for_each(BMF_ITOG_VALUES_EX.at("").begin(), BMF_ITOG_VALUES_EX.at("").end(), [&](auto &itog_values_itr){ // Расчет значений
			TMs itog_values; TMMi ITOG, INDEX; string bin = ""; double dec;
			if(3 < argc){ mpre("Не расчитываем результат итог "+ to_string(divider), __LINE__);
			}else if(BMF_INDEX_EX.at("").empty()){ mpre("Установка первоначальных морфов", __LINE__);
			}else if([&](){ itog_values = itog_values_itr.second; return itog_values.empty(); }()){ mpre("ОШИБКА получения значения", __LINE__);
			}else if([&](){ ITOG = rb(BMF_ITOG_EX, {{"itog_values_id", itog_values.at("id")}}); return ITOG.empty(); }()){ mpre("ОШИБКА получения списка знаков значения", __LINE__); //mpre(itog_values, __LINE__, "Значение"); mpre(BMF_ITOG_EX.at(""), __LINE__, "ОШИБКА получения списка знаков значения");
			}else if([&](){ for(auto &itog_itr:ITOG){ //for_each(ITOG.begin(), ITOG.end(), [&](auto itog_itr){ // Сортировка списка
					TMs itog, index; int pos, os;
					if([&](){ itog = itog_itr.second; return itog.empty(); }()){ mpre("ОШИБКА получения итога", __LINE__);
					}else if(BMF_INDEX_EX.at("").end() == BMF_INDEX_EX.at("").find(stoi(itog["index_id"]))){ mpre("Морф итога не указан", __LINE__);
					}else if([&](){ index = erb(BMF_INDEX_EX, {{"id", itog.at("index_id")}}); return index.empty(); }()){ //mpre("ОШИБКА получения морфа итога", __LINE__);
					}else if([&](){ index = Calc(index, false); return index.empty(); }()){ mpre("ОШИБКА расчета значения морфа", __LINE__);
					}else if([&](){ INDEX.insert(make_pair(stoi(itog.at("name")), index)); return INDEX.empty(); }()){ mpre("ОШИБКА добавления итогов к сортированному списку", __LINE__);
					}else{ //mpre(itog, __LINE__, "Итог");
					}
				}; return ITOG.empty(); }()){ mpre("ОШИБКА получения сортированного списка значений", __LINE__);
			}else if([&](){ for(auto &index_itr:INDEX){ //for_each(INDEX.begin(), INDEX.end(), [&](auto index_itr){ // Установка знаков в значение
					TMs index; int pos, os; string val, _bin;
					if([&](){ index = index_itr.second; return index.empty(); }()){ mpre("ОШИБКА получения индекса", __LINE__);
					}else if([&](){ pos = index_itr.first; return false; }()){ mpre("ОШИБКА получения позиции знака", __LINE__);
					}else if([&](){ os = INDEX.rbegin()->first-pos; return (0 > os); }()){ mpre("ОШИБКА получения смещения (ноль и больше)", __LINE__);
					}else if([&](){ bin += (os >= bin.length() ? std::string(os-bin.length()+1, '-') : ""); return (os >= bin.length()); }()){ mpre("ОШИБКА увеличения длинны строки до нужного размера", __LINE__);
					}else if([&](){ val = (0 == pos ? "." : index["val"]); return (0 >= val.length()); }()){ mpre("ОШИБКА получения символа знака", __LINE__);
					}else if([&](){ _bin = bin; bin = bin.substr(0, os)+ val+ bin.substr(os+1, bin.length()); return (0 >= bin.length()); }()){ mpre("ОШИБКА установки символа знака", __LINE__);
					}else{ //mpre("Расчеты позиции и смещения pos="+ to_string(pos)+ " os="+ to_string(os)+ " length="+ to_string(bin.length())+ " val="+ val, __LINE__);
						//mpre("Изменения "+ _bin+ " >> "+ bin, __LINE__);
					}
				}; return INDEX.empty(); }()){ mpre("Морфы значения не установлены `"+ itog_values["name"]+ "`", __LINE__);
			}else if([&](){ bin = ((INDEX.find(0) != INDEX.end()) && (INDEX.at(0).at("val") == "1") ? "-" : "")+ bin; return (0 >= bin.length()); }()){ mpre("ОШИБКА установки символа отрицания", __LINE__);
			}else if([&](){ dec = Bin2dec(bin); return false; }()){ mpre("ОШИБКА конвертации двоичной в десятичную систему", __LINE__);
			}else if([&](){ dec = Bin2dec(bin); return false; }()){ mpre("ОШИБКА перевода двоичной строки в десятичное число", __LINE__);
			}else if([&](){ itog_values["value"] = to_string(dec); return itog_values.empty(); }()){ mpre("ОШИБКА получения конвертируемого значения", __LINE__);
			}else if([&](){ itog_values_itr.second = itog_values; return itog_values_itr.second.empty(); }()){ mpre("ОШИБКА сохранения результатов расчета в справочник", __LINE__);
			}else{ //mpre("Расчетное значение `"+ itog_values["name"]+ "` "+ to_string(dec)+ " ("+ bin+ ")", __LINE__);
				//mpre(BMF_ITOG_EX, __LINE__, "Итоги");
			}
		}; return false; }()){ mpre("Ошибка отображение результата расчета", __LINE__);
	}else if([&](){ std::cerr << endl;
			Save = ([&](string table, TMMi ROW){ // Расчет значения формулы
				TMs index; int count_insert = 0, count_update = 0, count_delete = 0; string sql;
				for(auto index_itr = ROW.begin(); index_itr != ROW.end(); index_itr++){ // Сохранение данных справочника
					TMs index, _index; int id, index_id;
					if([&](){ index = index_itr->second; return index.empty(); }()){ mpre("ОШИБКА выборки значения итератора", __LINE__);
					}else if(index.empty()){ mpre("ОШИБКА пустой морф в справочнике", __LINE__);
					}else if([&](){ index_id = index_itr->first; return (0 == index_id); }()){ mpre("ОШИБКА получения ключа строки", __LINE__);
					}else if([&](){ // Обновление
							if("0" != index["id"]){ //mpre("Не удаляем идентификатор не пуст", __LINE__);
							}else if(0 > index_id){ //mpre("Морф еще не сохранен в таблицу", __LINE__);
							}else if([&](){ exec("DELETE FROM `"+ table+ "` WHERE id="+ to_string(index_id)); return false; }()){ mpre("ОШИБКА удаления морфа", __LINE__);
							}else{ //mpre(index, __LINE__, "Удаление морфа");
								count_delete++;
							} return false;
						}()){ mpre("ОШИБКА удаления морфа", __LINE__);
					}else if([&](){ // Обновление
							if(0 > index_id){ //mpre("Не обновляем вновь добавленный "+ to_string(id), __LINE__);
							}else if("0" == index["id"]){ //mpre("Не обновляем удаленный морф", __LINE__);
							}else if([&](){ index = fk(table, {{"id", index["id"]}}, {}, index); return index.empty(); }()){ mpre(index, __LINE__, "ОШИБКА обновления морфа "+ to_string(id));
							}else{ //mpre(index, __LINE__, "Обновление морфа");
								count_update++;
							} return false;
						}()){ mpre("ОШИБКА обновления значения морфа", __LINE__);
					}else if([&](){ // Добавление
							if(0 < index_id){ //mpre("Не добавляем морф (создан ранее)", __LINE__);
							}else if("0" == index["id"]){ //mpre("Не обновляем удаленный морф", __LINE__);
							}else if([&](){ index = fk(table, {}, index, {}); return index.empty(); }()){ mpre("ОШИБКА сохранения нового значения в базу", __LINE__);
							}else{ //mpre(index, __LINE__, "Добавление нового морфа "+ to_string(index_id));
								count_insert++;
							} return false;
						}()){ mpre("ОШИБКА добавления нового морфа", __LINE__);
					}else if([&](){ ROW.find(index_id)->second = index; return index.empty(); }()){ mpre(index, __LINE__, "Сохраняем в справочник");
					}else{ //exit(mpre(ROW, __LINE__, "Обновление "+ to_string(index_id)));
					}
				} mpre("Данные таблицы `"+ table+ "` удалений: "+ to_string(count_delete)+ " обновлений: "+ to_string(count_update)+ " добавлений: "+ to_string(count_insert), __LINE__);
			return ROW; });
		return false; }()){ mpre("ОШИБКС создания функции сохранения в базу данных", __LINE__);
	}else if([&](){ // Вывод итоговых значений
			nlohmann::json j;
			for(auto &itog_values_itr:BMF_ITOG_VALUES_EX.at("")){
				TMMi TITLES; TMs itog_values, itog_titles; string name, value; 
				if([&](){ itog_values = itog_values_itr.second; return itog_values.empty(); }()){ mpre("ОШИБКА получения значения итога", __LINE__);
				}else if([&](){ name = itog_values.at("name"); return (0 >= name.length()); }()){ mpre("ОШИБКА слишком короткое имя параметра", __LINE__);
				}else if([&](){ value = itog_values.at("value"); return (0 > value.length()); }()){ mpre("ОШИБКА слишком короткое значение параметра", __LINE__);
				}else if([&](){ char dig[100]; sprintf(dig,"%g", stod(value)); j[name] = value = dig; return false; }()){ mpre("ОШИБКА присвоения значения массиву", __LINE__);
				//}else if([&](){ j[name] = value; return false; }()){ mpre("ОШИБКА установки значения", __LINE__);
				}else if([&](){ int pos = value.rfind("."); return (0 < pos); }()){ //mpre("Итоговое значение `"+ itog_values["name"]+ "` с дробной частью `"+ value+ "` На заголовки не проверяем", __LINE__);
				}else if([&](){ TITLES = rb(BMF_ITOG_TITLES_EX, {{"itog_values_id", itog_values["id"]}}); return TITLES.empty(); }()){ //mpre("Загловки у значения `"+ itog_values["name"]+ "` не найдены "+ itog_values["id"], __LINE__);
				}else if([&](){ itog_titles = erb(TITLES, {{"value", value}}); return itog_titles.empty(); }()){ mpre("Заголовок значения `"+ itog_values["name"]+ "` не найден", __LINE__);
				}else if(itog_titles.end() == itog_titles.find("name")){ mpre("ОШИБКА поле `name` не найдено у заголовка", __LINE__);
				}else if([&](){ j[name] = itog_titles.at("name"); return false; }()){ mpre("ОШИБКА установки заголовка значению", __LINE__);
				}else{ //mpre(itog_titles, __LINE__, "Заголовок значения "+ value);
				}
			};
			if(3 < argc){ mpre("Указан итог результат не выводим "+ to_string(divider), __LINE__);
			}else{
				std::cout << "{\n\t\"stats\":{\"size\":\"" << in.size() << "\",\"change\":\"" << change << "\",\"pips\":\"" << pips_perc << "\",\"index\":\"" << BMF_INDEX_EX.at("").size() << "\"},\n\t\"itog\":";
				std::cout << fixed << j.dump();
				std::cout << "\n}\n\n";
			}
		return false; }()){ mpre("Ошибка отображение результата расчета", __LINE__);
	}else if([&](){ // Сохранение
			TMMi _BMF_INDEX, _BMF_INDEX_STAT, _BMF_DANO_VALUES, _BMF_ITOG_VALUES, _BMF_DANO_TITLES, _BMF_ITOG_TITLES, _BMF_DANO, _BMF_ITOG;
			if([&](){ std::experimental::filesystem::perms p = std::experimental::filesystem::status(dbname).permissions(); return ((p & std::experimental::filesystem::perms::others_write) == std::experimental::filesystem::perms::none); }()){ mpre("ОШИБКА файл БД не доступен для записи $chmod o+w "+ dbname, __LINE__);
			//}else if([&](){ divider = (argc > 3 ? stoi(argv[3]) : 0); return (0 > divider); }()){ mpre("ОШИБКА получения номера итога", __LINE__);
			}else if([&](){ //mpre("Сохранение результатов в тест", __LINE__);
					TMs test; string shift, date;
					if((in.size() == 1) && (0 >= change)){ mpre("Один пример без изменений не сохраняем историю", __LINE__);
					}else if([&](){ exec("CREATE TABLE IF NOT EXISTS mp_bmf_test (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,`time` INTEGER, `date` TEXT, `size` INTEGER, `divider` INTEGER, `change` INTEGER, `duration` INTEGER, `bmf` INTEGER, `loop` INTEGER, `perc` REAL, `pips` REAL, `clump` TEXT)"); return false; }()){ mpre("ОШИБКА создания значения итога", __LINE__);
					}else if([&](){ char mbstr[100]; time_t t = time(nullptr); std::strftime(mbstr, sizeof(mbstr), "%c", std::localtime(&t)); date = string(mbstr); return (0 >= date.length()); }()){ mpre("ОШИБКА расчета времени", __LINE__);
					}else if([&](){ test = fk("mp_bmf_test", {}, {{"time", to_string(timestamp)}, {"date", date}, {"size", to_string(in.size())}, {"change", to_string(change)}, {"duration", to_string(time(0)-timestamp)}, {"clump", clump_id}, {"loop", to_string(loop)}, {"perc", to_string(perc)}, {"pips", to_string(pips_perc)}, {"bmf", to_string(BMF_INDEX_EX.at("").size())}, {"divider", (3 < argc ? to_string(divider) : "")}}, {}); return false; }()){ mpre("ОШИБКА сохранения результатов тестов", __LINE__);
					//}else if(test.end() != test.find("divider")){ //mpre("Поле пипсов найдено в таблице тестов", __LINE__);
					//}else if([&](){ exec("ALTER TABLE mp_bmf_test ADD COLUMN divider INTEGER;"); return false; }()){ mpre("ОШИБКА добавления нового поля pips к таблице тестов", __LINE__);
					}else{ mpre("Сохранение статистики "+ test["id"], __LINE__);
					} return false;
				}()){ mpre("ОШИБКА сохранения теста", __LINE__);
			}else if([&](){ // Режим расчета с указанным 0 в качестве итога
					if(argc < 4){ //mpre("Недостаточно аргументов для режима расчета", __LINE__);
					}else if(0 < divider){ //mpre("Указан реальный итог "+ to_string(divider), __LINE__);
					}else{ //mpre("Указан нулевой итог", __LINE__);
						return true;
					} return false;
				}()){ mpre("Режим расчета не сохраняем данные итог "+ to_string(divider), __LINE__);
			}else if([&](){ exec("BEGIN TRANSACTION"); return false; }()){ mpre("ОШИБКА запуска начала транзакции", __LINE__);
			}else if([&](){
					if(false){ //exit(mpre(calc_rand, __LINE__, "Случайный расчет"));
					}else if(0 == index_save){ mpre("Сохранение морфов отключено "+ (calc_rand.empty() ? "" : calc_rand["name"])+ " "+ to_string(BMF_INDEX_EX.at("").size())+ " "+ to_string(loop), __LINE__);
					}else if([&](){ _BMF_DANO = Save("mp_bmf_dano", BMF_DANO_EX.at("")); return _BMF_DANO.empty(); }()){ mpre("ОШИБКА сохранения исходных данных", __LINE__);
					}else if([&](){ _BMF_ITOG = Save("mp_bmf_itog", BMF_ITOG_EX.at("")); return false; }()){ mpre("ОШИБКА сохранения итоговых данных", __LINE__);
					}else if([&](){ _BMF_DANO_VALUES = Save("mp_bmf_dano_values", BMF_DANO_VALUES_EX.at("")); return _BMF_DANO_VALUES.empty(); }()){ mpre("ОШИБКА сохранения изначальных значений", __LINE__);
					}else if([&](){ _BMF_ITOG_VALUES = Save("mp_bmf_itog_values", BMF_ITOG_VALUES_EX.at("")); return _BMF_ITOG_VALUES.empty(); }()){ mpre("ОШИБКА сохранения итоговых значений", __LINE__);
					}else if((in.size() == 1) && (0 >= change)){ mpre("Один пример и отсутствие изменений", __LINE__);
					}else if([&](){ _BMF_DANO_TITLES = Save("mp_bmf_dano_titles", BMF_DANO_TITLES_EX.at("")); return false; }()){ mpre("ОШИБКА сохранения изначальных заголовков", __LINE__);
					}else if([&](){ _BMF_ITOG_TITLES = Save("mp_bmf_itog_titles", BMF_ITOG_TITLES_EX.at("")); return false; }()){ mpre("ОШИБКА сохранения итоговых заголовков", __LINE__);
					}else if([&](){ _BMF_INDEX = Save("mp_bmf_index", BMF_INDEX_EX.at("")); return false; }()){ mpre("ОШИБКА сохранения справочника морфа", __LINE__);
					}else{ std::cerr << endl << __LINE__ << " Сохранение списка морфов" << (calc_rand.empty() ? "" : " "+ calc_rand["name"]) << " Эпох: " << loop << " Морфов: " << BMF_INDEX_EX.at("").size() << " Время: " << time(0)-timestamp << endl << endl;
						return false;
					} return true;
				}()){ mpre("Не сохраняем историю изменение заголовков и морфов", __LINE__);
			}else if([&](){ for(auto &itog_itr:_BMF_ITOG){ //for_each(_BMF_ITOG.begin(), _BMF_ITOG.end(), [&](auto itog_itr){ // Корректировка связи с итогом
					TMs itog, _itog;
					if([&](){ itog = _itog = itog_itr.second; return itog.empty(); }()){ mpre("ОШИБКА получения морфа из итератора", __LINE__);
					}else if([&](){ // Установка ссылки на морф
							TMs _index;
							if("" == itog["index_id"]){ mpre("ОШИБКА Идентификатор морфа не указан", __LINE__);
							}else if(_BMF_INDEX.end() == _BMF_INDEX.find(stoi(itog["index_id"]))){ //mpre("Морф не найден", __LINE__);
							}else if([&](){ _index = _BMF_INDEX.at(stoi(itog["index_id"])); return _index.empty(); }()){ mpre("ОШИБКА выборки морфа по первой связи", __LINE__);
							}else if(_index.end() == _index.find("id")){ mpre("ОШИБКА получения идентификатора связи", __LINE__);
							}else if([&](){ itog["index_id"] = _index.at("id"); return false; }()){ mpre("ОШИБКА установки значения связи", __LINE__);
							}else{ //mpre("Установка ссылки на морф", __LINE__);
							}
						return false; }()){ mpre("ОШИБКА установка ссылки на морф", __LINE__);
					}else if([&](){ // Установка ссылки на морф
							TMs _itog_values;
							if(_BMF_ITOG_VALUES.end() == _BMF_ITOG_VALUES.find(stoi(itog["itog_values_id"]))){ mpre("ОШИБКА морф не значение", __LINE__);
							}else if([&](){ _itog_values = _BMF_ITOG_VALUES.at(stoi(itog["itog_values_id"])); return _itog_values.empty(); }()){ mpre("ОШИБКА выборки морфа по первой связи", __LINE__);
							}else if(_itog_values.end() == _itog_values.find("id")){ mpre("ОШИБКА получения идентификатора связи", __LINE__);
							}else if([&](){ itog["itog_values_id"] = _itog_values.at("id"); return false; }()){ mpre("ОШИБКА установки значения связи", __LINE__);
							}else{ //mpre("Установка ссылки на морф", __LINE__);
							}
						return false; }()){ mpre("ОШИБКА установка ссылки на морф", __LINE__);
					}else if(itog == _itog){ //mpre("Изменений в итоге не найдено", __LINE__);
					}else if([&](){ _itog = fk("mp_bmf_itog", {{"id", itog["id"]}}, {}, itog); return _itog.empty(); }()){ mpre(itog, __LINE__, "ОШИБКА обновления значений морфа");
					}else{ //mpre(_index, __LINE__, "Связь 1"); mpre(index, __LINE__, "Коррекция ключей морфа "+ to_string(id));
					}
				}; return false; }()){ mpre("ОШИБКА корректировки свзи с итогом", __LINE__);
			}else if([&](){ for(auto &dano_itr:_BMF_DANO){ //for_each(_BMF_DANO.begin(), _BMF_DANO.end(), [&](auto dano_itr){ // Корректировка связи с итогом
					TMs dano, _dano;
					if([&](){ dano = _dano = dano_itr.second; return dano.empty(); }()){ mpre("ОШИБКА получения морфа из итератора", __LINE__);
					}else if([&](){ // Установка ссылки на морф
							TMs _dano_values;
							if(_BMF_DANO_VALUES.end() == _BMF_DANO_VALUES.find(stoi(dano["dano_values_id"]))){ mpre("ОШИБКА морф не найден", __LINE__);
							}else if([&](){ _dano_values = _BMF_DANO_VALUES.at(stoi(dano["dano_values_id"])); return _dano_values.empty(); }()){ mpre("ОШИБКА выборки морфа по первой связи", __LINE__);
							}else if(_dano_values.end() == _dano_values.find("id")){ mpre("ОШИБКА получения идентификатора связи", __LINE__);
							}else if([&](){ dano["dano_values_id"] = _dano_values.at("id"); return false; }()){ mpre("ОШИБКА установки значения связи", __LINE__);
							}else{ //mpre("Установка ссылки на морф", __LINE__);
							}
						return false; }()){ mpre("ОШИБКА установка ссылки на морф", __LINE__);
					}else if(dano == _dano){ //mpre("Изменений в итоге не найдено", __LINE__);
					}else if([&](){ _dano = fk("mp_bmf_dano", {{"id", dano["id"]}}, {}, dano); return _dano.empty(); }()){ mpre(dano, __LINE__, "ОШИБКА обновления значений морфа");
					}else{ //mpre(_index, __LINE__, "Связь 1"); mpre(index, __LINE__, "Коррекция ключей морфа "+ to_string(id));
					}
				}; return false; }()){ mpre("ОШИБКА корректировки свзи с итогом", __LINE__);
			}else if([&](){ for(auto &index_itr:_BMF_INDEX){ //for_each(_BMF_INDEX.begin(), _BMF_INDEX.end(), [&](auto &index_itr){ // Правка связей
					TMs index, _index;
					if([&](){ _index = index = index_itr.second; return index.empty(); }()){ mpre("ОШИБКА получения морфа из итератора", __LINE__);
					}else if("0" == index["id"]){ //mpre("Не правим удаленные морфы", __LINE__);
					}else if([&](){ // Установка младшего морфа
							TMs _index_; string id;
							if(index.end() == index.find("bmf-index")){ mpre("ОШИБКА поле со старшим морфом не найдено", __LINE__);
							}else if([&](){ id = index.at("bmf-index"); return (0 >= id.length()); }()){ //mpre("Идентификатор связи не найден", __LINE__);
							}else if(_BMF_INDEX.end() == _BMF_INDEX.find(stoi(id))){ mpre("ОШИБКА морф в списке не найден "+ id, __LINE__);
							}else if([&](){ _index_ = _BMF_INDEX.at(stoi(id)); return _index_.empty(); }()){ mpre("ОШИБКА морф не найден", __LINE__);
							}else if([&](){ index["bmf-index"] = _index_.at("id"); return false; }()){ mpre("ОШИБКА установки значения связи", __LINE__);
							}else{ //mpre(index, __LINE__, "Измененная связь");
							} return index.empty();
						}()){ mpre("ОШИБКА установки старшего морфа", __LINE__);
					}else if([&](){ // Установка старшего морфа
							TMs _index_; string id;
							if(index.end() == index.find("index_id")){ mpre("ОШИБКА поле со старшим морфом не найдено", __LINE__);
							}else if([&](){ id = index.at("index_id"); return (0 >= id.length()); }()){ //mpre("Идентификатор связи не найден", __LINE__);
							}else if(_BMF_INDEX.end() == _BMF_INDEX.find(stoi(id))){ mpre("ОШИБКА морф в списке не найден "+ id, __LINE__);
							}else if([&](){ _index_ = _BMF_INDEX.at(stoi(id)); return _index_.empty(); }()){ mpre("ОШИБКА морф не найден", __LINE__);
							}else if([&](){ index["index_id"] = _index_.at("id"); return false; }()){ mpre("ОШИБКА установки значения связи", __LINE__);
							}else{ //mpre(index, __LINE__, "Измененная связь");
							} return index.empty();
						}()){ mpre("ОШИБКА установки старшего морфа", __LINE__);
					}else if([&](){ // Установка связи со значением
							TMs _itog_values_; string id;
							if(index.end() == index.find("itog_values_id")){ mpre("ОШИБКА поле со старшим морфом не найдено", __LINE__);
							}else if([&](){ id = index.at("itog_values_id"); return (0 >= id.length()); }()){ //mpre("Идентификатор связи не найден", __LINE__);
							}else if(_BMF_ITOG_VALUES.end() == _BMF_ITOG_VALUES.find(stoi(id))){ mpre("ОШИБКА морф в списке не найден "+ id, __LINE__);
							}else if([&](){ _itog_values_ = _BMF_ITOG_VALUES.at(stoi(id)); return _itog_values_.empty(); }()){ mpre("ОШИБКА морф не найден", __LINE__);
							}else if([&](){ index["itog_values_id"] = _itog_values_.at("id"); return false; }()){ mpre("ОШИБКА установки значения связи", __LINE__);
							}else{ //mpre(index, __LINE__, "Измененная связь");
							} return index.empty();
						}()){ mpre("ОШИБКА установки старшего морфа", __LINE__);
					}else if([&](){ // Установка связи со значением
							TMs dano; string id;
							if(index.end() == index.find("dano_id")){ mpre("ОШИБКА поле с исходным значением", __LINE__);
							}else if([&](){ id = index.at("dano_id"); return (0 >= id.length()); }()){ //mpre("Идентификатор связи не найден", __LINE__);
							}else if(_BMF_DANO.end() == _BMF_DANO.find(stoi(id))){ mpre("ОШИБКА исходник в списке не найден "+ id, __LINE__);
							}else if([&](){ dano = _BMF_DANO.at(stoi(id)); return dano.empty(); }()){ mpre("ОШИБКА дано не найден", __LINE__);
							}else if([&](){ index["dano_id"] = dano.at("id"); return false; }()){ mpre("ОШИБКА установки значения связи", __LINE__);
							}else{ //mpre(index, __LINE__, "Измененная связь");
							} return index.empty();
						}()){ mpre("ОШИБКА установки старшего морфа", __LINE__);
					}else if([&](){ // Установка связи со значением
							TMs itog; string id;
							if(index.end() == index.find("itog_id")){ mpre("ОШИБКА поле с итоговым значением", __LINE__);
							}else if([&](){ id = index.at("itog_id"); return (0 >= id.length()); }()){ //mpre("Идентификатор связи не найден", __LINE__);
							}else if(_BMF_ITOG.end() == _BMF_ITOG.find(stoi(id))){ mpre("ОШИБКА итог в списке не найден "+ id, __LINE__);
							}else if([&](){ itog = _BMF_ITOG.at(stoi(id)); return itog.empty(); }()){ mpre("ОШИБКА итог не найден", __LINE__);
							}else if([&](){ index["itog_id"] = itog.at("id"); return false; }()){ mpre("ОШИБКА установки значения связи", __LINE__);
							}else{ //mpre(index, __LINE__, "Измененная связь");
							} return index.empty();
						}()){ mpre("ОШИБКА установки старшего морфа", __LINE__);
					}else if(index == _index){ //mpre("Изменений не установлено", __LINE__);
					}else if([&](){ _index = fk("mp_bmf_index", {{"id", index["id"]}}, {}, index); return _index.empty(); }()){ mpre(index, __LINE__, "ОШИБКА обновления значений морфа");
					}else{ //mpre(_index, __LINE__, "Связь 1"); mpre(index, __LINE__, "Коррекция ключей морфа "+ to_string(id));
					}
				} return false; }()){ mpre("ОШИБКА правки связей морфов", __LINE__);
			}else if([&](){ for(auto &dano_titles_itr:_BMF_DANO_TITLES){ //for_each(_BMF_DANO_TITLES.begin(), _BMF_DANO_TITLES.end(), [&](auto &dano_titles_itr){ // Правка связей
					TMs dano_titles, _dano_titles;
					if([&](){ _dano_titles = dano_titles = dano_titles_itr.second; return dano_titles.empty(); }()){ mpre("ОШИБКА получения заголовка", __LINE__);
					}else if([&](){ // Установка ссылки на морф
							TMs _dano_values;
							if(_BMF_DANO_VALUES.end() == _BMF_DANO_VALUES.find(stoi(dano_titles["dano_values_id"]))){ mpre("ОШИБКА морф не найден", __LINE__);
							}else if([&](){ _dano_values = _BMF_DANO_VALUES.at(stoi(dano_titles["dano_values_id"])); return _dano_values.empty(); }()){ mpre("ОШИБКА выборки морфа по первой связи", __LINE__);
							}else if(_dano_values.end() == _dano_values.find("id")){ mpre("ОШИБКА получения идентификатора связи", __LINE__);
							}else if([&](){ dano_titles["dano_values_id"] = _dano_values.at("id"); return false; }()){ mpre("ОШИБКА установки значения связи", __LINE__);
							}else{ //mpre("Установка ссылки на морф", __LINE__);
							}
						return false; }()){ mpre("ОШИБКА установка ссылки на морф", __LINE__);
					}else if([&](){ _dano_titles = fk("mp_bmf_dano_titles", {{"id", dano_titles["id"]}}, {}, dano_titles); return _dano_titles.empty(); }()){ mpre(dano_titles, __LINE__, "ОШИБКА обновления заголовка");
					}else{ //mpre(_dano_titles, __LINE__, "Связь 1"); //mpre(dano_titles, __LINE__, "Коррекция ключей морфа "+ to_string(id));
					}
				} return false; }()){ mpre("ОШИБКА правки связей морфов", __LINE__);
			}else if([&](){ for(auto &itog_titles_itr:_BMF_ITOG_TITLES){ //for_each(_BMF_ITOG_TITLES.begin(), _BMF_ITOG_TITLES.end(), [&](auto &itog_titles_itr){ // Правка связей
					TMs itog_titles, _itog_titles;
					if([&](){ _itog_titles = itog_titles = itog_titles_itr.second; return itog_titles.empty(); }()){ mpre("ОШИБКА получения заголовка", __LINE__);
					}else if([&](){ // Установка ссылки на морф
							TMs _itog_values;
							if(_BMF_ITOG_VALUES.end() == _BMF_ITOG_VALUES.find(stoi(itog_titles["itog_values_id"]))){ mpre("ОШИБКА морф не найден", __LINE__);
							}else if([&](){ _itog_values = _BMF_ITOG_VALUES.at(stoi(itog_titles["itog_values_id"])); return _itog_values.empty(); }()){ mpre("ОШИБКА выборки морфа по первой связи", __LINE__);
							}else if(_itog_values.end() == _itog_values.find("id")){ mpre("ОШИБКА получения идентификатора связи", __LINE__);
							}else if([&](){ itog_titles["itog_values_id"] = _itog_values.at("id"); return false; }()){ mpre("ОШИБКА установки значения связи", __LINE__);
							}else{ //mpre("Установка ссылки на морф", __LINE__);
							}
						return false; }()){ mpre("ОШИБКА установка ссылки на морф", __LINE__);
					}else if([&](){ _itog_titles = fk("mp_bmf_itog_titles", {{"id", itog_titles["id"]}}, {}, itog_titles); return _itog_titles.empty(); }()){ mpre(itog_titles, __LINE__, "ОШИБКА обновления заголовка");
					}else{ //mpre(_itog_titles, __LINE__, "Связь 1"); //mpre(itog_titles, __LINE__, "Коррекция ключей морфа "+ to_string(id));
					}
				} return false; }()){ mpre("ОШИБКА правки связей морфов", __LINE__);
			}else if([&](){ exec("COMMIT TRANSACTION"); return false; }()){ mpre("ОШИБКА начала сессии к БД", __LINE__);
			}else{ //mpre(_BMF_INDEX, __LINE__, "Сохранение");
			} return false; // exit(mpre("Остановка подпрограммы", __LINE__));
		}()){ mpre("ОШИБКА установки ключей", __LINE__);
	}else{
	}
}

