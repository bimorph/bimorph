<?

if(!require_once("phar://../../../index.phar/include/func.php")){ mpre("ОШИБКА подключения файла функций");
}elseif(!$conf['user']['gid'][1] = "Администратор"){ mpre("ОШИБКА уставоки пользователя администратором");
}elseif(!$file = file($f = "titanic.csv")){ mpre("ОШИБКА откртия файла `{$f}`");
}elseif(!$CSV = array_map(function($csv){ return str_getcsv($csv); }, $file)){ mpre("ОШИБКА парсинга csv строк");
}elseif(!$titles = get($CSV, 0)){ mpre("Список заголовков полей");
}elseif(!$CSV = array_slice($CSV, 1)){ mpre("ОШИБКА получения данных без заголовка полей");
}elseif(!$ru = ['PassengerID'=>"Идентификатор", 'Name'=>"Имя", "PClass"=>"Палуба", "Age"=>"Возраст", 'Sex'=>"ПолСтрока", "Survived"=>"Выжил", "SexCode"=>"Пол"]){ mpre("ОШИБКА установки российских названий полей");
}elseif(!$structure = ['class'=>'itog', 'secret'=>'Uho*7hkeefsdfUYT', 'dano'=>['Возраст', 'Пол', "Палуба"], 'itog'=>["Выжил"]]){ mpre("ОШИБКА создания структуры аякс запроса");
}elseif(!$href = "http://0.0.0.0/bmf/bmf-clump:12/null/json"){ mpre("ШИБКА установки api адреса");
}elseif(!$ajax = function($values) use($titles, $ru, $href, $structure){ # Функция отправки данных
		if(!$query = array_map(function($struct) use($values){ # Устанавливаем в структуру значения полей
				if(!is_array($struct)){ return $struct; mpre("Жестко установленные значения структуры");
				}elseif(!$_struct = array_flip($struct)){ mpre("ОШИБКА Получение названий структуры в ключах");
				}elseif(!$data = array_replace($_struct, $values)){ mpre("Установка значений в структуру");
				}elseif(!$_data = array_intersect_key($data, $_struct)){ mpre("Ограничение данных только поляи структуры");
				}else{ return $_data; }
			}, $structure)){ mpre("ОШИБКА формирования пост запроса к серверу");
		}elseif(!$content = http_build_query($query)){ mpre("ОШИБКА формирования строки данных запроса");
		}elseif(!$context = stream_context_create(['http'=>['method'=>'POST', 'header'=>'Content-type: application/x-www-form-urlencoded', 'content'=>$content]])){ mpre("ОШИБКА формирования пост данных запроса");
		}elseif(!$response = file_get_contents($href, false, $context)){ mpre("ОШИБКА запроса к апи сервиса", $href, $query);
		}elseif(!$json = json_decode($response, true)){ mpre("ОШИБКА парстинга json массива", $response);
		}else{ return ['href'=>$href, 'query'=>$query, 'json'=>$json]; }
	}){ mpre("ОШИБКА создания функции отправки");
}elseif(!is_numeric($true = (get($argv, 1) ?: 0))){ mpre("ОШИБКА получения успешных обучений из консоли");
}elseif(!is_numeric($false = (get($argv, 2) ?: 0))){ mpre("ОШИБКА получения не успешных обучений из консоли");
}else{// mpre($argv);
	while(true){ // mpre("Обучение выборки")
		if(!is_numeric($from = is_numeric(get($argv, 1)) ? $argv[1] : 0)){ mpre("ОШИБКА выборки с какого элемента отправлять");
		}elseif(!is_numeric($count = is_numeric(get($argv, 2)) ? $argv[2] : count($CSV))){ mpre("ОШИБКА выборки количества элементов для отправки");
		}elseif(!shuffle($CSV)){ mpre("ОШИБКА замешивания списка");
		}elseif(!$_CSV = array_slice($CSV, $from, $count)){ mpre("ОШИБКА выборки отправляемых данных");
		}elseif(!$_CSV = array_map(function($csv) use($ajax){ # Запуск функции отправки строки на сервер
				if(!$csv = array_map(function($val){ # Приведение всех числовых значений к целым числам
						if(!is_numeric($val)){ return $val;
						}else{ return $val; }
					}, $csv)){ mpre("ОШИБКА преобразования формата данных");
				}else{ return $csv; }
			}, $_CSV)){ mpre("ОШИБКА получения данных отправки");
		}else{
			foreach($_CSV as $csv){
				if(!$params = array_combine($titles, $csv)){ mpre("ОШИБКА получения значений с ключами заголовков");
				}elseif(!$values = array_combine($ru, $params)){ mpre("ОШИБКА получения дано значений");
				}elseif(!$response = $ajax($values)){ mpre("ОШИБКА получения результатов загрузки страницы");
				}elseif(!$finding = array_intersect_key($values, get($response, 'json', 'itog'))){ mpre("ОШИБКА получения ответа");
				}elseif(!is_numeric($response["Расчет"] = $true = (get($response, 'json', 'itog') == $finding ? $true+1 : (int)$true))){ mpre("ОШИБКА расчета успешных результатов");
				}elseif(!is_numeric($response["Обучение"] = $false = (get($response, 'json', 'itog') == $finding ? (int)$false : $false+1))){ mpre("ОШИБКА расчета не успешных результатов");
				}elseif(!$response["Процент"] = number_format($response["Расчет"] * 100 / ($response["Обучение"]+$response["Расчет"]), 2). "%"){ mpre("ОШИБКА определения количества совпадений подряд");
				}elseif(!is_numeric($response["Совпадений"] = $match = (get($response, 'json', 'itog') == $finding ? $match+1 : 0))){ mpre("ОШИБКА определения количества совпадений подряд");
				}elseif(!is_numeric($response["Решение"] = ($response["Обучение"]+$response["Расчет"]-$response["Совпадений"]))){ mpre("ОШИБКА определения количества совпадений подряд");
				}else{ mpre($response); }
			}
		}
	}
}
