<?

if(!$DIR = explode("/", __DIR__)){ print_r("Ошибка определения текущей директории");
}else if(!$offset = array_search($d = "xn--90aomikyxn--p1ai", $DIR)){ print_r("Директория с проектом не найдена `{$d}`"); print_r($DIR);
}else if(!$folder = implode("/", array_slice($DIR, 0, $offset+2))){ print_r("Ошибка выборка директории");
}else if(!$dir = implode("/", array_slice($DIR, $offset+2))){ print_r("Ошибка выборка директории");
}else if(!chdir($folder)){ print_r("Ошибка установки директории `{$dir}`");
}else if(!$inc = function($file) use(&$conf, $dir){
	if(($f = realpath($file)) && include($f)){ return $f;
	}else if(($f = "phar://index.phar/{$file}") && file_exists($f) && include($f)){ return $f;
	}else{ return print_r("Директория не найдена `{$file}`"); }
}){ print_r("Функция подключения файлов");
}else if(!$conf['user']['gid'] = array(1=>"Администратор")){ mpre("Устанавливаем администратора");
}else if(!$inc($f = "include/mpfunc.php")){ print_r("Ошибка подключения `{$f}`");
}else if(!$inc($f = "include/config.php")){ pre("Ошибка подключения `{$f}`");
}else if(!$modpath = (isset($argv) ? basename(dirname(dirname(__FILE__))) : basename(dirname(__FILE__)))){ mpre("Ошибка вычисления имени модуля");
}else if(!$arg = ['modpath'=>$modpath, "fn"=>implode(".", array_slice(explode(".", basename(__FILE__)), 0, -1))]){ mpre("Установка аргументов");
}else if(!isset($argv)){ pre("Запуск из веб интерфейса");
}else if(!$conf['db']['conn'] = conn("sqlite:tmpfs/.htdb")){ mpre("Ошибка подключения БД");
}else if(array_search($cmd["info"] = "Текущая информация", $cmd) == get($argv, 1)){// pre("Метод", get($argv, 1));
	if(!print(str_repeat("=", 160). "\n")){ mpre("Отбивка между запусками");
	}else if(!$bm0_clump = call_user_func(function($argv){ # Выбор текущего скопления или видимого если не указано скопление
			if(!$BM0_CLUMP = rb("bm0-clump", 'hide', 'id', 0)){ mpre("Видимых скоплений не найдено");
			}else if(1 < count($BM0_CLUMP)){ mpre("Видимо больше одного скопления");
			}else if(!$bm0_clump = first($BM0_CLUMP)){ mpre("Ошибка нахождения текущего скопления");
			}else{ return $bm0_clump; }
		}, $argv)){ mpre("Номер скопления не указан");
	}else{ mpre("Текущий элемент", $bm0_clump); }
}else if(array_search($cmd["clean"] = "Обнуление всех элементов", $cmd) == get($argv, 1)){// pre("Метод", get($argv, 1));
	if(!print(str_repeat("=", 160). "\n")){ mpre("Отбивка между запусками");
/*	}else if(!$bm0_clump = call_user_func(function($argv){ # Выбор текущего скопления или видимого если не указано скопление
			if($bm0_clump = rb('bm0-clump', 'id', get($argv, 2))){ return $bm0_clump;
			}else if(!$BM0_CLUMP = rb("bm0-clump", 'hide', 'id', 0)){ mpre("Видимых скоплений не найдено");
			}else if(1 < count($BM0_CLUMP)){ mpre("Видимо больше одного скопления");
			}else if(!$bm0_clump = first($BM0_CLUMP)){ mpre("Ошибка нахождения текущего скопления");
			}else{ return $bm0_clump; }
		}, $argv)){ mpre("Номер скопления не указан");*/
	}else if(!$BM0_CLUMP = rb('bm0-clump', 'id', 'id', (get($argv, 2) ?: true))){ mpre("Ошибка выборки списка скоплений");
	}else if(qw("UPDATE `{$conf['db']['prefix']}{$arg['modpath']}_clump` SET hide=1 WHERE 1")){ mpre("Ошибка скрытия всех скоплений");
	}else if(!$_BM0_CLUMP = array_filter(array_map(function($bm0_clump){ mpre("Очистка скопления `{$bm0_clump['id']}`");
			if(!$bm0_clump = fk('bm0-clump', ['id'=>$bm0_clump['id']], null, ['retry'=>0, 'clump_history_id'=>null, 'hide'=>0])){ mpre("Ошибка инкримента итерации");
			}else if(qw($sql = "DELETE FROM `mp_bm0_clump_history` WHERE `clump_id`=". (int)$bm0_clump['id'])){ mpre("Ошибка удаления истории `{$sql}`");
			}else if(qw($sql = "DELETE FROM `mp_bm0_clump_shot` WHERE `clump_id`=". (int)$bm0_clump['id'])){ mpre("Ошибка удаления истории `{$sql}`");
		//	}else if(qw($sql = "DELETE FROM `mp_bm0_itog` WHERE `clump_id`=". (int)$bm0_clump['id'])){ mpre("Ошибка удаления итоговых данных `{$sql}`");
			}else if(!is_array($BM0_INDEX = rb('bm0-index', 'clump_id', 'id', $bm0_clump['id']))){ mpre("Ошибка выборки всех морфов");
			}else if(!is_array($_BM0_INDEX = array_filter(array_map(function($bm0_index){
					if(qw($sql = "DELETE FROM mp_bm0_index WHERE id=". (int)$bm0_index['id'])){ mpre("Ошибка удаления морфа `{$sql}`");
					}else{ mpre($sql); }
				}, $BM0_INDEX)))){ mpre("Ошибка удаления не корневых морфов");
			}else if(qw($sql = "DELETE FROM `mp_bm0_dano` WHERE `clump_id`=". (int)$bm0_clump['id'])){ mpre("Ошибка удаления входящих данных `{$sql}`");
			}else if(qw($sql = "DELETE FROM `mp_bm0_itog` WHERE `clump_id`=". (int)$bm0_clump['id'])){ mpre("Ошибка удаления выходных данных `{$sql}`");
/*			}else if(!is_array($_BM0_ITOG = array_filter(array_map(function($bm0_itog){
					if(!$bm0_itog = fk('bm0-itog', ['id'=>$bm0_itog['id']], null, ['index_id'=>$bm0_itog['id'], 'index_id'=>null, 'val'=>null, 'value'=>null])){ mpre("Ошибка установки морфа итога");
					}else{ return $bm0_itog; }
				}, rb('bm0-itog', 'clump_id', 'id', $bm0_clump['id']))))){ mpre("Ошибка удаления не корневых морфов");*/
			}else if(!is_array($_BM0_DANO = array_filter(array_map(function($bm0_dano){
					if(!$bm0_dano = fk('bm0-dano', ['id'=>$bm0_dano['id']], null, ['value'=>'', 'val'=>''])){ mpre("Ошибка обнуления обучения дано");
					}else{ return $bm0_dano; }
				}, rb('bm0-dano', 'clump_id', 'id', $bm0_clump['id']))))){ mpre("Ошибка удаления не корневых морфов");
			}else{ return $bm0_clump; }
		}, $BM0_CLUMP))){ mpre("Ошибка перебора скоплений");
	}else{ print(str_repeat("=", 160). "\n"); }
}else if(array_search($cmd["history"] = "Добавление обучающих вариантов в истории", $cmd) == get($argv, 1)){// pre("Метод", get($argv, 1));
		print("\n". str_repeat("=", 160). "\n");
	if(!$bm0_clump = call_user_func(function($argv){ # Выбор текущего скопления или видимого если не указано скопление
			if(!$BM0_CLUMP = rb("bm0-clump", 'hide', 'id', 0)){ mpre("Видимых скоплений не найдено");
			}else if(1 < count($BM0_CLUMP)){ mpre("Видимо больше одного скопления");
			}else if(!$bm0_clump = first($BM0_CLUMP)){ mpre("Ошибка нахождения текущего скопления");
			}else{ return $bm0_clump; }
		}, $argv)){ mpre("Номер скопления не указан");
	}else if(!$CALC = array_slice($argv, 2)){ mpre("Ошибка получения обучающих сигналов");
	}else if(!array_filter(array_map(function($dano) use($bm0_clump){
			if(!$bm0_clump_history = fk('bm0-clump_history', $w = ['clump_id'=>$bm0_clump['id'], 'dano'=>$dano], $w += ['name'=>$dano])){ mpre("Ошибка добавления варианта итога");
			}else{ return $bm0_clump_history; }
		}, $CALC))){ mpre("Ошибка обработки списка входящих значений");
	}else{ mpre("Варианты итога сохранены");
	}
}else if(array_search($cmd["bin"] = "Строка с цифровыми данными", $cmd) == get($argv, 1)){// pre("Метод", get($argv, 1));
		print("\n". str_repeat("=", 160). "\n");
	if(!$bm0_clump = call_user_func(function($argv){ # Выбор текущего скопления или видимого если не указано скопление
			if(!$BM0_CLUMP = rb("bm0-clump", 'hide', 'id', 0)){ mpre("Видимых скоплений не найдено");
			}else if(1 < count($BM0_CLUMP)){ mpre("Видимо больше одного скопления");
			}else if(!$bm0_clump = first($BM0_CLUMP)){ mpre("Ошибка нахождения текущего скопления");
			}else if(!$bm0_clump = fk('bm0-clump', ['id'=>$bm0_clump['id']], null, ['retry'=>$bm0_clump['retry']+1])){ mpre("Ошибка установки номера текущей операции");
			}else{ return $bm0_clump; }
		}, $argv)){ mpre("Номер скопления не указан");
	}else if(!$bm0_clump_history = fk('bm0-clump_history', $w = ['dano'=>($c = get($argv, 2))], $w += ['learn'=>($l = get($argv, 3)), 'clump_id'=>$bm0_clump['id'], 'name'=>"{$c} {$l}"])){ mpre("Ошибка сохранения истории");
	}else if(!$bm0_clump_shot = fk('bm0-clump_shot', null, $w = ['clump_history_id'=>$bm0_clump_history['id'], 'clump_id'=>$bm0_clump['id']])){ mpre("Ошибка добавления попытки обучения");
	}else if(!$bm0_clump_history = fk('bm0-clump_history', $w = ['id'=>$bm0_clump_history['id']], null, ['clump_shot_id'=>$bm0_clump_shot['id']])){ mpre("Ошибка сохранения истории");
	}else if(!$bm0_clump = fk('bm0-clump', ['id'=>$bm0_clump['id']], null, ['clump_history_id'=>$bm0_clump_history['id']])){ mpre("Ошибка сохранения текущей истории");

	}else if(!strlen($vals = get($argv, 2))){ mpre("Значение цифровых данных не задано");
	}else if(!preg_match("#[01]+#", $vals)){ mpre("Не верный формат данных цифровых значений `{$vals}`. Допустимые значения 0,1");
//	}else if(!is_array($BM0_DANO = rb("bm0-dano", 'clump_id', 'id', $bm0_clump['id']))){ mpre("Ошибка выборки входящих знаков");
	}else if(!$split = str_split($vals)){ mpre("Ошибка разбивки строки на символы");
	}else if(krsort($split) && (!$split = array_values($split))){ mpre("Ошибка выполнения обратной сортировки");
//	}else if(qw("BEGIN;")){ mpre("Начало транзакции");
	}else if((!$_BM0_DANO = call_user_func(function($split, $BM0_DANO = []) use($bm0_clump){// mpre($BM0_DANO, $bm0_clump, rb('bm0-dano'));
			foreach($split as $name=>$value){
				if(!$bm0_dano = fk("bm0-dano", $w = ['clump_id'=>$bm0_clump['id'], "name"=>$name], $w += ['val'=>$value], $w)){ mpre("Ошибка установки знака");
//				}else if(!$BM0_INDEX = rb("bm0-index", 'dano_id', '')){
				}else if($bm0_dano['value'] == $bm0_dano['val']){// mpre("Значения равны расчетным", $bm0_dano);
				}else if(!is_array($BM0_INDEX = rb('bm0-index', 'dano_id', 'id', $bm0_dano['id']))){ mpre("Ошибка получения списка измененных морфов");
				}else if(!is_array($_BM0_INDEX = array_map(function($bm0_index) use($bm0_clump){
						if(!$bm0_index = fk('bm0-index', ['id'=>$bm0_index['id']], null, ['retry'=>$bm0_clump['retry']])){ mpre("Ошибка обновления повторов у изменившихся морфов");
						}else{// mpre("Обновление значения морфа из дано");
							return $bm0_index;
						}
					}, $BM0_INDEX))){ mpre("Ошибка обновления списка морфов");
				}else{ $BM0_DANO[$bm0_dano['id']] = $bm0_dano; }
			} return $BM0_DANO;
		}, $split)) && !mpre("Знаков с измененным значением не найдено")){ mpre("Ошибка обработки значений входящих сигналов");
	# Расчет результата
	}else if(!$BM0_DANO = rb("bm0-dano", 'clump_id', 'id', $bm0_clump['id'])){ mpre("Ошибка выборки знаков");
	}else if(!$_BM0_DANO = array_filter(array_map(function($bm0_dano) use($bm0_clump){
			if(!is_numeric($bm0_dano['val'])){// mpre("Значение не задано");
			}else if((!$bm0_index = rb("bm0-index", 'dano_id', $bm0_dano['id'])) && (!$bm0_index = call_user_func(function($bm0_dano) use($bm0_clump){// mpre("Создание нового связного морфа");
					if(!$bm0_index = fk('bm0-index', $w = ['clump_id'=>$bm0_clump['id'], 'dano_id'=>$bm0_dano['id']], $w += ['hide'=>0, 'val'=>$bm0_dano['val'], 'value'=>$bm0_dano['val'], 'retry'=>$bm0_clump['retry'], 'form'=>"{{$bm0_dano['name']}}"], $w)){ mpre("Ошибка создания нового морфа");
					}else{ return $bm0_index; }
				}, $bm0_dano))){ die(!mpre("Ошибка связания позиции морфа по входному знаку"));
			}else if(!is_array($BM0_INDEX = rb("bm0-index", "dano_id", "id", $bm0_dano['id']))){ mpre("Ошибка выборки позиции зависимых от знаков значений");
			}else{// mpre("{$bm0_index['id']}. Создание нового связного морфа", $bm0_index);
				return $bm0_dano;
			}
		}, $BM0_DANO))){ mpre("Изменяемые значения не найдены");
	}else if(!$BM0_CALC_POS = rb('bm0-calc_pos')){ mpre("Ошибка выборки списка позиций");
	}else if(!$BM0_CALC_VAL = rb('bm0-calc_val')){ mpre("Ошибка выборки списка значений");
	}else if(!$BM0_CALC = rb('bm0-calc')){ mpre("Ошибка выборки списка расчетов");
	}else if(array_key_exists(3, $argv) && is_numeric($reval = call_user_func(function($total) use($bm0_clump, $BM0_DANO){ mpre("Пересчет по формуле `{$total}`");
			if(!preg_match("#\d?#", $total)){ mpre("Не верный формат данных цифровых значений. Допустимые значения 0,1");
			}else if(!$split = str_split($total)){ mpre("Ошибка разбивки строки на символы");
			}else if(!$dano = array_column($BM0_DANO, 'val', 'name')){ mpre("Ошибка подготовки списка замены");
			}else if(!$zam = mpzam($dano)){ mpre("Ошибка расчета массива замены");
			}else if(!is_array($BM0_ITOG = rb('bm0-itog', 'clump_id', 'name', 'id', $bm0_clump['id'], $split))){ mpre("Ошибка выборки итогов скопления");
			}else if(!is_array($_BM0_ITOG = array_map(function($bm0_itog) use($zam){
					if(!$form = $bm0_itog['form']){ mpre("Ошибка выборки формы итога для расчета");
					}else if(!$_form = strtr($form, $zam)){ mpre("Ошибка установки значений в форму");
					}else if(!$cmd = "return {$_form};"){ mpre("Ошибка формирования команды расчета формы");
					}else if(!is_numeric($value = (eval($cmd) ? 1 : 0))){ mpre("Ошибка рсчета значения формы `{$cmd}` => `{$value}`");
					}else{ mpre("Расчет итога `{$bm0_itog['id']}` по формуле {$_form} `{$value}`"); return $bm0_itog += ['reval'=>$value]; }
				}, $BM0_ITOG))){ mpre("Ошибка расчета итогов");
			}else if(($reval = implode(null, array_column($_BM0_ITOG, 'reval'))) &&0){ mpre("Ошибка расчета списка итогов");
			}else{ mpre("Результат расчета форм >>>>>{$reval}<<<<<");
				return $reval;
			}
		}, $argv[3])) && ($argv[3] === $reval)){ mpre("Результат расчетов форм `{$reval}` совпадает с обучением `{$argv[3]}`"); die(!print(str_repeat("=", 160). "\n"));
	}else if(!$calc = function($bm0_index, $bm0_dano = null) use(&$calc, $bm0_clump, $BM0_CALC_POS, $BM0_CALC_VAL, $BM0_CALC){ echo "{$bm0_index['id']}.";//  mpre("{$bm0_index['id']}. Расчет морфа"); # Функция расчета
			if(!is_array($bm0_calc_pos = rb($BM0_CALC_POS, 'id', $bm0_index['calc_pos_id']))){ mpre("Ошибка установки позиции морфа");
			}else if(!is_array($bm0_index_[$n = "Старший"] = ($bm0_index['index_id'] ? rb('bm0-index', 'id', $bm0_index['index_id']) : []))){ mpre("Ошибка выборки морфа `{$n}`");
			}else if(!is_array($bm0_index_[$n = "Младший"] = ($bm0_index['bm0-index'] ? rb('bm0-index', 'id', $bm0_index['bm0-index']) : []))){ mpre("Ошибка выборки морфа `{$n}`");
			}else if(array_filter($bm0_index_) && (!$bm0_index = call_user_func(function($bm0_index) use($bm0_index_, $bm0_calc_pos, $BM0_CALC_VAL, $BM0_CALC){
					if(empty($bm0_calc_pos)){ die(mpre("Не задана позиция морфа с нижестоящими элементами"));
					}else if(!$bm0_calc_val = rb($BM0_CALC_VAL, 'v1', 'v0', get($bm0_index_, 'Старший', 'val'), get($bm0_index_, 'Младший', 'val'))){ mpre("Ошибка установки значения по нижестоящим морфам");
					}else if(!$bm0_calc = rb($BM0_CALC, 'calc_pos_id', 'calc_val_id', $bm0_calc_pos['id'], $bm0_calc_val['id'])){ mpre("Ошибка выборки калькуляции морфа");
					}else if(!$bm0_index = fk('bm0-index', ['id'=>$bm0_index['id']], null, ['calc_val_id'=>$bm0_calc_val['id'], 'val'=>$bm0_calc['val']])){ mpre("Ошибка обновления морфа расчетным значением");
					}else{ return $bm0_index; }
				}, $bm0_index))){ mpre("Ошибка расчета нижестоящих морфов");
			}else if(!is_array($BM0_INDEX_[$n = "Старший"] = rb('bm0-index', 'index_id', 'id', $bm0_index['id']))){ mpre("Ошибка выборки списка морфов `{$n}`");
			}else if(!is_array($BM0_INDEX_[$n = "Младший"] = rb('bm0-index', 'bm0-index', 'id', $bm0_index['id']))){ mpre("Ошибка выборки списка морфа `{$n}`");
			}else if(!is_array($bm0_calc = rb($BM0_CALC, 'calc_pos_id', 'calc_val_id', $bm0_index['calc_pos_id'], $bm0_index['calc_val_id']))){ mpre("{$bm0_index['id']}. Ошибка выборки расчета морфа");
			}else if($bm0_calc && ($bm0_calc_amend = rb('bm0-calc_amend', 'id', $bm0_calc['calc_amend_id'])) && !$bm0_calc_amend['value']){ echo "."; // mpre("{$bm0_index['id']}. Прерываем расчет");
				return $bm0_index;
			}else if($bm0_dano && (!$bm0_index = call_user_func(function($bm0_dano) use($bm0_index){
					if(!$bm0_index = fk('bm0-index', ['id'=>$bm0_index['id']], null, ['val'=>$bm0_dano['val']])){ mpre("Ошибка обновления морфа из значения дано");
					}else{ return $bm0_index; }
				}, $bm0_dano))){ mpre("Ошибка расчета значения морфа по значению дано");
			}else if(!is_array($_BM0_INDEX = call_user_func_array('array_merge', $BM0_INDEX_))){ mpre("Ошибка формирования общего списка вышестоящих морфов");
			}else if($_BM0_INDEX && !array_filter(array_map(function($index) use($calc, $bm0_index){// mpre("{$bm0_index['id']}. Расчет вышестоящего элемента", $index);
					if(!$_index = call_user_func($calc, $index)){ mpre("Ошибка расчета выfk(шестоящего элемента");
					}else{ return $_index; }
				}, $_BM0_INDEX))){ mpre("Ошибка обработки вышестоящих морфов");
			}else if(!is_array($BM0_ITOG = rb('bm0-itog', 'index_id', 'id', $bm0_index['id']))){ mpre("Ошибка выборки элемента дано");
			}else if($BM0_ITOG && !array_map(function($bm0_itog) use($bm0_index){
					if(!$bm0_itog = fk('bm0-itog', ['id'=>$bm0_itog['id']], null, ['val'=>$bm0_index['val'], 'value'=>$bm0_index['val']])){ mpre("Ошибка обновления итога");
					}else{ return $bm0_itog; }
				}, $BM0_ITOG)){ mpre("Ошибка расчета итоговых значений");
			}else if(!$bm0_index = fk('bm0-index', ['id'=>$bm0_index['id']], null, ['value'=>$bm0_index['val']])){ mpre("Ошибка обновления значения морфа");
			}else{ return $bm0_index; }
		}){ mpre("Ошибка установки функции обучения");
//}else if(qw("COMMIT;")){ mpre("Окончание транзакции");

/*	}else if(!is_array($_BM0_DANO = array_filter(array_map(function($_bm0_dano) use($bm0_clump, $calc, $BM0_DANO, $_BM0_DANO){ mpre("Расчет входных значений дано `{$_bm0_dano['id']}`");
			if(!$BM0_INDEX = rb('bm0-index', 'dano_id', 'id', $_bm0_dano['id'])){ mpre("Связанных с дано связей не найдено");
			}else if(!$_BM0_INDEX = array_map(function($bm0_index) use($calc, $_bm0_dano){// mpre("{$bm0_index['id']}. Расчет связанного морфа"); # Обучение всех связанных с дано морфов
				if($bm0_index['calc_pos_id']){// mpre("Морф не является исходным");
					}else if(!is_array($_bm0_index = call_user_func($calc, $bm0_index, $_bm0_dano))){ mpre("Ошибка возвращенного значения функции расчета");
					}else if(!$_bm0_dano = fk("bm0-dano", ['id'=>$_bm0_dano['id']], null, ['value'=>$_bm0_index['val']])){ mpre("Ошибка обновления значения");
					}else{ echo "\n"; return $_bm0_index; }
				}, $BM0_INDEX)){ mpre("Ошибка расчета списка морфов дано");
			}else{ return $_bm0_dano; }
		}, $_BM0_DANO)))){ mpre("Ошибка расчета входных знаков");*/

	}elseif(!$_BM0_DANO = call_user_func(function($_BM0_DANO) use($bm0_clump){
			if(!$cmd = "cd ./.htmp/sqlite/; ./sl"){ mpre("Ошибка установки пути к скрипту расчетов");
			}else if(!mpre("Расчет значений морфов", $cmd)){
			}else if(passthru($cmd)){ mpre("Ошибка запуска расчета значений");
			}else if(!$_BM0_DANO = rb("bm0-dano", 'clump_id', 'id', $bm0_clump['id'])){ mpre("Ошибка выборки исходного знаков");
			}else{ return $_BM0_DANO; }
		}, $_BM0_DANO)){ mpre("Ошибка запуска расчета морфов");

	}else if(!is_array($BM0_ITOG = rb("bm0-itog", 'clump_id', 'id', $bm0_clump['id']))){ mpre("Ошибка выборки исходного знаков");
	}else if(!$BM0_DANO = rb("bm0-dano", 'clump_id', 'id', $bm0_clump['id'])){ mpre("Ошибка выборки исходного знаков");
	}else if(!is_array($ITOG = array_column($BM0_ITOG, 'val', 'name'))){ mpre("Ошибка выборки значений из массива");
	}else if(!ksort($ITOG)){ mpre("Ошибка сортировки итогов");
//	}else if(!is_string($itog = implode('', $ITOG))){ mpre("Ошибка обьединения результатов");
	}else if(!$keys = array_column($BM0_DANO, 'val', 'name')){ mpre("Ошибка формирования массива значений");
	}else if(!krsort($keys) || !is_string($dano = implode("", $keys))){ mpre("Ошибка формирования строки исходных значений");
	}else if(!is_array($keys = array_column($BM0_ITOG, 'value', 'name'))){ mpre("Ошибка формирования массива значений");
	}else if(!ksort($keys) || !is_string($itog = implode("", $keys))){ mpre("Ошибка формирования строки исходных значений");
	}else if(!$bm0_clump_history = fk('bm0-clump_history', ['id'=>$bm0_clump['clump_history_id']], null, ['calc'=>$itog])){ mpre("Ошибка сохранения истории");

	}else if(!strlen($total = get($argv, 3))){ mpre("Значение обучающих знаков не задано. Обучение `{$bm0_clump['id']}`, `{$dano}` результаты `{$itog}`");
	}else if(!preg_match("#\d+#", $total)){ die(!mpre("Не верный формат данных цифровых значений. Обучение `{$bm0_clump['id']}`, Допустимые значения 0,1"));
	}else if(!mpre("Верный формат значений обучения `{$total}`")){
	}else if(!$split = str_split($total)){ mpre("Ошибка разбивки строки на символы");
	}else if(!$_BM0_ITOG = call_user_func(function($split, $BM0_ITOG = []) use($bm0_clump){// mpre("Итоговые значения", $split);
			foreach($split as $name=>$value){
				if(!$bm0_itog = fk("bm0-itog", $w = ['clump_id'=>$bm0_clump['id'], "name"=>$name], $w += ['value'=>$value], $w += ['hide'=>0])){ mpre("Ошибка установки знака");
//				}else if(!$bm0_itog = fk('bm0-itog', ['id'=>$bm0_itog['id']], null, ['retry'=>($bm0_itog['retry']+1)])){ mpre("Ошибка обновления повторов значения");
				}else if($bm0_itog['value'] == $bm0_itog['val']){// mpre("Значения равны расчетным");
				}else{ $BM0_ITOG[$bm0_itog['id']] = $bm0_itog; }
			} return $BM0_ITOG;
		}, $split)){ mpre("Результат обучения не изменился `". implode(null, array_column($BM0_ITOG, 'val')). "`"); print(str_repeat("=", 160). "\n");
	}else if(qw("BEGIN;")){ mpre("Начало транзакции");
	}else if(!$BM0_CALC_SEQUENCE = rb('bm0-calc_sequence')){ mpre("Ошибка получения списка распиновки");
	}else if(!$learn= function($bm0_index, $_bm0_index = null, $value = null) use(&$learn, $bm0_clump, $BM0_ITOG, $BM0_CALC_POS, $BM0_CALC, $BM0_CALC_VAL, $BM0_CALC_SEQUENCE){ !mpre("{$bm0_index['id']}. Обучение морфа `{$value}`");
			/*if($bm0_index['retry'] >= $bm0_clump['retry']){ mpre("{$bm0_index['id']}. Элемент уже расчитывался");
			}else*/ if(!is_numeric($value) && !is_numeric($value = (int)!$bm0_index['value'])){ mpre("Ошибка расчета необходимого значения");
			}else if(!$bm0_index = fk('bm0-index', ['id'=>$bm0_index['id']], null, ['value'=>$value])){ mpre("{$bm0_index['id']}. Ошибка обновления значения обучения");
			}else if(!is_array($_bm0_calc_sequence = ($bm0_index['calc_sequence_id'] ? rb('bm0-calc_sequence', 'id', $bm0_index['calc_sequence_id']) : []))){ mpre("{$bm0_index['id']}. Ошибка выборки распиновки морфа");
			}else if(!is_array($bm0_dano = ($bm0_index['dano_id'] ? rb('bm0-dano', 'id', $bm0_index['dano_id']) : []))){ mpre("{$bm0_index['id']}. Ошибка выборки дано значения");
			}else if(!is_array($bm0_index_[$n = 'Старший'] = ($bm0_index['index_id'] ? rb('bm0-index', 'id', $bm0_index['index_id']) : []))){ mpre("{$bm0_index['id']}. Ошибка выборки морфа `{$n}`");
			}else if(!is_array($bm0_index_[$n = 'Младший'] = ($bm0_index['bm0-index'] ? rb('bm0-index', 'id', $bm0_index['bm0-index']) : []))){ mpre("{$bm0_index['id']}. Ошибка выборки морфа `{$n}`");
			}else if(!$empty = ['v1'=>null, 'v2'=>null, 'v3'=>null, 'v4'=>null]){ mpre("Ошибка задания пустых значений выборки");
			}else if(!$bm0_index = (array_filter($bm0_index_) ? $bm0_index : call_user_func(function($bm0_index, $value) use($_bm0_index, $bm0_index_, $bm0_clump, $bm0_dano, $BM0_CALC_POS){ mpre("{$bm0_index['id']}. Расширение морфа");
//					if(!$_BM0_INDEX = rb('bm0-index', 'clump_id', 'calc_pos_id', 'id', $bm0_clump['id'], '[0,NULL,]')){ mpre("Ошибка выборки всех морфов скопления");
					if(!$_BM0_INDEX = rb('bm0-index', 'clump_id', 'calc_pos_id', 'id', $bm0_clump['id'], '[0,NULL,]')){ mpre("Ошибка выборки всех морфов скопления");
//					}else if(!$_BM0_INDEX += qn("SELECT * FROM `mp_bm0_index` WHERE clump_id=". (int)$bm0_clump['id']. " AND id>=". (int)$bm0_index['id'])){ mpre("Ошибка выбора подходящего морфа");
					}else if(!$_BM0_INDEX = array_diff_key($_BM0_INDEX, [$bm0_index['id']=>$bm0_index])){ mpre("Ошибка исключения текущего элемента из списка доступных к расширению морфов");
					}else if(!$bm0_index_[$n = 'Старший'] = $bm0_index){ mpre("Ошибка установка нижестоящего морфа `{$n}`");
					}else if(!$bm0_dano_[$n = "Старший"] = rb('bm0-dano', 'id', get($bm0_index_, $n, 'dano_id'))){ mpre("Ошибка выборки дано `{$n}`");
//					}else if(!$bm0_index_[$n = 'Младший'] = first($_BM0_INDEX)){ mpre("Ошибка выборки подходящего для расширения элемента `{$n}`");
					}else if(!$bm0_index_[$n = 'Младший'] = call_user_func(function($_BM0_INDEX){
							if(!is_numeric($retry = max(array_column($_BM0_INDEX, 'retry')))){ mpre("Ошибка нахождения максимального значения повторов");
							}else if(!$_bm0_index = rb($_BM0_INDEX, 1, 'retry', $retry)){ die(!mpre("Ошибка нахождения подходящего морфа"));
							}else{// die(!mpre("Максимальное значение `{$retry}`", $_BM0_INDEX));
								return $_bm0_index;
							}
						}, $_BM0_INDEX)){ mpre("Ошибка выборки подходящего для расширения элемента `{$n}`");
					}else if(!$bm0_dano_[$n = "Младший"] = rb('bm0-dano', 'id', get($bm0_index_, $n, 'dano_id'))){ mpre("Ошибка выборки дано `{$n}`");
					}else if(!$bm0_calc_val = rb('bm0-calc_val', 'v1', 'v0', $bm0_index_['Старший']['val'], $bm0_index_['Младший']['val'])){ die(!mpre("{$bm0_index['id']}. Ошибка получения значения нижестоящих сигналов", $bm0_index_));
					}else if(!$bm0_calc_balance = rb('bm0-calc_balance', 'calc_val_id', 'val', $bm0_calc_val['id'], $value)){ mpre("Ошибка получения баланса нижестоящих сигналов");
					}else if(!$calc_pos_id = ((get($_bm0_index, 'calc_pos_id') == $bm0_calc_balance['calc_pos_id']) ? $bm0_calc_balance['bm0-calc_pos'] : $bm0_calc_balance['calc_pos_id'])){ mpre("Ошибка получения идентификатора сбалансированной позиции");
					}else if(!$bm0_calc_pos = rb($BM0_CALC_POS, 'id', $calc_pos_id)){ mpre("Ошибка выборки сбалансированной позиции нового морфа");
					}else if(!$form = $bm0_calc_pos['form']){ mpre("Строка формуля в позиции не задана");
					}else if(!$form = strtr($form, ['v1'=>$bm0_dano_[$n = 'Старший']['name']])){ mpre("Ошибка установка номера элемента `{$n}`");
					}else if(!$form = strtr($form, ['v0'=>$bm0_dano_[$n = 'Младший']['name']])){ mpre("Ошибка установка номера элемента `{$n}`");
					}else if(!$index = fk('bm0-index', null, ['clump_id'=>$bm0_clump['id'], 'dano_id'=>$bm0_dano['id'], 'val'=>$value, 'value'=>$value, 'calc_pos_id'=>$bm0_calc_pos['id'], 'calc_val_id'=>$bm0_calc_val['id'], 'retry'=>$bm0_clump['retry'], 'form'=>$form, 'index_id'=>$bm0_index_['Старший']['id'], 'bm0-index'=>$bm0_index_['Младший']['id']])){ mpre("Ошибка создания расширяемого элемента");
					}else if(!$bm0_index = fk('bm0-index', ['id'=>$bm0_index['id']], null, ['value'=>$bm0_dano['val']])){ mpre("Ошибка установки значения расширенного морфа");
//					}else if(!$index = fk('bm0-index', null, ['clump_id'=>$bm0_clump['id'], 'dano_id'=>$bm0_dano['id'], 'value'=>$bm0_dano['val'], 'val'=>$bm0_dano['val'], 'retry'=>$bm0_clump['retry'], 'form'=>$bm0_index['form']])){ mpre("Ошибка создания расширяемого элемента");
//					}else if(!$bm0_index = fk('bm0-index', ['id'=>$bm0_index['id']], null, ['calc_pos_id'=>$bm0_calc_pos['id'], 'calc_val_id'=>$bm0_calc_val['id'], 'val'=>$value, 'value'=>$value, 'index_id'=>$index['id'], 'bm0-index'=>$bm0_index_['Младший']['id'], 'retry'=>$bm0_clump['retry'], 'form'=>$form])){ mpre("Ошибка установки значения расширенного морфа");
					}else{ mpre("{$index['id']}=>{$bm0_index['id']}. Расширяем элемент [". implode(', ', array_column($bm0_index_, 'id')). "]");
							return $index;
//						return $bm0_index;
					}
				}, $bm0_index, $value))){ mpre("{$bm0_index['id']}. Ошибка расширения морфа");
			}else if(!$bm0_calc_sequence = call_user_func(function($_bm0_calc_sequence) use($value, $bm0_index, $empty, $bm0_index_, $BM0_CALC, $BM0_CALC_SEQUENCE){ mpre("{$bm0_index['id']}. Расчет обновленной распиновки");
					if(!$bm0_calc_val = rb('bm0-calc_val', 'id', $bm0_index['calc_val_id'])){ mpre("{$bm0_index['id']}. Ошибка выборки значения морфа", $bm0_index_);
					}else if(!$field = "v". ($bm0_calc_val['value']+1)){ mpre("Ошибка расчета изменившегося поля");
					}else if(!is_array($CALC = call_user_func(function($BM0_CALC) use($bm0_index, $empty, $_bm0_calc_sequence, $field, $value){
							if(!is_array($values = array_intersect_key($_bm0_calc_sequence, $empty))){ mpre("Ошибка получения списка знаков выборки");
							}else if(!is_array($vals = array_filter(array_map(function($val){ return is_numeric($val); }, $values)))){ mpre("Ошибка расчета старого значения");
							}else if(!$_BM0_CALC = call_user_func(function($vals) use($bm0_index, $BM0_CALC){
									if(!$vals){ return $BM0_CALC;
									}else if(!$VALS = array_map(function($key, $val){ return "`{$key}`=". (int)$val; }, array_keys($vals), $vals)){ mpre("Ошибка составления массива sql условий");
									}else if(!$sql = "SELECT * FROM `mp_bm0_calc_sequence` WHERE ". implode(" AND ", $VALS)){ mpre("Ошибка составления запроса на выборку распиновки");
									}else if(!$CALC_SEQUENCE = ($vals ? qn($sql) : $BM0_CALC_SEQUENCE)){ mpre("Ошибка выборки нового значения распиновки");
									}else if(!$_BM0_CALC = rb($BM0_CALC, 'id', 'id', rb($CALC_SEQUENCE, 'calc_id'))){ mpre("Ошибка выборки расчетов текущей распиновки");
									}else{ return $_BM0_CALC; }
								}, $vals)){ mpre("Ошибка получения списка калькуляции по массиву условий");
							}else{// mpre("{$bm0_index['id']}. Состояние распиновки",  $_BM0_CALC, $_bm0_calc_sequence, count($_BM0_CALC));
								return $_BM0_CALC;
							}
						}, $BM0_CALC))){ mpre("Ошибка получения возможных калькуляций распиновки");
					}else if(get($_bm0_calc_sequence, $field)){ return $_bm0_calc_sequence; mpre("Значение уже использовалось");
					}else if(!is_array($fields = array_intersect_key($_bm0_calc_sequence, $empty))){ mpre("Ошибка расчета значимых значений");
					}else if(!$fields = array_replace($empty, $fields)){ mpre("Ошибка формирования текущего фильтра выборки распиновки");
					}else if(!$fields = array_replace($fields, [$field=>$value])){ mpre("Ошибка формирования условий выборки распиновки");
					}else if(!$vals = array_map(function($key, $val){
							if(is_numeric($val)){ return "`{$key}`=". (int)$val;
							}else if(!$val){ return "`{$key}` IN ('', NULL)";
							}else{ mpre("Неустановленное значение параметров распиновки", $key, $val);
								return "`{$key}`='". $val. "'";
							}
						}, array_keys($fields), $fields)){ mpre("Ошибка получения значений запроса к БД");
					}else if(!$sql = "SELECT * FROM `mp_bm0_calc_sequence` WHERE ". implode(" AND ", $vals)){ mpre("Ошибка составления запроса на выборку распиновки");
					}else if(!$bm0_calc_sequence = ql($sql, 0)){ mpre("Ошибка выборки нового значения распиновки");
//					}else if(count($CALC) > 2){ mpre("Возвращаем новую распиновку")
					}else{ mpre("{$bm0_index['id']}. Найдена обновленная распиновки `{$bm0_calc_sequence['name']}`");
						return $bm0_calc_sequence;
					}
				}, $_bm0_calc_sequence)){ mpre("Ошибка расчета следующей распиновки");
			}else if((get($_bm0_calc_sequence, 'id') != $bm0_calc_sequence['id']) && (!$bm0_index = call_user_func(function($bm0_index) use($bm0_calc_sequence, $_bm0_calc_sequence, $empty, $BM0_CALC_POS, $BM0_CALC){ mpre("{$bm0_index['id']}. Запуск корректировки морфа");
					if(!$values = array_intersect_key($bm0_calc_sequence, $empty)){ mpre("Ошибка расчета условий выборки");
					}elseif(!$scale = array_filter(array_map(function($val){ return is_numeric($val); }, $values))){ mpre("{$bm0_index['id']}. Ошибка установки весомых значений для выборки возможных варианвтор распиновки");
					}elseif(!$vals = array_intersect_key($values, $scale)){ mpre("{$bm0_index['id']}. Ошибка расчета массива условий выборки");
					}elseif(!$VALS = array_map(function($key, $val){ return "`{$key}`=". (int)$val; }, array_keys($vals), $vals)){ mpre("{$bm0_index['id']}. Ошибка составления условий запроса");
					}elseif(!$sql = "SELECT * FROM `mp_bm0_calc_sequence` WHERE ". implode(" AND ", $VALS)){ mpre("{$bm0_index['id']}. Ошибка составления запроса поиска возможных вариантов распиновки");
					}elseif(!$BM0_CALC_SEQUENCE = qn($sql)){ mpre("{$bm0_index['id']}. Ошибка выборки списка возможных распиновок");
//					}elseif(!$BM0_CALC_SEQUENCE = fk("bm0-calc_sequence", $vals)){ mpre("Ошибка выборки возвожных распиновок");
					}else if(!$BM0_CALC = rb($BM0_CALC, 'id', 'id', rb($BM0_CALC_SEQUENCE, 'calc_id'))){ mpre("{$bm0_index['id']}. Ошибка выборки возможных калькуляций");
					}else if(!$BM0_CALC_ORIENT = rb("bm0-calc_orient", 'id', 'id', rb($BM0_CALC, 'calc_orient_id'))){ mpre("{$bm0_index['id']}. Ошибка поиска подходящей ориентации");
					}else if(!$_BM0_CALC_POS = rb($BM0_CALC_POS, 'id', 'id', rb($BM0_CALC_ORIENT, $f = 'calc_pos_id'))){ mpre("Ошибка нахождения позиций `{$f}`");
					}else if(get($_BM0_CALC_POS, $bm0_index['calc_pos_id'])){ mpre("{$bm0_index['id']}. Позиция морфа найдена среди основных позиций"); return $bm0_index;
					}else if(!$_BM0_CALC_POS = rb($BM0_CALC_POS, 'id', 'id', rb($BM0_CALC_ORIENT, $f = 'bm0-calc_pos'))){ mpre("Ошибка нахождения позиций `{$f}`");
					}else if(get($_BM0_CALC_POS, $bm0_index['calc_pos_id'])){ mpre("{$bm0_index['id']}. Позиция морфа найдена среди дополнительных позиций"); return $bm0_index;
					}else if(!$bm_calc_pos = rb($BM0_CALC_POS, 'id', $bm0_index['calc_pos_id'])){ mpre("{$bm0_index['id']}. Ошибка нахождения текущей позиции морфа");
					}else if(!$index = fk('bm0-index', ['id'=>$bm0_index['id']], null, ['calc_pos_id'=>$bm_calc_pos['calc_pos_id']])){ mpre("Ошибка установки морфу полярной позиции");
					}else{ mpre("{$bm0_index['id']}. Изменение позиции с `{$bm0_index['calc_pos_id']}` на `{$bm_calc_pos['calc_pos_id']}`");
						return $index;
					}
				}, $bm0_index))){ die(!mpre("{$bm0_index['id']}. Ошибка корректировки позиции элемента"));
			}else if(!$bm0_calc = rb($BM0_CALC, 'calc_pos_id', 'calc_val_id', $bm0_index['calc_pos_id'], $bm0_index['calc_val_id'])){ mpre("Ошибка выборки расчета морфа");
			}else if(!mpre("{$bm0_index['id']}. Калькуляция морфа value: {$bm0_index['val']} расчет: {$bm0_calc['val']}")){
			}else if(($bm0_index['value'] != $bm0_calc['val']) /*(get($_bm0_calc_sequence, 'id') == $bm0_calc_sequence['id'])*/ && (!$bm0_index = call_user_func(function($bm0_index) use($learn, $_bm0_index, &$bm0_index_, $BM0_CALC, $BM0_CALC_VAL){ mpre("{$bm0_index['id']}. Значение распиновки не изменилось (ищем виновных)");
					if(!$bm0_calc = rb($BM0_CALC, 'calc_pos_id', 'calc_val_id', $bm0_index['calc_pos_id'], $bm0_index['calc_val_id'])){ mpre("Ошибка нахождения калькуляции морфа");
					}else if(!$bm0_calc_amend = rb('bm0-calc_amend', 'id', $bm0_calc['calc_amend_id'])){ mpre("Ошибка выборки изменений");
//					}else if(!mpre("{$bm0_index['id']}. Начало выбора", $bm0_calc_amend)){
					}else if((0 /* Оба */ == $bm0_calc_amend['value']) && (!$_bm0_index_ = call_user_func(function($bm0_index_) use($bm0_index, $bm0_calc_amend){ mpre("{$bm0_index['id']}. Начало изменения `{$bm0_calc_amend['name']}`");
							if(!$_bm0_index_ = $bm0_index_){ mpre("Ошибка определения морфа по направлению `{$next}`");
							}else{ mpre("{$bm0_index['id']}. Изменение `{$bm0_calc_amend['name']}`"); return $_bm0_index_; }
						}, $bm0_index_))){ mpre("Ошибка расчета массива направлений `{$bm0_calc_amend['name']}`");
					}else if((1 /* Младший */ == $bm0_calc_amend['value']) && (!$_bm0_index_ = call_user_func(function($bm0_index_) use($bm0_index, $bm0_calc_amend){ mpre("{$bm0_index['id']}. Начало изменения `{$bm0_calc_amend['name']}`");
							if(!$next = 'Младший'){ mpre("Ошибка установки направления вины");
							}else if(!$_bm0_index_ = array_intersect_key($bm0_index_, [$next=>true])){ mpre("Ошибка определения морфа по направлению `{$next}`");
							}else{ mpre("{$bm0_index['id']}. Изменение `{$bm0_calc_amend['name']}`"); return $_bm0_index_; }
						}, $bm0_index_))){ mpre("Ошибка расчета массива направлений `{$bm0_calc_amend['name']}`");
					}else if((2 /* Старший */ == $bm0_calc_amend['value']) && (!$_bm0_index_ = call_user_func(function($bm0_index_) use($bm0_index, $bm0_calc_amend){ mpre("{$bm0_index['id']}. Начало изменения `{$bm0_calc_amend['name']}`");
							if(!$next = 'Старший'){ mpre("Ошибка установки направления вины");
							}else if(!$_bm0_index_ = array_intersect_key($bm0_index_, [$next=>true])){ mpre("Ошибка определения морфа по направлению `{$next}`");
							}else{ mpre("{$bm0_index['id']}. Изменение `{$bm0_calc_amend['name']}`"); return $_bm0_index_; }
						}, $bm0_index_))){ mpre("Ошибка расчета массива направлений `{$bm0_calc_amend['name']}`");
					}else if(empty($_bm0_index_) && !$bm0_index_[$n = 'Младший']['index_id'] && (!$_bm0_index_ = call_user_func(function($bm0_index_, $next) use($bm0_index, $bm0_calc_amend, $n){ mpre("{$bm0_index['id']}. Начало изменения `{$n}` пустой выбираем его");
							if(!$_bm0_index_ = array_intersect_key($bm0_index_, [$next=>true])){ mpre("Ошибка определения морфа по направлению `{$next}`");
							}else{ mpre("{$bm0_index['id']}. Изменение `{$bm0_calc_amend['name']}`"); return $_bm0_index_; }
						}, $bm0_index_, $n))){ mpre("Ошибка расчета массива направлений `{$bm0_calc_amend['name']}`");
					}else if(empty($_bm0_index_) && !$bm0_index_[$n = 'Старший']['index_id'] && (!$_bm0_index_ = call_user_func(function($bm0_index_, $next) use($bm0_index, $bm0_calc_amend, $n){ mpre("{$bm0_index['id']}. Начало изменения `{$n}` пустой выбираем его");
							if(!$_bm0_index_ = array_intersect_key($bm0_index_, [$next=>true])){ mpre("Ошибка определения морфа по направлению `{$next}`");
							}else{ mpre("{$bm0_index['id']}. Изменение `{$bm0_calc_amend['name']}`"); return $_bm0_index_; }
						}, $bm0_index_, $n))){ mpre("Ошибка расчета массива направлений `{$bm0_calc_amend['name']}`");
					}else if(empty($_bm0_index_) && (3 /* Любой */ == $bm0_calc_amend['value']) && (!$_bm0_index_ = call_user_func(function($bm0_index_) use($bm0_index, $bm0_calc_amend){ mpre("{$bm0_index['id']}. Начало изменения `{$bm0_calc_amend['name']}`");
							if(!$next = ($bm0_index['value'] ? 'Младший' : "Старший")){ mpre("Ошибка установки направления вины");
							}else if(!$_bm0_index_ = array_intersect_key($bm0_index_, [$next=>true])){ mpre("Ошибка определения морфа по направлению `{$next}`");
							}else{ mpre("{$bm0_index['id']}. Изменение `{$bm0_calc_amend['name']}`"); return $_bm0_index_; }
						}, $bm0_index_))){ mpre("Ошибка расчета массива направлений `{$bm0_calc_amend['name']}`");
					}else if(empty($_bm0_index_)){ mpre("Ошибка расчета элементов по изминениям");
//					}else if(!mpre("{$bm0_index['id']}. Выборанные значения", $bm0_calc_amend, $_bm0_index_)){
//					}else if(!mpre("{$bm0_index['id']}. Выбранные элементы для расширения", $bm0_index, $bm0_calc, $bm0_calc_amend, $_bm0_index_, $bm0_index_)){
					}else if(!$bm0_index_ = array_map(function($index) use($learn, &$bm0_index_, $bm0_index, $_bm0_index, $_bm0_index_, $BM0_CALC_VAL){ mpre("{$bm0_index['id']}. Расчет нижестоящего морфа");
							if(!$name = array_search($index, $bm0_index_)){ mpre("Ошибка расчета имени элемента");
							}else if(!get($_bm0_index_, $name)){ mpre("{$bm0_index['id']}. Морф не отмечен для обучения `{$name}`"); return $index;
							}else if(!$_name = first(array_diff(array_keys($bm0_index_), [$name]))){ mpre("Ошибка нахождения имени родственного морфа");
//							}else if(!$__index = first(array_diff_key($bm0_index_, [$name=>false]))){ mpre("Ошибка определения другого родственного морфа `{$name}`", $bm0_index_);
							}else if(!is_numeric($value = (int)!$index['val'])){ mpre("Ошибка определения значения");
							}else if(!mpre("{$bm0_index['id']}. Переход к нижестоящему морфу `{$name}`")){
							}else if(!$bm0_index_[$name] = $_index = call_user_func($learn, $index, $bm0_index_[$_name])){ mpre("Ошибка получения расчета нижестоящего обучаемого элемента");
							}else if(($_index['id'] != $index['id']) && (!$_index = call_user_func(function($bm0_index) use($index, $_index, $name, $BM0_CALC_VAL, $bm0_index_){ mpre("{$bm0_index['id']}. Подменяем изменившийся морф `{$name}`");
									if(!$fields = ['Младший'=>'bm0-index', 'Старший'=>'index_id']){ mpre("Ошибка создания массива ассоциаций имен и полей таблицы");
									}else if(!$field = get($fields, $name)){ mpre("Ошибка расчета поля обновления `{$name}`", $fields);
									}else if(!$calc_val = rb($BM0_CALC_VAL, "v1", "v0", $bm0_index_['Старший']['val'], $bm0_index_['Младший']['val'])){ mpre("Ошибка выборки нового значения морфа");
//									}else if(!$bm0_calc_pos = rb('bm0-calc_pos', 'id', $bm0_index['calc_pos_id'])){ mpre("Ошибка выборки позиции текущего элемента");
									}else if(!$bm0_index = fk("bm0-index", ['id'=>$bm0_index['id']], null, [$field=>$_index['id'], 'calc_val_id'=>$calc_val['id']])){ mpre("{$bm0_index['id']}. Ошибка обновления изменившегося морфа");
									}else{ mpre("{$index['id']}=>{$_index['id']} Обновление нижестоящего морфа");
										return $_index;
									}
								}, $bm0_index))){ mpre("Ошибка обновления морфа");
							}else{ mpre("{$index['id']}. Расширение морфа `{$name}`");
								return $_index;
							}
						}, $bm0_index_)){ mpre("Ошибка перебора расширяемых морфов", $_bm0_index_);
					}else{// mpre("{$bm0_index['id']}. Расширение морфа", $bm0_index_);
						return $bm0_index;
					}
				}, $bm0_index))){ mpre("Ошибка расчета виновного элемента");
			}else if(!is_array($bm0_calc_val = (array_filter($bm0_index_) ? rb('bm0-calc_val', "v1", "v0", get($bm0_index_, 'Старший', 'val'), get($bm0_index_, 'Младший', 'val')) : []))){ mpre("Ошибка выборки значения текущего морфа");
			}else if(!is_array($bm0_calc = (($bm0_index['calc_pos_id'] && $bm0_calc_val) ? rb('bm0-calc', 'calc_pos_id', 'calc_val_id', $bm0_index['calc_pos_id'], $bm0_calc_val['id']) : []))){ mpre("Ошибка расчета морфа");
			}else if(!is_numeric($value = get($bm0_calc, 'val') ?: $bm0_index['value'])){ mpre("Ошибка расчета знака морфа");
			}else if(!is_numeric($retry = (($bm0_index['value'] == $value) ? (int)$bm0_index['retry'] : (int)$bm0_clump['retry']))){ mpre("Ошибка расчета количества повторов");
			}else if(!$form = call_user_func(function($bm0_index) use($bm0_index_, $BM0_CALC_POS){// mpre("{$bm0_index['id']}. Расчет формы морфа", $bm0_index, $bm0_index_);
					if(!array_filter($bm0_index_)){ return $bm0_index['form'];
						
					}else if(!$bm0_calc_pos = rb($BM0_CALC_POS, 'id', $bm0_index['calc_pos_id'])){ mpre("Ошибка выборки позиции текущего элемента");
					}else if(!$form = $bm0_calc_pos['form']){ mpre("Форма позиции не установлена");
					}else if(!$form = strtr($form, ['{v1}'=>$bm0_index_['Старший']['form']])){ mpre("Ошибка установки формы старшего элемента");
					}else if(!$form = strtr($form, ['{v0}'=>$bm0_index_['Младший']['form']])){ mpre("Ошибка установки формы старшего элемента");
					}else{ mpre("{$bm0_index['id']}. Расчетная форма `{$form}`"); return $form; }
				}, $bm0_index)){ mpre("Ошибка расчета формы элемента");
			}else if(!$bm0_index = fk('bm0-index', ['id'=>$bm0_index['id']], null, [/*'calc_val_id'=>get($bm0_calc_val, 'id'),*/ 'val'=>$value, 'value'=>$value /* На случаи изменения морфа */, 'calc_sequence_id'=>$bm0_calc_sequence['id'], 'retry'=>$retry, 'form'=>$form])){ mpre("Ошибка обновления распиновки элемента");
			}else{ mpre("{$bm0_index['id']}. Обучение `{$value}` {$bm0_calc_sequence['name']}"); return $bm0_index; }
		}){ mpre("Ошибка создания функции обучения");
	}else if(!$_BM0_ITOG = array_filter(array_map(function($_bm0_itog) use($learn, $bm0_clump, $BM0_ITOG, $_BM0_ITOG){
			if(!$_bm0_itog = call_user_func(function($bm0_itog) use($learn, $bm0_clump){// mpre($_bm0_itog);
					# Если морф по итогу не найден добавляем совпавший по значению или сбалансированный результат из двух
					if(!$bm0_index = call_user_func(function($bm0_itog) use($bm0_clump){// mpre("Первоначальная связь");
							if($bm0_index = rb('bm0-index', 'id', $bm0_itog['index_id'])){ return $bm0_index;
							}else if(!$_bm0_index = rb('bm0-index', 1, 'clump_id', $bm0_clump['id'])){ mpre("Ошибка Первоначальная связь по значению", $_bm0_index);
							}else if(!$bm0_itog = fk('bm0-itog', ['id'=>$bm0_itog['id']], null, ['index_id'=>$_bm0_index['id'], 'form'=>$_bm0_index['form']])){ mpre("Ошибка установки в итог связь с морфом");
							}else{ mpre("{$_bm0_index['id']}. Создание первоначальной двойной связи"); return $_bm0_index; }
						}, $bm0_itog)){ mpre("Ошибка поиска/создания первоначальной связи");
					# Обучаем все изменившиеся морфы
					}else if(!$_bm0_index = call_user_func(function($bm0_index) use($learn, $bm0_clump, $bm0_itog){// mpre("Обучение", $bm0_itog);
							if(!$_bm0_index = call_user_func($learn, $bm0_index, null, $bm0_itog['value'])){ mpre("Ошибочный тип возвращенного значения функции обучения");
							}else if(!$bm0_itog = fk('bm0-itog', ['id'=>$bm0_itog['id']], null, ['val'=>$_bm0_index['val'], 'index_id'=>$_bm0_index['id'], 'form'=>$_bm0_index['form']])){ mpre("Ошибка обновления значения позиции");
							}else if(!$bm0_clump_history = call_user_func(function($bm0_clump) use($bm0_index, $_bm0_index, $bm0_itog){
									if(!is_numeric($val = ($bm0_index['value'] == $bm0_itog['value'] ? 0 : 1))){ mpre("Ошибка расчета обучения");
									}else if(!$bm0_clump_history = fk('bm0-clump_history', ['id'=>$bm0_clump['clump_history_id']], null, ['learn'=>$bm0_itog['value'], 'calc'=>$_bm0_index['value'], 'val'=>$val])){ mpre("Ошибка выборки истории скопления");
									}else if(!$bm0_clump_shot = fk('bm0-clump_shot', ['id'=>$bm0_clump_history['clump_shot_id']], null, ['val'=>$bm0_clump_history['val']])){ mpre("Ошибка добавления попытки обучения");
									}else{ return $bm0_clump_history; }
								}, $bm0_clump)){
							}else{// mpre("{$bm0_index['id']}. Корневой морф", $bm0_itog);
								return $_bm0_index;
							}
						}, $bm0_index)){ die(!mpre("Ошибка обучения исходящего знака"));
					}else{ return $bm0_itog; }
				}, $_bm0_itog)){ die(!mpre("Ошибка обучения выходного знака"));
			}else{ return $_bm0_itog; }
		}, $_BM0_ITOG))){ mpre("Измененных значений не найдно");
	}else if(qw("COMMIT;")){ mpre("Окончание транзакции");
	# Расчет результата исторических данных
	}else if('rec' != get($argv, 4)){ mpre("Перерасчет `rec` не задан `{$bm0_clump['name']}`");
		 print(str_repeat("=", 160). "\n");
	}else if(!mpre("Расчет истории")){
	}else if(!$BM0_CLUMP_HISTORY = rb('bm0-clump_history', 'clump_id', 'id', $bm0_clump['id'])){ mpre("История перерасчета не найдена", $bm0_clump);
	}else if(!array_filter(array_map(function($bm0_clump_history) use($arg, $argv, $bm0_clump){
			if(!$cmd = "php -f modules/{$arg['modpath']}/sh/". basename($argv[0]). " bin {$bm0_clump_history['dano']}"){ mpre("Установка команды запуска");
			}else if(!mpre("Запуск обучения выходных знаков `{$cmd}`")){
			}else if(passthru($cmd)){ mpre("Выполнение команды");
			}else{ return $bm0_clump_history; }
		}, $BM0_CLUMP_HISTORY))){ mpre("Ошибка расчета");
	}else if(!$cmd = "php -f modules/{$arg['modpath']}/sh/". basename($argv[0]). " bin {$vals}"){ mpre("Установка команды запуска");
	}else if(!mpre("Запуск обучения выходных знаков `{$cmd}`")){
	}else if(passthru($cmd)){ mpre("Выполнение команды");
	}else{ mpre("Перерасчет окончен `{$bm0_clump['name']}`"); print(str_repeat("=", 160). "\n"); }
}else if(array_search($cmd["values"] = "Значения фиксированных типов данными", $cmd) == get($argv, 1)){// pre("Метод", get($argv, 1));
}else{ mpre($cmd);}
