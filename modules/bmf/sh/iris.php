<?

if(!file_exists($func_file = "phar://../../../index.phar/include/func.php")){ mpre("ОШИБКА подключения файла функций {$func_file}");
}else if(!require_once($func_file)){ mpre("ОШИБКА подключения файла функций {$func_file}");
}elseif(!$conf['user']['gid'][1] = "Администратор"){ mpre("ОШИБКА уставоки пользователя администратором");
}elseif(!file_exists($conf_file = "/var/www/192.168.1.6/include/config.php")){ mpre("ОШИБКА конфигурационный файл не найден {$conf_file}");
}elseif(!require_once($conf_file)){ mpre("ОШИБКА подключения конфигурационного файла {$conf_file}");
}else if(!file_exists($htdb = "/var/www/192.168.1.6/.htdb")){ mpre("ОШИБКА файла базы данных не найден {$htdb}");
}elseif(!$conf['db']['conn'] = conn("{$conf["db"]["type"]}:{$htdb}")){ mpre("Ошибка подключения БД попробуйте установить `apt install php-sqlite3`");
}else if(!$bmf_clump = rb("bmf-clump", "name", $w = "[Ирис]")){ mpre("ОШИБКА скопление не найдено {$w}");
}else if(!$clump_db = "/var/www/192.168.1.6/modules/bmf/db/{$bmf_clump["id"]}.sqlite"){ mpre("ОШИБКА файла данных скопления не найден");
}else if(!qw($s = "ATTACH DATABASE '{$clump_db}' AS clump")){ mpre("ОШИБКА подключения файла базы скопления {$s}");
}elseif(!$file = file($f = "iris.csv")){ mpre("ОШИБКА откртия файла `{$f}`");
}elseif(!$CSV = array_map(function($csv){ return str_getcsv($csv); }, $file)){ mpre("ОШИБКА парсинга csv строк");
}elseif(!$titles = array_flip(array_shift($CSV))){ mpre("Список заголовков полей");
}elseif(!$_data = ["dano"=>[ // "Поле csv"=>"Параметр в биморфе"
			"sepal_length"=>"ВысотаЧашелистика",
			"sepal_width"=>"ШиринаЧашелистика",
			"petal_length"=>"ВысотаЛепестка",
			"petal_width"=>"ШиринаЛепестка",
		], "itog"=>[
			"species"=>"Семейство",
		]
	]){ mpre("ОШИБКА структуры");
}elseif(!$DATA = array_filter(array_map(function($csv) use($titles, $_data){ // Формирование списка данных запроса
		if(!$_dano = get($_data, 'dano')){ mpre("ОШИБКА не задан список исходных полей");
		}else if(!$_dano_fields = array_flip(array_intersect_key($titles, $_dano))){ mpre("ОШИБКА получения списка полей дано");
		}else if(!$_dano_data = array_intersect_key($csv, $_dano_fields)){ mpre("ОШИБКА получения значений дано");
		}else if(!$_dano_titles = array_intersect_key($_dano, $titles)){ mpre("ОШИБКА получения списка заголовков дано");
		}else if(!$dano = array_combine($_dano_titles, $_dano_data)){ mpre("ОШИБКА комбинирования данных дано");
		}elseif(!$_itog = get($_data, 'itog')){ mpre("ОШИБКА не задан список итоговых полей");
		}else if(!$_itog_fields = array_flip(array_intersect_key($titles, $_itog))){ mpre("ОШИБКА получения списка полей дано");
		}else if(!$_itog_data = array_intersect_key($csv, $_itog_fields)){ mpre("ОШИБКА получения значений дано");
		}else if(!$_itog_titles = array_intersect_key($_itog, $titles)){ mpre("ОШИБКА получения списка заголовков дано");
		}else if(!$itog = array_combine($_itog_titles, $_itog_data)){ mpre("ОШИБКА комбинирования данных дано");
		}else if(!$data = ["dano"=>$dano, 'itog'=>$itog]){ mpre("ОШИБКА формирования записи данных");
		}else{ //mpre($titles, $csv, $dano, $itog, $data);
			return $data;
		}
	}, $CSV))){ mpre("ОШИБКА формирования обучающих данных");
}else if(!$species = ["setosa", "versicolor", "virginica"]){ mpre("ОШИБКА списка видов цветов");
}else if(!$DATA = array_filter(array_map(function($data) use($species){
		if(!$dano = get($data, 'dano')){ mpre("ОШИБКА получения исходных значений");
		}else if(!$data["dano"] = array_map(function($val){ return "". $val*10; }, $dano)){ mpre("ОШИБКА коррекции исходных значений");
		}else if(!$itog = get($data, 'itog')){ mpre("ОШИБКА получения исходных значений");
		}else if(!$value = get($itog, 'Семейство')){ mpre("ОШИБКА получения значения итога", $itog);
		}else if(!is_numeric($val = array_search($value, $species))){ mpre("ОШИБКА получения номера списка цветов", $species, $value);
		}else if(!is_numeric($itog['Семейство'] = "". $val)){ mpre("ОШИБКА установки значения вида цветов");
		}else if(!$data['itog'] = $itog){ mpre("ОШИБКА обновления итоговых данных");
		}else{ return $data; }
	}, $DATA))){ mpre("ОШИБКА пост обработки входящих данных");
/*}else if(!$HISTORY = array_map(function($nn, $data) use($bmf_clump){ // Сохранение истории
		if(!$clump_history = fk("bmf-clump_history", $w = ["clump_id"=>$bmf_clump["id"], "name"=>$nn], $w, $w)){ mpre("ОШИБКА добавления нового пункта истории");
		}else if(!$dano = get($data, "dano")){ mpre("ОШИБКА выборки исходных данных");
		}else if(!$CLUMP_HISTORY_DANO = array_map(function($name, $value) use($bmf_clump, $clump_history){ // Сохраняем исходные данные
				if(!$dano_values = rb("bmf-dano_values", "clump_id", "name", $bmf_clump["id"], $w = "[{$name}]")){ mpre("ОШИБКА получения значения дано {$w}");
				}else if(!$clump_history_dano = fk("bmf-clump_history_dano", $w = ["clump_history_id"=>$clump_history["id"], "dano_values_id"=>$dano_values["id"]], $w+= ["value"=>$value], $w)){ mpre("ОШИБКА добавления значения дано истории");
				}else{ return "{$dano_values["name"]}=>{$value}"; }
			}, array_keys($dano), $dano)){ mpre("ОШИБКА сохранения исходных данных");
		}else if(!$itog = get($data, "itog")){ mpre("ОШИБКА выборки итоговых данных");
		}else if(!$CLUMP_HISTORY_ITOG = array_map(function($name, $value) use($bmf_clump, $clump_history){ // Сохраняем исходные данные
				if(!$itog_values = rb("bmf-itog_values", "clump_id", "name", $bmf_clump["id"], $w = "[{$name}]")){ mpre("ОШИБКА получения значения итог {$w}");
				}else if(!$clump_history_itog = fk("bmf-clump_history_itog", $w = ["clump_history_id"=>$clump_history["id"], "itog_values_id"=>$itog_values["id"]], $w+= ["value"=>$value], $w)){ mpre("ОШИБКА добавления значения дано истории");
				}else{ return "{$itog_values["name"]}=>{$value}"; }
			}, array_keys($itog), $itog)){ mpre("ОШИБКА сохранения исходных данных");
		}else{ mpre("Добавление истории {$clump_history["id"]}", $CLUMP_HISTORY_DANO, $CLUMP_HISTORY_ITOG);
		}
	}, array_keys($DATA), $DATA)){ mpre("ОШИБКА сохранения данных в историю");*/
/*}else if(!$DATA = array_values(array_filter(array_map(function($nn, $data){
		if($nn%5){ //mpre("Скидыванем данные");
		}else{ return $data; }
	}, array_keys($DATA), $DATA)))){ mpre("ОШИБКА прореживания данных");*/
}else if(!$json = json_encode($DATA, 3|256/*Русские символы*/)){ mpre("ОШИБКА формирования строки данных");
}else if(!$cmd = "echo '{$json}' | cpp/bmf 9"){ mpre("ОШИБКА формирования запуска обучения");
}else{ //mpre($json, $cmd);
	system($cmd);
}
