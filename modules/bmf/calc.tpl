<h1>Решения</h1>
<? if(!$BM0_CALC = rb('bmf-calc')): mpre("Ошибка выборки калькуляции") ?>
<? elseif(!$BM0_CALC_SOLUTIONS = rb('bmf-calc_solutions', 'hide', 'id', 0)): mpre("Ошибка выбора решений") ?>
<?// elseif(!usort($BM0_CALC_SOLUTIONS, function($one, $two){ return $one['calc_pos_id'] < $two['calc_pos_id']; })): mpre("Ошибка сортировки") ?>
<? elseif(!$BM0_CALC_POS = rb('bmf-calc_pos')): mpre("Ошибка выорки списка позиций") ?>
<? elseif(!$BM0_CALC_SOLUTIONS_[$n = '00'] = rb($BM0_CALC_SOLUTIONS, 'bmf-calc_pos', 'bmf_calc_pos', 'id', '', '')): mpre("Ошибка выбора решений `{$n}`") ?>
<?// elseif(!mpre($BM0_CALC_SOLUTIONS_[$n = '00'])): ?>
<? elseif(!$BM0_CALC_[$n = '00'] = rb($BM0_CALC, 'id', 'id', rb($BM0_CALC_SOLUTIONS_[$n], 'calc_id'))): mpre("Ошибка выборки калькуляции `{$n}`") ?>
<? elseif(!$BM0_CALC_SOLUTIONS_[$n = '01'] = rb($BM0_CALC_SOLUTIONS, 'bmf-calc_pos', 'bmf_calc_pos', 'id', '', $BM0_CALC_POS)): mpre("Ошибка выбора решений `{$n}`") ?>
<? elseif(!$BM0_CALC_[$n = '01'] = rb($BM0_CALC, 'id', 'id', rb($BM0_CALC_SOLUTIONS_[$n], 'calc_id'))): mpre("Ошибка выборки калькуляции `{$n}`") ?>
<? elseif(!$BM0_CALC_SOLUTIONS_[$n = '10'] = rb($BM0_CALC_SOLUTIONS, 'bmf-calc_pos', 'bmf_calc_pos', 'id', $BM0_CALC_POS, '')): mpre("Ошибка выбора решений `{$n}`") ?>
<? elseif(!$BM0_CALC_[$n = '10'] = rb($BM0_CALC, 'id', 'id', rb($BM0_CALC_SOLUTIONS_[$n], 'calc_id'))): mpre("Ошибка выборки калькуляции `{$n}`") ?>
<? elseif(!$BM0_CALC_SOLUTIONS_[$n = '11'] = rb($BM0_CALC_SOLUTIONS, 'bmf-calc_pos', 'bmf_calc_pos', 'id', $BM0_CALC_POS, $BM0_CALC_POS)): mpre("Ошибка выбора решений `{$n}`") ?>
<? elseif(!$BM0_CALC_[$n = '11'] = rb($BM0_CALC, 'id', 'id', rb($BM0_CALC_SOLUTIONS_[$n], 'calc_id'))): mpre("Ошибка выборки калькуляции `{$n}`") ?>
<? elseif(!$BM0_CALC_POS_[$n = 'bold'] = rb($BM0_CALC_POS, 'value', 'id', '[0,1]')): mpre("Ошибка расчета вращения `{$n}`") ?>
<? elseif(!$BM0_CALC_POS_[$n = 'normal'] = rb($BM0_CALC_POS, 'value', 'id', '[2,3]')): mpre("Ошибка расчета вращения `{$n}`") ?>
<? else: ?>
	<div class="table calc">
		<style>
			.table.calc strong {background-color:#eee; /*color:white;*/ border:1px solid gray; padding:0 3px;}
		</style>
		<div>
			<span>
				<ul>
					<? foreach($BM0_CALC_[$name = '10'] as $bmf_calc): ?>
						<? if(!is_array($BM0_CALC_['_11'] = rb($BM0_CALC_['11'], 'id', 'id', $bmf_calc['id']))): mpre("Ошибка нахождения нижних элементов") ?>
						<? elseif(!is_array($BM0_CALC_['_01'] = rb($BM0_CALC_['01'], 'id', 'id', $bmf_calc['id']))): mpre("Ошибка нахождения нижних элементов") ?>
						<? elseif(!$_BM0_CALC_SOLUTIONS = rb($BM0_CALC_SOLUTIONS_[$name], 'calc_id', 'id', $bmf_calc['id'])): mpre("Ошибка выборки решений калькуляции") ?>
						<? elseif(!$color = call_user_func(function($BM0_CALC_){
								if(!$BM0_CALC_['_01']){ return "red";
								}elseif(!$BM0_CALC_['_11']){ return "blue";
								}else{ return "inherit"; }
							}, $BM0_CALC_)): mpre("Ошибка формирования цвета калькуляции") ?>
						<? else:// mpre($spin) ?>
							<?// foreach($BM0_CALC as $calc): ?>
								<li>
									<strong style="color:<?=$color?>"><?=$bmf_calc['name']?></strong>
									<? foreach($_BM0_CALC_SOLUTIONS as $bmf_calc_solutions): ?>
										<? if(!$spin = call_user_func(function($bmf_calc) use($bmf_calc_solutions, $BM0_CALC_POS_){
												if(get($BM0_CALC_POS_, 'bold', $bmf_calc_solutions['calc_pos_id'])){ return 'bold'; mpre("Отображаем жирным шрифтом");
												}else{ return 'normal'; }
											}, $bmf_calc)): mpre("Ошибка расчета вращения") ?>
										<? else: ?>
											<span style="font-weight:<?=$spin?>;"><?=$bmf_calc_solutions['name']?></span>;
										<? endif; ?>
									<? endforeach; ?>
								</li>
							<?// endforeach; ?>
						<? endif; ?>
					<? endforeach; ?>
				</ul>
			</span>
			<span>
				<ul>
					<li>
						<? foreach($BM0_CALC_[$name = '00'] as $bmf_calc): ?>
							<? if(!$_BM0_CALC_SOLUTIONS = rb($BM0_CALC_SOLUTIONS_[$name], 'calc_id', 'id', $bmf_calc['id'])): mpre("Ошибка выборки решений калькуляции") ?>
							<? else: ?>
								<span>
									<strong><?=$bmf_calc['name']?></strong>
										<? foreach($_BM0_CALC_SOLUTIONS as $bmf_calc_solutions): ?>
											<? if(!$spin = call_user_func(function($bmf_calc) use($bmf_calc_solutions, $BM0_CALC_POS_){
													if(get($BM0_CALC_POS_, 'bold', $bmf_calc_solutions['calc_pos_id'])){ return 'bold'; mpre("Отображаем жирным шрифтом");
													}else{ return 'normal'; }
												}, $bmf_calc)): mpre("Ошибка расчета вращения") ?>
											<? else: ?>
												<span style="font-weight:<?=$spin?>; padding:0 3px; color:<?=($bmf_calc_solutions['hide'] ? "#ddd" : "black")?>"><?=$bmf_calc_solutions['name']?></span>;
											<? endif; ?>
										<? endforeach; ?>
								</span>
							<? endif; ?>
						<? endforeach; ?>
					</li>
				</ul>
				<hr />
				<ul style="margin-top:40px; width:400px;">
					<? foreach($BM0_CALC_[$name = '11'] as $bmf_calc): ?>
						<? if(!is_array($BM0_CALC_['_01'] = rb($BM0_CALC_['11'], 'bmf-calc_pos', 'id', 'id', '', $bmf_calc['id']))): mpre("Ошибка нахождения нижних элементов") ?>
						<? elseif(!is_array($BM0_CALC_['_10'] = rb($BM0_CALC_['10'], 'bmf_calc_pos', 'id', 'id', '', $bmf_calc['id']))): mpre("Ошибка нахождения нижних элементов") ?>
						<? elseif(!$_BM0_CALC_SOLUTIONS = rb($BM0_CALC_SOLUTIONS_[$name], 'calc_id', 'id', $bmf_calc['id'])): mpre("Ошибка выборки решений калькуляции") ?>
						<? elseif(!$color = call_user_func(function($BM0_CALC_){
								if(!$BM0_CALC_['_10'] || !$BM0_CALC_['_01']){ return "red";
								}else{ return "inherit"; }
							}, $BM0_CALC_)): mpre("Ошибка формирования цвета калькуляции") ?>
						<? else:// mpre($BM0_CALC_) ?>
							<li style="color:<?=$color?>">
								<strong><?=$bmf_calc['name']?></strong>
									<? foreach($_BM0_CALC_SOLUTIONS as $bmf_calc_solutions): ?>
										<? if(!$spin = call_user_func(function($bmf_calc) use($bmf_calc_solutions, $BM0_CALC_POS_){
												if(get($BM0_CALC_POS_, 'bold', $bmf_calc_solutions['calc_pos_id'])){ return 'bold'; mpre("Отображаем жирным шрифтом");
												}else{ return 'normal'; }
											}, $bmf_calc)): mpre("Ошибка расчета вращения") ?>
										<? else: ?>
											<span style="font-weight:<?=$spin?>; color:<?=($bmf_calc_solutions['hide'] ? "#ddd" : "black")?>"><?=$bmf_calc_solutions['name']?></span>;
										<? endif; ?>
									<? endforeach; ?>
							</li>
						<? endif; ?>
					<? endforeach; ?>
				</ul>
			</span>
			<span>
				<ul>
					<? foreach($BM0_CALC_[$name = '01'] as $bmf_calc): ?>
						<? if(!is_array($BM0_CALC_['_11'] = rb($BM0_CALC_['11'], 'id', 'id', $bmf_calc['id']))): mpre("Ошибка нахождения нижних элементов") ?>
						<? elseif(!is_array($BM0_CALC_['_10'] = rb($BM0_CALC_['10'], 'id', 'id', $bmf_calc['id']))): mpre("Ошибка нахождения нижних элементов") ?>
						<? elseif(!$_BM0_CALC_SOLUTIONS = rb($BM0_CALC_SOLUTIONS_[$name], 'calc_id', 'id', $bmf_calc['id'])): mpre("Ошибка выборки решений калькуляции") ?>
						<? elseif(!$color = call_user_func(function($BM0_CALC_){
								if(!$BM0_CALC_['_10']){ return "red";
								}elseif(!$BM0_CALC_['_11']){ return "blue";
								}else{ return "inherit"; }
							}, $BM0_CALC_)): mpre("Ошибка формирования цвета калькуляции") ?>
						<? else:// mpre($color) ?>
							<li>
								<strong style="color:<?=$color?>"><?=$bmf_calc['name']?></strong>
								<? foreach($_BM0_CALC_SOLUTIONS as $bmf_calc_solutions): ?>
									<? if(!$spin = call_user_func(function($bmf_calc) use($bmf_calc_solutions, $BM0_CALC_POS_){
											if(get($BM0_CALC_POS_, 'bold', $bmf_calc_solutions['calc_pos_id'])){ return 'bold'; mpre("Отображаем жирным шрифтом");
											}else{ return 'normal'; }
										}, $bmf_calc)): mpre("Ошибка расчета вращения") ?>
									<? else: ?>
										<span style="font-weight:<?=$spin?>; color:<?=($bmf_calc_solutions['hide'] ? "#ddd" : "black")?>"><?=$bmf_calc_solutions['name']?></span>;
									<? endif; ?>
								<? endforeach; ?>
							</li>
						<? endif; ?>
					<? endforeach; ?>
				</ul>
			</span>
		</div>
	</div>
<? endif; ?>
