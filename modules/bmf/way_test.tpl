<? if(!$CALC = rb("calc")): mpre("ОШИБКА выборки списка расчетов") ?>
<? elseif(!$WAY = rb("way")): mpre("ОШИБКА выборки списка маршрутов") ?>
<? elseif(!$WAY_TEST = rb("way_test")): mpre("ОШИБКА получения результатов тестов") ?>
<? elseif(!$clump = rb("clump", "hide", 0)): mpre("ОШИБКА получения текущего скопления") ?>
<? else: //mpre($WAY) ?>
	<div class="table">
		<style>
			.table { border-collapse:collapse; }
			.table > div > span { border:1px solid gray; }
		</style>
		<div class="th">
			<span>Расчет</span>
			<span>Морфов</span>
			<span>МинИт</span>
			<span>СрИт</span>
			<span>МаксИт</span>
			<span>МинМф</span>
			<span>СрМф</span>
			<span>МаксМф</span>
		</div>
		<? foreach($CALC as $calc): ?>
			<? if(!is_array($_WAY_TEST = rb($WAY_TEST, "calc_id", "id", $calc["id"]))): mpre("ОШИБКА выборки списка тестов") ?>
			<? elseif(!is_array($loop = array_column($_WAY_TEST, "loop"))): mpre("ОШИБКА получения списка итераций") ?>
			<? elseif(!is_numeric($loop_min = ($loop ? min($loop) : 0))): mpre("ОШИБКА расчета минимального количества итераций") ?>
			<? elseif(!is_numeric($loop_avg = number_format(($loop ? array_sum($loop)/count($loop) : 0), 2))): mpre("ОШИБКА расчета минимального количества итераций") ?>
			<? elseif(!is_numeric($loop_max = ($loop ? max($loop) : 0))): mpre("ОШИБКА расчета максимального количества итераций") ?>
			<? elseif(!is_array($index = array_column($_WAY_TEST, "index"))): mpre("ОШИБКА получения списка морфов") ?>
			<? elseif(!is_numeric($index_min = ($index ? min($index) : 0))): mpre("ОШИБКА расчета минимального количества итераций") ?>
			<? elseif(!is_numeric($index_avg = number_format(($index ? array_sum($index)/count($index) : 0), 2))): mpre("ОШИБКА расчета минимального количества итераций") ?>
			<? elseif(!is_numeric($index_max = ($index ? max($index) : 0))): mpre("ОШИБКА расчета максимального количества итераций") ?>
			<? elseif(!is_array($_way_test = rb($_WAY_TEST, "loop", 1000))): mpre("ОШИБКА выборки максимального количества итераций") ?>
			<? //elseif(!$name = strtr($calc["name"], ["0"=>"Лв.", "1"=>"Пр."])): mpre("ОШИБКА формирования имени расчета") ?>
			<? elseif(!$name = $calc["name"]): mpre("ОШИБКА формирования имени расчета") ?>
			<? else: ?>
				<div style="background-color:<?=($_way_test ? "#fcc" : "inherit")?>; font-weight:<?=($clump["calc_id"] == $calc["id"] ? "bold" : "inherit")?>;">
					<span>
						<a href="/bmf:admin/r:bmf-way_test?&where[calc_id]=<?=$calc["id"]?>"><?=$name?></a>
					</span>
					<span><?=count($loop)?></span>
					<span><?=$loop_min?></span>
					<span><?=$loop_avg?></span>
					<span><?=$loop_max?></span>
					<span><?=$index_min?></span>
					<span><?=$index_avg?></span>
					<span><?=$index_max?></span>
				</div>
			<? endif; ?>
		<? endforeach; ?>
	</div>
<? endif; ?>
