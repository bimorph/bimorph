<? $trigger_dano = <<<EOF
CREATE TRIGGER bm0_dano BEFORE UPDATE ON mp_bm0_dano WHEN (OLD.val != NEW.val)
BEGIN
	INSERT INTO `mp_bm0_calc_trigger` (time, name, dano_id, value, val) VALUES (datetime('now'), 'dano', NEW.id, NEW.value, NEW.val);
	UPDATE `mp_bm0_index` SET `val`=NEW.val WHERE `dano_id`=NEW.id;
END;
EOF;

$trigger_index_val = <<<EOF
CREATE TRIGGER bm0_index_val BEFORE UPDATE ON mp_bm0_index WHEN (OLD.val != NEW.val)
BEGIN
	INSERT INTO `mp_bm0_calc_trigger` (time, name, dano_id, value, val) VALUES (datetime('now'), 'index', NEW.id, NEW.value, NEW.val);
END;
EOF;

$trigger_index_value = <<<EOF
CREATE TRIGGER bm0_index_value BEFORE UPDATE ON mp_bm0_index WHEN (OLD.value != NEW.value)
BEGIN
	INSERT INTO `mp_bm0_calc_trigger` (time, name, dano_id, value, val) VALUES (datetime('now'), 'index', NEW.id, NEW.value, NEW.val);
END;
	
EOF;

$trigger_itog = <<<EOF
CREATE TRIGGER bm0_itog BEFORE UPDATE ON mp_bm0_itog
BEGIN
	INSERT INTO `mp_bm0_calc_trigger` (time, name, dano_id, value, val) VALUES (datetime('now'), 'itog', NEW.id, NEW.value, NEW.val);
END;
EOF;

?>

<h1>Установка структуры базы данных</h1>
<? if($BM0_DANO = rb('bm0-dano')): mpre("Таблица `mp_bm0_dano` не пуста") ?>
<? elseif(!$SQL[] = "DROP TRIGGER IF EXISTS mp_bm0_dano.bm0_dano;"): mpre("Удаление триггера если есть") ?>
<? elseif(!$SQL[] = "DROP TABLE IF EXISTS `mp_bm0_dano`;"): mpre("Удаление таблицы") ?>
<? elseif(!$SQL[] = "CREATE TABLE `mp_bm0_dano`(`id` INTEGER PRIMARY KEY, `time` INTEGER, `clump_id` INTEGER, `name` TEXT, `val` INTEGER ,`value` INTEGER, `tail` INTEGER);"): mpre("Создание страницы") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_dano-id` ON `mp_bm0_dano` (`id`);"): mpre("Ключ идентификатора") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_dano-clump_id` ON `mp_bm0_dano` (`clump_id`);"): mpre("Ключ скопления") ?>
<?// elseif(!$SQL[] = $trigger_dano): mpre("Триггер дано не задан") ?>
<? elseif(!$_SQL = array_map(function($sql){
		if(!mpre($sql)){ mpre("Уведомление");
		}elseif(qw($sql)){ mpre("Ошибка выполнения запроса");
		}else{ return $sql; }
	}, $SQL)): mpre("Ошибка выбополнения списка запросов") ?>
<? elseif($BM0_INDEX = rb('bm0-index')): mpre("Таблица `mp_bm0_index` не пуста") ?>

<? elseif($SQL = []): mpre("Обнуление списка запросов") ?>
<? elseif(!$SQL[] = "DROP TABLE IF EXISTS `mp_bm0_index`;"): mpre("Удаление таблицы") ?>
<? elseif(!$SQL[] = "CREATE TABLE `mp_bm0_index`(`id` INTEGER PRIMARY KEY,`time` INTEGER,`clump_id` INTEGER,`dano_id` INTEGER,`value` INTEGER,`val` INTEGER,`calc_sequence_id` INTEGER,`calc_pos_id` INTEGER,`calc_val_id` INTEGER,`index_id` INTEGER,`bm0-index` INTEGER,`retry` INTEGER,`form` TEXT, FOREIGN KEY (dano_id) REFERENCES mp_bm0_dano(id) ON DELETE RESTRICT ON UPDATE RESTRICT FOREIGN KEY (index_id) REFERENCES mp_bm0_index(id) ON DELETE CASCADE ON UPDATE CASCADE  FOREIGN KEY (`bm0-index`) REFERENCES mp_bm0_index(id) ON DELETE CASCADE ON UPDATE CASCADE);"): mpre("Создание страницы") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_index-id` ON `mp_bm0_index` (`id`);"): mpre("Ключ идентификатора") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_index-clump_id` ON `mp_bm0_index` (`clump_id`);"): mpre("Ключ скопления") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_index-dano_id` ON `mp_bm0_index` (`dano_id`);"): mpre("Ключ скопления") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_index-calc_sequence_id` ON `mp_bm0_index` (`calc_sequence_id`);"): mpre("Ключ скопления") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_index-calc_pos_id` ON `mp_bm0_index` (`calc_pos_id`);"): mpre("Ключ скопления") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_index-calc_val_id` ON `mp_bm0_index` (`calc_val_id`);"): mpre("Ключ скопления") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_index-index_id` ON `mp_bm0_index` (`index_id`);"): mpre("Ключ скопления") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_index-bm0-index` ON `mp_bm0_index` (`bm0-index`);"): mpre("Ключ скопления") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_index-retry` ON `mp_bm0_index` (`retry`);"): mpre("Ключ скопления") ?>
<? elseif(!$SQL[] = "DROP TRIGGER IF EXISTS mp_bm0_index.bm0_index_val;"): mpre("Удаление триггера если есть") ?>
<?// elseif(!$SQL[] = $trigger_index_val): mpre("Триггер морфов не задан") ?>
<? elseif(!$SQL[] = "DROP TRIGGER IF EXISTS mp_bm0_index.bm0_index_value;"): mpre("Удаление триггера если есть") ?>
<?// elseif(!$SQL[] = $trigger_index_value): mpre("Триггер морфов не задан") ?>
<? elseif(!$_SQL = array_map(function($sql){
		if(!mpre($sql)){ mpre("Уведомление");
		}elseif(qw($sql)){ mpre("Ошибка выполнения запроса");
		}else{ return $sql; }
	}, $SQL)): mpre("Ошибка выбополнения списка запросов") ?>

<? elseif($SQL = []): mpre("Обнуление списка запросов") ?>
<? elseif(!$SQL[] = "DROP TRIGGER IF EXISTS mp_bm0_itog.bm0_itog;"): mpre("Удаление триггера если есть") ?>
<? elseif(!$SQL[] = "DROP TABLE IF EXISTS `mp_bm0_itog`;"): mpre("Удаление таблицы") ?>
<? elseif(!$SQL[] = "CREATE TABLE `mp_bm0_itog`(`id` INTEGER PRIMARY KEY,`time` INTEGER,`clump_id` INTEGER,`index_id` INTEGER,`name` TEXT,`value` TEXT,`val` INTEGER,`form` TEXT, FOREIGN KEY (index_id) REFERENCES mp_bm0_index(id) ON DELETE RESTRICT ON UPDATE RESTRICT);"): mpre("Создание страницы") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_itog-id` ON `mp_bm0_itog` (`id`);"): mpre("Ключ идентификатора") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_itog-clump_id` ON `mp_bm0_itog` (`clump_id`);"): mpre("Ключ идентификатора") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_itog-index_id` ON `mp_bm0_itog` (`index_id`);"): mpre("Ключ идентификатора") ?>
<? elseif(!$SQL[] = "CREATE INDEX `bm0_itog-value` ON `mp_bm0_itog` (`value`);"): mpre("Ключ идентификатора") ?>
<?// elseif(!$SQL[] = $trigger_itog): mpre("Триггер морфов не задан") ?>
<? elseif(!$_SQL = array_map(function($sql){
		if(!mpre($sql)){ mpre("Уведомление");
		}elseif(qw($sql)){ mpre("Ошибка выполнения запроса");
		}else{ return $sql; }
	}, $SQL)): mpre("Ошибка выбополнения списка запросов") ?>
<? else:// mpre($sql) ?>
<? endif; ?>