<?

// Подключаем консоль
if(!$conf = call_user_func(function($conf = []){ // Подключение компонентов
		if(array_key_exists('conf', $GLOBALS)){ $conf = $GLOBALS["conf"]; // mpre("Используем соединение основной системы");
		}elseif(!$DIR = explode("/", __DIR__)){ print_r("Ошибка определения текущей директории");
		}elseif(!$offset = array_search($d = "2018.12.10", $DIR)){ print_r("Директория с проектом не найдена `{$d}`"); print_r($DIR);
		}elseif(!$folder = implode("/", array_slice($DIR, 0, $offset+1))){ print_r("Ошибка выборка директории");
		}elseif(!$dir = implode("/", array_slice($DIR, $offset+1))){ print_r("Ошибка выборка директории");
		}elseif(!chdir($folder)){ print_r("Ошибка установки директории `{$dir}`");
		}elseif(!$inc = function($file) use(&$conf, $dir){
			if(($f = realpath($file)) && include($f)){ return $f;
			}elseif(($f = "phar://index.phar/{$file}") && file_exists($f) && include($f)){ return $f;
			}else{ return pre(null, "Директория не найдена `{$file}`"); }
		}){ print_r("Функция подключения файлов");
		}elseif(!$conf['user']['gid'] = array(1=>"Администратор")){ mpre("Устанавливаем администратора");
		}elseif(!$inc($f = "include/func.php")){ print_r("Ошибка подключения `{$f}`");
		}elseif(!$short_open_tag = ini_get("short_open_tag")){ pre("Установите параметр `short_open_tag`\nsed -i \"s/short_open_tag = .*/short_open_tag = On/\" /etc/php/7.0/cli/php.ini");
		}elseif(!$inc($f = "include/config.php")){ pre("Ошибка подключения `{$f}`");
		}elseif(!$modpath = (isset($argv) ? basename(dirname(dirname(__FILE__))) : basename(dirname(__FILE__)))){ mpre("Ошибка вычисления имени модуля");
		}elseif(!$arg = ['modpath'=>$modpath, "fn"=>implode(".", array_slice(explode(".", basename(__FILE__)), 0, -1))]){ mpre("Установка аргументов");
		}elseif(isset($argv)){ pre("Запуск из веб интерфейса");
		}elseif(!$GLOBALS["conf"] = $conf){ mpre("ОШИБКА сохранеения данных в глобальной переменной");
		}elseif(!$conf['db']['conn'] = conn()){ mpre("Ошибка подключения БД попробуйте установить `apt install php-sqlite3`");
		}else{ mpre("Домашняя директория ". $folder);
		} return $conf;
	})){ mpre("ОШИБКА подключения компонентов");
}elseif(!$argv = call_user_func(function($argv = ""){
		if(array_key_exists('argv', $GLOBALS)){ $argv = $GLOBALS['argv'];
		}elseif(!$argv = array_merge([0=>basename(__FILE__)], (array)get($_GET, 'argv'))){ mpre("ОШИБКА получения аргументов адреса");
		}else{// mpre($argv);
		} return $argv;
	})){ mpre("ОШИБКА получения аргументов");
}elseif(!is_array($command = call_user_func(function($command = [], $cmd = "phpinfo", $name = "Отображение настроек системы и загруженых модулей") use($argv){ // Связывание новой таблицы и списка источников
		if(array_key_exists($cmd, $command)){ mpre("Дублирование команды {$cmd}");
		}else if(array_search($command[$cmd] = $name, $command) != get($argv, 1)){ //mpre("Пропускам выполнение команды {$cmd}");
		}else{ pre($name. " (". $cmd. ")");
			phpinfo();
		} return $command;
	}))){ mpre("ОШИБКА выполнения команды", $argv);
}elseif(!is_array($command = call_user_func(function($command, $cmd = "calc_bin", $name = "Загрузка информации о каналах") use($argv){ // Связывание новой таблицы и списка источников
		if(array_key_exists($cmd, $command)){ mpre("Дублирование команды {$cmd}");
		}else if(array_search($command[$cmd] = $name, $command) != get($argv, 1)){ //mpre("Пропускам выполнение команды {$cmd}");
		}else if(!$TEST = rb("bmf-test", "id", "id", (get($argv, 2) ?: true))){ mpre("ОШИБКА получения результатов теста");
		}else if(!$TEST = array_map(function($test){ // Расчет параметров
				if(!$calc_bin = get($test, "calc_bin")){ mpre("ОШИБКА получения двоичных данных");
				}else if(!$bin = str_split($calc_bin)){ mpre("ОШИБКА разбивки строки на параметры");
				}else if(!krsort($bin)){ mpre("ОШИБКА обратной сортировки параметров");
				}else if(!$bin = array_values($bin)){ mpre("ОШИБКА установки порядка ключей значений");
				}else if(!$keys = array_map(function($key){ return "f{$key}"; }, array_keys($bin))){ mpre("ОШИБКА получения ключей значений");
				}else if(!$vals = array_combine($keys, $bin)){ mpre("ОШИБКА совмещения ключей и значений");
				}else if(!is_string($group = substr($calc_bin, 0, 8))){ mpre("ОШИБКА получения имени группы", $calc_bin);
				}else if(!is_string($bmf_group = substr($calc_bin, 8))){ mpre("ОШИБКА получения имени группы", $calc_bin);
				}else if(!$test_group = fk("bmf-test_group", $w = ["name"=>$group], $w)){ mpre("ОШИБКА получения группы");
				}else if(!$bmf_test_group = fk("bmf-test_group", $w = ["name"=>$bmf_group], $w, $w)){ mpre("ОШИБКА получения группы");
				}else if(!$test_bin = fk("bmf-test_bin", $w = ["name"=>$calc_bin], $w+= ["calc_num"=>$test["calc_num"], "loop"=>$test["loop"], "index"=>$test["index"], "test_group_id"=>$test_group["id"], "bmf-test_group"=>$bmf_test_group["id"], "hide"=>($test["loop"] >= 100 ? "1" : "0")], $w)){ mpre("ОШИБКА получения двоичного расчета");
				}else if(!$test = fk("bmf-test", ["id"=>$test["id"]], null, $vals+= ["test_bin_id"=>$test_bin["id"]])){ mpre("ОШИБКА обновления значений в таблице");
				}else{ mpre($test);
				} return $test;
			}, $TEST)){ mpre("ОШИБКА расчета параметров");
		}else{ //mpre($TELEGRAM_PEER); //$me = $MadelineProto->get_self();
		} return $command;
	}, $command))){ pre($command. " (". $argv[1]. ")");
}else{ mpre("Список доступных параметров", $command);
}

