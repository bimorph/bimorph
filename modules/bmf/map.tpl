<? if(!$BMF_CALC_POS = rb('bmf-calc_pos')): mpre("Ошибка выборки списка позиций") ?>
<? elseif(!$BMF_CALC_VAL = rb('bmf-calc_val')): mpre("Ошибка выборки списка значений") ?>
<? elseif(!$BMF_CALC = rb('bmf-calc')): mpre("Ошибка выборки расчетов") ?>
<? else: ?>
	<? foreach($BMF_CALC_POS as $bmf_calc_pos): ?>
		<? if(!$calc = rb($BMF_CALC, 'id', $bmf_calc_pos['calc_id'])): mpre("Ошибка результата расчета") ?>
		<? elseif(!$bmf_calc_solutions_type = rb('bmf-calc_solutions_type', 'name', $w = '[Верх]')): mpre("Ошибка выбора типа решения `{$n}`") ?>
		<? elseif(!$bmf_calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>null, 'calc_pos_id'=>$bmf_calc_pos['id'], 'bmf_calc_pos'=>null], $w += ['hide'=>0, 'calc_solutions_type_id'=>$bmf_calc_solutions_type['id'], 'name'=>"[{$bmf_calc_pos['name']}]", 'calc_id'=>$calc['id']], $w)): mpre("Ошибка выборки решения") ?>
		<? elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$bmf_calc_solutions['id'], 'bmf-calc_solutions'=>$bmf_calc_solutions['id']], $w += ['hide'=>0], $w)): mpre("Ошибка установки перехода на смого себя") ?>
		<? elseif(!array_map(function($pos) use($bmf_calc_pos, $bmf_calc_solutions, $BMF_CALC_POS){
				if($bmf_calc_pos['id'] == $pos['id']){// mpre("Переход на самого себя не корректен");
				}elseif(!$calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>null, 'calc_pos_id'=>$pos['id'], 'bmf_calc_pos'=>null])){ mpre("Ошибка выборки решения");
				}elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$bmf_calc_solutions['id'], 'bmf-calc_solutions'=>$calc_solutions['id']], $w += ['hide'=>0], $w)){ mpre("Ошибка установки перехода");
				}elseif(!array_map(function($calc_pos) use($pos, $bmf_calc_solutions){
						if(!$calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>$calc_pos['id'], 'calc_pos_id'=>$pos['id'], 'bmf_calc_pos'=>null])){ mpre("Ошибка выборки решения");
						}elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$bmf_calc_solutions['id'], 'bmf-calc_solutions'=>$calc_solutions['id']], $w += ['hide'=>0], $w)){ mpre("Ошибка установки перехода");
						}elseif(!$calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>null, 'calc_pos_id'=>$pos['id'], 'bmf_calc_pos'=>$calc_pos['id']])){ mpre("Ошибка выборки решения");
						}elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$bmf_calc_solutions['id'], 'bmf-calc_solutions'=>$calc_solutions['id']], $w += ['hide'=>0], $w)){ mpre("Ошибка установки перехода");
						}else{ return $calc_pos; }
					}, $BMF_CALC_POS)){ mpre("Ошибка добавления двойного изменения");
				}else{// mpre($solutions);
					return $bmf_calc_solutions_link;
				}
			}, $BMF_CALC_POS)): mpre("Ошибка добавления списка возможных переходов") ?>
		<? else: ?>
			<div class="map">
				<style>
					.map .table {border-collapse:collapse;}
					.map .table > div > span {border:1px solid gray; padding:0;}
					.map h1 { margin:0; }
				</style>
				<h1><?=$bmf_calc_pos['name']?> <span style="font-size:16px;"><?=$calc['name']?></span></h1>
				<div class="table">
					<div class="th">
						<? foreach($BMF_CALC_POS as $calc_pos): ?>
							<span style="text-align:center;">
								<strong><?=$calc_pos['name']?></strong>
								<span style="border:1px solid #ddd; padding-left:3px; margin-left:5px;">
									<? if(!$VALUES = array_map(function($calc_val) use($BMF_CALC, $BMF_CALC_VAL, $bmf_calc_pos, $calc_pos){
											if(!$bmf_calc = rb($BMF_CALC, 'calc_pos_id', 'calc_val_id', $calc_pos['id'], $calc_val['id'])){ mpre("Ошибка выборки расчета");
											}elseif(!$val = rb($BMF_CALC_VAL, 'v1', 'v0', $bmf_calc['val'], $calc_val['v0'])){ mpre("Ошика выборки второго расчета");
											}elseif(!$calc = rb($BMF_CALC, 'calc_pos_id', 'calc_val_id', $bmf_calc_pos['id'], $val['id'])){ mpre("Ошибка выборки расчета");
											}else{ return $calc['val']; }
										}, $BMF_CALC_VAL)): mpre("Ошибка расчета значений") ?>
									<? elseif(!$values = implode('', $VALUES)): mpre("Ошибка составления строки значений") ?>
									<? elseif(!$pos = rb($BMF_CALC_POS, 'v1', 'v0', substr($values, 0, 1), substr($values, 1, 1))): mpre("Ошибка нахождения позиции") ?>
									<? elseif(!$val = rb($BMF_CALC_VAL, 'v1', 'v0', substr($values, 2, 1), substr($values, 3, 1))): mpre("Ошибка нахождения значения") ?>
									<? elseif(!$calc = rb($BMF_CALC, 'calc_pos_id', 'calc_val_id', $pos['id'], $val['id'])): mpre("Ошибка выборки калькуляции") ?>

									<? elseif(!$bmf_calc_solutions_type = rb('bmf-calc_solutions_type', 'name', $w = '[Лево]')): mpre("Ошибка выбора типа решения `{$n}`") ?>
									<? elseif(!$bmf_calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>$calc_pos['id'], 'calc_pos_id'=>$bmf_calc_pos['id'], 'bmf_calc_pos'=>null], $w += ['hide'=>0, 'calc_solutions_type_id'=>$bmf_calc_solutions_type['id'], 'name'=>"{$calc_pos['name']}[{$bmf_calc_pos['name']}]", 'calc_id'=>$calc['id']], $w)): mpre("Ошибка выборки решения") ?>

									<? elseif(!$calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>null, 'calc_pos_id'=>$bmf_calc_pos['id'], 'bmf_calc_pos'=>null])): mpre("Ошибка выборки решения") ?>
									<?// elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$calc_solutions['id'], 'bmf-calc_solutions'=>$calc_solutions['id']], $w += ['hide'=>0])): mpre("Ошибка добавления связи") ?>
									<? elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$calc_solutions['id'], 'bmf-calc_solutions'=>$bmf_calc_solutions['id']], $w += ['hide'=>0])): mpre("Ошибка добавления связи") ?>

									<? elseif(!array_map(function($bmf_calc_pos) use($calc_pos, $bmf_calc_solutions){
											if(!$calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>null, 'calc_pos_id'=>$bmf_calc_pos['id'], 'bmf_calc_pos'=>$calc_pos['id']])){ mpre("Ошибка выборки решения");
											}elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$calc_solutions['id'], 'bmf-calc_solutions'=>$bmf_calc_solutions['id']], $w += ['hide'=>0])){ mpre("Ошибка добавления связи");
											}else{ return $bmf_calc_pos; }
										}, $BMF_CALC_POS)): mpre("Ошибка добавления двойного изменения") ?>
									<? else: ?>
										<?=$calc['name']?>
									<? endif; ?>
								</span>
								<span style="background-color:gray; padding:0 3px; color:white;">
									<? if(!$VALUES = array_map(function($calc_val) use($BMF_CALC, $BMF_CALC_VAL, $bmf_calc_pos, $calc_pos){
											if(!$bmf_calc = rb($BMF_CALC, 'calc_pos_id', 'calc_val_id', $calc_pos['id'], $calc_val['id'])){ mpre("Ошибка выборки расчета");
											}elseif(!$val = rb($BMF_CALC_VAL, 'v1', 'v0', $calc_val['v1'], $bmf_calc['val'])){ mpre("Ошика выборки второго расчета");
											}elseif(!$calc = rb($BMF_CALC, 'calc_pos_id', 'calc_val_id', $bmf_calc_pos['id'], $val['id'])){ mpre("Ошибка выборки расчета");
											}else{ return $calc['val']; }
										}, $BMF_CALC_VAL)): mpre("Ошибка расчета значений") ?>
									<? elseif(!$values = implode('', $VALUES)): mpre("Ошибка составления строки значений") ?>
									<? elseif(!$pos = rb($BMF_CALC_POS, 'v1', 'v0', substr($values, 0, 1), substr($values, 1, 1))): mpre("Ошибка нахождения позиции") ?>
									<? elseif(!$val = rb($BMF_CALC_VAL, 'v1', 'v0', substr($values, 2, 1), substr($values, 3, 1))): mpre("Ошибка нахождения значения") ?>
									<? elseif(!$calc = rb($BMF_CALC, 'calc_pos_id', 'calc_val_id', $pos['id'], $val['id'])): mpre("Ошибка выборки калькуляции") ?>

									<? elseif(!$bmf_calc_solutions_type = rb('bmf-calc_solutions_type', 'name', $w = '[Право]')): mpre("Ошибка выбора типа решения `{$n}`") ?>
									<? elseif(!$bmf_calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>null, 'calc_pos_id'=>$bmf_calc_pos['id'], 'bmf_calc_pos'=>$calc_pos['id']], $w += ['hide'=>0, 'calc_solutions_type_id'=>$bmf_calc_solutions_type['id'], 'name'=>"[{$bmf_calc_pos['name']}]{$calc_pos['name']}", 'calc_id'=>$calc['id']], $w)): mpre("Ошибка выборки решения") ?>

									<? elseif(!$calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>null, 'calc_pos_id'=>$bmf_calc_pos['id'], 'bmf_calc_pos'=>null])): mpre("Ошибка выборки решения") ?>
									<?// elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$calc_solutions['id'], 'bmf-calc_solutions'=>$calc_solutions['id']], $w += ['hide'=>0])): mpre("Ошибка добавления связи") ?>
									<? elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$calc_solutions['id'], 'bmf-calc_solutions'=>$bmf_calc_solutions['id']], $w += ['hide'=>0])): mpre("Ошибка добавления связи") ?>

									<? elseif(!$calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>$calc_pos['id'], 'calc_pos_id'=>$bmf_calc_pos['id'], 'bmf_calc_pos'=>null])): mpre("Ошибка выборки решения") ?>
									<? elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$calc_solutions['id'], 'bmf-calc_solutions'=>$bmf_calc_solutions['id']], $w += ['hide'=>0])): mpre("Ошибка добавления связи") ?>
									<? else: ?>
										<?=$calc['name']?>
									<? endif; ?>
								</span>
								<strong><?=$calc_pos['name']?></strong>
							</span>
						<? endforeach; ?>
					</div>
					<? foreach($BMF_CALC_POS as $calc_pos): ?>
						<div>
							<? foreach($BMF_CALC_POS as $pos): ?>
								<? if(!is_numeric($hide = call_user_func(function($pos) use($calc_pos){
										if($calc_pos['id'] == $pos['id']){ return 1;
										}elseif($calc_pos['id'] == $pos['calc_pos_id']){ return 1;
										}else{ return 0; }
									}, $pos))): mpre("Ошибка расчета цвета") ?>
								<? else: ?>
									<span style="text-align:center; color:<?=($hide ? "#ddd" : "black")?>;">
										<strong><?=$calc_pos['name']?></strong>
										<span style="padding:0 20px;">
												<? if(!$VALUES = array_map(function($calc_val) use($BMF_CALC, $BMF_CALC_VAL, $bmf_calc_pos, $calc_pos, $pos){
														if(!$bmf_calc = rb($BMF_CALC, 'calc_pos_id', 'calc_val_id', $calc_pos['id'], $calc_val['id'])){ mpre("Ошибка выборки расчета");
														}elseif(!$calc = rb($BMF_CALC, 'calc_pos_id', 'calc_val_id', $pos['id'], $calc_val['id'])){ mpre("Ошибка выборки расчета");
														}elseif(!$val = rb($BMF_CALC_VAL, 'v1', 'v0', $bmf_calc['val'], $calc['val'])){ mpre("Ошика выборки второго расчета");
														}elseif(!$clc = rb($BMF_CALC, 'calc_pos_id', 'calc_val_id', $bmf_calc_pos['id'], $val['id'])){ mpre("Ошибка выборки расчета");
														}else{ return $clc['val']; }
													}, $BMF_CALC_VAL)): mpre("Ошибка расчета значений") ?>
												<?// elseif(!$values = implode('', $VALUES)): mpre("Ошибка составления строки значений") ?>
												<? elseif(!$_pos = rb($BMF_CALC_POS, 'v1', 'v0', $VALUES[1], $VALUES[2])): mpre("Ошибка нахождения позиции") ?>
												<? elseif(!$val = rb($BMF_CALC_VAL, 'v1', 'v0', $VALUES[3], $VALUES[4])): mpre("Ошибка нахождения значения") ?>
												<? elseif(!$calc = rb($BMF_CALC, 'calc_pos_id', 'calc_val_id', $_pos['id'], $val['id'])): mpre("Ошибка выборки калькуляции") ?>

												<? elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$bmf_calc_solutions['id'], 'bmf-calc_solutions'=>$bmf_calc_solutions['id']], $w += ['hide'=>0])): mpre("Ошибка добавления связи") ?>

												<? elseif(!$bmf_calc_solutions_type = rb('bmf-calc_solutions_type', 'name', $w = '[Низ]')): mpre("Ошибка выбора типа решения `{$n}`") ?>
												<? elseif(!$bmf_calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>$calc_pos['id'], 'calc_pos_id'=>$bmf_calc_pos['id'], 'bmf_calc_pos'=>$pos['id']], $w += ['hide'=>$hide, 'calc_solutions_type_id'=>$bmf_calc_solutions_type['id'], 'name'=>"{$calc_pos['name']}[{$bmf_calc_pos['name']}]{$pos['name']}", 'calc_id'=>$calc['id']], $w)): mpre("Ошибка выборки решения") ?>

												<? elseif(!array_map(function($bmf_calc_pos) use($bmf_calc_solutions){
														if(!$calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>null, 'calc_pos_id'=>$bmf_calc_pos['id'], 'bmf_calc_pos'=>null])){ mpre("Ошибка выборки решения");
														}elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$calc_solutions['id'], 'bmf-calc_solutions'=>$bmf_calc_solutions['id']], $w += ['hide'=>0])){ mpre("Ошибка добавления связи");
														}else{ }
													}, $BMF_CALC_POS)): mpre("Ошибка установки всем первым элементам") ?>

												<? elseif(!$calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>$calc_pos['id'], 'calc_pos_id'=>$bmf_calc_pos['id'], 'bmf_calc_pos'=>null])): mpre("Ошибка выборки решения") ?>
												<? elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$calc_solutions['id'], 'bmf-calc_solutions'=>$calc_solutions['id']], $w += ['hide'=>0])): mpre("Ошибка добавления связи") ?>
												<? elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$calc_solutions['id'], 'bmf-calc_solutions'=>$bmf_calc_solutions['id']], $w += ['hide'=>0])): mpre("Ошибка добавления связи") ?>

												<? elseif(!$calc_solutions = fk('bmf-calc_solutions', $w = ['bmf-calc_pos'=>null, 'calc_pos_id'=>$bmf_calc_pos['id'], 'bmf_calc_pos'=>$pos['id']])): mpre("Ошибка выборки решения") ?>
												<? elseif(!$bmf_calc_solutions_link = fk('bmf-calc_solutions_link', $w = ['calc_solutions_id'=>$calc_solutions['id'], 'bmf-calc_solutions'=>$bmf_calc_solutions['id']], $w += ['hide'=>0])): mpre("Ошибка добавления связи") ?>
												<? else:// print_r($VALUES, $values) ?>
													<?=$calc['name']?>
												<? endif; ?>
										</span>
										<strong><?=$pos['name']?></strong>
									</span>
								<? endif; ?>
							<? endforeach; ?>
						</div>
					<? endforeach; ?>
				</div>
			</div>
		<? endif; ?>
	<? endforeach; ?>
	<? if(!$BMF_CALC_SOLUTIONS = rb('bmf-calc_solutions')): mpre("Ошибка выборки списка решений") ?>
	<? else: ?>
		<a href="/bmf:calc">Решения</a>
		<div class="table">
			<div class="th">
				<span>Результат</span>
				<span>Исходные</span>
				<span>Левые</span>
				<span>Правые</span>
				<span>Оба</span>
				<span style="width:300px;">Скрытые</span>
			</div>
			<? foreach($BMF_CALC as $bmf_calc): ?>
				<? if(!is_array($BMF_CALC_SOLUTIONS_['00'] = rb($BMF_CALC_SOLUTIONS, 'hide', 'bmf-calc_pos', 'calc_id', 'bmf_calc_pos', 'id', 0, '', $bmf_calc['id'], ''))): mpre("Ошибка выборки всех решений") ?>
				<? elseif(!is_array($BMF_CALC_SOLUTIONS_['01'] = rb($BMF_CALC_SOLUTIONS, 'hide', 'bmf_calc_pos', 'calc_id', 'id', 0, '', $bmf_calc['id']))): mpre("Ошибка выборки всех решений") ?>
				<? elseif(!is_array($BMF_CALC_SOLUTIONS_['10'] = rb($BMF_CALC_SOLUTIONS, 'hide', 'calc_id', 'bmf-calc_pos', 'id', 0, $bmf_calc['id'], ''))): mpre("Ошибка выборки всех решений") ?>
				<? elseif(!is_array($BMF_CALC_SOLUTIONS_['11'] = rb($BMF_CALC_SOLUTIONS, 'hide', 'bmf-calc_pos', 'calc_id', 'bmf_calc_pos', 'id', 0, $BMF_CALC_POS, $bmf_calc['id'], $BMF_CALC_POS))): mpre("Ошибка выборки всех решений") ?>
				<? elseif(!is_array($BMF_CALC_SOLUTIONS_['0'] = rb($BMF_CALC_SOLUTIONS, 'hide', 'bmf-calc_pos', 'calc_id', 'bmf_calc_pos', 'id', 1, $BMF_CALC_POS, $bmf_calc['id'], $BMF_CALC_POS))): mpre("Ошибка выборки всех решений") ?>
				<? else: ?>
					<div>
						<span><?=$bmf_calc['name']?></span>
						<? foreach($BMF_CALC_SOLUTIONS_ as $key=>$_BM0_CALC_SOLUTIONS): ?>
							<span>
								<? foreach($_BM0_CALC_SOLUTIONS as $solutions): ?>
									<span style="color:<?=($solutions['hide'] ? "#bbb" : "black")?>;"><?=$solutions['name']?></span>;
								<? endforeach; ?>
							</span>
						<? endforeach; ?>
					</div>
				<? endif; ?>
			<? endforeach; ?>
		</div>
	<? endif; ?>
<? endif; ?>
