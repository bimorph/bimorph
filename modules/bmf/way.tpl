<? if(!$WAY = rb("way")): mpre("ОШИБКА выборки списка путей") ?>
<? elseif(call_user_func(function(){ // Устанвока значений расчета
		if(!$way_id = get($_GET, "bmf-way")){ //mpre("Параметр установки не задан");
		}else if(!$way = rb("way", "id", $way_id)){ mpre("ОШИБКА выборки пути изменений");
		}else if(!$WAY_CALC = rb("way_calc", "way_id", "id", $way["id"])){ mpre("Расчет пути не задан {$way["name"]}");
		}else if(!$CALL = array_map(function($way_calc){ // Установка расчета пути
				if(!$_calc = rb("calc", "calc_pos_id", "calc_val_id", $way_calc["bmf-calc_pos"], $way_calc["bmf-calc_val"])){ mpre("ОШИБКА выборки расчета");
				}else if(!$calc = fk("calc", ["id"=>$way_calc["calc_id"]], null, ["bmf-calc_pos"=>$way_calc["bmf-calc_pos"], "bmf-calc_val"=>$way_calc["bmf-calc_val"], "bmf-calc"=>$_calc["id"]])){ mpre("ОШИБКА установки нового значения расчета пути");
				}else{ return $calc; }
			}, $WAY_CALC)){ mpre("ОШИБКА установки расчета пути");
		}else{ mpre($way, $WAY_CALC);
			exit(header("Location: /bmf:way"));
		}
	})): mpre("ОШИБКА установки значений расчета") ?>
<? elseif(!$WAY = array_map(function($way){ // Расчет текущего списка
		if(!$_WAY_CALC = rb("way_calc", "way_id", "id", $way["id"])){ mpre("ОШИБКА получения расчетов пути");
		}else if(!is_array($_CALC = array_filter(array_map(function($way_calc, $_calc = []){ // Выборка совпадающих расчетов
				if(!$_calc = rb("calc", "id", "bmf-calc_pos", "bmf-calc_val", $way_calc["calc_id"], $way_calc["bmf-calc_pos"], $way_calc["bmf-calc_val"])){ //mpre("Расчет не найден");
				}else{ //mpre("Расчет совпадает", $_calc);
				} return $_calc;
			}, $_WAY_CALC)))){ //mpre("ОШИБКА выборки установленных расчетов");
		}else if(count($_WAY_CALC) != count($_CALC)){ //mpre("Количество установленных расчетов не совпадает с расчетами пути");
		}else{ $way["_val"] = $way["val"];
		} return $way;
	}, $WAY)): mpre("ОШИБКА определения совпадения с путем") ?>
<? elseif(!$_WAY = rb($WAY, "_val", "id", array_flip([0, 1]))): mpre("ОШИБКА получения знаков расчета") ?>
<? elseif(!$calc_name = implode(null, array_column($_WAY, "_val"))): mpre("ОШИБКА получения имени расчета") ?>
<? elseif(!$calc = fk("calc", ["name"=>$calc_name])): mpre("ОШИБКА получения расчета `{$calc_name}`") ?>
<? elseif(!$clump = fk("clump", ["hide"=>0], null, ["calc_id"=>$calc["id"]])): mpre("ОШИБКА установки расчета скоплению") ?>
<? else: ?>
	<ul>
		<? foreach($WAY as $way): ?>
			<li style="font-weight:<?=(array_key_exists("_val", $way) ? "bold" : "inherit")?>;">
				<a href="/bmf:way/bmf-way:<?=$way["id"]?>"><?=$way["name"]?></a>
			</li>
		<? endforeach; ?>
	</ul>
<? endif; mpre($calc_name) ?>
