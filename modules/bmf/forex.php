<? if(!ini_set('memory_limit', '1G')): mpre("ОШИБКА увеличения лимита") ?>
<? elseif(!$JSON = file("modules/bmf/sh/cpp/json/forex-m5.csv")): mpre("Ошибка отображения файла") ?>
<? elseif(!$JSON = array_slice($JSON, 300, 500)): mpre("ОШИБКА сокращения количества записей") ?>
<? //elseif(!$JSON = array_slice($JSON, 1440, 1440*2)): mpre("ОШИБКА сокращения количества записей") ?>
<? elseif(!$Format = function($_json){ // Формат строки
		if(!$_json = strtr($_json, ['""'=> '"'])){ mpre("ОШИБКА замены ковыек");
		}else if(!$_json = iconv("CP1251", "UTF-8", $_json)){ mpre("ОШИБКА замены ковыек");
		}else if(!preg_match("#{.*}#", $_json, $match)){ mpre("ОШИБКА определения регулярки");
		}else if(!$_json = get($match, 0)){ mpre("ОШИБКА полученния json строки");
		}else if(!$json = json_decode($_json, true)){ mpre("ОШИБКА парсинга json строки");
		}else{ //mpre($_json, $match);
		} return $json;
	}): mpre("ОШИБКА получения форматированной строки"); ?>
<? elseif(!$json = $Format(first($JSON))): mpre("ОШИБКА получения первой записи") ?>
<? elseif(!$JSON = array_map(function($_json) use($Format, $json){ // Изменяем кодировку
		if(!$_json = $Format($_json)){ mpre("ОШИБКА форматирования строки");
		}else if($diff = array_diff_key($_json, $json)){ mpre("Неформат", $diff);
		}else if($diff = array_diff_key($json, $_json)){ mpre("Неформат", $diff);
		}else if(!$_json = json_encode($_json, 256)){ mpre("ОШИБКА обратного преобразования обьекта");
		}else{
		} return $_json;
	}, $JSON)): mpre("ОШИБКА преобразования кодировки") ?>
<? elseif(!$JSON = "[". implode(",", $JSON). "]"): mpre("ОШИБКА обьединения json элементов") ?>
<? elseif(!file_put_contents($f = "modules/bmf/sh/cpp/json/forex-m5_week2.json", $JSON)): mpre("ОШИБКА сохранения в файл {$f}") ?>
<? else: mpre($JSON) ?>
<? endif; ?>
