<h1>Результаты теста</h1>
<? if(!$BM0_CALC = rb("bm0-calc")): mpre("Ошибка выборки списка расчетов")  ?>
<? elseif(!$BM0_CLUMP_SHOT = rb("bm0-clump_shot")): mpre("Ошибка выборки списка результатов") ?>
<? elseif(!$BM0_CALC_ORIENT = rb("bm0-calc_orient", "id", "id", "[2,4]")): mpre("Список орентаций") ?>
<? elseif(!$BM0_CALC = array_map(function($bm0_calc) use($BM0_CALC_ORIENT, $BM0_CLUMP_SHOT){
		if(!is_array($_BM0_CLUMP_SHOT = rb($BM0_CLUMP_SHOT, "calc_id", "id", $bm0_calc["id"]))){ mpre("Данных расчета ненайдено");
		}else if(!is_numeric($bm0_calc["count"] = count($_BM0_CLUMP_SHOT))){ mpre("Ошибка расчета суммы");
		}else if(!is_numeric($bm0_calc["length"] = ($_BM0_CLUMP_SHOT ? number_format(array_sum(array_column($_BM0_CLUMP_SHOT, "length"))/count($_BM0_CLUMP_SHOT), 2) : 0))){ mpre("Ошибка расчета размера");
		}else{
			foreach($BM0_CALC_ORIENT as $bm0_calc_orient){
				if(!is_array($__BM0_CLUMP_SHOT = rb($_BM0_CLUMP_SHOT, "bm0-calc_orient", "id", $bm0_calc_orient["id"]))){ mpre("Данных не найдено", $bm0_calc_orient);
				}else if(!is_numeric($bm0_calc["sum_{$bm0_calc_orient["id"]}"] = (!$__BM0_CLUMP_SHOT ? 0 : number_format(array_sum(array_column($__BM0_CLUMP_SHOT, "val"))/count($__BM0_CLUMP_SHOT), 2)))){ mpre("Ошибка расчета среднего размера");
				}else if(!is_numeric($bm0_calc["sum_max"] = $bm0_calc["sum_{$bm0_calc_orient["id"]}"] > get($bm0_calc, "sum_max") ? $bm0_calc["sum_{$bm0_calc_orient["id"]}"] : (float)get($bm0_calc, "sum_max"))){ mpre("Ошибка расчета максимального элемента");

				}else if(!is_numeric($bm0_calc["count_{$bm0_calc_orient["id"]}"] = count($__BM0_CLUMP_SHOT))){ mpre("Ошибка расчета среднего размера");
				}else if(!is_numeric($bm0_calc["count_max"] = $bm0_calc["count_{$bm0_calc_orient["id"]}"] > get($bm0_calc, "count_max") ? $bm0_calc["count_{$bm0_calc_orient["id"]}"] : (float)get($bm0_calc, "count_max"))){ mpre("Ошибка расчета максимального элемента");

				}else if(!is_numeric($bm0_calc["length_{$bm0_calc_orient["id"]}"] = (!$__BM0_CLUMP_SHOT ? 0 : number_format(array_sum(array_column($__BM0_CLUMP_SHOT, "length"))/count($__BM0_CLUMP_SHOT), 2)))){ mpre("Ошибка расчета среднего размера");
				}else if(!is_numeric($bm0_calc["length_max"] = $bm0_calc["length_{$bm0_calc_orient["id"]}"] > get($bm0_calc, "length_max") ? $bm0_calc["length_{$bm0_calc_orient["id"]}"] : (float)get($bm0_calc, "length_max"))){ mpre("Ошибка расчета максимального элемента");

				}else{// mpre($bm0_calc);
				}
			}
		} return $bm0_calc;
	}, $BM0_CALC)): mpre("Ошибка расчета"); ?>
<? elseif(call_user_func(function() use($BM0_CLUMP_SHOT){ ?>
		<? if(!$max = max(array_column($BM0_CLUMP_SHOT, "nn"))): mpre("Ошибка расчета максимального количества обучений") ?>
		<? elseif(!$stat['Процент полных совпадений'] = number_format(($t = (count($BM0_CLUMP_SHOT) - count(rb($BM0_CLUMP_SHOT, 'nn', 'id', $max))))/($f = count($BM0_CLUMP_SHOT))*100, 2). " % ({$t} из {$f})"): mpre("Ошибка расчета количества удачных") ?>
		<? elseif(!$stat['Количество шагов до результата'] = number_format(array_sum(array_column($BM0_CLUMP_SHOT, "nn"))/count($BM0_CLUMP_SHOT), 2)): mpre("Ошибка расчета количества обучений") ?>
		<? elseif(!$stat['Количество сделанных изменений'] = number_format(array_sum(array_column($BM0_CLUMP_SHOT, "change"))/count($BM0_CLUMP_SHOT), 2)): mpre("Ошибка расчета количества обучений") ?>
		<? elseif(!$stat['Размер структуры'] = number_format(array_sum(array_column($BM0_CLUMP_SHOT, "size"))/count($BM0_CLUMP_SHOT), 2)): mpre("Ошибка расчета количества обучений") ?>
		<? elseif(!$stat['Количество проведенных тестов'] = count($BM0_CLUMP_SHOT)): mpre("Ошибка расчета количества обучений") ?>
		<? else: ?>
			<div class="table" style="width:auto;">
				<? foreach($stat as $name=>$value): ?>
					<div><span><?=$name?></span><span style="font-weight:bold;"><?=$value?></span></div>
				<? endforeach; ?>
			</div>
		<? endif; ?>
	<? })): mpre("Ошибка отображения статистической таблицы") ?>
<? else:// mpre($BM0_CALC) ?>
	<?/*<div class="table">
		<div class="th">
			<span>Расчет</span>
			<span>Количество</span>
			<span>Размер</span>
			<? foreach($BM0_CALC_ORIENT as $bm0_calc_orient): ?>
				<span>Тестов</span>
				<span>Обучений <?=$bm0_calc_orient['name']?></span>
				<span>Размер</span>
			<? endforeach; ?>
		</div>
		<? foreach($BM0_CALC as $bm0_calc):// mpre($bm0_calc) ?>
				<div>
					<span><?=$bm0_calc["name"]?></span>
					<span><?=$bm0_calc["count"]?></span>
					<span><?=$bm0_calc["length"]?></span>
					<? foreach($BM0_CALC_ORIENT as $bm0_calc_orient): ?>
							<span style="color:<?=($bm0_calc["count_{$bm0_calc_orient["id"]}"] == $bm0_calc["count_max"] ? "green" : "red")?>" title="<?=$bm0_calc["count_max"]?>"><?=$bm0_calc["count_{$bm0_calc_orient["id"]}"]?></span>
							<span style="font-weight:bold; color:<?=($bm0_calc["sum_{$bm0_calc_orient["id"]}"] == $bm0_calc["sum_max"] ? "green" : "red")?>" title="<?=$bm0_calc["sum_max"]?>"><?=$bm0_calc["sum_{$bm0_calc_orient["id"]}"]?></span>
							<span style="color:<?=($bm0_calc["length_{$bm0_calc_orient["id"]}"] != $bm0_calc["length_max"] ? "green" : "red")?>" title="<?=$bm0_calc["length_max"]?>"><?=$bm0_calc["length_{$bm0_calc_orient["id"]}"]?></span>
					<? endforeach; ?>
				</div>
		<? endforeach; ?>
	</div>*/?>
<? endif; ?>