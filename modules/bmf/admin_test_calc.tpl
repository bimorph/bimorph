<? if(!$CALC = rb("calc")): mpre("ОШИБКА выборки расчетов") ?>
<? elseif(!$CALC_VAL = rb("calc_val")): mpre("ОШИБКА выборки списка значений") ?>
<? elseif(!$_CALC_VAL = rb($CALC_VAL, "name", "id", "[01,10]")): mpre("ОШИБКА выборки списка значений") ?>
<? elseif(!$calc_val = rb($_CALC_VAL, "name", "[01]")): mpre("ОШИБКА выборки списка значений") ?>
<? elseif(!$_calc_val = rb($_CALC_VAL, "name", "[10]")): mpre("ОШИБКА выборки списка значений") ?>
<? elseif(!$TEST_CALC = rb("test_calc")): mpre("ОШИБКА данные в таблице не найдены `test_calc`") ?>
<? elseif(!is_array($_TEST_CALC = rb($TEST_CALC, "calc_id", "bmf-calc", "id"))): mpre("ОШИБКА формирования кластеров") ?>
<? else: //mpre($_TEST_CALC) ?>
	<h2>Тестов <?=count($TEST_CALC)/16?></h2>
	<div class="table stats">
		<style>
			.table.stats { width:auto; }
			.table.stats > div > span { padding: 0 10px; }
		</style>
		<div class="th">
			<span>расчет</span>
			<span>меньший</span>
			<span>оба</span>
			<span>больший</span>
		</div>
		<? foreach($_TEST_CALC as $calc_id=>$_TEST_CALC): ?>
			<? /*if(!$_TEST_CALC = array_map(function($_test_calc){ return (is_array($_test_calc) ? $_test_calc : []); }, $_TEST_CALC)): mpre("ОШИБКА форматирования даных") ?>
			<? else*/if(!$calc = rb($CALC, "id", $calc_id)): mpre("ОШИБКА получения расчета") ?>
			<? elseif(!is_array($stats = call_user_func(function($name, $stats = []) use($_CALC_VAL, $CALC, $_TEST_CALC){ // Список положительных значений
					if(!$calc_val = rb($_CALC_VAL, "name", $name)){ mpre("ОШИБКА выборки значения по имени");
					}else if(!$_CALC = rb($CALC, "calc_val_id", "id", $calc_val["id"])){ mpre("ОШИБКА получения всех расчетов имени значения");
					}else if(!is_array($TEST = array_intersect_key($_TEST_CALC, $_CALC))){ mpre("ОШИБКА получения списка тестов имени");
					}else if(!is_array($_TEST = ($TEST ? call_user_func_array("array_merge", $TEST) : []))){ mpre("ОШИБКА получения одноуровневого списка всех значений");
					}else if(!is_array($_LOOP = array_column($_TEST, "loop"))){ mpre("ОШИБКА получения списка итераций");
					}else if(!is_numeric($stats["loop"][$name] = $_LOOP ? array_sum($_LOOP)/count($_LOOP) : 0)){ mpre("ОШИБКА расчета суммы");
					}else if(!is_array($_PERC = array_column($_TEST, "perc"))){ mpre("ОШИБКА получения списка итераций");
					}else if(!is_numeric($stats["perc"][$name] = $_PERC ? array_sum($_PERC)/count($_PERC) : 0)){ mpre("ОШИБКА расчета суммы");
					}else if(!is_array($_BMF = array_column($_TEST, "bmf"))){ mpre("ОШИБКА получения списка итераций");
					}else if(!is_numeric($stats["bmf"][$name] = $_BMF ? array_sum($_BMF)/count($_BMF) : 0)){ mpre("ОШИБКА расчета суммы");
					}else{ //return array_sum($_COLUMN);
					} return $stats;
				}, "01"))): mpre("ОШИБКА получения меньших значений {$n}") ?>
			<? elseif(!is_array($stats = call_user_func(function($name, $stats = []) use($_CALC_VAL, $CALC, $_TEST_CALC){ // Список положительных значений
					if(!$calc_val = rb($_CALC_VAL, "name", $name)){ mpre("ОШИБКА выборки значения по имени");
					}else if(!$_CALC = rb($CALC, "calc_val_id", "id", $calc_val["id"])){ mpre("ОШИБКА получения всех расчетов имени значения");
					}else if(!is_array($TEST = array_intersect_key($_TEST_CALC, $_CALC))){ mpre("ОШИБКА получения списка тестов имени");
					}else if(!is_array($_TEST = ($TEST ? call_user_func_array("array_merge", $TEST) : []))){ mpre("ОШИБКА получения одноуровневого списка всех значений");
					}else if(!is_array($_LOOP = array_column($_TEST, "loop"))){ mpre("ОШИБКА получения списка итераций");
					}else if(!is_numeric($stats["loop"][$name] = $_LOOP ? array_sum($_LOOP)/count($_LOOP) : 0)){ mpre("ОШИБКА расчета суммы");
					}else if(!is_array($_PERC = array_column($_TEST, "perc"))){ mpre("ОШИБКА получения списка итераций");
					}else if(!is_numeric($stats["perc"][$name] = $_PERC ? array_sum($_PERC)/count($_PERC) : 0)){ mpre("ОШИБКА расчета суммы");
					}else if(!is_array($_BMF = array_column($_TEST, "bmf"))){ mpre("ОШИБКА получения списка итераций");
					}else if(!is_numeric($stats["bmf"][$name] = $_BMF ? array_sum($_BMF)/count($_BMF) : 0)){ mpre("ОШИБКА расчета суммы");
					}else{ //return array_sum($_COLUMN); 
					} return $stats;
				}, "10", $stats))): mpre("ОШИБКА получения меньших значений {$n}") ?>
			<? elseif(!is_array($stats = call_user_func(function($name, $stats = []) use($_CALC_VAL, $CALC, $_TEST_CALC){ // Список положительных значений
					if(!is_array($_TEST = (get($_TEST_CALC, "") ?: []))){ mpre("ОШИБКА получения списка тестов имени");
					//}else if(!is_array($_TEST = ($TEST ? call_user_func_array("array_merge", $TEST) : []))){ mpre("ОШИБКА получения одноуровневого списка всех значений");
					}else if(!is_array($_LOOP = array_column($_TEST, "loop"))){ mpre("ОШИБКА получения списка итераций");
					}else if(!is_numeric($stats["loop"][$name] = $_LOOP ? array_sum($_LOOP)/count($_LOOP) : 0)){ mpre("ОШИБКА расчета суммы");
					}else if(!is_array($_PERC = array_column($_TEST, "perc"))){ mpre("ОШИБКА получения списка итераций");
					}else if(!is_numeric($stats["perc"][$name] = $_PERC ? array_sum($_PERC)/count($_PERC) : 0)){ mpre("ОШИБКА расчета суммы");
					}else if(!is_array($_BMF = array_column($_TEST, "bmf"))){ mpre("ОШИБКА получения списка итераций");
					}else if(!is_numeric($stats["bmf"][$name] = $_BMF ? array_sum($_BMF)/count($_BMF) : 0)){ mpre("ОШИБКА расчета суммы");
					}else{ //mpre($_CALC, $_COLUMN);
					} return $stats;
				}, "", $stats))): mpre("ОШИБКА получения меньших значений {$n}") ?>
			<? //elseif(!$count_max = array_max($count_max)): mpre() ?>
			<? else: //mpre($stats['loop'], array_filter($stats['loop'])) ?>
				<div>
					<span><?=$calc["name"]?></span>
					<? foreach(["01", "", "10"] as $n): ?>
						<? if(!$weight = call_user_func(function($weight = []) use($n, $stats){ // Расчет подсветки
								if(!$weight[$s = "loop"] = ($stats[$s][$n] && max(array_filter($stats[$s])) != $stats[$s][$n] ? "bold" : "inherit")){ mpre("ОШИБКА расчета посветки {$n}");
								}else if(!$weight[$s = "perc"] = ($stats[$s][$n] && min(array_filter($stats[$s])) != $stats[$s][$n] ? "bold" : "inherit")){ mpre("ОШИБКА расчета посветки {$n}");
								}else if(!$weight[$s = "bmf"] = ($stats[$s][$n] && max(array_filter($stats[$s])) != $stats[$s][$n] ? "bold" : "inherit")){ mpre("ОШИБКА расчета посветки {$n}");
								}else{ //mpre($weight);
								} return $weight;
							})): mpre("ОШИБКА расчета подсветки") ?>
						<? else: ?>
							<span>
								<span style="font-weight:<?=$weight["loop"]?>;" title="Итераций">
									<?=number_format($stats['loop'][$n], 3)?>
								</span>/
								<span style="font-weight:<?=$weight["perc"]?>;" title="Процент">
									<?=number_format($stats['perc'][$n], 3)?>
								</span>/
								<span style="font-weight:<?=$weight["bmf"]?>;" title="Правил">
									<?=number_format($stats['bmf'][$n], 3)?>
								</span>
							</span>
						<? endif; ?>
					<? endforeach; ?>
				</div>
			<? endif; ?>
		<? endforeach;; ?>
	</div>
<? endif; ?>
