<? if(!$CLUMP = rb('clump')): mpre("ОШИБКА получения списка скоплений") ?>
<? elseif(!$clump = rb($CLUMP, 'id', get($_GET, 'id'))): mpre("ОШИБКА получения текущего скопления") ?>
<? elseif(!$DANO = rb('dano', 'clump_id', 'id', $clump['id'])): mpre("Дано не установлено") ?>
<? elseif(!$ITOG = $GLOBALS['BMF-ITOG'] = rb('itog', 'clump_id', 'id', $clump['id'])): mpre("Итогов не установлено") ?>
<? elseif(!$CALC_POS = $GLOBALS['BMF-CALC_POS'] = rb('bmf-calc_pos')): mpre("ОШИБКА получения списка морфов") ?>
<? elseif(!$CALC_VAL = $GLOBALS['BMF-CALC_VAL'] = rb('bmf-calc_val')): mpre("ОШИБКА получения списка значений") ?>
<? elseif(!$CALC = $GLOBALS['BMF-CALC'] = rb('bmf-calc')): mpre("ОШИБКА получения списка расчетов") ?>
<? elseif(!$CALC_VAL = rb('calc_val')): mpre("ОШИБКА выборки значени расчетов") ?>
<? elseif(!$CALC_AMEND = rb('calc_amend')): mpre("ОШИБКА выборки направлений") ?>
<? elseif(call_user_func(function($clump) use($conf, $ITOG, $CLUMP){ # Удаление морфов скопления при наличае сигнала в адресе страницы
		if(!array_key_exists('clean', $_GET)){// mpre("Не найден сигнал ошистки");
		}elseif(!qw("DELETE FROM `{$conf['db']['prefix']}bmf_index` WHERE `clump_id`=". (int)$clump['id'])){ mpre("ОШИБКА удаления всех морфов скопления");
		}elseif(!qw("UPDATE `{$conf['db']['prefix']}bmf_clump_history` SET val='' WHERE `clump_id`=". (int)$clump['id'])){ mpre("ОШИБКА очистки истории");
		}elseif(!is_array($_ITOG = array_map(function($itog){ # Очистка связи итогов с морфами
				if(!$itog = fk('itog', ['id'=>$itog['id']], null, ['index_id'=>null])){ mpre("ОШИБКА скидывания связи с морфом (для исключения дальнейшей связи с новыми созданными  тем же идентификатором)");
				}else{ return $itog; }
			}, $ITOG))){ mpre("ОШИБКА скидывания связей с морфом у итогов");
		}elseif(!$links = array_map(function($clump){ # Формирование ссылок для переходов при очистке
				return "<a href='/bmf:clump/{$clump['id']}'>{$clump['name']}</a>";
			}, $CLUMP)){ mpre("ОШИБКА получения списка переходов по скоплениям");
		}else{// call_user_func_array('mpre', array_merge(['Скопления'], $links));
			mpre("[Скопления]", implode(" ", $links));
		}
	}, $clump)): mpre("ОШИБКА очищения результатов") ?>
<? elseif(!is_array($CLUMP_HISTORY = rb('clump_history', 'clump_id', 'id', $clump['id']))): mpre("ОШИБКА получения истории") ?>
<? elseif(!is_array($clump_history = rb($CLUMP_HISTORY, 'id', get($_GET, 'bmf-clump_history')))): mpre("ОШИБКА получения истории"); ?>
<? elseif(!$DANO = call_user_func(function($clump_history) use(&$DANO){// mpre($clump_history); # Установка значений дано из истории
		if(!$dano = get($clump_history, 'dano')){ return $DANO; mpre("ОШИБКА не указано дано в исторических данных");
		}elseif(strlen($dano) != count($DANO)){ mpre("Количество исторических данных и сигналов дано не совпадает");
		}elseif(!$split = str_split($dano)){ mpre("ОШИБКА получения массива сигналов");
//		}elseif(true){ mpre($split, );
		}elseif(!$DANO = array_map(function($num, $dano) use($split){// mpre($split); // mpre($dano, $split); # Установка значений дано
				if(!is_numeric($val = get($split, $num))){ mpre("ОШИБКА получения значения дано");
				}elseif(!$dano = fk('dano', ['id'=>$dano['id']], null, ['val'=>$val/*, 'tail'=>$tail*/])){ mpre("ОШИБКА сохранения значения в базу");
				}else{// pre($dano);
					return $dano;
				}
			}, array_keys(array_values($DANO)), $DANO)){ mpre("ОШИБКА установки значений дано");
		}else{ return $DANO; }
	}, $clump_history)): mpre("ОШИБКА восстановления дано по историческим данным") ?>
<? elseif(!is_array($GLOBALS['BMF-INDEX'] = rb('index', 'clump_id', 'id', $clump['id']))): mpre("ОШИБКА выборки морфов скопления") ?>
<? elseif(call_user_func(function($clump) use($conf, $arg, $DANO, $CALC, $ITOG, $CALC_POS, $CALC_VAL, $CALC_AMEND){ # Обработка пришедших с формы данных
		if(!is_array($INDEX = &$GLOBALS["BMF-INDEX"])){ mpre("ОШИБКА получения данных справочника");
		}elseif(!is_array($_index = call_user_func(function($_index = []) use($INDEX, $CALC_POS){ // Изменение позиции морфа
				if(!$index = rb($INDEX, "id", get($_GET, 'bmf-index'))){ //mpre("ОШИБКА определения морфа для изменения позиции");
				}elseif(!$_calc_pos = rb($CALC_POS, "id", get($_GET, 'bmf-calc_pos'))){ mpre("ОШИБКА выборки позиции устанавливаемой морфу");
				}elseif($index["calc_pos_id"] == $_calc_pos["id"]){ mpre("ОШИБКА позиция уже установлена у морфа");
				}elseif(!$_index = fk("bmf-index", ["id"=>$index["id"]], null, ["calc_pos_id"=>$_calc_pos["id"]])){ mpre("ОШИБКА устанвоки новой позиции морфу");
				}elseif(!$GLOBALS["BMF-INDEX"][$_index["id"]] = $_index){ mpre("ОШИБКА сохранения в справочник");
				}else{// die(mpre("Обновление позиции у морфа", $index, $_calc_pos, $_index));
				} return $_index;
			}))){ mpre("ОШИБКА изменения позиции морфа");
		}elseif(!$_POST){ return []; // mpre("Пост запрос не задан");
		}elseif(!is_array($dano = rb($DANO, "id", get($_POST, 'dano_id')))){ mpre("ОШИБКА Выборки дано"); return [];
		}elseif(!is_array([$field, $val_name] = call_user_func(function($field = ["", ""]){ // Получение поля для расширения и имя поля в занчении
				if(array_key_exists($n = "index_id", $_POST)){ $field = [$n, "v1"];
				}elseif(array_key_exists($n = "bmf-index", $_POST)){ $field = [$n, "v0"];
				}else{// mpre("ОШИБКА поле не задано");
				} return $field;
			}))){ mpre("ОШИБКА получения имени поля для добавления", $_POST);
		}elseif(!is_array($INDEX = call_user_func(function($_INDEX = []) use($conf, $arg, $field, $dano, &$INDEX, $CALC, $CALC_POS, $CALC_VAL, $CALC_AMEND, $ITOG){ # Переворачиваем указанные морфы
				if(!$index_post = get($_POST, 'index')){// Изменение позиции и значения
				//}elseif(true){ mpre($index_post);
				}elseif(!is_array($index_edit = get($index_post, 'edit') ?: [])){ mpre("ОШИБКА список изменений не найден");
				}elseif(!is_array($_INDEX = array_map(function($index_id, $val) use($conf, $arg, $INDEX){ // Получение массива изменений и удаление отрицательных значений
						if($val){// mpre("Только значения меньше нуля");
						}elseif(!qw("DELETE FROM `{$conf["db"]["prefix"]}{$arg["modpath"]}_index` WHERE id=". (int)$index_id)){ mpre("ОШИБКА удаления морфа");
						}else{ return true; }
					}, array_keys($index_edit), $index_edit))){ mpre("ОШИБКА получения списка морфов для изменений");
				//}elseif(!array_filter($_INDEX) && ($field) && empty($dano)){ mpre("ОШИБКА элементов на удаление нет, на добавление тоже");
				}elseif(!is_array($index_add = (array)get($index_post, 'add'))){ mpre("ОШИБКА список добавления не задан");
				}elseif(!is_array($_INDEX = array_map(function($index_id, $field) use($conf, $dano, &$INDEX, $CALC, $CALC_POS, $CALC_VAL, $ITOG){ // Получение массива изменений и удаление отрицательных значений
						if(!$dano){// mpre("ОШИБКА исходное значение для добавления к морфу не задано");
						}elseif(!$bmf_index = rb($INDEX, "id", $index_id)){ mpre("ОШИБКА получеия списка морфа для изменнения позиции `{$index_id}`");
						}elseif($_index = rb('index', 'id', $bmf_index[$field])){ mpre("ОШИБКА поле уже содержит морф");
						}elseif(!$bmf_calc = rb("calc", "calc_pos_id", "calc_val_id", $bmf_index['calc_pos_id'], $bmf_index['calc_val_id'])){ mpre("ОШИБКА получения калькуляции элемента");
						}elseif(!$bmf_calc_val = rb('calc_val', 'id', $bmf_calc['calc_val_id'])){ mpre("ОШИБКА получения значения калькуляции");
						}elseif(!$val_name = get(["index_id"=>"v1", "bmf-index"=>"v0"], $field)){ mpre("ОШИБКА получения поля значения");
						}elseif(!is_numeric($val = ($bmf_calc_val[$val_name] ? 0 : 1))){ mpre("ОШИБКА получения необходимого сигнала в морфе", $calc);
						}elseif(!$calc_pos = rb($CALC_POS, 'itog', 'dano', $val, get($dano, 'val'))){ mpre("ОШИБКА получения позиции морфа");
						}elseif(!$bmf_itog = rb($ITOG, "id", $bmf_index["itog_id"])){ mpre("ОШИБКА получения итога родителя");
						}elseif(!$_index = fk('index', null, $w = ['clump_id'=>$bmf_index['clump_id'], 'bmf-dano'=>get($bmf_index, 'dano_id'), 'calc_pos_id'=>$calc_pos['id'], 'dano_id'=>$dano['id'], "itog_id"=>$bmf_itog["id"], "calc_val_id"=>$bmf_calc_val["id"], 'itog_values_id'=>$bmf_itog["itog_values_id"], "val"=>$val])){ mpre("ОШИБКА обновления родительского морфа");
						}elseif(!$bmf_index = fk("index", ['id'=>$bmf_index['id']], null, [$field=>$_index['id']])){ mpre("ОШИБКА добавления морфа родителю");
						}else{ //mpre("Добавление", $index_id, $field, $bmf_index, $dano);
							return $_index;
						}
					}, array_keys($index_add), $index_add))){ mpre("ОШИБКА добавления");
				}elseif(!is_array($_INDEX = array_map(function($index_id, $val) use($conf, &$INDEX, $CALC, $CALC_POS, $CALC_VAL){ // Получение массива изменений и удаление отрицательных значений
						if(!$val){// mpre("Позиция не задана");
						}elseif(!$index = rb($INDEX, "id", $index_id)){ mpre("ОШИБКА обновляемый морф не найден");
						}elseif(!$calc = rb($CALC, "id", $val)){ mpre("ОШИБКА выборки обновленного расчета `{$val}` для морфа `{$index_id}`");
						}elseif(!$index = fk("bmf-index", ["id"=>$index["id"]], null, ["calc_pos_id"=>$calc["calc_pos_id"], "calc_val_id"=>$calc["calc_val_id"]])){ mpre("ОШИБКА обновления позиции морфа");
						}elseif(!$INDEX[$index["id"]] = $index){ mpre("ОШИБКА сохранения значения в справочник");
						}else{// mpre("Обновление", $index, $calc);
							return $index;
						}
					}, array_keys($index_edit), $index_edit))){ mpre("ОШИБКА изменения позиции");
				}else{// mpre("Перевернуты морфы", $_INDEX);
//					exit(header("Location: {$_SERVER["REQUEST_URI"]}"));
				} return $INDEX;
			}))){ mpre("ОШИБКА переворачивание морфов");
		}elseif(!is_array($itog = rb("itog", "id", get($_POST, 'itog_id')))){ mpre("ОШИБКА выборки итога из запроса");
		}elseif(!is_array($index = call_user_func(function($index = []) use($dano, $itog, $clump, $DANO, $ITOG, $CALC_POS, $CALC_VAL){ # Добавление первоначального морфа
				if(empty($itog)){// mpre("Не указан прямой источник сигнала первого морфа");
				}else if($index_id = get($_POST, 'index_id')){ mpre("ОШИБКА выбран смежный морф", $_POST);
				}elseif($index = rb('index', 'clump_id', 'id', $clump['id'], $itog['index_id'])){ mpre("Морф у дано уже установлен");
				}elseif(!$calc_pos = rb($CALC_POS, 'itog', 'dano', $itog['val'], $dano['val'])){ mpre("ОШИБКА получения позиции морфа", $_POST);
				}else if(!$dano = rb($DANO, "id", get($_POST, 'dano_id'))){ mpre("ОШИБКА получения исходного значения", $_POST);
				}elseif(!$calc_val = rb($CALC_VAL, "v1", "v0", $dano["val"], $dano["val"])){ mpre("ОШИБКА получения значения первоначального морфа");
				}else if(!$itog = rb($ITOG, "id", get($_POST, 'itog_id'))){ mpre("ОШИБКА получения исходного значения", $_POST);
				}elseif(!$index = fk('index', null, ['clump_id'=>$clump['id'], 'dano_id'=>$dano['id'], "itog_id"=>$itog["id"], "itog_values_id"=>$itog["itog_values_id"], 'calc_pos_id'=>$calc_pos['id'], "calc_val_id"=>$calc_val["id"], "val"=>$itog['val']])){ mpre("ОШИБКА добавления нового морфа");
				}elseif(!$itog = fk('itog', ['id'=>$itog['id']], null, ['index_id'=>$index['id']])){ mpre("ОШИБКА установки связи дано с морфом");
				}else{// mpre("Добавление первоначального морфа", $index);
				} return $index;
			}))){ mpre("ОШИБКА добавления первоначального морфа");
		}elseif(!is_array($GLOBALS['BMF-INDEX'] = rb('index', 'clump_id', 'id', $clump['id']))){ mpre("ОШИБКА обновления справочника");
		}else{// mpre($index);
		}
	}, $clump)): mpre("ОШИБКА обработки формы") ?>
<? elseif(!is_array($INDEX = call_user_func(function() use($clump_history, $ITOG, $CALC, $CALC_VAL, $DANO){ // Расчитываем значения морфов
		if(!is_array($INDEX = &$GLOBALS['BMF-INDEX'])){ mpre("ОШИБКА получения данных справочника");
		}elseif(!array_key_exists("calc", $_GET)){// mpre("Расчет выключен");
		}else if(!$clump_history){ mpre("Обучение не задано");
		}elseif(!is_array($_INDEX = array_map(function($index) use($INDEX){ // Список крайних морфов не имеющих потомков
				if($_index = rb($INDEX, "id", $index["bmf-index"])){// mpre("Старший морф не найден");
				}elseif($_index = rb($INDEX, "id", $index["index_id"])){// mpre("Младший морф не найден");
				}else{ return $index;
				}
			}, $INDEX))){ mpre("ОШИБКА выборки висящих морфов");
		}else if(!is_array($_INDEX = array_filter($_INDEX))){ mpre("ОШИБКА получения списка висячих морфов");
		}else if(!$Calc = function($index) use(&$Calc, $CALC, $CALC_VAL, $DANO){
//				mpre($index);
				if(!$INDEX = &$GLOBALS['BMF-INDEX']){ mpre("ОШИБКА получения данных справочника");
				}elseif(!$dano = rb($DANO, "id", $index["dano_id"])){ mpre("ОШИБКА получения исходного значения морфа");
				}elseif(!is_array($index_[$n = "Старший"] = rb($INDEX, "id", $index["index_id"]))){ mpre("ОШИБКА получения смежного морфа `{$n}`");
				}elseif(!is_array($index_[$n = "Младший"] = rb($INDEX, "id", $index["bmf-index"]))){ mpre("ОШИБКА получения смежного морфа `{$n}`");
				}elseif(!is_numeric($v1 = is_numeric(get($index_, $n = "Старший", "val")) ? $index_[$n]["val"] : $dano["val"])){ mpre("ОШИБКА расчета сигнала `{$n}`");
				}elseif(!is_numeric($v0 = is_numeric(get($index_, $n = "Младший", "val")) ? $index_[$n]["val"] : $dano["val"])){ mpre("ОШИБКА расчета сигнала `{$n}`");
				}elseif(!$calc_val = rb($CALC_VAL, "v1", "v0", $v1, $v0)){ mpre("ОШИБКА получения значения морфа");
				}elseif(!$calc = rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $calc_val["id"])){ mpre("ОШИБКА получения расчета морфа", $CALC, $index, $calc_val);
				}elseif(!$index = (["calc_val_id"=>$calc_val["id"], "val"=>$calc["val"]]+ $index)){ mpre("ОШИБКА сохранения расчетных значений");
				}elseif(!$index = fk("bmf-index", ["id"=>$index["id"]], null, $index)){ mpre("ОШИБКА сохранения расчетных значений морфа");
				}elseif(!$GLOBALS['BMF-INDEX'][$index["id"]] = $index){ mpre("ОШИБКА сохранения результатов расчета в справочник");
				}elseif(!is_array($_INDEX = array_merge(rb($INDEX, "index_id", "id", $index["id"]), rb($INDEX, "bmf-index", "id", $index["id"])))){ mpre("Вышестоящие морфы не найдены");
				}elseif(!is_array($_INDEX = array_map(function($index) use($Calc){ return $Calc($index); }, $_INDEX))){ mpre("ОШИБКА расчета вышестоящих морфов");
				}else{// mpre("Расчет", $index);
				} return $index;
			}){
		}else if(!is_array(array_map(function($index) use($Calc){ // Расчет морфа с низу
				if(!$_index = $Calc($index)){ mpre("ОШИБКА расчета значения морфа");
				}else{ return $_index; }
			}, $_INDEX))){ mpre("ОШИБКА расчета нижестоящего");
		}elseif(!is_array($_INDEX = rb($INDEX, "id", "id", $_INDEX))){ mpre("ОШИБКА обновления списка");
		}else{// mpre("Список висячих морфов", $_INDEX);
		} return $INDEX;
	}))): mpre("ОШИБКА расчета морфов") ?>
<? else:// mpre($INDEX, $GLOBALS['BMF-INDEX']) ?>
/*
	0101	0011
	1010	1100
*/
	<div>
		<style>
			.bs-header { display:none; }
			.bs-old-docs { display:none; }
			ul li { cursor:pointer; }
			ul li.active { background-color:yellow; }
		</style>
		<script sync>
			(function($, script){
				$(script).parent().on("ajax", function(e, table, get, post, complete, rollback){
					var href = "/<?=$arg['modname']?>:ajax/class:"+table;
					$.each(get, function(key, val){ href += "/"+ (key == "id" ? parseInt(val) : key+ ":"+ val); });
					$.post(href, post, function(data){ if(typeof(complete) == "function"){
						complete.call(e.currentTarget, data);
					}}, "json").fail(function(error) {if(typeof(rollback) == "function"){
							rollback.call(e.currentTarget, error);
					} alert(error.responseText) });
				}).one("init", function(e){
				}).on('dblclick', 'label', function(e){
					$(e.currentTarget).find("input[type=radio]:checked").prop("checked", false);
				}).ready(function(e){ $(script).parent().trigger("init"); })
			})(jQuery, document.currentScript)
		</script>
		<h1><?=$clump['name']?></h1>
		<? if(!$ITOG = call_user_func(function($clump_history) use($ITOG){ # Установка значений из истории
				if(!strlen(get($clump_history, 'itog'))){// mpre("Параметр итог истории не задан");
				}elseif(!$split = str_split($clump_history['itog'])){ mpre("ОШИБКА получения сигналов итога");
				}elseif(count($split) != count($ITOG)){ mpre("ОШИБКА разница в количестве элементов итого и количестве обучающих сигналов");
				}elseif(!$_ITOG = array_map(function($itog, $val) use($split){ # Установка значений итога
						if(!$_itog = fk('itog', ['id'=>$itog['id']], null, ['val'=>$val/*, 'tail'=>$tail*/])){ mpre("ОШИБКА обновления значения морфа");
						}else{// mpre("Не измененное и измененное значени морфа", $itog, $_itog);
							return $_itog;
						} return $itog;
					}, $ITOG, $split)){ mpre("ОШИБКА установки итогам обучающих сигналов");
				}elseif(!$_ITOG = rb($_ITOG, 'id')){ mpre("Восстановление ключей массива");
				}else{// mpre("Обновленные итоги", $_ITOG);
					return $_ITOG;
				} return $ITOG;
			}, $clump_history)): mpre("ОШИБКА установки значений итогов") ?>
		<? elseif(!is_array($_INDEX = rb($INDEX, "id", "id", rb($ITOG, 'index_id')))): mpre("ОШИБКА получения морфов по итогам") ?>
		<? else:// die(!mpre($ITOG, $_INDEX)) ?>
			<form method="post">
				<span style="float:right;">
					<a href="/bmf:clump/<?=$clump['id']?>/clean">Очистить</a>
				</span>
				<div class="table">
					<div>
						<span>
							<h3>История</h3>
							<? if(!is_array($CLUMP_HISTORY = array_map(function($_clump_history) use($clump_history){ # Получение разницы в сигналах с текущий историей
									if(!is_numeric($min = min(strlen(get($clump_history, 'dano')), strlen($_clump_history['dano'])))){ mpre("ОШИБКА определения длинны минимальной истории", $clump_history, $_clump_history);
									}elseif(!is_numeric($max = max(strlen(get($clump_history, 'dano')), strlen($_clump_history['dano'])))){ mpre("ОШИБКА определения длинны минимальной истории");
									}elseif(!is_string($dano = (get($clump_history, 'dano') ? substr($clump_history['dano'], 0, $min) : ""))){ mpre("Получение строки основной истории");
									}elseif(!is_string($_dano = substr($_clump_history['dano'], 0, $min))){ mpre("Получение строки основной истории");
									}elseif(!is_array($diff = array_filter(array_map(function($chr, $_chr){ return ($chr !== $_chr); }, str_split($dano), str_split($_dano))))){ mpre("Функция сравнения посимвольна");
									}elseif(!is_numeric($_clump_history['_diff'] = count($diff)+($max-$min))){ mpre("ОШИБКА расчета количество различий");
									}else{ return $_clump_history; }
								}, $CLUMP_HISTORY))): mpre("ОШИБКА установки разницы в сигналах") ?>
							<? elseif(!is_array($_CLUMP_HISTORY = rb($CLUMP_HISTORY, '_diff', 'val', 'id', 1, '[]'))): mpre("ОШИБКА получения списка с 1 различием") ?>
							<? elseif(!is_array($_clump_history = ($_CLUMP_HISTORY ? get($_CLUMP_HISTORY, array_rand($_CLUMP_HISTORY)) : []))): mpre("ОШИБКА получения случайного следующего элемента из списка с 1 различием") ?>
							<? else:// mpre($CLUMP_HISTORY) ?>
								<ul>
									<? foreach($CLUMP_HISTORY as $history): ?>
										<? if(!$color_bg = call_user_func(function($clump_history) use($_clump_history){
												if(get($_GET, 'bmf-clump_history') == $clump_history['id']){ return "#bbb";
												}elseif(get($_clump_history, 'id') == $clump_history['id']){ return "#cfc";
												}elseif(1 == $clump_history['_diff']){ return "#eee";
												}else{ return "inherit"; }
											}, $history)): mpre("ОШИБКА расчета цвета элемента") ?>
										<? elseif(!is_string($a_calc = (array_key_exists("calc", $_GET) ? "/calc" : ""))): mpre("ОШИБКА получения параметра расчета") ?>
										<? elseif(!$a_href = "/bmf:clump/{$clump['id']}/bmf-clump_history:{$history['id']}{$a_calc}"): mpre("ОШИБКА расчета адреса ссылки") ?>
										<? else: ?>
											<li style="padding:2px;">
												<?=$history['id']?>.
												<span>
													<span style="color:<?=($history['itog'] ? "red" : "blue")?>"><?=$history['itog']?></span>
													<a href="<?=$a_href?>" style="background-color:<?=$color_bg?>; padding:3px;">
														<? foreach(str_split($history['dano']) as $nn=>$val): ?>
															<span style="color:<?=($val ? "red" : "blue")?>;" title="<?=$nn?>"><?=$val?></span>
														<? endforeach; ?>
													</a>
													<span style="color:<?=($history['val'] ? "red" : "blue")?>;"><?=$history['val']?></span>
												</span>
											</li>
										<? endif; ?>
									<? endforeach; ?>
								</ul>
								<? if(!$_clump_history):// mpre("Последующий переход не задан") ?>
								<? else: ?>
									<div>Далее: <a href="/bmf:clump/<?=$clump['id']?>/bmf-clump_history:<?=$_clump_history['id']?>"><?=$_clump_history['dano']?></a></div>
								<? endif; ?>
							<? endif; ?>
							<?// mpre($clump_history) ?>
						</span>
						<span>
							<? if(!is_string($calc_history = ($clump_history ? "/bmf-clump_history:{$clump_history["id"]}" : ""))): mpre("ОШИБКА получения параметра истории адреса") ?>
							<? elseif(!$calc_name = (array_key_exists("calc", $_GET) ? "Вкл" : "Выкл")): mpre("ОШИБКА расчета заголовка ссылки")  ?>
							<? elseif(!$calc_href = (array_key_exists("calc", $_GET) ? "/bmf:clump{$calc_history}/{$clump["id"]}" : "/bmf:clump{$calc_history}/{$clump["id"]}/calc")): mpre("ОШИБКА получения ссылки вкл/выкл расчета") ?>
							<? else: ?>
								<span style="float:right">Расчет <a href="<?=$calc_href?>"><?=$calc_name?></a></span>
							<? endif; ?>
							<h3>Морфы</h3>
							<? if(!$tree = function($index, $bmf_index = [], $amend = "", $attr = ["del"=>0, "top"=>1]) use(&$tree, $CALC_POS, $CALC_VAL, $DANO, $ITOG, $CALC, $CALC_AMEND){ ?>
									<? if(!$INDEX = &$GLOBALS['BMF-INDEX']): mpre("ОШИБКА получения данных справочника"); ?>
									<? elseif(!is_array($dano = (get($index, 'dano_id') ? rb($DANO, 'id', $index['dano_id']) : []))): mpre("ОШИБКА выборки дано", $index) ?>
									<? elseif(!is_array($index_back = $index)): mpre("ОШИБКА сохранения значений до изменений") ?>
									<? elseif(!is_string($dano_val = call_user_func(function($val = "") use($index, $dano, $CALC_VAL, $CALC){
											if(!$dano){ //mpre("Висящий морф");
											}elseif(!$calc_val = rb($CALC_VAL, "v1", "v0", $dano["val"], $dano["val"])){ mpre("ОШИБКА расчета контрольного значения");
											}elseif(!$calc = rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $calc_val["id"])){ mpre("ОШИБКА получения расчетного расчета");
											}elseif(!is_numeric($val = $calc["val"])){ mpre("ОШИБКА установки расчетного значения");
											}else{ //mpre("Расчет значения", $calc);
											} return $val;
										}))): mpre("ОШИБКА расчета контрольного значения") ?>
									<? elseif(!is_array($index_[$f = 'index_id'] = (get($index, $f) ? rb($INDEX, 'id', $index[$f]) : []))): mpre("ОШИБКА выборки нижестоящих `{$f}`") ?>
									<? elseif(!is_array($index_[$f = 'bmf-index'] = (get($index, $f) ? rb($INDEX, 'id', $index[$f]) : []))): mpre("ОШИБКА выборки нижестоящих `{$f}`") ?>
									<? elseif(!is_numeric($attr["del"] = call_user_func(function($del = 0) use($attr, $index, $dano_val, $index_){
											if(!$attr["top"]){ //mpre("Не редактируемая нить");
											}elseif(!$index){ //mpre("Пустой морф");
											}elseif(!$attr["del"] && (get($index, "val") == $dano_val)){ //mpre("{$index["id"]} Не удаляем");
											}else{ $del = 1;
											} return $del;
										}))): mpre("ОШИБКА расчета признака удаления") ?>
									<? elseif(!is_array($bmf_dano = ($bmf_index ? rb($DANO, 'id', $bmf_index['dano_id']) : []))): mpre("Получение дано родителя") ?>
									<? elseif(!is_array($calc = ($index ? rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $index["calc_val_id"]) : []))): mpre("ОШИБКА получения расчета морфа"); ?>
									<? elseif(!is_array($calc_val = (get($index, 'calc_val_id') ? rb($CALC_VAL, 'id', $index['calc_val_id']) : []))): mpre("ОШИБКА выборки значения морфа"); ?>
									<? elseif(!is_array($calc_pos = (get($index, 'calc_pos_id') ? rb($CALC_POS, 'id', $index['calc_pos_id']) : []))): mpre("ОШИБКА выборки позиции морфа") ?>
									<? elseif(!is_array($calc_amend = ($calc ? rb($CALC_AMEND, "id", $calc["calc_amend_id"]) : []))): mpre("ОШИБКА получения изменения значения"); ?>
									<? /*elseif(!is_array($calc_amend = call_user_func(function($calc_amend = []) use($calc, $calc_pos, $calc_val, $index, $attr, $CALC, $CALC_POS, $CALC_AMEND){
											if(!$calc){ //mpre("Висячий морф");
											}elseif(!$field = ($calc["val"] ? "index_id" : "bmf-index")){ mpre("ОШИБКА выборки направления");
											}elseif(!$_calc_pos = rb($CALC_POS, "id", $calc_pos["calc_pos_id"])){ mpre("ОШИБКА выборки смежной позиции");
											//}elseif(!$_calc = rb($CALC, "calc_pos_id", "calc_val_id", $_calc_pos["id"], $calc_val["id"])){ mpre("ОШИБКА выборки возможного расчета");
											//}elseif(!$attr["del"] && $_calc["val"] != $calc["val"]){ //mpre("Подходит и без смены значения");
											}elseif(!$calc_amend = rb($CALC_AMEND, "field", $w = "[{$field}]")){ mpre("ОШИБКА выборки направления {$w}");
											//}elseif(!$attr["del"]){ //mpre("Прямая ветка");
											//}elseif(!$calc_amend = rb($CALC_AMEND, "id", $calc_amend["calc_amend_id"])){ mpre("ОШИБКА получения изменения значения");
											}else{// mpre("Направление", $calc_pos, $_calc_pos, $calc_amend);
											} return $calc_amend;
										}))): mpre("ОШИБКА расчета направления развития") ?>
									<? elseif(!is_array($calc_amend = call_user_func(function($calc_amend = []) use($calc, $attr, $index_, $calc_val, $CALC_AMEND){
											if(!$calc){ //mpre("Висячий морф");
//											}elseif((2 == count(array_filter($index_))) && $attr["del"] && $calc_val["v1"] == $calc_val["v0"]){ //mpre("Неопределенное состояние");
											}elseif(!$calc_amend = rb($CALC_AMEND, "id", $calc["calc_amend_id"])){ mpre("ОШИБКА получения изменения значения");
//											}elseif($rand = rand(0, 1)){// mpre($rand);
//											}elseif(!$calc_amend = rb($CALC_AMEND, "id", $calc_amend["calc_amend_id"])){ mpre("ОШИБКА получения изменения значения");
											}else{ //mpre("Перевернутое направление", $calc_amend);
											} return $calc_amend;
										}))): mpre("ОШИБКА расчета направления развития") ?>
									<? elseif(!is_array($calc_amend = call_user_func(function($calc_amend = []) use($calc, $attr, $index_, $calc_val, $CALC_AMEND){
											if(!$calc){// mpre("Висячий морф");
//											}elseif($attr["del"] && $calc_val["v1"] == $calc_val["v0"]){// mpre("Неопределенный режим");
											}elseif(!$field = ($calc["val"] ? "index_id" : "bmf-index")){ mpre("ОШИБКА выборки направления");
											}elseif(!is_array($_index = get($index_, $field))){ mpre("ОШИБКА выборки мофа перехода");
											//}elseif(!$_index && $attr["del"] && array_filter($index_)){ mpre("Правка морфа");
											}elseif(!$calc_amend = rb($CALC_AMEND, "field", $w = "[{$field}]")){ mpre("ОШИБКА выборки направления {$w}");
											}else{// mpre("Направление развития", $calc_amend);
											} return $calc_amend;
										}))): mpre("ОШИБКА выбора направления развития")*/ ?>
									<? //elseif(true): mpre($calc_amend) ?>
									<? elseif(!is_string($content = call_user_func(function() use($tree, $attr, $index, $dano, $index_, $calc_amend){ // Получение содержимого отображения нижестоящих
											if(!ob_start()){ mpre("ОШИБКА включения буфера");
											}elseif(!$index_ = array_map(function($_amend, $_index) use($tree, $attr, $dano, $calc_amend, $index, $index_){ // Перебираем нижестоящие
													if(!$index){// mpre("У пустого элемента не отображаем нижестоящие");
													//}elseif(!is_numeric($attr["top"] = ($attr["top"] && (get($calc_amend, "field") == $_amend)) ? 1 : 0)){ mpre("ОШИБКА расчета признака верности");
													}elseif(!is_numeric($attr["top"] = call_user_func(function($top = 1) use($attr, $_amend, $calc_amend){
															/*if(!$top){ mpre("Вне ветки");
															}else*/if(get($calc_amend, "field") == $_amend){ //mpre("Совпало имя");
															}elseif(get($calc_amend, "name") == "Оба"){ //mpre("Все");
															}else{ $top = 0;
															} return $top;
														}))){ mpre("ОШИБКА получения расчетной ветки");
													/*}elseif(!is_numeric($attr["top"] = call_user_func(function($top = 0) use($index, $attr, $calc_amend, $_amend, $index_){
															if(!array_filter($index_) && !$calc_amend){ mpre("{$index["id"]} Нет нижестоящих знаков");
//															}elseif(!array_filter($index_)){ mpre("Нет морфов");
															}elseif(!$attr["top"]){ //mpre("Уже вышли из нити");
															}elseif($calc_amend && $calc_amend["field"] != $_amend){ //mpre("Другая ветка  `{$_amend}`", $calc_amend);
															}else{ $top = 1;
															} return $top;
														}))){ mpre("ОШИБКА расчета ветки расчетов");*/
													}elseif(!is_array($_index = $tree($_index, $index, $_amend, $attr))){ mpre("ОШИБКА запуска фнукции отрисовки нижестоящего морфа");
													}else{// mpre($index["id"], $attr);
													}
												}, array_keys($index_), $index_)){ mpre("ОШИБКА перебора нижестоящих значений");
											}elseif(!is_string($content = ob_get_clean())){ mpre("ОШИБКА получения буфера");
											}else{ return $content; }
										}))): mpre("ОШИБКА расчета содержимого нижестоящих") ?>
									<? elseif(!$INDEX = &$GLOBALS['BMF-INDEX']): mpre("ОШИБКА получения данных справочника"); ?>
									<? elseif(!is_array($_calc = call_user_func(function($_calc = []) use($index, $attr, $calc, $calc_pos, $dano, $INDEX, $CALC, $CALC_POS, $CALC_VAL){ // Расчет морфа
											if(!is_array($index_[$f = 'index_id'] = (get($index, $f) ? rb($INDEX, 'id', $index[$f]) : []))){ mpre("ОШИБКА выборки нижестоящих `{$f}`");
											}elseif(!is_array($index_[$f = 'bmf-index'] = (get($index, $f) ? rb($INDEX, 'id', $index[$f]) : []))){ mpre("ОШИБКА выборки нижестоящих `{$f}`");
											}elseif(!is_numeric($v1 = is_numeric(get($index_, $f = "index_id", "val")) ? $index_[$f]["val"] : get($dano, "val"))){// mpre("ОШИБКА получения знака `{$f}`");
											}elseif(!is_numeric($v0 = is_numeric(get($index_, $f = "bmf-index", "val")) ? $index_[$f]["val"] : $dano["val"])){// mpre("ОШИБКА получения знака `{$f}`");
											}elseif(!$_calc_val = rb($CALC_VAL, "v1", "v0", $v1, $v0)){ mpre("ОШИБКА получения значения");
											}elseif(!$_calc = rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $_calc_val["id"])){ mpre("ОШИБКА поиска нового расчета с изменившимся значением");
											}else{// mpre($index["id"], $calc, $_calc);
											} return $_calc;
										}))): mpre("ОШИБКА расчета нового значения морфа") ?>
									<? elseif(!is_array($index = call_user_func(function($index) use($calc, $index_back, $calc_pos, $calc_val, $dano, $index_, $attr, $calc_amend, &$INDEX, $CALC_POS, $CALC_VAL, $CALC){ // Расчет значения морфа
											if(!$index){// mpre("Пустой морф");
											}elseif(!is_string($field = $calc_amend ? get($calc_amend, 'field') : "")){ mpre("ОШИБКА получения поля дальнейшего развития");
											}elseif(!is_array($_index = ($field ? get($index_, $field) : []))){ mpre("ОШИБКА получения нижестоящего морфа");
											}elseif(!is_array($index_[$f = 'index_id'] = (get($_index, $f) ? rb($INDEX, 'id', $_index[$f]) : []))){ mpre("ОШИБКА выборки нижестоящих `{$f}`");
											}elseif(!is_array($index_[$f = 'bmf-index'] = (get($_index, $f) ? rb($INDEX, 'id', $_index[$f]) : []))){ mpre("ОШИБКА выборки нижестоящих `{$f}`");
											}elseif(!$index = call_user_func(function($index) use($calc, $calc_pos, $calc_val, $_index, $index_, $attr, $field, $CALC_VAL){
													if($_index){ //mpre("Нижестоящий морф не пуст");
													}elseif(!$attr["top"]){ //mpre("Не определяющая нить");
													}elseif(!$field){ //mpre("Ситуация удаления");
													}elseif(array_filter($index_)){ mpre("У нижестоящего морфа есть смежные");
													}elseif(!$val_name = get(["index_id"=>"v1", "bmf-index"=>"v0"], $field)){ mpre("ОШИБКА получения поля значения");
													}elseif(!$calc_val = rb($CALC_VAL, "id", $index["calc_val_id"])){ mpre("ОШИБКА получения текущего значения");
													}elseif(!is_numeric($val = get($calc_val, $val_name))){ mpre("ОШИБКА получения текущего значения");
													}elseif(!is_numeric($_val = $val ? 0 : 1)){ mpre("ОШИБКА инвертирования значения");
//													}elseif(!is_numeric($_val = ($calc["id"] == $calc_pos["calc_id"] ? $val : ($val ? 0 : 1)))){ mpre("ОШИБКА инвертирования значения");
													}elseif(!$vals = [$val_name=>$_val]+ array_intersect_key($calc_val, array_flip(["v1", "v0"]))){ mpre("ОШИБКА инвертирование текущего значения");
													}elseif(!$_calc_val = rb($CALC_VAL, "v1", "v0", $vals["v1"], $vals["v0"])){ mpre("ОШИБКА получения обновленного значения");
													}elseif(!$index["calc_val_id"] = $_calc_val["id"]){ mpre("ОШИБКА установки значения морфу");
													}else{// mpre("{$index["id"]} Расчет добавления нижестоящего морфа `{$_val}`", $calc, $calc_pos);
													} return $index;
												}, $index)){ mpre("ОШИБКА получения значения при добавлении морфа");
											}elseif(!$index = call_user_func(function($index) use($_index, $index_, $dano, $attr, $field, &$INDEX, $CALC_VAL){
													if(!$_index){ //mpre("Нижестоящий морф пуст");
													}elseif(!$attr["top"]){// mpre("Не определяющая нить");
													}elseif(!$attr["del"]){// mpre("Ситуация удаления");
													}elseif(array_filter($index_)){// mpre("У нижестоящего морфа есть смежные");
													}elseif(!$val_name = get(["index_id"=>"v1", "bmf-index"=>"v0"], $field)){ mpre("ОШИБКА получения поля значения");
													}elseif(!$calc_val = rb($CALC_VAL, "id", $index["calc_val_id"])){ mpre("ОШИБКА получения текущего значения");
													}elseif(!$vals = [$val_name=>$dano["val"]]+ array_intersect_key($calc_val, array_flip(["v1", "v0"]))){ mpre("ОШИБКА инвертирование текущего значения");
													}elseif(!$_calc_val = rb($CALC_VAL, "v1", "v0", $vals["v1"], $vals["v0"])){ mpre("ОШИБКА получения обновленного значения");
													}elseif(!$index["calc_val_id"] = $_calc_val["id"]){ mpre("ОШИБКА установки значения морфу");
													}else{// mpre("{$index["id"]} Расчет удаления нижестоящего морфа", "$field ({$val_name})", $_calc_val);
													} return $index;
												}, $index)){ mpre("ОШИБКА получения значения при удалении морфа");
											}elseif(!$index = call_user_func(function($index) use($calc, $calc_pos, $_index, $index_, $dano, $attr, $field, &$INDEX, $CALC_VAL){
													if(!$_index){ //mpre("Нижестоящий морф пуст");
													}elseif(!$attr["top"]){// mpre("Не определяющая нить");
													}elseif($attr["del"] && !array_filter($index_)){// mpre("Морф удаления обрабатываем отдельно");
													}elseif(!$val_name = get(["index_id"=>"v1", "bmf-index"=>"v0"], $field)){ mpre("ОШИБКА получения поля значения");
													}elseif(!$calc_val = rb($CALC_VAL, "id", $index["calc_val_id"])){ mpre("ОШИБКА получения текущего значения");
													}elseif(!is_numeric($val = get($calc_val, $val_name))){ mpre("ОШИБКА получения текущего значения");
													}elseif(!is_numeric($_val = $val ? 0 : 1)){ mpre("ОШИБКА инвертирования значения");
//													}elseif(!is_numeric($_val = ($calc["id"] == $calc_pos["calc_id"] ? $val : ($val ? 0 : 1)))){ mpre("ОШИБКА инвертирования значения");
													}elseif(!$vals = [$val_name=>$_val]+ array_intersect_key($calc_val, array_flip(["v1", "v0"]))){ mpre("ОШИБКА инвертирование текущего значения");
													}elseif(!$_calc_val = rb($CALC_VAL, "v1", "v0", $vals["v1"], $vals["v0"])){ mpre("ОШИБКА получения обновленного значения");
													}elseif(!$index["calc_val_id"] = $_calc_val["id"]){ mpre("ОШИБКА установки значения морфу");
													}else{ //mpre("{$index["id"]} Расчет изменений ветки", $index);
													} return $index;
												}, $index)){ mpre("ОШИБКА получения значения в изменяемой ветке");
											//}elseif(!$calc = rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $index["calc_val_id"])){ mpre("ОШИБКА получения текущего расчета");
//											}elseif($calc["val"] != $index["val"]){ //mpre("{$index["id"]} Значение изменилось. Не меняем позицию");
											}elseif(!$index = call_user_func(function($index) use($attr, $index_back, $calc_pos, $calc_val, $CALC, $CALC_POS){
													if(!$_calc_pos = rb($CALC_POS, "id", $index["calc_pos_id"])){ mpre("ОШИБКА получения текущей позиции");
													}elseif(!$_CALC_POS = rb($CALC_POS, "id", "id", array_flip([$_calc_pos["calc_pos_id"], $_calc_pos["bmf-calc_pos"]]))){ mpre("ОШИБКА получения позиций для перехода");
													}elseif(!is_numeric($val = $index["val"])){ mpre("ОШИБКА не определен знак морфа");
													}elseif(!is_numeric($_val = $attr["top"] ? ($val ? 0 : 1) : $val)){ mpre("ОШИБКА инвертирования текущего знака");
													/*}elseif(!is_numeric($_val = call_user_func(function($_val) use($attr, $calc, $calc_pos){
															if(!$attr["top"]){ mpre("Вне нити изменений");
															}elseif($calc["id"] = $calc_pos["calc_id"]){ mpre("Исключение - не переворачиваем результат");
															}elseif(!is_numeric($_val = $_val ? 0 : 1)){ mpre("ОШИБКА инвертации сигнала");
															}else{ mpre("Инвертированный сигнал");
															} return $_val;
														}, $val))){ mpre("ОШИБКА расчета необходимого результата");*/
													}elseif(!$_calc = rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $index["calc_val_id"])){ mpre("ОШИБКА выборки расчета с измененными значениями");
													
													}elseif(/*($calc_val["v1"] == $calc_val["v0"]) &&*/ ($_calc["val"] != $index_back["val"])){// mpre("Пропуск изменений позиции");
													}elseif(!$_calc = rb($CALC, "calc_pos_id", "calc_val_id", "val", $_CALC_POS, $index["calc_val_id"], $_val)){ mpre("ОШИБКА выборки подходящего значения");
													}elseif(!$_calc_pos = rb($CALC_POS, "id", $_calc["calc_pos_id"])){ mpre("ОШИБКА выборки обновленной позиции");
													}elseif(!$index["calc_pos_id"] = $_calc_pos["id"]){ mpre("ОШИБКА установки расчетной позиции");
													}else{ //mpre("Позиции перехода", $calc_pos, $index);
													} return $index;
												}, $index)){ mpre("ОШИБКА получения обновленной позиции морфа");
											}elseif(!is_array($_calc = $index ? rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $index["calc_val_id"]) : [])){ mpre("ОШИБКА выборки обновленной позиции");
											}elseif(!is_numeric($index["val"] = $_calc ? $_calc["val"] : $index["val"])){ mpre("ОШИБКА обновления значения морфа");
											}elseif(!$GLOBALS['BMF-INDEX'][$index["id"]] = $index){ mpre("ОШИБКА сохранения значения в справочник");
											}else{ //mpre("{$index["id"]} Расчет значения", $index);
											} return $index;
										}, $index))): mpre("ОШИБКА расчета управляющего значения") ?>
									<? elseif(!is_array($_calc_pos = $index ? rb($CALC_POS, "id", $index["calc_pos_id"]) : [])): mpre("ОШИБКА получения обновленной позиции") ?>
									<? elseif(!is_array($_calc_val = $index ? rb($CALC_VAL, "id", $index["calc_val_id"]) : [])): mpre("ОШИБКА получения обновленного значения") ?>
									<? elseif(!is_array($_calc = $index ? rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $index["calc_val_id"]) : [])): mpre("ОШИБКА выборки обновленной позиции") ?>
									<? elseif(!$INPUT = call_user_func(function($INPUT = []) use($amend, $attr, $index, $index_, $dano, $bmf_index, $bmf_dano, $_calc, $calc_pos, $calc_val, $DANO){// mpre("Расчет типа, имени и количества элементов выбора");
											if(!$calc_pos){ /*mpre($bmf_index["id"], $attr);*/ $INPUT = [["name"=>"index[add][{$amend}]", "disable"=>false, "readonly"=>false, "value"=>$amend, "type"=>"radio", "checked"=>(!$attr["del"] && $attr["top"]), "title"=>"Расширить морф"]];
											}elseif(!is_bool($checked_val = (bool)array_filter($index_))){ mpre("Признак изменения по позиции и значению");
											}elseif(!is_bool($checked_pos = call_user_func(function() use($attr, $index, $dano, $index_, $calc_val, $DANO){ // Только позицию без значения
													if(!$attr["top"]){// mpre("{$index["id"]}.Ветка морфа не является расчетной");
													}else{ return true; } return false;
												}))){ mpre("ОШИБКА расчета признака позиции");
											}elseif(!is_bool($checked_del = call_user_func(function() use($attr, $index, $index_, $bmf_dano){ // Удаление морфа
													if(!$attr["top"]){// mpre("{$index["id"]} Ветка морфа не является расчетной");
													}elseif(!$attr["del"]){// mpre("{$index["id"]} Ветка морфа не является расчетной");
													}elseif(array_filter($index_)){// mpre("{$index["id"]} Есть смежные элементы (не можем удалить)");
													}else{ return true; } return false;
												}))){ mpre("ОШИБКА расчета признака удаления морфа");
											}else{// mpre($index, $index_);
												$INPUT = [
													["name"=>"index[edit][{$index["id"]}]", "disabled"=>false, "readonly"=>false, "value"=>$_calc["id"], "type"=>"radio", "checked"=>$checked_pos, "title"=>"Изменить позицию и значение"],
													["name"=>"index[edit][{$index["id"]}]", "disabled"=>false, "readonly"=>false, "value"=>"", "type"=>"radio", "checked"=>$checked_del, "title"=>"Удаление морфа"],
												];
											} return $INPUT;
										})): mpre("ОШИБКА определения типа выбора элемента") ?>
									<? elseif(![$color_id, $title_id] = call_user_func(function() use($index_back, $calc){
											if(!$index_back){ return ["inherit", "Морф пуст"];
											}elseif($index_back["val"] != $calc["val"]){ return ["red", "Значение расчета не верно"];
											}else{// mpre("ОШИБКА цвета не установлена");
											} return ["inherit", "Нет ошибок"];
										})): mpre("ОШИБКА расчета цвета идентификатора") ?>
									<? elseif(!$pos_href = "/bmf:clump". (($h = get($_GET, "bmf-clump_history")) ? "/bmf-clump_history:{$h}" : ""). (($i = get($_GET, 'id')) ? "/{$i}" : ""). (array_key_exists('calc', $_GET) ? "/calc" : "")): mpre("ОШИБКА составления адреса ссылки для смены позиции") ?>
									<? elseif(!$pos_href = call_user_func(function($href = "/bmf:clump") use($index, $calc_pos, $CALC_POS){ // Адрес изменения позиции морфа
											if(!$href .= (($h = get($_GET, "bmf-clump_history")) ? "/bmf-clump_history:{$h}" : "")){ mpre("ОШИБКА добавления истории");
											}elseif(!$calc_pos){ //mpre("Позиция текущего морфа не задана");
											}elseif(!$href .= (($i = get($_GET, 'id')) ? "/{$i}" : "")){ mpre("ОШИБКА добавления идентификатора скопления");
											}elseif(!$href .= (array_key_exists('calc', $_GET) ? "/calc" : "")){ mpre("ОШИБКА добавления признака пересчета");
											}elseif(!$_calc_pos = rb($CALC_POS, "value", $calc_pos["value"]+1) ?: rb($CALC_POS, "value", 0)){ mpre("ОШИБКА получения следующей позиции для изменения");
											}elseif(!$href .= "/bmf-index:{$index["id"]}/bmf-calc_pos:{$_calc_pos["id"]}"){ mpre("ОШИБКА добавления номера текущего морфа");
											}else{ //mpre("Адрес изменнения позиции", $href);
											} return $href;
										})): mpre("ОШИБКА получения адреса изменения позиции") ?>
									<? else:// mpre($index, $dano, $attr) ?>
										<li style="opacity:<?=($index ? 1 : 0.5)?>;">
											<span style="color:<?=$color_id?>;" title="<?=$title_id?>"><?=get($index, 'id')?><?=get($index, 'id_')?></span>.
											<? if(empty($bmf_index)): ?>
												<?=$dano['val']?>
											<? endif; ?>
											<label style="font-weight:normal;background-color:<?=($attr["top"] ? "#ddd" : "inherit")?>;">
												<? foreach($INPUT as $input): ?>
													<input name="<?=$input["name"]?>" value="<?=$input["value"]?>" type="<?=$input["type"]?>" title="<?=$input["title"]?>" <?=(get($input, "checked") ? "checked" : "")?> <?=(get($input, "disabled") ? "disabled" : "")?> <?=(get($input, "readonly") ? "readonly" : "")?>>
												<? endforeach; ?>
												<span title="Идентификатор дано">#<?=($dano ? $dano['id'] : $bmf_dano['id'])?></span>
												<span style="background-color:<?=($attr["del"] ? "yellow" : "inherit")?>">
													<span title="Значение морфа" style="color:<?=(get($index_back, 'val') ? "red" : "blue")?>">
														<?=get($index_back, 'val')?>
													</span> &raquo;
													<span title="Значение после изменений" style="color:<?=(get($_calc, 'val') ? "red" : "blue")?>"><?=get($_calc, "val")?></span>
													<span title="Контрольное значение" style="color:<?=($dano_val ? "red" : "blue")?>">
														<?=$dano_val?>
													</span>
												</span>
												<span>
													<a title="Позиция морфа" style="font-weight:bold;" href="<?=$pos_href?>"><?=get($calc_pos, 'name')?></a> &raquo;
													<span><?=get($_calc_pos, "name")?></span> :
													<span title="Нижестоящие сигналы" style="font-weight:bold;"><?=get($calc_val, 'name')?></span> &raquo;
													<span><?=get($_calc_val, "name")?></span>
													<span title="Новая позиция морфа">(<?=get($calc_amend, "name")?>)</span>
													<? if(!$index): ?>
														<select>
															<? foreach($DANO as $_dano): ?>
																<option><?=$_dano["id"]?> <?=$_dano["val"]?></option>
															<? endforeach; ?>
														</select>
													<? endif; ?>
												</span>
												<span title="Значение дано" style="color:<?=(get($dano, "val") ? "red" : "blue")?>"><?=get($dano, "val")?></span>
											</label>
											<ul><?=$content?></ul>
										</li>
									<? endif; ?>
								<? return $index_back; }): mpre("ОШИБКА обьявления функции дерева") ?>
							<? else:// mpre($_INDEX) ?>
								<ul>
									<? foreach($_INDEX as $index): ?>
										<? if(!$itog = rb('itog', 'index_id', $index['id'])): mpre("ОШИБКА получения итога морфа") ?>
										<? elseif(!$learn = ($index['val'] == $itog['val'] ? -1 : 1)): mpre("ОШИБКА получения флага обучения") ?>
										<? elseif(!is_array($CLUMP_HISTORY = array_map(function($_clump_history) use($index, $clump_history, $learn){
												if($_clump_history['id'] == get($clump_history, 'id')){// mpre("Текущий элемент истории", $learn);
													if(1 == $learn){ return $_clump_history; mpre("Не записываем ошибку");
													}elseif(!$_clump_history = fk('clump_history', ['id'=>$_clump_history['id']], null, ['val'=>$index['val']])){ mpre("ОШИБКА установки текущего значения морфу");
													}else{ return $_clump_history; }
												}elseif(1 != $learn){ return $_clump_history;
												}elseif(!$_clump_history = fk('clump_history', ['id'=>$_clump_history['id']], null, ['val'=>null])){ mpre("ОШИБКА обнуления значений истории");
												}else{ return $_clump_history; }
											}, $CLUMP_HISTORY))): mpre("ОШИБКА установки значения истории морфа") ?>
										<? elseif(!$index = $tree($index, $learn)): mpre("ОШИБКА запуска функции отрисовки дерева") ?>
										<? else:// mpre($index) ?>
										<? endif; ?>
									<? endforeach; ?>
								</ul>
							<? endif; ?>
						</span>
						<span style="width:20%;">
							<h3>Итоги</h3>
							<?// mpre($clump_history) ?>
							<ul>
								<? foreach($ITOG as $itog): ?>
									<? if(!is_array($_index = rb($_INDEX, 'id', $itog['index_id']))): mpre("ОШИБКА выборки корневого дано") ?>
									<? else:// mpre($_index) ?>
										<li>
											<label style="background-color:<?=(get($_index, "val") == get($clump_history, "itog") ? "#dfd" : "#fdd")?>; padding:0 5px;">
												<input name="itog_id" value="<?=$itog['id']?>" type="radio" <?=($_index ? "disabled" : "")?>>
												#<?=$itog['name']?>
												<span title="Значение итога" style="color:<?=($itog['val'] ? "red" : "blue")?>">
													<?=$itog['val']?>
												</span>
												<?//=$itog['shifting_tmp']?>
											</label>
										</li>
									<? endif; ?>
								<? endforeach; ?>
							</ul>
							<h3>Дано</h3>
							<ul>
								<? foreach($DANO as $dano): ?>
									<li>
										<label style="font-weight:normal; white-space:nowrap;">
											<input name="dano_id" value="<?=$dano['id']?>" type="radio">
											<?=$dano['name']?>#<?=$dano['id']?>
											<span title="Значение дано" style="font-weight:bold; color:<?=($dano['val'] ? "red" : "blue")?>">
												<?=$dano['val']?>
											</span>
											<span style="display:inline-block; max-width:100px; overflow:hidden; text-overflow:ellipsis;" title="<?=$dano['values']?>"><?=$dano['values']?></span>
										</label>
									</li>
								<? endforeach; ?>
							</ul>
							<p>
								<button type="submit" <?=(get($_index, "val") == get($clump_history, "itog") ? "disabled" : "")?>>Править</button>
							</p>
							<!--<p><button type="submit" name="act" value="delete">Сникуть</button></p>-->
						</span>
					</div>
				</div>
			</form>
		<? endif; ?>
	</div>
<? endif; ?>
