<?

if(!$cut = ["m"]){ mpre("ОШИБКА установки вырезаемых значений");
}else if(!$data = ['dano', 'itog', 'дано', 'итог']){ mpre("ОШИБКА установки неиспользуемых имен");
}else if(!is_array($get = array_diff_key(array_diff_key($_GET, array_flip($cut)), array_flip($data)))){ mpre("Убираем информацию о модуле");
}else if(!is_array($flip = array_flip($get))){ mpre("ОШИБКА переварачивания входных значений");
//}else if(!$db = get($flip, "")){ //mpre("ОШИБКА сеть не указана <a href='/bmf/test'>/bmf/test</a>");
}else if(!$db = (array_key_exists("null", $_GET) ? get($_GET, "") : array_search("", $_GET))){ mpre("ОШИБКА сеть не указана <a href='/bmf/test'>/bmf/test</a>", $flip);
}else if(!$clump_db = "modules/bmf/sh/cpp/clump/{$db}.sqlite"){ mpre("ОШИБКА файла данных скопления не найден");
}else if(!is_array($request = array_intersect_key($_REQUEST, array_flip($data)))){ mpre("ОШИБКА определения параметра");
}else if(!is_array([$DANO_VALUES, $ITOG_VALUES, $INDEX] = call_user_func(function($DANO_VALUES = [], $ITOG_VALUES = [], $return = [[], [], []]) use($clump_db, $request){
		if($_POST){ //mpre("Параметры заданы");
		}else if(!file_exists($clump_db)){ //mpre("Файл БД {$clump_db} не найден");
		//}else if(true){ mpre("Проверка");
		}else if(!qw($s = "ATTACH DATABASE '{$clump_db}' AS tmp")){ mpre("ОШИБКА подключения файла базы скопления {$s}");
		}else if(!is_array($DANO_VALUES = qn("SELECT * FROM tmp.mp_bmf_dano_values"))){ mpre("ОШИБКА получения списка значений");
		}else if(!is_array($ITOG_VALUES = qn("SELECT * FROM tmp.mp_bmf_itog_values"))){ mpre("ОШИБКА получения списка значений");
		}else if(!is_array($INDEX = ql("SELECT COUNT(*) as `cnt` FROM tmp.mp_bmf_index"))){ mpre("ОШИБКА получения списка морфов");
		}else if(!qw($s = "DETACH DATABASE 'tmp'")){ mpre("ОШИБКА подключения файла базы скопления {$s}");
		}else if(!$return = [$DANO_VALUES, $ITOG_VALUES, $INDEX]){ mpre("ОШИБКА получения данных БД");
		}else{
		} return $return;
	}))){ exit(mpre("ОШИБКА Подключения к БД"));
}else if(!$dano = call_user_func(function($response = "dano[]=") use($DANO_VALUES, $data){ // Строка запросов
		if(!$DANO_VALUES){ //mpre("Список значений пуст");
		//}else if($request = array_intersect_key($_REQUEST, array_flip($data))){ return $request;
		}else if(!$NAME = array_column($DANO_VALUES, "name")){ mpre("ОШИБКА получения имен параметров");
		}else if(!$VALUE = array_column($DANO_VALUES, "value")){ mpre("ОШИБКА получения значений параметров", $DANO_VALUES);
		//}else if(true){ mpre($DANO_VALUES);
		}else if(!$MESS = array_map(function($name, $value){ // Составление списка
				return "dano[{$name}]={$value}";
			}, $NAME, $VALUE)){ mpre("ОШИБКА составления списка");
		}else if(!$response = implode("&", $MESS)){ mpre("ОШИБКА составления итоговой строки");
		}else{ //mpre($NAME, $VALUE, $MESS);
		} return $response;
	})){ mpre("Сеть <b>". $db. "</b>", "Не указаны входные данные", "?{$dano}");
}else if(!$itog = call_user_func(function($response = "itog[]=") use($ITOG_VALUES, $data){ // Строка запросов
		if(!$ITOG_VALUES){ //mpre("Список значений пуст");
		}else if(!$NAME = array_column($ITOG_VALUES, "name")){ mpre("ОШИБКА получения имен параметров");
		}else if(!$VALUE = array_column($ITOG_VALUES, "value")){ mpre("ОШИБКА получения значений параметров", $DANO_VALUES);
		}else if(!$MESS = array_map(function($name, $value){ // Составление списка
				return "itog[{$name}]={$value}";
			}, $NAME, $VALUE)){ mpre("ОШИБКА составления списка");
		}else if(!$response = implode("&", $MESS)){ mpre("ОШИБКА составления итоговой строки");
		}else{ //mpre($NAME, $VALUE, $MESS);
		} return $response;
	})){ mpre("Сеть <b>". $db. "</b>", "Не указаны входные данные", "&{$itog}");
}else if(!$request){ //pre("<nobr>Сеть <b>". $db. "</b> (". (int)get($INDEX, 0, 'cnt'). ")</nobr>", "Параметры", "?{$dano}", "&{$itog}");
//}else if(true){ exit(mpre("Проверка"));
}else if($data = array_filter(array_map(function($val){ return (is_numeric($val) ? 0 : $val); }, $request["dano"]))){ mpre("ОШИБКА параметры допускаются только числовые", $data);
}else if(!$json = json_encode($request, 256)){ mpre("ОШИБКА формирования json строки");
}else if(!$file_name = "modules/bmf/sh/cpp/bimorph"){ mpre("ОШИБКА исполняемый файл не существует `{$file_name}`");
}else if(!touch($clump_db)){ mpre("ОШИБКА создания файла БД");
//}else if(!is_bool(chmod($clump_db, 0777))){ mpre("ОШИБКА установки всем полных прав на файл");
}else if(call_user_func(function() use($clump_db){ // Установка прав доступа
		if(!is_writable($clump_db)){ mpre("Нет прав доступа к файлу");
		}else if(!$own = fileowner($clump_db)){ mpre("ОШИБКА получения идентификатора владельца");
		}else if(!$uid = posix_getpwuid($own)){ //mpre("ОШИБКА получения свойств пользователя {$own}");
		//}else if(!$user = get_current_user($uid)){ //mpre("ОШИБКА получения имени пользователя");
		//}else if(!chmod($clump_db, 0777)){ mpre("ОШИБКА установки всем полных прав на файл", $own);
		}else{ //mpre("Права доступа к файлу изменены");
		}
	})){ mpre("ОШИБКА установки прав доступа");
}else if(!$cmd = "echo '[{$json}]' | {$file_name} {$clump_db} 2>/dev/null"){ mpre("ОШИБКА установки строки запуска");
//}else if(true){ mpre($cmd);
}else if(!$data = shell_exec($cmd)){ mpre("ОШИБКА запуска приложения", $cmd);
}else if(!$response = json_decode($data, true)){ mpre("ОШИБКА расшифровки json файла", $cmd, $data);
}else if($_POST){ exit(json_encode($response));
}else if($itog = call_user_func(function($itog = "") use($request, $response){
		if(get($request, 'itog')){ //mpre("Итог задан");
		}else if(!$itog = get($response, "itog")){ mpre("ОШИБКА результаты не найдены в возвращенных данных", $response);
		}else if(!$NAME = array_keys($itog)){ mpre("ОШИБКА получения имен параметров");
		}else if(!$VALUE = array_values($itog)){ mpre("ОШИБКА получения значений параметров", $DANO_VALUES);
		}else if(!$MESS = array_map(function($name, $value){ // Составление списка
				return "itog[{$name}]={$value}";
			}, $NAME, $VALUE)){ mpre("ОШИБКА составления списка");
		}else if(!$response = implode("&", $MESS)){ mpre("ОШИБКА составления итоговой строки");
		}else{
			return $response;
		}
	})){ pre("<nobr>Сеть <b>". $db. "</b> (". (int)get($INDEX, 0, 'cnt'). ")</nobr>", $request, "&{$itog}", $response);
//}else if(!get($request, 'itog')){ pre("<nobr>Сеть <b>". $db. "</b></nobr> (". (int)get($INDEX, 0, 'cnt'). ")", $request, "&{$itog}", $response);
}else{ pre("<nobr>Сеть <b>". $db. "</b> (". (int)get($INDEX, 0, 'cnt'). ")</nobr>", $request, $response);
	mpre($cmd);
}
