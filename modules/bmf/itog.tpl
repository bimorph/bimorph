<a name="itog"></a>
<? if(!is_array($bm0_itog = rb('bm0-itog', 'id', get($_GET, 'id')))): mpre("Ошибка выборки итога") ?>
<? elseif(array_key_exists('refresh', $_GET) && !call_user_func(function($GET){// mpre($GET) ?>
		<meta http-equiv="refresh" content="<?=(get($GET, 'refresh') ? $GET['refresh'] : 1)?>">
	<? return $GET; }, $_GET)): mpre("Ошибка вывода обновления страницы") ?>
<? elseif(!$bm0_index = rb('bm0-index', 'id', get($bm0_itog, 'index_id'))): mpre("Ошибка выборки корневого морфа") ?>
<?// elseif(!is_array($bm0_index_link = rb('bm0-index_link', 'id', $bm0_index['index_link_id']))): mpre("") ?>
<? elseif(!$bm0_clump = rb('bm0-clump', 'id', $bm0_index['clump_id'])): mpre("Ошибка выборки скопления") ?>
<? elseif(!$BM0_DANO = rb('bm0-dano', 'clump_id', 'id', $bm0_clump['id'])): mpre("Ошибка выборки источников сигнала") ?>
<? elseif(!$BM0_INDEX = rb('bm0-index', 'clump_id', 'id', $bm0_clump['id'])): mpre("Ошибка получения списка морфов") ?>
<? elseif(!$BM0_CALC_ORIENT = rb('bm0-calc_orient')): mpre("Ошибка выборки справочника ориентации") ?>
<? elseif(!$BM0_CALC_BALANCE = rb('bm0-calc_balance')): mpre("Ошибка выборки справочника баланса") ?>
<? elseif(!$length = count($BM0_INDEX)): mpre("Ошибка расчета количества морфов") ?>
<?// elseif(!$BM0_CLUMP_HISTORY = qn("SELECT * FROM `{$conf['db']['prefix']}bm0_clump_history` WHERE `clump_id`=". (int)$bm0_clump['id'])): mpre("Ошибка нахождения истории") ?>
<? elseif(!$tree = function($index, $INDEX = [], $weight = true) use(&$tree, $bm0_clump, $length, $BM0_CALC_ORIENT, $BM0_CALC_BALANCE){// mpre($bm0_index);
		if(empty($index)){ mpre("Пустой морф");
		}elseif(!$INDEX[$index['id']][] = $index){ mpre("Ошибка увеличения счетчика", $index);
		}elseif(!is_array($calc_pos = rb('bm0-calc_pos', 'id', $index['calc_pos_id']))){ mpre("Ошибка поиска позиции");
		}elseif(!is_array($calc_val = ($index['calc_val_id'] ? rb('bm0-calc_val', 'id', $index['calc_val_id']) : []))){ mpre("Ошибка выборки значения морфа");
		}elseif(!is_array($calc_sequence = ($index['calc_sequence_id'] ? rb('bm0-calc_sequence', 'id', $index['calc_sequence_id']) : []))){ mpre("Ошибка выборки распиновки морфа");
		}elseif(!is_array($index_['Первая голова'] = ($index['index_id'] ? rb('bm0-index', 'id', $index['index_id']) : []))){ mpre("Ошибка поиска первого элемента (нормально)");
		}elseif(!is_array($index_['Вторая голова'] = ($index['bm0-index'] ? rb('bm0-index', 'id', $index['bm0-index']) : []))){ die(mpre("Ошибка поиска первого элемента (фатально)"));
		}elseif(!is_array($bm0_calc_val = rb('bm0-calc_val', 'v1', 'v0', get($index_, 'Первая голова', 'val'), get($index_, 'Вторая голова', 'val')))){ mpre("Ошибка выборки расчетной позиции");
		}elseif(!is_array($calc = (($calc_pos && $bm0_calc_val) ? rb('bm0-calc', 'calc_pos_id', 'calc_val_id', $calc_pos['id'], $bm0_calc_val['id']) : []))){ mpre("Ошибка расчета необходимого значения");
		}elseif(!is_array($calc_amend = ($calc ? rb('bm0-calc_amend', 'id', $calc['calc_amend_id']) : []))){ mpre("Ошибка выборки изменений калькуляции");
		}elseif(!is_array($bm0_dano = rb('bm0-dano', 'id', $index['dano_id']))){ mpre("Ошибка выбора входных значений");
		}elseif(!call_user_func(function($index) use($tree, $INDEX, $calc, $calc_amend, $calc_val, $bm0_calc_val, $calc_pos, $calc_sequence, $bm0_dano, $index_, $BM0_CALC_ORIENT, $weight, $BM0_CALC_BALANCE){ //mpre($calc) ?>
				<span style="font-weight:<?=($weight ? "bold" : "inherit")?>;">
					морф-<?=$index['id']?>
					<span style="color:gray;">
						<span style="color:<?=($index['value'] == $index['val'] ? "inherit" : "red")?>" title="value"><?=$index['value']?></span>:<span style="color:<?=($index['val'] == $index['value'] ? "inherit" : "orange")?>;" title="val"><?=$index['val']?></span>
					</span>
					<strong title="Позиция морфа <?=get($calc_pos, 'id')?>"><?=get($calc_pos, 'name')?></strong>
					<strong title="Значение морфа <?=get($calc_val, 'id')?>"><?=get($calc_val, 'name')?></strong>
					<span title="Распиновка морфа"><?=get($calc_sequence, 'name')?></span>
				</span>
				<? if($bm0_dano): ?>
					<span style="opacity:<?=(array_filter($index_) ? ".3" : "1")?>;">
						(дано-<?=$bm0_dano['id']?>[<?=$bm0_dano['name']?>]
						<span style="color:gray;">
							<span title="value"><?=$bm0_dano['value']?></span>:<span title="val"><?=$bm0_dano['val']?></span>
						</span>)
					</span>
				<? endif; ?>
				<? if(array_filter($index_)): ?>
					<ul>
						<? foreach($index_ as $n=>$_index):// mpre($n, $_index) ?>
							<? if(empty($_index)):// mpre("Пустой морф") ?>
							<? elseif(($rep = get($INDEX, $_index['id'])) && (count($rep) > ($cnt = 3))): ?>
								<li style="color:red">Цикл более <?=$cnt?> повторов</li>
							<? elseif(!is_array($bm0_calc_balance = rb($BM0_CALC_BALANCE, "calc_val_id", "val", $_index['calc_val_id'], $_index['val']))): mpre("Ошибка выборки баланса элемента", $_index) ?>
							<? elseif(!is_array($bm0_calc_orient = rb($BM0_CALC_ORIENT, "id", get($bm0_calc_balance, 'calc_orient_id')))): mpre("Ошибка выборки ориентации структуры", $bm0_calc_balance) ?>
							<? elseif(!is_bool($_weight = call_user_func(function($bm0_calc_orient) use($index, $n, $weight){
									if(!$weight){ return false;
//									}elseif(!mpre($bm0_calc_orient, $n)){
									}elseif(('Лево' == get($bm0_calc_orient, 'name')) && ('Первая голова' == $n)){ return true;
									}elseif(('Право' == get($bm0_calc_orient, 'name')) && ('Вторая голова' == $n)){ return true;
									}else{ return false; }
								}, $bm0_calc_orient))):// mpre("Расчет веса") ?>
							<? else:// mpre($bm0_calc_balance) ?>
								<li>
									<? $tree($_index, $INDEX, $_weight) ?>
								</li>
							<? endif; ?>
						<? endforeach; ?>
					</ul>
				<? endif; ?>
				
			<? return $index; }, $index)){ mpre("Ошибка оторажения тела `{$index_link['id']}`");
		}else{ return $index; }
	}): mpre("Ошибка объявления функции дерева") ?>
<? elseif(!is_array($BM0_CLUMP_HISTORY = rb("bm0-clump_history", "clump_id", 'id', $bm0_clump['id']))): mpre("Ошибка нахождения истории") ?>
<? elseif(!is_array($BM0_CLUMP_SHOT = rb('bm0-clump_shot', 100, 'clump_id', 'id', $bm0_clump['id']))): mpre("Ошибка получения истории"); ?>
<? elseif(!is_array($bm0_clump_history = rb('bm0-clump_history', 'id', $bm0_clump['clump_history_id']))): mpre("Ошибка выборки текущей истории") ?>
<? elseif(!is_array($bm0_clump_shot = (get($bm0_clump_history, 'clump_shot_id') ? rb('bm0-clump_shot', 'id', $bm0_clump_history['clump_shot_id']) : []))): mpre("Ошибка получения истории"); ?>
<?// elseif(!$count = count(get(rb($BM0_CLUMP_SHOT, 'val'), 1))): mpre("Ошибка расчета количества записей") ?>
<? elseif(!is_numeric($count = ql("SELECT COUNT(*) AS cnt FROM `mp_bm0_clump_shot` WHERE `clump_id`=". (int)$bm0_clump['id'], 0, 'cnt'))): mpre("Ошибка расчета количество записей истории") ?>
<? elseif(!is_numeric($true = ql("SELECT COUNT(*) AS `cnt` FROM `mp_bm0_clump_shot` WHERE val=1 AND `clump_id`=". (int)$bm0_clump['id'], 0, 'cnt'))): mpre("Ошибка выборки обучений из истории") ?>
<? elseif(!is_numeric($false = ql("SELECT COUNT(*) AS `cnt` FROM `mp_bm0_clump_shot` WHERE val IS NULL AND `clump_id`=". (int)$bm0_clump['id'], 0, 'cnt'))): mpre("Ошибка выборки обучений из истории") ?>
<? elseif(!is_numeric($t = count(get(rb($BM0_CLUMP_SHOT, 'val', 'id'), '')))): mpre("Ошибка получения обучения из последних ста результатов"); ?>
<? elseif(!is_numeric($f = count(get(rb($BM0_CLUMP_SHOT, 'val', 'id'), '1')))): mpre("Ошибка получения верного результата из последних ста"); ?>
<?// elseif(!is_numeric($d = number_format(($count / $true) * 100, 2))): mpre("Ошибка получения процента результатов") ?>
<?// elseif(!mpre($t, $f)): ?>
<? elseif(!is_numeric($perc = $t /*min($t, $d)*/)): mpre("Ошибка расчета процентов") ?>
<? elseif((!$max = ql("SELECT MAX(id) AS max FROM `mp_bm0_clump_shot` WHERE `clump_id`=". (int)$bm0_clump['id'], 0, 'max')) &&0): mpre("Ошибка расчета количество записей истории") ?>
<? elseif(!is_numeric($learn = (int)ql("SELECT MAX(id) AS `learn` FROM `mp_bm0_clump_shot` WHERE `val`=1 AND `clump_id`=". (int)$bm0_clump['id'], 0, 'learn'))): mpre("Ошибка расчета последнего обучения") ?>
<?// elseif(!mpre($max, $learn)): ?>
<? elseif(!$color = call_user_func(function($bm0_clump_shot){
		if(get($bm0_clump_shot, 'val')){ return 'pink';
		}else{ return "lime"; }
	}, $bm0_clump_shot)): mpre("Ошибка расчета цвета") ?>
<? elseif(!$INDEX = call_user_func(function($bm0_clump){
		if(!$INDEX = rb('bm0-index', 'clump_id', 'id', $bm0_clump['id'])){ mpre("Ошибка выборки списка морфов");
//		}elseif(!$keys = array_column($INDEX, 'retry', 'id')){ mpre("Ошибка получения ключей массива");
//		}elseif(!arsort($keys)){ mpre("Ошибка сортировки по значению");
//		}elseif(!$INDEX = array_replace($keys, $INDEX)){ mpre("Ошибка восстановления значения отсортированного массива");
		}else{ return $INDEX; }
	}, $bm0_clump)): mpre("Ошибка выборки и сортировки морфов") ?>
<? elseif(!$BM0_ITOG = rb("bm0-itog", 'clump_id', "id", $bm0_clump['id'])): mpre("Ошибка выборки списка итогов") ?>
<? elseif(!$BM0_INDEX = rb("bm0-index", "clump_id", "id", $bm0_clump['id'])): mpre("Ошибка выборки всех морфов") ?>
<? elseif(!call_user_func(function($bm0_itog) use($tree, $bm0_index, $bm0_clump, $INDEX, $BM0_DANO, $BM0_ITOG, $BM0_INDEX, $BM0_CLUMP_HISTORY, $bm0_clump_history, $bm0_clump_shot, $length, $color, $max, $count, $true, $false, $perc, $learn){ ?>
		<?// mpre($_GET) ?>
		<h1><?=$bm0_clump['name']?> (Итог-<?=$bm0_itog['id']?>)</h1>
		<div>
			<? foreach($BM0_ITOG as $_bm0_itog): ?>
				<? /*if(!$_bm0_index = rb($BM0_INDEX, 'id', $bm0_itog['index_id'])): mpre("Ошибка выборки морфа итога") ?>
				<? elseif(!$_BM0_INDEX = rb($BM0_INDEX, "dano_id", "id", $_bm0_index['dano_id'])): mpre("Ошибка получения списка морфов итога") ?>
				<? else:*/ ?>
					<span style="font-weight:<?=(get($_GET, 'id') == $_bm0_itog['id'] ? "bold" : "inherit")?>">
						<a href="/bm0:itog/<?=$_bm0_itog['id']?><?=(array_key_exists('refresh', $_GET) ? "/refresh". ($_GET['refresh'] ? ":{$_GET['refresh']}" : "") : "")?>#itog"><?=$_bm0_itog['name']?></a>&nbsp;<?//=count($_BM0_INDEX)?>
					</span>
				<?// endif; ?>
			<? endforeach; ?>
		</div>
		<div style="margin:10px 0; font-size:80%;"><?=$bm0_itog['form']?></div>
		<style>
			ul {padding-left:15px;}
		</style>
		<span style="float:right;">
<!--			<div style="font-size:500%;"><?=($perc)?>%</div>-->
			<div><?=$bm0_clump['id']?>. <b><?=$bm0_clump['name']?></b> итр: <?=$bm0_clump['shift']?></div>
			<div>
				<span title="Количество морфов скопления"><?=$length?></span> /
				<span title="Количество записей истории"><?=$count?></span> /
				<span title="Количество обучений" style="color:red; font-weight:bold;"><?=$true?></span> /
				<span title="Всего безошибочных предсказаний" style="color:orange; font-weight:bold;"><?=$false?></span> /
				<span title="Безошибочных непрерывных предсказаний" style="color:green; font-weight:bold;"><?=$max-$learn?></span> /
				<span style="font-weight:bold;"><?=($perc)?>%</span>
			</div>
			<ul>
				<?/* foreach($BM0_DANO as $bm0_dano): ?>
					<li>дано-<?=$bm0_dano['id']?> <b><?=$bm0_dano['name']?></b> <?=$bm0_dano['val']?></li>
				<? endforeach;*/ ?>
			</ul>
			<div class="table history">
				<style>
					.table.history { border-collapse:collapse; }
					.table.history> div> span { border:1px solid gray; padding:0px 2px; color:gray; }
					.table.history> div.active> span { color:black; }
				</style>
				<div class="th">
					<span>dano</span>
					<span>learn</span>
					<span>calc</span>
					<span>val</span>
				</div>
				<? foreach($BM0_CLUMP_HISTORY as $clump_history): ?>
					<div class="<?=($clump_history['id'] == $bm0_clump["clump_history_id"] ? "active" : "")?>" style="background-color:<?=($clump_history["calc"] == $clump_history["val"] ? "#b1e697" : "red")?>;">
						<span><?=$clump_history['dano']?></span>
						<span><?=$clump_history['learn']?></span>
						<span><?=$clump_history['calc']?></span>
						<span><?=$clump_history['val']?></span>
					</div>
				<? endforeach; ?>
			</div>
			<ul>
				<? /*foreach($INDEX as $index): ?>
					<li>морф-<?=$index['id']?> <?=$index['retry']?></li>
				<? endforeach;*/ ?>
			</ul>
		</span>
		<span>
			(итог-<?=$bm0_itog['id']?> [<?=$bm0_itog['name']?>]
			<span style="color:gray;"><span style="color:<?=($bm0_itog['value'] == $bm0_itog['val'] ? "inherit" : "orange")?>;" title="value"><?=$bm0_itog['value']?></span>:<span style="color:<?=(get($bm0_index, 'val') == $bm0_itog['val'] ? "inherit" : "red")?>" title="val"><?=$bm0_itog['val']?></span></span>)
			<span><? $tree($bm0_index) ?></span>
		</span>
	<? return $bm0_itog; }, $bm0_itog)): mpre("Ошибка отрисовки итога") ?>
<? else:// mpre($bm0_itog, $bm0_index) ?>
<? endif; ?>
