<?

function inc2($file_name, $variables = [], $req = false){ global $conf;
	if(!extract($variables)){ mpre("Ошибка восстановления переданных значений");
	}elseif(!preg_match("#(.*)(\.php|\.tpl|\.html)$#", $file_name, $match)){// mpre("Расширение не указано подключаем оба формата `{$file_name}`");
		$php = inc2("{$file_name}.php", $variables, $req);
		$tpl = inc2("{$file_name}.tpl", $variables, $req);
		return ($tpl ?: $php);
	}elseif(!$file = mpopendir($file_name)){ mpre("Файл в файловой системе не найден `{$file_name}`");
	}elseif(!$_arg = $GLOBALS['arg']){ mpre("Ошибка сохранения вышестоящих аргументов");
	}elseif(($path = explode("/", $file_name)) && (!$path[0] == "modules")){ mpre("Файл не из директории с модулями");
	}elseif(!$mod = get($conf, 'modules', $path[1])){ mpre("Директория раздела не установлена", $path);
	}elseif(!$arg = array("modpath"=>$path[1], 'modname'=>$mod['modname'], "admin_access"=>$mod['admin_access'], "fn"=>first(explode(".", $path[2])))){ mpre("Ошибка установки аргументов файла");
//	}elseif($GLOBALS['arg'] = $arg){ mpre("Устанавливаем глобальную переменную аргументов");
	}elseif($return = false){ mpre("Установка значения возврата");
	}elseif(!$content = call_user_func(function($file, $content = '') use(&$conf, &$return, $match, $variables, $req){
			if(($modules_start = get($conf, 'settings', 'modules_start')) &&0){ mpre("Идентификатор начала блока");
			}elseif(($modules_stop = get($conf, 'settings', 'modules_stop')) &&0){ mpre("Идентификатор конца блока");
			}elseif(($content = call_user_func(function($file) use(&$conf, $variables, &$tpl, $req, &$arg, &$return){
					ob_start(); extract($variables);
					($req ? require($file) : include($file));
					return ob_get_clean();
				}, $file)) &&0){ mpre("Ошибка получения вывода файла");
			}elseif(array_search("Администратор", get($conf, 'user', 'gid')) && (!$content = "{$modules_start}{$content}{$modules_stop}")){ mpre("Ошибка добавления тегов подсказок администратору");
			}else{ return $content; }
		}, $file)){ mpre("Ошибка получения содержимого файла");
	}elseif(!$GLOBALS['arg'] = $_arg){ mpre("Ошибка возврата сохраненных значений аргумента");
//	}elseif(!$conf["content"] .= $content){ mpre("Ошибка добавления содержимого к контенту страницы");
	}else{ echo $content;
		return $return;
	}
} inc2("modules/bm0/admin2", ['test'=>'test']);
