<? if(!$CLUMP = rb('clump')): mpre("ОШИБКА получения списка скоплений") ?>
<? elseif(!$clump = rb($CLUMP, 'id', get($_GET, 'id'))): mpre("ОШИБКА получения текущего скопления") ?>
<? elseif(!qw("ATTACH DATABASE '/var/www/192.168.1.6/modules/bmf/db/". $clump["id"]. ".sqlite' AS bmf")): mpre("ОШИБКА подключения базы скапления") ?>
<? //elseif(true): mpre(ql("SELECT name FROM bmf.sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';")); ?>
<? elseif(!$DANO = rb('bmf.mp_bmf_dano', 'clump_id', 'id', $clump['id'])): mpre("Дано не установлено", $clump) ?>
<? elseif(!$ITOG = $GLOBALS['BMF-ITOG'] = rb('bmf.mp_bmf_itog', 'clump_id', 'id', $clump['id'])): mpre("Итогов не установлено") ?>
<? elseif(!$CALC_POS = $GLOBALS['BMF-CALC_POS'] = rb('bmf-calc_pos')): mpre("ОШИБКА получения списка морфов") ?>
<? elseif(!$CALC_VAL = $GLOBALS['BMF-CALC_VAL'] = rb('bmf-calc_val')): mpre("ОШИБКА получения списка значений") ?>
<? elseif(!$CALC = $GLOBALS['BMF-CALC'] = rb('bmf-calc')): mpre("ОШИБКА получения списка расчетов") ?>
<? elseif(!$CALC_VAL = rb('calc_val')): mpre("ОШИБКА выборки значени расчетов") ?>
<? elseif(!$CALC_AMEND = rb('calc_amend')): mpre("ОШИБКА выборки направлений") ?>
<? elseif(call_user_func(function($clump) use($conf, $ITOG, $CLUMP){ # Удаление морфов скопления при наличае сигнала в адресе страницы
		if(!array_key_exists('clean', $_GET)){// mpre("Не найден сигнал ошистки");
		}elseif(!qw("DELETE FROM `{$conf['db']['prefix']}bmf_index` WHERE `clump_id`=". (int)$clump['id'])){ mpre("ОШИБКА удаления всех морфов скопления");
		}elseif(!qw("UPDATE `{$conf['db']['prefix']}bmf_clump_history` SET val='' WHERE `clump_id`=". (int)$clump['id'])){ mpre("ОШИБКА очистки истории");
		}elseif(!is_array($_ITOG = array_map(function($itog){ # Очистка связи итогов с морфами
				if(!$itog = fk('itog', ['id'=>$itog['id']], null, ['index_id'=>null])){ mpre("ОШИБКА скидывания связи с морфом (для исключения дальнейшей связи с новыми созданными  тем же идентификатором)");
				}else{ return $itog; }
			}, $ITOG))){ mpre("ОШИБКА скидывания связей с морфом у итогов");
		}elseif(!$links = array_map(function($clump){ # Формирование ссылок для переходов при очистке
				return "<a href='/bmf:clump/{$clump['id']}'>{$clump['name']}</a>";
			}, $CLUMP)){ mpre("ОШИБКА получения списка переходов по скоплениям");
		}else{// call_user_func_array('mpre', array_merge(['Скопления'], $links));
			mpre("[Скопления]", implode(" ", $links));
		}
	}, $clump)): mpre("ОШИБКА очищения результатов") ?>
<? elseif(!is_array($CLUMP_HISTORY = rb('clump_history', 'clump_id', 'id', $clump['id']))): mpre("ОШИБКА получения истории") ?>
<? elseif(!is_array($CLUMP_HISTORY_DANO = rb("clump_history_dano", "clump_history_id", "id", $CLUMP_HISTORY))): mpre("ОШИБКА получения исходных данных истории") ?>
<? elseif(!is_array($CLUMP_HISTORY_ITOG = rb("clump_history_itog", "clump_history_id", "id", $CLUMP_HISTORY))): mpre("ОШИБКА получения итоговых данных истории") ?>
<? elseif(!is_array($clump_history = rb($CLUMP_HISTORY, 'id', get($_GET, 'bmf-clump_history')))): mpre("ОШИБКА получения истории"); ?>
<? elseif(!$DANO = call_user_func(function($clump_history) use(&$DANO){// mpre($clump_history); # Установка значений дано из истории
		if(!$dano = get($clump_history, 'dano')){ return $DANO; mpre("ОШИБКА не указано дано в исторических данных");
		}elseif(strlen($dano) != count($DANO)){ mpre("Количество исторических данных и сигналов дано не совпадает");
		}elseif(!$split = str_split($dano)){ mpre("ОШИБКА получения массива сигналов");
//		}elseif(true){ mpre($split, );
		}elseif(!$DANO = array_map(function($num, $dano) use($split){// mpre($split); // mpre($dano, $split); # Установка значений дано
				if(!is_numeric($val = get($split, $num))){ mpre("ОШИБКА получения значения дано");
				}elseif(!$dano = fk('dano', ['id'=>$dano['id']], null, ['val'=>$val/*, 'tail'=>$tail*/])){ mpre("ОШИБКА сохранения значения в базу");
				}else{// pre($dano);
					return $dano;
				}
			}, array_keys(array_values($DANO)), $DANO)){ mpre("ОШИБКА установки значений дано");
		}else{ return $DANO; }
	}, $clump_history)): mpre("ОШИБКА восстановления дано по историческим данным") ?>
<? elseif(!is_array($GLOBALS['BMF-INDEX'] = rb('index', 'clump_id', 'id', $clump['id']))): mpre("ОШИБКА выборки морфов скопления") ?>
<? elseif(call_user_func(function($clump) use($conf, $arg, $DANO, $CALC, $ITOG, $CALC_POS, $CALC_VAL, $CALC_AMEND){ # Обработка пришедших с формы данных
		if(!is_array($INDEX = &$GLOBALS["BMF-INDEX"])){ mpre("ОШИБКА получения данных справочника");
		}elseif(!is_array($_index = call_user_func(function($_index = []) use($INDEX, $CALC_POS){ // Изменение позиции морфа
				if(!$index = rb($INDEX, "id", get($_GET, 'bmf-index'))){ //mpre("ОШИБКА определения морфа для изменения позиции");
				}elseif(!$_calc_pos = rb($CALC_POS, "id", get($_GET, 'bmf-calc_pos'))){ mpre("ОШИБКА выборки позиции устанавливаемой морфу");
				}elseif($index["calc_pos_id"] == $_calc_pos["id"]){ mpre("ОШИБКА позиция уже установлена у морфа");
				}elseif(!$_index = fk("bmf-index", ["id"=>$index["id"]], null, ["calc_pos_id"=>$_calc_pos["id"]])){ mpre("ОШИБКА устанвоки новой позиции морфу");
				}elseif(!$GLOBALS["BMF-INDEX"][$_index["id"]] = $_index){ mpre("ОШИБКА сохранения в справочник");
				}else{// die(mpre("Обновление позиции у морфа", $index, $_calc_pos, $_index));
				} return $_index;
			}))){ mpre("ОШИБКА изменения позиции морфа");
		}elseif(!$_POST){ return []; // mpre("Пост запрос не задан");
		}elseif(!is_array($INDEX = &$GLOBALS["BMF-INDEX"])){ mpre("ОШИБКА получения данных справочника");
		}elseif(!is_array($dano = rb($DANO, "id", get($_POST, 'dano_id')))){ mpre("ОШИБКА Выборки дано"); return [];
		}elseif(!is_array([$field, $val_name] = call_user_func(function($field = ["", ""]){ // Получение поля для расширения и имя поля в занчении
				if(array_key_exists($n = "index_id", $_POST)){ $field = [$n, "v1"];
				}elseif(array_key_exists($n = "bmf-index", $_POST)){ $field = [$n, "v0"];
				}else{// mpre("ОШИБКА поле не задано");
				} return $field;
			}))){ mpre("ОШИБКА получения имени поля для добавления", $_POST);
		}elseif(!is_array($INDEX = call_user_func(function($_INDEX = []) use($conf, $arg, $field, $dano, &$INDEX, $CALC, $CALC_POS, $CALC_VAL, $CALC_AMEND, $ITOG, $DANO){ # Переворачиваем указанные морфы
				if(!$index_post = get($_POST, 'index')){// Изменение позиции и значения
				//}elseif(true){ mpre($index_post);
				}elseif(!$index_edit = get($index_post, 'edit')){ mpre("ОШИБКА список изменений не найден");
				}elseif(!is_array($_INDEX = array_map(function($index_id, $val) use($conf, $arg, $INDEX){ // Получение массива изменений и удаление отрицательных значений
						if($val){// mpre("Только значения меньше нуля");
						}elseif(!qw("DELETE FROM `{$conf["db"]["prefix"]}{$arg["modpath"]}_index` WHERE id=". (int)$index_id)){ mpre("ОШИБКА удаления морфа");
						}else{ return true; }
					}, array_keys($index_edit), $index_edit))){ mpre("ОШИБКА получения списка морфов для изменений");
				}elseif(!is_array($index_add = (array)get($index_post, 'add'))){ mpre("ОШИБКА список добавления не задан");
				}elseif(!is_array($_INDEX = array_map(function($index_id, $field) use($conf, $dano, &$INDEX, $CALC, $CALC_POS, $CALC_VAL, $ITOG, $DANO){ // Получение массива изменений и удаление отрицательных значений
						if(!$dano){// mpre("ОШИБКА исходное значение для добавления к морфу не задано");
						}elseif(!$bmf_index = rb($INDEX, "id", $index_id)){ mpre("ОШИБКА получеия списка морфа для изменнения позиции `{$index_id}`");
						}elseif($_index = rb('bmf-index', 'id', $bmf_index[$field])){ mpre("ОШИБКА поле уже содержит морф");
						}elseif(!$bmf_dano = rb($DANO, "id", $bmf_index["dano_id"])){ mpre("ОШИБКА получения исходных значений родителя");
						}elseif(!$bmf_calc_val = rb('calc_val', 'v1', 'v0', $bmf_dano['val'], $bmf_dano['val'])){ mpre("ОШИБКА получения значения калькуляции");
						}elseif(!$bmf_calc = rb("calc", "calc_pos_id", "calc_val_id", $bmf_index['calc_pos_id'], $bmf_calc_val['id'])){ mpre("ОШИБКА получения калькуляции элемента");
						}elseif(!is_numeric($_val = $bmf_dano["val"] ? 0 : 1)){ mpre("ОШИБКА получения инвертированного значения");
						}elseif(!$calc_pos = rb($CALC_POS, 'itog', 'dano', $_val, $dano['val'])){ mpre("ОШИБКА получения позиции морфа");
						}elseif(!$bmf_itog = rb($ITOG, "id", $bmf_index["itog_id"])){ mpre("ОШИБКА получения итога родителя");
						}elseif(!$_index = fk('index', null, $w = ['clump_id'=>$bmf_index['clump_id'], 'bmf-dano'=>get($bmf_index, 'dano_id'), 'calc_pos_id'=>$calc_pos['id'], 'dano_id'=>$dano['id'], "itog_id"=>$bmf_itog["id"], "calc_val_id"=>$bmf_calc_val["id"], 'itog_values_id'=>$bmf_itog["itog_values_id"], "val"=>$_val])){ mpre("ОШИБКА обновления родительского морфа");
						}elseif(!$bmf_index = fk("index", ['id'=>$bmf_index['id']], null, [$field=>$_index['id']])){ mpre("ОШИБКА добавления морфа родителю");
						}else{ //mpre("Добавление", $bmf_dano['val'], $_val, $calc_pos);
							return $_index;
						}
					}, array_keys($index_add), $index_add))){ mpre("ОШИБКА добавления");
				//}elseif($index_add && !array_filter($_INDEX)){ mpre("ОШИБКА добавления морфа");
				}elseif(!is_array($_INDEX = array_map(function($index_id, $val) use($conf, &$INDEX, $CALC, $CALC_POS, $CALC_VAL){ // Получение массива изменений и удаление отрицательных значений
						if(!$val){// mpre("Позиция не задана");
						}elseif(!$index = rb($INDEX, "id", $index_id)){ mpre("ОШИБКА обновляемый морф не найден");
						}elseif(!$calc = rb($CALC, "id", $val)){ mpre("ОШИБКА выборки обновленного расчета `{$val}` для морфа `{$index_id}`");
						}elseif(!$index = fk("bmf-index", ["id"=>$index["id"]], null, ["calc_pos_id"=>$calc["calc_pos_id"], "calc_val_id"=>$calc["calc_val_id"]])){ mpre("ОШИБКА обновления позиции морфа");
						}elseif(!$INDEX[$index["id"]] = $index){ mpre("ОШИБКА сохранения значения в справочник");
						}else{// mpre("Обновление", $index, $calc);
							return $index;
						}
					}, array_keys($index_edit), $index_edit))){ mpre("ОШИБКА изменения позиции");
				}else{// mpre("Перевернуты морфы", $_INDEX);
//					exit(header("Location: {$_SERVER["REQUEST_URI"]}"));
				} return $INDEX;
			}))){ mpre("ОШИБКА переворачивание морфов");
		}elseif(!is_array($itog = rb("itog", "id", get($_POST, 'itog_id')))){ mpre("ОШИБКА выборки итога из запроса");
		}elseif(!is_array($index = call_user_func(function($index = []) use($dano, $itog, $clump, $DANO, $ITOG, $CALC_POS, $CALC_VAL){ # Добавление первоначального морфа
				if(empty($itog)){// mpre("Не указан прямой источник сигнала первого морфа");
				}else if($index_id = get($_POST, 'index_id')){ mpre("ОШИБКА выбран смежный морф", $_POST);
				}elseif($index = rb('bmf-index', 'clump_id', 'id', $clump['id'], $itog['index_id'])){ mpre("Морф у дано уже установлен");
				}elseif(!$calc_pos = rb($CALC_POS, 'itog', 'dano', $itog['val'], $dano['val'])){ mpre("ОШИБКА получения позиции морфа", $_POST);
				}else if(!$dano = rb($DANO, "id", get($_POST, 'dano_id'))){ mpre("ОШИБКА получения исходного значения", $_POST);
				}elseif(!$calc_val = rb($CALC_VAL, "v1", "v0", $dano["val"], $dano["val"])){ mpre("ОШИБКА получения значения первоначального морфа");
				}else if(!$itog = rb($ITOG, "id", get($_POST, 'itog_id'))){ mpre("ОШИБКА получения исходного значения", $_POST);
				}elseif(!$index = fk('bmf-index', null, ['clump_id'=>$clump['id'], 'dano_id'=>$dano['id'], "itog_id"=>$itog["id"], "itog_values_id"=>$itog["itog_values_id"], 'calc_pos_id'=>$calc_pos['id'], "calc_val_id"=>$calc_val["id"], "val"=>$itog['val']])){ mpre("ОШИБКА добавления нового морфа");
				}elseif(!$itog = fk('bmf-itog', ['id'=>$itog['id']], null, ['index_id'=>$index['id']])){ mpre("ОШИБКА установки связи дано с морфом");
				}else{// mpre("Добавление первоначального морфа", $index);
				} return $index;
			}))){ mpre("ОШИБКА добавления первоначального морфа");
		}elseif(!is_array($GLOBALS['BMF-INDEX'] = rb('index', 'clump_id', 'id', $clump['id']))){ mpre("ОШИБКА обновления справочника");
		}else{// mpre($index);
		}
	}, $clump)): mpre("ОШИБКА обработки формы") ?>
<? elseif(!is_array($INDEX = call_user_func(function() use($clump_history, $ITOG, $CALC, $CALC_VAL, $DANO){ // Расчитываем значения морфов
		if(!is_array($INDEX = &$GLOBALS['BMF-INDEX'])){ mpre("ОШИБКА получения данных справочника");
		}elseif(!array_key_exists("calc", $_GET)){// mpre("Расчет выключен");
		}else if(!$clump_history){ mpre("Обучение не задано");
		}elseif(!is_array($_INDEX = array_map(function($index) use($INDEX){ // Список крайних морфов не имеющих потомков
				if($_index = rb($INDEX, "id", $index["bmf-index"])){// mpre("Старший морф не найден");
				}elseif($_index = rb($INDEX, "id", $index["index_id"])){// mpre("Младший морф не найден");
				}else{ return $index;
				}
			}, $INDEX))){ mpre("ОШИБКА выборки висящих морфов");
		}else if(!is_array($_INDEX = array_filter($_INDEX))){ mpre("ОШИБКА получения списка висячих морфов");
		}else if(!$Calc = function($index) use(&$Calc, $CALC, $CALC_VAL, $DANO){
//				mpre($index);
				if(!$INDEX = &$GLOBALS['BMF-INDEX']){ mpre("ОШИБКА получения данных справочника");
				}elseif(!$dano = rb($DANO, "id", $index["dano_id"])){ mpre("ОШИБКА получения исходного значения морфа");
				}elseif(!is_array($index_[$n = "Старший"] = rb($INDEX, "id", $index["index_id"]))){ mpre("ОШИБКА получения смежного морфа `{$n}`");
				}elseif(!is_array($index_[$n = "Младший"] = rb($INDEX, "id", $index["bmf-index"]))){ mpre("ОШИБКА получения смежного морфа `{$n}`");
				}elseif(!is_numeric($v1 = is_numeric(get($index_, $n = "Старший", "val")) ? $index_[$n]["val"] : $dano["val"])){ mpre("ОШИБКА расчета сигнала `{$n}`");
				}elseif(!is_numeric($v0 = is_numeric(get($index_, $n = "Младший", "val")) ? $index_[$n]["val"] : $dano["val"])){ mpre("ОШИБКА расчета сигнала `{$n}`");
				}elseif(!$calc_val = rb($CALC_VAL, "v1", "v0", $v1, $v0)){ mpre("ОШИБКА получения значения морфа");
				}elseif(!$calc = rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $calc_val["id"])){ mpre("ОШИБКА получения расчета морфа");
				}elseif(!$index = (["calc_val_id"=>$calc_val["id"], "val"=>$calc["val"]]+ $index)){ mpre("ОШИБКА сохранения расчетных значений");
				}elseif(!$index = fk("bmf-index", ["id"=>$index["id"]], null, $index)){ mpre("ОШИБКА сохранения расчетных значений морфа");
				}elseif(!$GLOBALS['BMF-INDEX'][$index["id"]] = $index){ mpre("ОШИБКА сохранения результатов расчета в справочник");
				}elseif(!is_array($_INDEX = array_merge(rb($INDEX, "index_id", "id", $index["id"]), rb($INDEX, "bmf-index", "id", $index["id"])))){ mpre("Вышестоящие морфы не найдены");
				}elseif(!is_array($_INDEX = array_map(function($index) use($Calc){ return $Calc($index); }, $_INDEX))){ mpre("ОШИБКА расчета вышестоящих морфов");
				}else{// mpre("Расчет", $index);
				} return $index;
			}){
		}else if(!is_array(array_map(function($index) use($Calc){ // Расчет морфа с низу
				if(!$_index = $Calc($index)){ mpre("ОШИБКА расчета значения морфа");
				}else{ return $_index; }
			}, $_INDEX))){ mpre("ОШИБКА расчета нижестоящего");
		}elseif(!is_array($_INDEX = rb($INDEX, "id", "id", $_INDEX))){ mpre("ОШИБКА обновления списка");
		}else{// mpre("Список висячих морфов", $_INDEX);
		} return $INDEX;
	}))): mpre("ОШИБКА расчета морфов") ?>
<? else:// mpre($INDEX, $GLOBALS['BMF-INDEX']) ?>
/*
	0101	0011
	1010	1100
*/
	<div>
		<style>
			.bs-header { display:none; }
			.bs-old-docs { display:none; }
			ul li { cursor:pointer; }
			ul li.active { background-color:yellow; }
		</style>
		<script sync>
			(function($, script){
				$(script).parent().on("ajax", function(e, table, get, post, complete, rollback){
					var href = "/<?=$arg['modname']?>:ajax/class:"+table;
					$.each(get, function(key, val){ href += "/"+ (key == "id" ? parseInt(val) : key+ ":"+ val); });
					$.post(href, post, function(data){ if(typeof(complete) == "function"){
						complete.call(e.currentTarget, data);
					}}, "json").fail(function(error) {if(typeof(rollback) == "function"){
							rollback.call(e.currentTarget, error);
					} alert(error.responseText) });
				}).one("init", function(e){
				}).on('dblclick', 'label', function(e){
					$(e.currentTarget).find("input[type=radio]:checked").prop("checked", false);
				}).ready(function(e){ $(script).parent().trigger("init"); })
			})(jQuery, document.currentScript)
		</script>
		<h1><?=$clump['name']?></h1>
		<? if(!$ITOG = call_user_func(function($clump_history) use($ITOG){ # Установка значений из истории
				if(!strlen(get($clump_history, 'itog'))){// mpre("Параметр итог истории не задан");
				}elseif(!$split = str_split($clump_history['itog'])){ mpre("ОШИБКА получения сигналов итога");
				}elseif(count($split) != count($ITOG)){ mpre("ОШИБКА разница в количестве элементов итого и количестве обучающих сигналов");
				}elseif(!$_ITOG = array_map(function($itog, $val) use($split){ # Установка значений итога
						if(!$_itog = fk('itog', ['id'=>$itog['id']], null, ['val'=>$val/*, 'tail'=>$tail*/])){ mpre("ОШИБКА обновления значения морфа");
						}else{// mpre("Не измененное и измененное значени морфа", $itog, $_itog);
							return $_itog;
						} return $itog;
					}, $ITOG, $split)){ mpre("ОШИБКА установки итогам обучающих сигналов");
				}elseif(!$_ITOG = rb($_ITOG, 'id')){ mpre("Восстановление ключей массива");
				}else{// mpre("Обновленные итоги", $_ITOG);
					return $_ITOG;
				} return $ITOG;
			}, $clump_history)): mpre("ОШИБКА установки значений итогов") ?>
		<? elseif(!is_array($_INDEX = rb($INDEX, "id", "id", rb($ITOG, 'index_id')))): mpre("ОШИБКА получения морфов по итогам") ?>
		<? else:// die(!mpre($ITOG, $_INDEX)) ?>
			<form method="post">
				<span style="float:right;">
					<a href="/bmf:clump/<?=$clump['id']?>/clean">Очистить</a>
				</span>
				<div class="table">
					<div>
						<span>
							<h3>История</h3>
							<? if(!is_array($CLUMP_HISTORY = array_map(function($_clump_history) use($clump_history){ # Получение разницы в сигналах с текущий историей
									/*if(!$clump_history){ mpre("История не установлена");
									}else*/if(!is_numeric($min = min(strlen(get($clump_history, 'dano')), strlen(get($_clump_history, 'dano'))))){ mpre("ОШИБКА определения длинны минимальной истории", $clump_history, $_clump_history);
									}elseif(!is_numeric($max = max(strlen(get($clump_history, 'dano')), strlen(get($_clump_history, 'dano'))))){ mpre("ОШИБКА определения длинны минимальной истории");
									}elseif(!is_string($dano = (get($clump_history, 'dano') ? substr($clump_history['dano'], 0, $min) : ""))){ mpre("Получение строки основной истории");
									}elseif(!is_string($_dano = substr(get($_clump_history, 'dano'), 0, $min))){ mpre("Получение строки основной истории");
									}elseif(!is_array($diff = array_filter(array_map(function($chr, $_chr){ return ($chr !== $_chr); }, str_split($dano), str_split($_dano))))){ mpre("Функция сравнения посимвольна");
									}elseif(!is_numeric($_clump_history['_diff'] = count($diff)+($max-$min))){ mpre("ОШИБКА расчета количество различий");
									}else{ return $_clump_history; }
								}, $CLUMP_HISTORY))): mpre("ОШИБКА установки разницы в сигналах") ?>
							<? elseif(!is_array($_CLUMP_HISTORY = rb($CLUMP_HISTORY, '_diff', 'val', 'id', 1, '[]'))): mpre("ОШИБКА получения списка с 1 различием") ?>
							<? elseif(!is_array($_clump_history = ($_CLUMP_HISTORY ? get($_CLUMP_HISTORY, array_rand($_CLUMP_HISTORY)) : []))): mpre("ОШИБКА получения случайного следующего элемента из списка с 1 различием") ?>
							<? else:// mpre($CLUMP_HISTORY) ?>
								<ul>
									<? foreach($CLUMP_HISTORY as $history): //mpre($history) ?>
										<? if(!$color_bg = call_user_func(function($clump_history) use($_clump_history){
												if(get($_GET, 'bmf-clump_history') == $clump_history['id']){ return "#bbb";
												}elseif(get($_clump_history, 'id') == $clump_history['id']){ return "#cfc";
												}elseif(1 == $clump_history['_diff']){ return "#eee";
												}else{ return "inherit"; }
											}, $history)): mpre("ОШИБКА расчета цвета элемента") ?>
										<? elseif(!is_string($a_calc = (array_key_exists("calc", $_GET) ? "/calc" : ""))): mpre("ОШИБКА получения параметра расчета") ?>
										<? elseif(!$a_href = "/bmf:clump/{$clump['id']}/bmf-clump_history:{$history['id']}{$a_calc}"): mpre("ОШИБКА расчета адреса ссылки") ?>
										<? elseif(!$_CLUMP_HISTORY_DANO = rb($CLUMP_HISTORY_DANO, "clump_history_id", "id", $history["id"])): mpre("ОШИБКА получения исходника истории") ?>
										<? elseif(!is_string($dano_value = implode(null, array_column($_CLUMP_HISTORY_DANO, "value")))): mpre("ОШИБКА составления списка исходных значений") ?>
										<? elseif(!$_CLUMP_HISTORY_ITOG = rb($CLUMP_HISTORY_ITOG, "clump_history_id", "id", $history["id"])): mpre("ОШИБКА получения итога истории") ?>
										<? elseif(!is_string($itog_value = implode(null, array_column($_CLUMP_HISTORY_ITOG, "value")))): mpre("ОШИБКА составления списка значений итогов") ?>
										<? else:// mpre($clump_history_itog) ?>
											<li style="padding:2px;">
												<?=$history['id']?>.
												<span>
													<span style="color:<?=(get($history, 'itog') ? "red" : "blue")?>"><?=$itog_value?></span>
													<a href="<?=$a_href?>" style="background-color:<?=$color_bg?>; padding:3px;">
														<?=$dano_value?>
													</a>
													<span style="color:<?=(get($history, 'val') ? "red" : "blue")?>;"><?=get($history, 'val')?></span>
												</span>
											</li>
										<? endif; ?>
									<? endforeach; ?>
								</ul>
								<? if(!$_clump_history):// mpre("Последующий переход не задан") ?>
								<? else: ?>
									<div>Далее: <a href="/bmf:clump/<?=$clump['id']?>/bmf-clump_history:<?=$_clump_history['id']?>"><?=$_clump_history['dano']?></a></div>
								<? endif; ?>
							<? endif; ?>
							<?// mpre($clump_history) ?>
						</span>
						<span>
							<? if(!is_string($calc_history = ($clump_history ? "/bmf-clump_history:{$clump_history["id"]}" : ""))): mpre("ОШИБКА получения параметра истории адреса") ?>
							<? elseif(!$calc_name = (array_key_exists("calc", $_GET) ? "Вкл" : "Выкл")): mpre("ОШИБКА расчета заголовка ссылки")  ?>
							<? elseif(!$calc_href = (array_key_exists("calc", $_GET) ? "/bmf:clump{$calc_history}/{$clump["id"]}" : "/bmf:clump{$calc_history}/{$clump["id"]}/calc")): mpre("ОШИБКА получения ссылки вкл/выкл расчета") ?>
							<? else: ?>
								<span style="float:right">Расчет <a href="<?=$calc_href?>"><?=$calc_name?></a></span>
							<? endif; ?>
							<h3>Морфы</h3>
							<? if(!$tree = function($index, $bmf_index = [], $amend = "", $attr = ["del"=>0, "top"=>1]) use(&$tree, $CALC_POS, $CALC_VAL, $DANO, $ITOG, $CALC, $CALC_AMEND){ //mpre($index, gettype($index)); ?>
									<? if(!$INDEX = &$GLOBALS['BMF-INDEX']): mpre("ОШИБКА получения данных справочника"); ?>
									<? elseif(!is_array($dano = (get($index, 'dano_id') ? rb($DANO, 'id', $index['dano_id']) : []))): mpre("ОШИБКА выборки дано", $index) ?>
									<? elseif(!is_array($index_back = $index)): mpre("ОШИБКА сохранения значений до изменений") ?>
									<? elseif(!is_string($dano_val = call_user_func(function($val = "") use($index, $dano, $CALC_VAL, $CALC){
											if(!$dano){ //mpre("Висящий морф");
											}elseif(!$calc_val = rb($CALC_VAL, "v1", "v0", $dano["val"], $dano["val"])){ mpre("ОШИБКА расчета контрольного значения");
											}elseif(!$calc = rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $calc_val["id"])){ mpre("ОШИБКА получения расчетного расчета");
											}elseif(!is_numeric($val = $calc["val"])){ mpre("ОШИБКА установки расчетного значения", $calc);
											}else{ //mpre("Расчет значения", $calc);
											} return $val;
										}))): mpre("ОШИБКА расчета контрольного значения") ?>
									<? elseif(!is_array($index_[$f = 'index_id'] = (get($index, $f) ? rb($INDEX, 'id', $index[$f]) : []))): mpre("ОШИБКА выборки нижестоящих `{$f}`") ?>
									<? elseif(!is_array($index_[$f = 'bmf-index'] = (get($index, $f) ? rb($INDEX, 'id', $index[$f]) : []))): mpre("ОШИБКА выборки нижестоящих `{$f}`") ?>
									<? elseif(!is_numeric($attr["del"] = call_user_func(function($del = 0) use($attr, $index, $dano_val, $index_){
											if(!$attr["top"]){ //mpre("Не редактируемая нить");
											}elseif(!$index){ //mpre("Пустой морф");
											}elseif(!$attr["del"] && (get($index, "val") == $dano_val)){ //mpre("{$index["id"]} Не удаляем");
											}else{ $del = 1;
											} return $del;
										}))): mpre("ОШИБКА расчета признака удаления") ?>
									<? elseif(!is_array($bmf_dano = ($bmf_index ? rb($DANO, 'id', $bmf_index['dano_id']) : []))): mpre("Получение дано родителя") ?>
									<? elseif(!is_array($calc = ($index ? rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $index["calc_val_id"]) : []))): mpre("ОШИБКА получения расчета морфа"); ?>
									<? elseif(!is_array($calc_val = (get($index, 'calc_val_id') ? rb($CALC_VAL, 'id', $index['calc_val_id']) : []))): mpre("ОШИБКА выборки значения морфа"); ?>
									<? elseif(!is_array($calc_pos = (get($index, 'calc_pos_id') ? rb($CALC_POS, 'id', $index['calc_pos_id']) : []))): mpre("ОШИБКА выборки позиции морфа") ?>
									<? //elseif(!is_array($calc_amend = ($calc ? rb($CALC_AMEND, "id", $calc["calc_amend_id"]) : []))): mpre("ОШИБКА получения изменения значения"); ?>
									<? //elseif(!is_array($_calc_amend = $calc ? rb($CALC_AMEND, "id", $calc["calc_amend_id"]) : [])): //mpre("ОШИБКА выбора направления"); ?>
									<? /*elseif(!is_array($calc_amend = call_user_func(function($calc_amend) use($index, $calc, $calc_pos, $dano, $CALC_AMEND){ // Выбор направления с учетом отрицания
											if(!$index){ //mpre("Висячий сигнал");
											}elseif(!$field = $dano["val"] ? "calc_amend_id" : "bmf-calc_amend"){ mpre("ОШИБКА расчета выбора имени направления развития");
											//}elseif(!$calc_amend = rb($CALC_AMEND, "name", "[{$amend}]")){ mpre("ОШИБКА выборки направления развития");
											}else if(!$calc_amend_id = get($calc, $field)){ //mpre("Идентификатор направления развития не указан в таблице расчетов");
											}else if(!$calc_amend = rb($CALC_AMEND, "id", $calc_amend_id)){ mpre("ОШИБКА выборки направления из расчетов в базе");
											}else{ //mpre("Отрицание {$denial} [{$_val}:{$dano["val"]}]");
											} return $calc_amend;
										}, $_calc_amend))): mpre("ОШИБКА расчета направления развития по значению")*/ ?>
									<? elseif(!is_array($calc_amend = call_user_func(function($calc_amend = []) use($index, $calc, $calc_pos, $calc_val, $dano, $CALC_AMEND){ // Выбор направления с учетом отрицания
											if(!$index){ //mpre("Висячий сигнал");
											//}else if(true){ mpre($calc_pos);
											}else if(!is_numeric($_val = ($index["val"] ? 0 : 1))){ mpre("ОШИБКА получения отрицания значения морфа");
											//}elseif(!is_numeric($denial = ($_val == $dano["val"] ? 0 : 1))){ mpre("ОШИБКА получения признака отрицания");
											}elseif(!$amend = ($calc_pos["v1"] == $calc_pos["v0"]) ? "Старший" : "Младший"){ mpre("ОШИБКА расчета выбора имени направления развития");
											}elseif(!$calc_amend = rb($CALC_AMEND, "name", "[{$amend}]")){ mpre("ОШИБКА выборки направления развития");
											}else{ //mpre("Отрицание {$denial} [{$_val}:{$dano["val"]}]");
											} return $calc_amend;
										}))): mpre("ОШИБКА расчета направления развития по отрицанию") ?>
									<? elseif(!is_array($calc_amend = call_user_func(function($calc_amend) use($index, $index_back, $calc, $CALC_AMEND){ // Выбираем направление
											if(!$index){
											}elseif($calc_amend){ //mpre("Направление установлено");
											}elseif(!$_CALC_AMEND = rb($CALC_AMEND, "name", "id", "[Старший,Младший]")){ mpre("ОШИБКА получения списка допустимых позиций");
											}elseif(!$calc_amend = get($_CALC_AMEND, array_rand($_CALC_AMEND))){ mpre("ОШИБКА получения случайной позиции из списка допустимых");
											}else{// mpre($_CALC_AMEND, $calc_amend);
											} return $calc_amend;
										}, $calc_amend))): mpre("ОШИБКА выбора направления развития") ?>
									<? /*elseif(!is_array($calc_amend = call_user_func(function($calc_amend = []) use($index, $index_back, $calc_pos, $calc_val, $CALC_AMEND){
											if(!$index){ //mpre("Сигнал морфа");
											}elseif(!$calc_amend = rb($CALC_AMEND, "name", $w = "[Старший]")){ mpre("ОШИБКА выборки направления {$w}");
											}elseif($index_back["val"] != $calc_pos["v0"]){ //mpre("Положительное направление");
											}elseif(!$calc_amend = rb($CALC_AMEND, "name", $w = "[Младший]")){ mpre("ОШИБКА выборки направления {$w}");
											//}elseif($index_back["val"] != $calc_pos["v1"]){ //mpre("Положительное направление");
											}else{ //mpre("{$index["id"]} Перевернутый морф");
											} return $calc_amend;
										}))): mpre("ОШИБКА получения направления развития")*/ ?>
									<? elseif(!is_string($content = call_user_func(function() use($tree, $attr, $index, $dano, $index_, $calc_amend){ // Получение содержимого отображения нижестоящих
											if(!ob_start()){ mpre("ОШИБКА включения буфера");
											}elseif(!$index_val_ = array_map(function($_amend, $_index) use($tree, $attr, $dano, $calc_amend, $index){ // Перебираем нижестоящие
													if(!$index){// mpre("У пустого элемента не отображаем нижестоящие");
													}elseif(!is_numeric($attr["top"] = ($attr["top"] && (get($calc_amend, "field") == $_amend)) ? 1 : 0)){ mpre("ОШИБКА расчета признака верности");
													}elseif(!is_array($_index = $tree($_index, $index, $_amend, $attr))){ mpre("ОШИБКА запуска фнукции отрисовки нижестоящего морфа");
													}else{// mpre("Запуск функции", gettype($_index));
														return $_index;
													}
												}, array_keys($index_), $index_)){ mpre("ОШИБКА перебора нижестоящих значений");
											}elseif(!is_string($content = ob_get_clean())){ mpre("ОШИБКА получения буфера");
											}else{// mpre($index, $index_val_);
												return $content;
											}
										}))): mpre("ОШИБКА расчета содержимого нижестоящих") ?>
									<? elseif(!$INDEX = &$GLOBALS['BMF-INDEX']): mpre("ОШИБКА получения данных справочника"); ?>
									<? elseif(!is_array($index_[$f = 'index_id'] = (get($index, $f) ? rb($INDEX, 'id', $index[$f]) : []))): mpre("ОШИБКА выборки нижестоящих `{$f}`") ?>
									<? elseif(!is_array($index_[$f = 'bmf-index'] = (get($index, $f) ? rb($INDEX, 'id', $index[$f]) : []))): mpre("ОШИБКА выборки нижестоящих `{$f}`") ?>
									<? elseif(!is_array($_calc = call_user_func(function($_calc = []) use($index, $index_, $attr, $calc, $calc_pos, $calc_val, $dano, $calc_amend, $INDEX, $CALC, $CALC_POS, $CALC_VAL){ // Расчет морфа
											if(!$index){ //mpre("Сигналы");
											}elseif(!$attr["top"]){ //mpre("{$index["id"]} Не изменилось", $calc); return $calc;
											}elseif(!$index_ = call_user_func(function() use($index, $attr, $calc_amend, $calc_val, $index_){
													if(!$attr["top"]){ //mpre("Не изменяемая нить");
													}elseif(!$field = $calc_amend["field"]){ mpre("ОШИБКА получения имени поля направления");
													}elseif($_index = get($index_, $field)){ //mpre("Нижестоящий морф уже задан");
													}elseif(!$v = get(["index_id"=>"v1", "bmf-index"=>"v0"], $field)){ mpre("ОШИБКА получения поля значения");
													}elseif(!is_numeric($val = $calc_val[$v])){ mpre("ОШИБКА получения знака развития");
													}elseif(!is_numeric($_val = $val ? 0 : 1)){ mpre("ОШИБКА инвертирования сигнала");
													}elseif(!$index_[$field] = ["val"=>$_val]){ mpre("ОШИБКА сохранения инвертированного значения");
													}else{ //mpre("{$index["id"]} Новый морф `{$field}`", $index_);
													} return $index_;
												})){ mpre("ОШИБКА установки значения перехода");
											}elseif(!is_numeric($v1 = is_numeric(get($index_, $f = "index_id", "val")) ? $index_[$f]["val"] : get($dano, "val"))){// mpre("ОШИБКА получения знака `{$f}`");
											}elseif(!is_numeric($v0 = is_numeric(get($index_, $f = "bmf-index", "val")) ? $index_[$f]["val"] : $dano["val"])){// mpre("ОШИБКА получения знака `{$f}`");
											}elseif(!$_calc_val = rb($CALC_VAL, "v1", "v0", $v1, $v0)){ mpre("ОШИБКА получения значения");
											}elseif(!$_calc = rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $_calc_val["id"])){ mpre("ОШИБКА поиска нового расчета с изменившимся значением");
											}else{ //mpre("{$index["id"]} Расчетное значение", $index_, $calc, $_calc);
											} return $_calc;
										}))): mpre("ОШИБКА расчета нового значения морфа") ?>
									<? elseif(!is_array($index = call_user_func(function($index) use($index_back, $attr, $index_, $calc_amend, $dano, $calc, $_calc, $calc_pos, $CALC, $CALC_POS, $CALC_VAL){ // Расчет с контролем нижестоящих
											if(!$index){ //mpre("Исходный сигнал");
											}elseif(!$attr["top"]){ //mpre("Не изменяемая нить");
											}elseif(!$field = get($calc_amend, "field")){ mpre("ОШИБКА получения поля дальнейшего развития");
											}elseif(!is_numeric($_val = $dano["val"] ? 0 : 1)){ mpre("ОШИБКА получения значения вновь добавляемого морфа");
											}elseif(!$index_[$field] = (get($index_, $field) ?: ["val"=>$_val])){ mpre("ОШИБКА установки значения нового морфа");
											}elseif(!is_numeric($v1 = is_numeric(get($index_, $f = "index_id", "val")) ? $index_[$f]["val"] : get($dano, "val"))){// mpre("ОШИБКА получения знака `{$f}`");
											}elseif(!is_numeric($v0 = is_numeric(get($index_, $f = "bmf-index", "val")) ? $index_[$f]["val"] : $dano["val"])){// mpre("ОШИБКА получения знака `{$f}`");
											}elseif(!$_calc_val = rb($CALC_VAL, "v1", "v0", $v1, $v0)){ mpre("ОШИБКА получения значения");
											}elseif(!$_calc = rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $_calc_val["id"])){ mpre("ОШИБКА выборки расчета после изменения значения");
											}elseif(!$index = ["calc_val_id"=>$_calc_val["id"], "val"=>$_calc["val"]]+ $index){ mpre("ОШИБКА установки изменения значения");
											}elseif(!$GLOBALS["BMF-INDEX"][$index["id"]] = $index){ mpre("ОШИБКА сохранения расчетного значения в справочник");
											//}elseif($index_back["val"] != $_calc["val"]){ //mpre("{$index["id"]} Результат изменился", $_calc_val, $_calc);
											}elseif(!$_CALC_POS = rb($CALC_POS, "id", "id", array_flip([$calc_pos["calc_pos_id"], $calc_pos["bmf-calc_pos"]]))){ mpre("ОШИБКА получения возможных вариантов позиции");
											}elseif(!is_numeric($_val = ($index_back["val"] ? 0 : 1))){ mpre("ОШИБКА инвертирования исходного значения морфа");
											}elseif(!$__calc = rb($CALC, "calc_pos_id", "calc_val_id", "val", $_CALC_POS, $_calc_val["id"], $_val)){ mpre("ОШИБКА выборки расчетного значения морфа");
											}elseif(!$index = ["calc_pos_id"=>$__calc["calc_pos_id"], "val"=>$__calc["val"]]+ $index){ mpre("ОШИБКА установки расчетной позиции морфу");
											}elseif(!$GLOBALS["BMF-INDEX"][$index["id"]] = $index){ mpre("ОШИБКА сохранения расчетного значения в справочник");
											}else{ //mpre($index, $_index);
											} return $index;
										}, $index))): mpre("ОШИБКА расчета морфа") ?>
									<? elseif(!is_array($_calc_pos = $index ? rb($CALC_POS, "id", $index["calc_pos_id"]) : [])): mpre("ОШИБКА получения обновленной позиции") ?>
									<? elseif(!is_array($_calc_val = $index ? rb($CALC_VAL, "id", $index["calc_val_id"]) : [])): mpre("ОШИБКА получения обновленного значения") ?>
									<? elseif(!is_array($_calc = $index ? rb($CALC, "calc_pos_id", "calc_val_id", $index["calc_pos_id"], $index["calc_val_id"]) : [])): mpre("ОШИБКА выборки обновленной позиции") ?>
									<? elseif(!$INPUT = call_user_func(function($INPUT = []) use($amend, $attr, $index, $index_, $dano, $bmf_index, $bmf_dano, $_calc, $calc_pos, $calc_val, $DANO){// mpre("Расчет типа, имени и количества элементов выбора");
											if(!$calc_pos){ /*mpre($bmf_index["id"], $attr);*/ $INPUT = [["name"=>"index[add][{$bmf_index["id"]}]", "disable"=>false, "readonly"=>false, "value"=>$amend, "type"=>"radio", "checked"=>(!$attr["del"] && $attr["top"]), "title"=>"Расширить морф"]];
											}elseif(!is_bool($checked_val = (bool)array_filter($index_))){ mpre("Признак изменения по позиции и значению");
											}elseif(!is_bool($checked_pos = call_user_func(function() use($attr, $index, $dano, $index_, $calc_val, $DANO){ // Только позицию без значения
													if(!$attr["top"]){// mpre("{$index["id"]}.Ветка морфа не является расчетной");
													}else{ return true; } return false;
												}))){ mpre("ОШИБКА расчета признака позиции");
											}elseif(!is_bool($checked_del = call_user_func(function() use($attr, $index, $index_, $bmf_dano){ // Удаление морфа
													if(!$attr["top"]){// mpre("{$index["id"]} Ветка морфа не является расчетной");
													}elseif(!$attr["del"]){// mpre("{$index["id"]} Ветка морфа не является расчетной");
													}elseif(array_filter($index_)){// mpre("{$index["id"]} Есть смежные элементы (не можем удалить)");
													}else{ return true; } return false;
												}))){ mpre("ОШИБКА расчета признака удаления морфа");
											}else{// mpre($index, $index_);
												$INPUT = [
													["name"=>"index[edit][{$index["id"]}]", "disabled"=>false, "readonly"=>false, "value"=>$_calc["id"], "type"=>"radio", "checked"=>$checked_pos, "title"=>"Изменить позицию и значение"],
													["name"=>"index[edit][{$index["id"]}]", "disabled"=>false, "readonly"=>false, "value"=>"", "type"=>"radio", "checked"=>$checked_del, "title"=>"Удаление морфа"],
												];
											} return $INPUT;
										})): mpre("ОШИБКА определения типа выбора элемента") ?>
									<? elseif(![$color_id, $title_id] = call_user_func(function() use($index_back, $calc){
											if(!$index_back){ return ["inherit", "Морф пуст"];
											}elseif($index_back["val"] != $calc["val"]){ return ["red", "Значение расчета не верно"];
											}else{// mpre("ОШИБКА цвета не установлена");
											} return ["inherit", "Нет ошибок"];
										})): mpre("ОШИБКА расчета цвета идентификатора") ?>
									<? elseif(!$pos_href = call_user_func(function($href = "/bmf:clump") use($index, $calc_pos, $CALC_POS){ // Адрес изменения позиции морфа
											if(!$href .= (($h = get($_GET, "bmf-clump_history")) ? "/bmf-clump_history:{$h}" : "")){ mpre("ОШИБКА добавления истории");
											}elseif(!$calc_pos){ //mpre("Позиция текущего морфа не задана");
											}elseif(!$href .= (($i = get($_GET, 'id')) ? "/{$i}" : "")){ mpre("ОШИБКА добавления идентификатора скопления");
											}elseif(!$href .= (array_key_exists('calc', $_GET) ? "/calc" : "")){ mpre("ОШИБКА добавления признака пересчета");
											}elseif(!$_calc_pos = rb($CALC_POS, "value", $calc_pos["value"]+1) ?: rb($CALC_POS, "value", 0)){ mpre("ОШИБКА получения следующей позиции для изменения");
											}elseif(!$href .= "/bmf-index:{$index["id"]}/bmf-calc_pos:{$_calc_pos["id"]}"){ mpre("ОШИБКА добавления номера текущего морфа");
											}else{ //mpre("Адрес изменнения позиции", $href);
											} return $href;
										})): mpre("ОШИБКА получения адреса изменения позиции") ?>
									<? elseif(!is_numeric($dano_value = is_numeric(get($dano, "val")) ? $dano["val"] : $bmf_dano["val"])): mpre("ОШИБКА расчета значения дано") ?>
									<? elseif(!$background_color = call_user_func(function($background_color = "inherit") use($attr){
											if(!$attr["top"]){ //mpre("Не основная нить");
											//}elseif(!$_calc_amend){ $background_color = "pink"; //mpre("Не задано направление");
											}else{ $background_color = "#ddd"; //mpre("Направление задано");
											} return $background_color;
										})): mpre("ОШИБКА получения фонового цвета морфа") ?>
									<? elseif(call_user_func(function() use($index, $index_back, $calc, $calc_pos, $calc_val, $_calc, $_calc_val, $calc_amend, $content, $dano, $bmf_dano, $dano_val, $dano_value, $attr, $pos_href, $INPUT, $color_id, $title_id, $background_color){ //mpre($background_color); // Отображение морфа ?>
											<li style="opacity:<?=($index ? 1 : 0.5)?>;">
												<span style="color:<?=$color_id?>;" title="<?=$title_id?>"><?=get($index, 'id')?><?=get($index, 'id_')?></span>.
												<?=get($dano, 'val')?>
												<label style="font-weight:normal;background-color:<?=$background_color?>;">
													<? foreach($INPUT as $input): ?>
														<input name="<?=$input["name"]?>" value="<?=$input["value"]?>" type="<?=$input["type"]?>" title="<?=$input["title"]?>" <?=(get($input, "checked") ? "checked" : "")?> <?=(get($input, "disabled") ? "disabled" : "")?> <?=(get($input, "readonly") ? "readonly" : "")?>>
													<? endforeach; ?>
													<span title="Идентификатор дано">#<?=($dano ? $dano['id'] : $bmf_dano['id'])?></span>
													<span style="background-color:<?=($attr["del"] ? "yellow" : "inherit")?>">
														<span title="Значение морфа" style="color:<?=(get($index_back, 'val') ? "red" : "blue")?>">
															<?=get($index_back, 'val')?>
														</span> &raquo;
														<span title="Значение после изменений" style="color:<?=(get($_calc, 'val') ? "red" : "blue")?>"><?=get($_calc, "val")?></span>
														<span title="Контрольное значение" style="color:<?=($dano_val ? "red" : "blue")?>">
															<?=$dano_val?>
														</span>
													</span>
													<span>
														<a title="Позиция морфа" style="font-weight:bold;" href="<?=$pos_href?>"><?=get($calc_pos, 'name')?></a> &raquo;
														<span title="Обновленная позиция"><?//=get($_calc_pos, "name")?></span> :
														<span title="Нижестоящие сигналы" style="font-weight:bold;"><?=get($calc_val, 'name')?></span> &raquo;
														<span><?=get($_calc_val, "name")?></span>
														<span title="Направление развития">(<?=get($calc_amend, "name")?>)</span>
													</span>
													<span title="Значение дано" style="color:<?=($dano_value ? "red" : "blue")?>">
														<?=$dano_value?>
													</span>
												</label>
												<ul><?=$content?></ul>
											</li>
										<? })): mpre("ОШИБКА отображения морфа") ?>
									<? elseif(!is_numeric($index_val = call_user_func(function($index_val = null) use($index, $bmf_dano){ // Расчет результата расчета морфа
												if(is_numeric(get($index, 'val'))){ $index_val = $index["val"]; //mpre("Расчетное значение морфа");
												}else{ $index_val = $bmf_dano["val"]; //mpre("Исходное значение");
												} return $index_val;
											}))): mpre("ОШИБКА расчета возвращаемого значения"); ?>
									<? else:// mpre($index, $dano, $attr) ?>
										<? return $index; ?>
									<? endif; ?>
								<? }): mpre("ОШИБКА обьявления функции дерева") ?>
							<? else:// mpre($_INDEX) ?>
								<ul>
									<? foreach($_INDEX as $index): ?>
										<? if(!$itog = rb('itog', 'index_id', $index['id'])): mpre("ОШИБКА получения итога морфа") ?>
										<? elseif(!$learn = ($index['val'] == $itog['val'] ? -1 : 1)): mpre("ОШИБКА получения флага обучения") ?>
										<? elseif(!is_array($CLUMP_HISTORY = array_map(function($_clump_history) use($index, $clump_history, $learn){
												if(!$clump_history){ //mpre("История не установлена");
												}elseif($_clump_history['id'] == get($clump_history, 'id')){// mpre("Текущий элемент истории", $learn);
													if(1 == $learn){ return $_clump_history; mpre("Не записываем ошибку");
													}elseif(!$_clump_history = fk('clump_history', ['id'=>$_clump_history['id']], null, ['val'=>$index['val']])){ mpre("ОШИБКА установки текущего значения морфу");
													}else{ return $_clump_history; }
												}elseif(1 != $learn){ return $_clump_history;
												}elseif(!$_clump_history = fk('clump_history', ['id'=>$_clump_history['id']], null, ['val'=>null])){ mpre("ОШИБКА обнуления значений истории");
												}else{ return $_clump_history; }
											}, $CLUMP_HISTORY))): mpre("ОШИБКА установки значения истории морфа") ?>
										<? elseif(!$index = $tree($index, $learn)): mpre("ОШИБКА запуска функции отрисовки дерева") ?>
										<? else:// mpre($index) ?>
										<? endif; ?>
									<? endforeach; ?>
								</ul>
							<? endif; ?>
						</span>
						<span style="width:20%;">
							<h3>Итоги</h3>
							<?// mpre($clump_history) ?>
							<ul>
								<? foreach($ITOG as $itog): ?>
									<? if(!is_array($_index = rb($_INDEX, 'id', $itog['index_id']))): mpre("ОШИБКА выборки корневого дано") ?>
									<? else:// mpre($_index) ?>
										<li>
											<label style="background-color:<?=(get($_index, "val") == get($clump_history, "itog") ? "#dfd" : "#fdd")?>; padding:0 5px;">
												<input name="itog_id" value="<?=$itog['id']?>" type="radio" <?=($_index ? "disabled" : "")?>>
												#<?=$itog['name']?>
												<span title="Значение итога" style="color:<?=($itog['val'] ? "red" : "blue")?>">
													<?=$itog['val']?>
												</span>
												<?//=$itog['shifting_tmp']?>
											</label>
										</li>
									<? endif; ?>
								<? endforeach; ?>
							</ul>
							<h3>Дано</h3>
							<ul>
								<? foreach($DANO as $dano): ?>
									<li>
										<label style="font-weight:normal; white-space:nowrap;">
											<input name="dano_id" value="<?=$dano['id']?>" type="radio">
											<?=$dano['name']?>#<?=$dano['id']?>
											<span title="Значение дано" style="font-weight:bold; color:<?=($dano['val'] ? "red" : "blue")?>">
												<?=$dano['val']?>
											</span>
											<span style="display:inline-block; max-width:100px; overflow:hidden; text-overflow:ellipsis;" title="<?=$dano['values']?>"><?=$dano['values']?></span>
										</label>
									</li>
								<? endforeach; ?>
							</ul>
							<p>
								<button type="submit" <?//=(get($_index, "val") == get($clump_history, "itog") ? "disabled" : "")?>>Править</button>
							</p>
							<!--<p><button type="submit" name="act" value="delete">Сникуть</button></p>-->
						</span>
					</div>
				</div>
			</form>
		<? endif; ?>
	</div>
<? endif; ?>
