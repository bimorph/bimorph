<? if(!$db = (array_key_exists("null", $_GET) ? get($_GET, "") : array_search("", $_GET))): mpre("ОШИБКА сеть не указана <a href='/bmf/test'>/bmf/test</a>", $flip); ?>
<? //elseif(true): mpre($_GET, $db) ?>
<? elseif(call_user_func(function($conn = null) use($db){ // Получения идентификатора соединения с БД
		if(!$dbname = "{$db}.sqlite"){ mpre("ОШИБКА получения файла БД {$db}");
		}else if(!$filename = "modules/bmf/sh/cpp/clump/{$dbname}"){ mpre("ОШИБКА получения пути до файла");
		}else if(!file_exists($filename)){ mpre("ОШИБКА файл с БД не найден (возможно он еще небыл создан)", $filename);
		//}else if(!$conn = conn("sqlite:{$filename}")){ mpre("ОШИБКА установки соединения с БД {$filename}");
		}else{ //return $conn;
		}
	})): mpre("<<< Загрузите данные для расчетов в форму слева") ?>
<? elseif(!call_user_func(function($alias) use($db, $conf){ // Получения идентификатора соединения с БД
		if(!$database = mpql(mpqw("PRAGMA database_list"))){ mpre("ОШИБКА получения списка баз данных");
		}else if(!$dbname = "{$db}.sqlite"){ mpre("ОШИБКА получения файла БД {$db}");
		}else if(!$path = "modules/bmf/sh/cpp/clump/{$dbname}"){ mpre("ОШИБКА получения пути до файла");
		}else if(!file_exists($path)){ //mpre("ОШИБКА файл с БД не найден (возможно он еще небыл создан)", $filename);
		}elseif(!$_database = array_column($database, "file", "name")){ mpre("ОШИБКА получения баз данных поименно");
		}else if(!is_string($now = (get($_database, $alias) ?: ""))){ mpre("ОШИБКА получения текущей подключенной БД");
		}else if(!$_database = call_user_func(function($_database) use($now, $alias){ // Отключение другой подключенной БД
				if(!$now){ //mpre("Других БД не подключено");
				//}elseif($realpath == $now){ //mpre("Подключена нужная БД ($now)");
				}elseif(!qw("DETACH DATABASE {$alias}")){ mpre("ОШИБКА подключения таблицы скопления", $_database);
				}elseif(!$database = mpql(mpqw("PRAGMA database_list"))){ mpre("ОШИБКА получения списка баз данных");
				}elseif(!$_database = array_column($database, "file", "name")){ mpre("ОШИБКА получения баз данных поименно");
				}else{ //mpre("Отключаем БД {$now}");
				} return $_database;
			}, $_database)){ mpre("ОШИБКА отключения БД");
		}elseif($f = get($_database, $alias)){ mpre("База данных уже подключена `{$f}`", $_database);
		}elseif(!qw("ATTACH DATABASE `{$path}` AS {$alias}")){ mpre("ОШИБКА подключения таблицы скопления", $_database);
		}elseif(!$database = mpql(mpqw("PRAGMA database_list"))){ mpre("ОШИБКА получения списка баз данных");
		}else{ //mpre($database);
		} return $database;
	}, 'test')): mpre("<<< Загрузите данные для расчетов в форму слева") ?>
<? elseif(!$BMF_TEST = mpqn(mpqw("SELECT * FROM test.mp_bmf_test ORDER BY id DESC"))): mpre("ОШИБКА получения списка дано") ?>
<? elseif(!$_BMF_TEST = call_user_func(function($_BMF_TEST = []){ // Список для вывода
		if(array_key_exists("divider", $_GET)){ $_BMF_TEST = mpqn(mpqw("SELECT * FROM test.mp_bmf_test WHERE divider=". (int)$_GET["divider"]. " ORDER BY id DESC"));
		}else if(array_key_exists("general", $_GET)){ $_BMF_TEST = mpqn(mpqw("SELECT * FROM test.mp_bmf_test WHERE divider IN (0,'',NULL) ORDER BY id DESC"));
		}else{ $_BMF_TEST = mpqn(mpqw("SELECT * FROM test.mp_bmf_test ORDER BY id DESC"));
		} return $_BMF_TEST;
	})): mpre("ОШИБКА получения списка для вывода") ?>
<? //elseif(!$BMF_TEST = mpqn(mpqw("SELECT * FROM mp_bmf_test WHERE ". (array_key_exists("general", $_GET) ? "divider IN (0,NULL,'')" : "1"). " ORDER BY id DESC", $conn))): mpre("ОШИБКА получения списка дано") ?>
<? elseif(!$DIVIDER = rb($BMF_TEST, "divider")): mpre("ОШИБКА получения списка по делителям") ?>
<? elseif(!ksort($DIVIDER)): mpre("ОШИБКА сортировки списка по делителям") ?>
<? else: //mpre($DIVIDER) ?>
	<span style="float:right;">
		<a href="/bmf:log/<?=$db?>" style="font-weight:<?=(!array_key_exists("general", $_GET) && !array_key_exists('divider', $_GET) ? "bold" : "inherit")?>;">Все</a>
		<? foreach($DIVIDER as $divider): ?>
			<a href="/bmf:log/<?=$db?>?divider=<?=$divider["divider"]?>" style="font-weight:<?=(get($_GET, "divider") == $divider["divider"] ? "bold" : "inherit")?>;"><?=$divider["divider"]?></a>
		<? endforeach; ?>
		<a href="/bmf:log/<?=$db?>/general" style="font-weight:<?=(array_key_exists("general", $_GET) ? "bold" : "inherit")?>;">Общие</a>
	</span>
	<h1 style="margin-top:0;">
		История расчета
	</h1>
	<div class="table log" style="width:100%;">
		<style>
			.table.log > div > span {border-collapse: collapse; border: 1px solid #aaa;}
		</style>
		<div class="th">
			<span>п.н.</span>
			<span>Время</span>
			<span>Примеров</span>
			<span>Эпох</span>
			<span>Поток</span>
			<span>Размер</span>
			<span>Время c.</span>
			<span>Побитно%</span>
			<span>Изменений</span>
			<span>Совпадение%</span>
		</div>
		<? foreach($_BMF_TEST as $bmf_test):// mpre($bmf_test) ?>
			<div style="background-color:<?=($bmf_test["divider"] ? "inherit" : "#ddd")?>;">
				<span><?=$bmf_test["id"]?></span>
				<span><?=$bmf_test["date"]?></span>
				<span><?=$bmf_test["size"]?></span>
				<span><?=$bmf_test["loop"]?></span>
				<span><?=$bmf_test["divider"]?></span>
				<span><?=$bmf_test["bmf"]?></span>
				<span><?=$bmf_test["duration"]?></span>
				<span><?=$bmf_test["pips"]?></span>
				<span><?=$bmf_test["change"]?></span>
				<span><?=$bmf_test["perc"]?></span>
			</div>
		<? endforeach; ?>
	</div>
<? endif; ?>
