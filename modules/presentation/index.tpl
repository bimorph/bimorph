<? if(!$INDEX = rb("index")): pre("ОШИБКА список слайдов <a href='/presentation:admin/r:presentation-index'>не найден</a>") ?>
<? else: ?>
	<div style="width:650px;">
		<? foreach($INDEX as $index): ?>
			<h2><?=$index['name']?></h2>
			<h5><?=$index['description']?></h5>
			<img src="/presentation:img/<?=$index["id"]?>/tn:index/fn:img/w:600/h:800/null/img.png">
			<div><?=$index['text']?></div>
			<div style="text-align:right;"><a target="blank" href="<?=$index["href"]?>"><?=$index["href"]?></a></div>
		<? endforeach; ?>
		<?//=$conf['settings']['comments']?>
	</div>
<? endif; ?>
