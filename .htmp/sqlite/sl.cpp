#include <stdio.h>
#include <iostream>
#include <utility>
//#include <iostream>
#include <string>
#include <sqlite3.h>
#include <map> /* Ассоциативные массивы */
//#include <iomanip>
//#include <typeinfo>
//#include <new>
//#include <locale>
//#include <array>
//#include <regex>
#include <ctime>
//#include <fstream>
#include <algorithm> // Количество символов в строке
#include <bitset> // Перевод десятичных чисел к двоичному виду
#include <cmath> // Возведение в степень
#include <unistd.h> // sleep


using namespace std; //setlocale(LC_ALL, "ru_RU");
#include "map.h"

typedef std::map<string, string> TMs;
typedef std::map<string, TMs> TMMs;

TMMs BM0_CLUMP, BM0_INDEX, BM0_DANO, BM0_ITOG; // Значения позиций морфов
TMMs INDEX_DANO, INDEX_ITOG;
TMs SPOOF, PARENT;
TMs bm0_itog, dano_index; // Расчетный итог

//TMMs link; // Список морфов на которые ссылается первое плече
TMs bm0_clump; // Текущее скопление
string VAL[2][2]; // Массив поиска идентификатора значения по старшему и млачшему значению нижестоящих
string CALC[5][5]; // Массив поиска расчета морфа по позиции и значению
sqlite3 *db = 0;
sqlite3_stmt *stmt; // Запрос к базе данных

bool data(TMMs TAB, int line, string tab = "", string comment = ""){
	string sepor;
	cout << line << " " << (tab == "" ? "" : "TMMs "+ tab+ " = ") << "{" << (comment == "" ? "" : " // "+comment) << endl;
	for(TMMs::iterator itr = TAB.begin(); itr != TAB.end(); itr++){
		string id = itr->first;
		TMs row = itr->second;
		cout << "\t{\""+ id+ "\", {";
		for(TMs::iterator it = row.begin(); it != row.end(); it++){
			string key = it->first;
			string val = it->second;
			sepor = (it == row.begin() ? "" : ", ");
			cout << sepor+ "{\""+ key+ "\", \""+ val+ "\"}";
		} cout << "}}," << endl;
	} cout << "};" <<  endl;
	return false;
}

int mpre(TMs row, int line, string comment = "", string prefix = ""){
	cout << prefix << "Array";
	cout << "__" << std::to_string(line) << "__" << prefix << "(" << (row.find("id") != row.end() ? " << "+ row["id"] : "") << " " << comment << "\n";
	for(TMs::iterator itr = row.begin(); itr != row.end(); itr++){
		string field = (string) itr->first;
		string val = (string) itr->second;
		cout << prefix << "\t[" << field << "]=>" << val << "," << endl;
	}; cout << prefix << ")\n";
	return 1;
}

int mpre(TMMs TAB, int line, string comment = ""){
	cout << "Array";
	if(line > 0){
		cout << "__" << std::to_string(line) << "__";
	}; cout << "( // " << comment << "\n";
	for(TMMs::iterator itr = TAB.begin(); itr != TAB.end(); itr++){
		TMs row = itr->second;
		mpre(row, line, comment, "\t");
	} cout << ")\n";
	return 1;
}

bool qw(string sql){
	while(SQLITE_OK != sqlite3_exec(db, sql.c_str(), 0, 0, 0)){
		cout << __LINE__ << " ОШИБКА выполнения запроса к базе данных: " << endl << sql << endl << " >> " << sqlite3_errmsg(db) << endl;
		sleep(1);
	} return false;
}

TMMs tab(string sql){
	char *val; int count; TMMs TAB;

	if(SQLITE_OK != sqlite3_prepare(db, sql.c_str(), -1, &stmt, 0)){ cout << __LINE__ << " ОШИБКА выполнения запроса к базе данных: " << sql << endl;
	}else if([&](){ count = sqlite3_column_count(stmt); return false; }()){ cout << " ОШИБКА расчета количества записей в таблице";
	}else{
		while(SQLITE_ROW == sqlite3_step(stmt)){
			TMs row;
			for(int num = 0; num < count; num++){
				string field = (string) sqlite3_column_name(stmt, num);
				val = (char*) sqlite3_column_text(stmt, num);
				string value = (val == NULL ? "" : val);
				row.insert(std::make_pair(field, value));
			} TAB.insert(std::make_pair(row["id"], row));
		}// cout << __LINE__ << " Запрос к БД " << sql << " размер: " << TAB.size() << endl;
	} return TAB;
}

std::string dec2bin(int n, int size){// cout << __LINE__ << " Расчет двоичного значения n < " << n << " size < " << size << endl;
	std::string res;
	while(n){
		res.push_back((n & 1) + '0');
		n >>= 1;
	} if(res.empty()){
		res = "0";
	}else{
		std::reverse(res.begin(), res.end());
	} if(size > res.size()){
		int count = size-res.size()-1;
		for(int i = 0; i <= count; i++){
			res.insert(0, 1, '0');
		}
	} return res.substr(0, size);
}

TMMs rb(TMMs &TAB, TMs values, bool debug = false){
	TMMs LIST; TMs::iterator it;
	if([&](){// cout << __LINE__ << "Количество элементов в условии << " << size << " keys_size:" << keys_size << " vals_size:" << vals_size << endl;
			for(TMMs::iterator itr = TAB.begin(); itr != TAB.end(); itr++){
				bool keep = true;
				string id = itr->first;
				TMs row = itr->second;// mpre(row, __LINE__); data(TAB, __LINE__);
				for(TMs::iterator it = values.begin(); it != values.end(); it++){
					string key, val;
					if([&](){ key = it->first; return false; }()){ cout << __LINE__ << " Ключ условия выборки << " << key << endl;
					}else if([&](){ val = it->second; return false; }()){ cout << __LINE__ << " Значения условия выборки << " << val << endl;
					}else if(row.find(key) == row.end()){ cout << __LINE__ << " Поле в таблице не найдено [" << key << "]=" << val << endl; mpre(values, __LINE__); mpre(row, __LINE__); keep = false;
					}else if(val == row.find(key)->second){// cout << __LINE__ << " Значение не соответствует уcловию << " << " " << it->second << ":" << values.find(key)->second << endl;
					}else{// cout << "i=" << i << ";" << endl;
						keep = false;
					}// cout << __LINE__ << " i[" << i++ << "]={" << keys[i] << ":" << vals[i] << "}" << endl;
				} if(keep){// mpre(row, __LINE__);
					LIST.insert(make_pair(id, row));
				}// cout << __LINE__ << " Количество элементов в выборке:" << LIST.size() << endl;
			} return false;
		}()){ cout << __LINE__ << " Расчет списка условий";
	}else{// mpre(values, __LINE__); data(TAB, __LINE__);
		return LIST;
	}
}

TMs erb(TMMs &TAB, TMs values, bool debug = false){
	TMMs LIST; TMMs::iterator itr; TMs row;
	if([&](){ LIST = rb(TAB, values); return false; }()){ cout << __LINE__ << " Получение списка подходящих под условия элементов" << endl;
	}else if(LIST.size() > 1){ cout << __LINE__ << " Количество элементов в выборке более одного" << endl; mpre(values, __LINE__); data(LIST, __LINE__); return row;
	}else if(LIST.empty()){ return row;// cout << __LINE__ << " Список выборки пуст" << endl; mpre(values, __LINE__); return row;
	}else if((itr = LIST.begin()) == LIST.end()){ cout << __LINE__ << " ОШИБКА выборки первого элемента из списка выборки" << endl; return row;
	}else{ row = itr->second;
	} return row;
}

TMs fk(string table, TMs where, TMs insert, TMs update){// mpre(where, __LINE__); mpre(insert, __LINE__); mpre(update, __LINE__);
	TMs row; string sql;
	if([&](){
			if(where.empty()){// cout << __LINE__ << " Условие на выборку записей не указано" << endl;
			}else if([&](){
					string values;
					sql = "SELECT * FROM `"+ table+ "`";
					for(TMs::iterator itr = where.begin(); itr != where.end(); itr++){
						string key = itr->first;
						string val = itr->second;
						string separ = (values.size() ? " AND " : "");
						values += separ+ "`"+ key+ "`='"+ val+ "'";
					} sql += " WHERE "+ values;
					return false;
				}()){ cout << __LINE__ << " Запрос добавления выборку записи из БД" << endl;
			}else if([&](){ TMMs TAB;
					if([&](){ TAB = tab(sql); return false; }()){ // Выполнение запроса к базе дыннх
					}else if([&](){ return TAB.size() < 1; }()){// cout << __LINE__ << " Размер выборки меньше нуля << " << sql << endl;
					}else if([&](){ row = TAB.begin()->second; return false; }()){ cout << __LINE__ << "Список измененных данных" << endl;
					}else{// mpre(row, __LINE__); // cout << __LINE__ << " Запрос к БД:" << sql << endl; // mpre(row);
					} return false;
				}()){ cout << " Запрос на получения вновь установленной записи" << endl;
//			}else if([&](){ cout << __LINE__ << " Условие выборки " << sql << endl; mpre(row); return false; }()){ // Уведомление
			}else{// cout << __LINE__ << " Запрос на выборку значений: " << sql << endl; // mpre(where, __LINE__);
			} return false;
		}()){ cout << __LINE__ << " Выборка записи по условию" << endl;
	}else if([&](){ int rowid;
			if(insert.empty()){// cout << __LINE__ << " Условия добавления новой записи не указаны" << endl;
//			}else if([&](){ cout << __LINE__ << " Добавление записи" << endl; return false; }()){ // Уведомление
			}else if(!row.empty()){// cout << __LINE__ << " Запись уже найдена по условиям << " << row["id"] << endl; mpre(where, __LINE__); mpre(insert, __LINE__); mpre(update, __LINE__);
			}else if([&](){
					string fields, values;
					sql = "INSERT INTO `"+ table+ "`";
					for(TMs::iterator itr = where.begin(); itr != where.end(); itr++){
						string key = itr->first;
						string val = itr->second;
						string separ = (fields.size() ? ", " : "");
						fields += separ+ "`"+ key+ "`";
						values += separ+ "'"+ val+ "'";
					} for(TMs::iterator itr = insert.begin(); itr != insert.end(); itr++){
						string key = itr->first;
						string val = itr->second;
						string separ = (fields.size() ? ", " : "");
						fields += separ+ "`"+ key+ "`";
						values += separ+ "'"+ val+ "'";
					} sql += "("+ fields+ ") VALUES ("+ values+ ")";
//					cout << __LINE__ << " (Запрос) Добавление новой записи: " << sql << endl; mpre(where, __LINE__); mpre(insert, __LINE__);
					return false;
				}()){ cout << __LINE__ << " Запрос добавления новой записи в БД" << endl;
			}else if(SQLITE_OK != sqlite3_exec(db, sql.c_str(), 0, 0, 0)){ cout << __LINE__ << " ОШИБКА выполнения запроса к базе данных: " << sql << " >> " << sqlite3_errmsg(db) << endl;// return false;
			}else if([&](){ rowid = sqlite3_last_insert_rowid(db); return false; }()){ cout << __LINE__ << " Идентификатор последней установленной записи << " << rowid << endl;
			}else if([&](){
					if([&](){ sql = "SELECT * FROM `"+ table+ "` WHERE id="+ to_string(rowid); return false; }()){ cout << __LINE__ << " Запрос на выборку внось установленной записи" << endl;
					}else if([&](){ TMMs TAB = tab(sql); row = erb(TAB, {{"id", to_string(rowid)}}); return false; }()){ cout << __LINE__ << "Список измененных данных" << endl;
					}else{// cout << __LINE__ << " Запрос к БД:" << sql << endl; // mpre(row);
					} return false;
				}()){ cout << " Запрос на получения вновь установленной записи" << endl;
			}else{// cout << __LINE__ << " Добавление новой записи к базе `"+ table+ "` << " << rowid << " " << endl;
			} return false;
		}()){ cout << __LINE__ << "ОШИБКА добавление запроса если не найден по условиям" << endl;
	}else if([&](){
			if(update.empty()){// cout << __LINE__ << " Условия обновления не указаны" << endl;
			}else if(row.empty()){ cout << __LINE__ << " Запись для обновления не найдена" << endl;
			}else if([&](){
					string fields, values;
					sql = "UPDATE `"+ table+ "`";
					for(TMs::iterator itr = update.begin(); itr != update.end(); itr++){
						string key = itr->first;
						if(key == "id"){ // Сохраняем идентификатор записи
						}else if(key.substr(0, 1) == "_"){ // Технические поля
						}else{
							string val = itr->second;
							string separ = (values.size() ? ", " : "");
							values += separ+ "`"+ key+ "`='"+ val+ "'";
						}
					} sql += " SET "+ values+ " WHERE id="+ row["id"];
					return false;
				}()){ cout << __LINE__ << " Запрос добавления новой записи в БД" << endl;
			}else if([&](){ qw(sql); return false; }()){ cout << __LINE__ << " ОШИБКА обновления записи" << endl;
			}else if([&](){ sql = "SELECT * FROM `"+ table+ "` WHERE id="+ row["id"]; return false; }()){ cout << __LINE__ << " Запрос на выборку внось установленной записи" << endl;
			}else if([&](){ TMMs TAB = tab(sql); row = erb(TAB, {{"id", row["id"]}}); return false; }()){ cout << __LINE__ << "Список измененных данных" << endl;
			}else{// cout << __LINE__ << " Обновляем запись: " << sql << endl;// mpre(update, __LINE__);
			} return false;
		}()){ cout << __LINE__ << " Обновение найденной по условию записи" << endl;
	}else{// mpre(where, __LINE__); mpre(insert, __LINE__); mpre(update, __LINE__);
	} return row;
}

TMs learn(TMs row, string value, TMs mate, TMMs PARENT, bool remove = false){ cout << __LINE__ << " Обучение < " << value << " remove:" << remove << " << " << row["id"] << endl;// mpre(row, __LINE__, "Ученик");
	TMs index, parent, bm0_index, bm0_calc, bm0_calc_pos, bm0_calc_val, calc_amend, bm0_calc_sequence, bm0_calc_balance, bm0_calc_orient, _bm0_calc_sequence;

	if(row.empty()){ cout << __LINE__ << " Пустой морф на обучении" << endl;
//}else if([&](){ cout << __LINE__ << " Уведомление" << endl; return false; }()){ // Уведомление
//	}else if((value == "") && [&](){ value = row["val"]; return false; }()){ // Выборка значения морфа
	}else if(value == ""){ cout << __LINE__ << " Обучающее значение не задано << " << row["id"] << endl; return row;
	}else if([&](){ cout << row["id"] << "."; return false; }()){ cout << __LINE__ << " Уведомление";
	}else if([&](){ index = erb(BM0_INDEX, {{"id", row["index_id"]}}); return false; }()){ cout << __LINE__ << " ОШИБКА расчета старшего морфа" << endl;
	}else if([&](){ bm0_index = erb(BM0_INDEX, {{"id", row["bm0-index"]}}); return false; }()){ cout << __LINE__ << " ОШИБКА расчета младшего морфа" << endl;
	}else if([&](){ bm0_calc_pos = erb(BM0_CALC_POS, {{"id", row["calc_pos_id"]}}); return false; }()){ cout << __LINE__ << " Получение позиции морфа" << endl;
	}else if([&](){ bm0_calc = erb(BM0_CALC, {{"calc_pos_id", row["calc_pos_id"]}, {"calc_val_id", row["calc_val_id"]}}); return false; }()){ cout << __LINE__ << " Значения расчета текущего элемента" << endl;
	}else if([&](){ calc_amend = erb(BM0_CALC_AMEND, {{"id", bm0_calc["calc_amend_id"]}}); return false; }()){ cout << __LINE__ << " Изменения морфов" << endl;
	}else if([&](){ bm0_calc_sequence = erb(BM0_CALC_SEQUENCE, {{"id", row["calc_sequence_id"]}}); return false; }()){ cout << __LINE__ << " ОШИБКА выборки распиновки" << endl;
	}else if([&](){ string val; TMs index_value, _bm0_calc_pos, _bm0_calc_val; // Если морф крайний расширяем
			if(row["id"] == ""){ cout << __LINE__ << " Пустое значение морфа " << endl; mpre(row, __LINE__);
			}else if(!index.empty()){ // Подключен основной морф
			}else if(!bm0_index.empty()){ // Подключен дополнительный морф
			}else if(row["val"] == value){// mpre(row, __LINE__, "Значение морфа совпадает с обучением");
			}else if(remove){ cout << __LINE__ << " Запрет на расширение листа << " << row["id"] << endl;
//			}else if([&](){ mpre(row, __LINE__, "обучаемый морф"); return false; }()){
			}else if([&](){ index = row; return false; }()){ // Сброс текущего морфа
			}else if([&](){ TMMs INDEX, _INDEX; // Перебор всех морфов поиск подходящих для расширения
					if([&](){ INDEX = rb(BM0_INDEX, {{"index_id", ""}, {"bm0-index", ""}}); return INDEX.empty(); }()){ cout << __LINE__ << " (Расширение) ОШИБКА выборка массива подходящих морфов" << endl;
					}else if([&](){ parent = (PARENT.empty() ? parent : (--PARENT.end())->second); return false; }()){ // Родитель
					}else if([&](){ TMs dano;
							for(auto itr = INDEX.rbegin(); itr != INDEX.rend(); itr++){
								string id = itr->first;
								TMs _index = itr->second;// mpre(_index, __LINE__);
								if([&](){ dano = erb(BM0_DANO, {{"id", _index["dano_id"]}}); return dano.empty(); }()){ cout << __LINE__ << " ОШИБКА выборки дано у морфа" << endl;
								}else if(_index["id"] == index["id"]){ cout << __LINE__ << " (Расширение) выбор < " << _index["id"] << " пропускаем себя << " << row["id"] << endl;
								}else if(_index["val"] != row["val"]){ cout << __LINE__ << " (Расширение) выбор < " << _index["id"] << " не равны значения << " << row["id"] << endl;

//								}else if((row["shift"] == bm0_clump["shift"]) && (_index["shift"] == bm0_clump["shift"])){ cout << __LINE__ << " (Расширение) выбор < " << _index["id"] << " изменения у итога отличаются от изменений дано << " << row["id"] << endl;
//								}else if((row["shift"] != bm0_clump["shift"]) && (_index["shift"] != bm0_clump["shift"])){ cout << __LINE__ << " (Расширение) выбор < " << _index["id"] << " изменения у итога отличаются от изменений дано << " << row["id"] << endl;

//								}else if([&](){ mpre(row, __LINE__, "Текущий морф"); mpre(dano, __LINE__, "Дано морфа"); mpre(bm0_itog, __LINE__, "Итоговый сигнал"); cin.get(); return false; }()){ // Уведомление
//								}else if([&](){ dano = erb(BM0_DANO, {{"id", _index["dano_id"]}}); return dano.empty(); }()){ cout << __LINE__ << " Дано морфа не найдено" << endl;
//								}else if(dano["val"] == dano["value"]){ cout << __LINE__ << " (Расширение) выбор < " << _index["id"] << " изменились значения << " << row["id"] << endl;
//								}else if(!mate.empty() && (_index["id"] == mate["id"])){ cout << __LINE__ << " (Расширение) выбор << " << _index["id"] << " морф партнера << " << row["id"] << endl;
//								}else if(_index["val"] == _index["value"]){ cout << __LINE__ << " (Расширение) выбор < " << _index["id"] << " не изменились << " << row["id"] << endl;
//								}else if((_index["val"] == _index["value"]) && (row["val"] == row["value"])){ cout << __LINE__ << " (Расширение) выбор < " << _index["id"] << " оба не изменились << " << row["id"] << endl;
//								}else if((_index["val"] != _index["value"]) && (row["val"] != row["value"])){ cout << __LINE__ << " (Расширение) выбор < " << _index["id"] << " оба изменились << " << row["id"] << endl;
//								}else if(_index["val"] != value){ cout << __LINE__ << " (Расширение) выбор < " << _index["id"] << " не равен значению обучения << " << row["id"] << endl;
//								}else if(_index["val"] != _index["value"]){ cout << __LINE__ << " (Расширения) выбор < " << _index["id"] << " изменился << " << row["id"] << endl;
//								}else if(!parent.empty() && (_index["val"] != parent["val"])){ cout << __LINE__ << " (Расширение) выбор < " << _index["id"] << " значение не равно родителю << " << row["id"] << endl;
								}else{ cout << __LINE__ << " (Расширение) выбор < " << _index["id"] << " подходит << " << row["id"] << endl;
									_INDEX.insert(make_pair(id, _index));
								}
							} cout << __LINE__ << " (Расширение) Результат расчета подходящих << " << index["id"] << endl;// mpre(bm0_index, __LINE__, "Выбранный элемент");
							return _INDEX.empty();
						}()){ cout << __LINE__ << " Список подходящих по условию сигналов пуст" << endl;
//					}else if([&](){ mpre(_INDEX, __LINE__, "Возможные варианты"); return false; }()){ // Уведомление
//					}else if(_INDEX.size() != 1){ cout << __LINE__ << " Допустим только один возможный вариант";
					}else if([&](){ 
							srand(time(NULL));
							auto it = _INDEX.begin();
							advance(it, rand()%_INDEX.size());
							bm0_index = it->second;
							return bm0_index.empty();
						}()){ cout << __LINE__ << " ОШИБКА выборки сигнала из списка подходящих" << endl;
					}else{// mpre(bm0_index, __LINE__, "Выбранный морф");// Выбор
					} return bm0_index.empty();
				}()){ cout << __LINE__ << " (Расширение) подходящие для расширения морфы не найдены" << endl;
//			}else if([&](){ mpre(parent, __LINE__, "Родитель"); return false; }()){ // Уведомление
			}else if([&](){ TMs dano; // Сохранение значений новых элементов и дано
					if([&](){ BM0_INDEX[index["id"]] = index = fk("mp_bm0_index", {{"id", index["id"]}}, {}, {{"value", index["val"]}}); return false; }()){ // Сохранение нижестоящей
					}else if([&](){ BM0_DANO[index["dano_id"]] = dano = fk("mp_bm0_dano", {{"id", index["dano_id"]}}, {}, {{"value", index["value"]}}); return dano.empty(); }()){ cout << __LINE__ << " ОШИБКА обновления источника основного морфа" << endl; mpre(index, __LINE__);
					}else if([&](){ BM0_INDEX[bm0_index["id"]] = bm0_index = fk("mp_bm0_index", {{"id", bm0_index["id"]}}, {}, {{"value", bm0_index["val"]}}); return false; }()){ // Сохранение нижестоящей
					}else if([&](){ BM0_DANO[bm0_index["dano_id"]] = dano = fk("mp_bm0_dano", {{"id", bm0_index["dano_id"]}}, {}, {{"value", bm0_index["value"]}}); return dano.empty(); }()){ cout << __LINE__ << " ОШИБКА обновления источника дополнительного морфа" << endl; mpre(index, __LINE__);
					}else{
					} return false;
				}()){
			}else if([&](){ cout << __LINE__ << " Расширение морфа < " << value << " << " << index["id"] << "=>" << bm0_index["id"] << endl; return false; }()){ // Уведомление
			}else if([&](){ string val; TMs _bm0_calc_val; // Расчет позиции исходя из подключенных морфов
					if([&](){ bm0_calc_val = erb(BM0_CALC_VAL, {{"v1", index["val"]}, {"v0", bm0_index["val"]}}); return false; }()){ cout << __LINE__ << " Выборка значения нижестоящих элементов";
					}else if([&](){ bm0_calc_balance = erb(BM0_CALC_BALANCE, {{"calc_val_id", bm0_calc_val["id"]}, {"val", value}}); return false; }()){ // Расчет устаревшего баланса
//					}else if(index["val"] == bm0_index["val"]){ return false;
//					}else if([&](){ _bm0_calc_val = erb(BM0_CALC_VAL, {{"v1", index["val"]}, {"v0", (bm0_index["val"] == "0" ? "1" : "0")}}); return false; }()){ // Выборка зеркального значения
//					}else if([&](){ bm0_calc_balance = erb(BM0_CALC_BALANCE, {{"calc_val_id", _bm0_calc_val["id"]}, {"val", (value == "0" ? "1" : "0")}}); return false; }()){ // Выборка зеркального баланса
					}else{ cout << __LINE__ << " (Расширение) значения морфов не совпадают" << endl;// mpre(_bm0_calc_val, __LINE__); mpre(bm0_calc_balance, __LINE__);
					} return false;
				}()){ cout << __LINE__ << " (Расширение) ОШИБКА расчета позиции нового морфа" << endl;
			}else if([&](){ _bm0_calc_val = erb(BM0_CALC_VAL, {{"v0", index.at("val")}, {"v1", bm0_index.at("val")}}); return _bm0_calc_val.empty(); }()){ cout << __LINE__ << "ОШИБКА расчета занчения расширяемого морфа" << endl;
			}else if([&](){ index_value["calc_val_id"] = _bm0_calc_val["id"]; return false; }()){ // Значение нового морфа
//			}else if([&](){ mate = erb(BM0_INDEX, {{"id", mate_id}}); return false; }()){ cout << __LINE__ << "Значение морфа партнера" << endl;
			}else if([&](){ TMs pos;
					if([&](){ pos = erb(BM0_CALC_POS, {{"id", mate["calc_pos_id"]}}); return false; }()){ // Позиция парного эллемента может быть пустой
					}else if([&](){ index_value["calc_pos_id"] = ((bm0_calc_balance["calc_pos_id"] == pos["id"]) || (bm0_calc_balance["calc_pos_id"] == pos["calc_pos_id"]) ? bm0_calc_balance["bm0-calc_pos"] : bm0_calc_balance["calc_pos_id"]); return false; }()){ // Расчет позиции из балансировки
					}else{// mpre(index_value, __LINE__, "Расширяемый морф"); mpre(mate, __LINE__, "Морф партнер"); mpre(bm0_calc_balance, __LINE__, "Баланс");
					} return false;
				}()){ cout << __LINE__ << " Выбор позиции для балансировки" << endl;
			}else if([&](){ _bm0_calc_pos = erb(BM0_CALC_POS, {{"id", index_value["calc_pos_id"]}}); return _bm0_calc_pos.empty(); }()){ cout << __LINE__ << " ОШИБКА получение позици нового морфа" << endl; mpre(index_value, __LINE__, "Создаваемый морф");
			}else if([&](){ bm0_calc = erb(BM0_CALC, {{"calc_pos_id", index_value["calc_pos_id"]}, {"calc_val_id", index_value["calc_val_id"]}}); return bm0_calc.empty(); }()){ cout << __LINE__ << " ОШИБКА выборки расчета морфа по нижестоящим элементам" << endl;
			}else if([&](){ string form; TMs first, second; // Составление и выполнение запроса на создание расширяющего морфа
					if([&](){ form = _bm0_calc_pos["form"]; return false; }()){ cout << __LINE__ << " Установка формы морфа" << endl;
					}else if([&](){ BM0_INDEX[index_value["id"]] = index_value = fk("mp_bm0_index", {}, {{"time", to_string(time(nullptr))}, {"clump_id", index["clump_id"]}, {"dano_id", index["dano_id"]}, {"value", bm0_calc["val"]}, {"val", bm0_calc["val"]}, {"calc_sequence_id", bm0_calc_sequence["id"]}, {"calc_pos_id", index_value["calc_pos_id"]}, {"calc_val_id", index_value["calc_val_id"]}, {"index_id", index["id"]}, {"bm0-index", bm0_index["id"]}, {"form", form}, {"shift", bm0_clump["shift"]}}, {}); return index_value.empty(); }()){ cout << __LINE__ << " Добавление нового морфа" << endl;
					}else{// mpre(index_value, __LINE__, "Вновь созданный морф");// mpre(bm0_calc, __LINE__);
					} return false;
				}()){ // Добавление морфа
			}else if([&](){ value = bm0_calc["val"]; return false; }()){ // Сохраняем значение предотвращяем дальнейшее расширение
			}else{ cout << __LINE__ << " Расширение морфа index:" << index["id"] << " bm0-index:" << bm0_index["id"] << endl;
				row = index_value;
			} return row.empty();
		}()){ cout << __LINE__ << " ОШИБКА расширение морфа" << endl;
	}else if([&](){ string val; bool rem; TMs _index, index_value, index_mate, bm0_calc_orient, bm0_calc_amend; cout << __LINE__ << " Расчет позиции морфа << " << row["id"] << endl;
//			mpre(row, __LINE__, "Морф"); mpre(bm0_calc, __LINE__, "Калькуляция"); cin.get();
			if(value == row["val"]){ cout << __LINE__ << " (Обучение) Значение равны расчетному < " << value << " << " << row["id"] << endl;
//			}else if([&](){ cout << __LINE__ << " value:" << value << endl; mpre(row, __LINE__, "Морф"); return false; }()){
//			}else if([&](){ cout << __LINE__, " (Обучение) Значение value < " << value << " не равно расчетному < " << row["val"] << " << " << row["id"] << endl; return false;  }()){
			}else if([&](){ index = erb(BM0_INDEX, {{"id", row["index_id"]}}); return index.empty(); }()){ cout << __LINE__ << " (Расширение) Основной морф пуст << " << row["id"] << endl;// mpre(row, __LINE__); mpre(index, __LINE__); data(BM0_INDEX);
			}else if([&](){ bm0_index = erb(BM0_INDEX, {{"id", row["bm0-index"]}}); return bm0_index.empty(); }()){ cout << __LINE__ << " (Расширение) Дополнительный морф пуст << " << row["id"] << endl; mpre(row, __LINE__); mpre(bm0_index, __LINE__);
			}else if([&](){ bm0_calc = erb(BM0_CALC, {{"calc_pos_id", row["calc_pos_id"]}, {"calc_val_id", row["calc_val_id"]}}); return bm0_calc.empty(); }()){ cout << __LINE__ << " ОШИБКА определения расчета морфа << " << row["id"] << endl;// mpre(row, __LINE__);
			}else if([&](){ row["val"] = bm0_calc["val"]; return false; }()){ // Установка обновленного значения морфа
			}else if([&](){ bm0_calc_amend = erb(BM0_CALC_AMEND, {{"id", bm0_calc["calc_amend_id"]}}); return false; }()){ // Получение баланса
			}else if([&](){ TMs pos, val, balance, orient, transit, calc;
					if([&](){ pos = erb(BM0_CALC_POS, {{"id", bm0_calc["calc_pos_id"]}}); return pos.empty(); }()){ cout << __LINE__ << " ОШИБКА определения позиции расчета" << endl;
					}else if([&](){ val = erb(BM0_CALC_VAL, {{"id", row["calc_val_id"]}}); return val.empty(); }()){ cout << __LINE__ << " ОШИБКА определения значения расчета" << endl;
					}else if([&](){ calc = erb(BM0_CALC, {{"calc_val_id", row["calc_val_id"]}, {"calc_pos_id", row["calc_pos_id"]}}); return calc.empty(); }()){ cout << __LINE__ << " ОШИБКА выборки расчета перехода";
					}else if([&](){ bm0_calc_orient = erb(BM0_CALC_ORIENT, {{"id", calc["bm0-calc_orient"]}}); return bm0_calc_orient.empty(); }()){ cout << __LINE__ << " ОШИБКА выборки ориентации сигнала выборки расширения << " << row["id"] << endl; mpre(balance, __LINE__, "Баланс перехода");
					}else if("Право" == bm0_calc_orient["name"]){ cout << __LINE__ << " (Расширение) `" << bm0_calc_orient["name"] << "` val:" << val["name"] << " value:" << balance["val"] << " морф << " << row["id"] << endl;
						index_value = bm0_index; index_mate = index; index_value["_field"] = "bm0-index";
					}else if("Лево" == bm0_calc_orient["name"]){ cout << __LINE__ << " (Расширение) `" << bm0_calc_orient["name"] << "` val:" << val["name"] << " value:" << balance["val"] << " морф << " << row["id"] << endl;
						index_value = index; index_mate = bm0_index; index_value["_field"] = "index_id";
					}else{ cout << __LINE__ << " Значение ориентации расширение не определено `" << bm0_calc_orient["name"] << "` << " << row["id"] << endl; mpre(bm0_calc_orient, __LINE__, "Ориентировка расширяемого сигнала");
						mpre(bm0_calc_pos, __LINE__, "Позиция расширяемого морфа");
					} return (index_value.empty() || index_mate.empty());
				}()){ cout << __LINE__ << " ОШИБКА расчета ответственных морфов" << endl; mpre(bm0_itog, __LINE__, "Итоговый морф"); // mpre(row, __LINE__, "Морф"); mpre(bm0_calc_balance, __LINE__, "Баланс"); //mpre(bm0_calc_pos, __LINE__, "Позиция"); mpre(bm0_calc_val, __LINE__, "Значение");
			}else if([&](){ val = (index_value["value"] == "1" ? "0" : "1"); return false; }()){ // Расчет значения морфа
			}else if([&](){ bm0_calc_orient = erb(BM0_CALC_ORIENT, {{"calc_pos_id", index["calc_pos_id"]}, {"bm0-calc_pos", bm0_index["calc_pos_id"]}}); return false; }()){ cout << __LINE__ << " ОШИБКА выборки ориентации структуры" << endl;
//			}else if([&](){ cout << __LINE__ << " (Делегирование) Требования к морфу < " << val << " << " << index_value["id"] << endl; return false; }()){ // Уведомление
//			}else if([&](){ remove = (remove || (bm0_calc_orient["calc_val_id"] == row["calc_val_id"])); return false; }()){ // Рсчет направления корректировки
			}else if([&](){ TMs balance, pos;
					if(remove){ // Запрет уже наложен вышестоящей структурой
					}else if(pos["v0"] == pos["v1"]){ cout << __LINE__ << " Расширение разрешено calc < `" << bm0_calc["name"] << "` << " << row["id"] << endl;// mpre(bm0_calc, __LINE__, "Калькуляция");
//					}else if([&](){ pos = erb(BM0_CALC_POS, {{"bm0-calc", bm0_calc["id"]}}); return !pos.empty(); }()){ cout << __LINE__ << " Расширение разрешено calc < `" << bm0_calc["name"] << "` << " << row["id"] << endl; mpre(bm0_calc, __LINE__, "Калькуляция");
//					}else if(bm0_calc_orient.empty()){ cout << __LINE__ << " Допустимый баланс структура не установлена << " << endl;
//					}else if([&](){ balance = erb(BM0_CALC_BALANCE, {{"calc_val_id", row["calc_val_id"]}, {"val", (value)}}); return balance.empty(); }()){ cout << __LINE__ << " (Запрет расширения) баланс не найден << " << row["id"] << endl;
//					}else if(balance["id"] == bm0_calc_orient["calc_balance_id"]){ cout << __LINE__ << " Допустимый баланс1 << " << row["id"] << endl;
//					}else if(balance["id"] == bm0_calc_orient["bm0-calc_balance"]){ cout << __LINE__ << " Допустимый баланс2 " << row["id"] << endl;
					}else{ cout << __LINE__ << " Расширение запрещено calc < `" << bm0_calc["name"] << "` << " << row["id"] << endl;
						remove = true;
					} return false;
				}()){ // Расчет запрета на расширение уструктуры
			}else if([&](){ PARENT.insert(make_pair(to_string(PARENT.size()), row)); return false; }()){ cout << __LINE__ << " ОШИБКА добавления родителя к списку родительских элементов";
			}else if([&](){ _index = learn(index_value, val, index_mate, PARENT, remove); return false; }()){ // Обучение
			}else if([&](){ // Обновление значений обученного морфа
					if(_index["id"] == index_value["id"]){ cout << __LINE__ << " (Делегирование) Обновление морфа не требуется" << endl;
					}else if(index_value["_field"] == ""){ cout << __LINE__ << " ОШИБКА не задано поле обновления" << endl;
					}else if([&](){ BM0_INDEX[row["id"]] = row = fk("mp_bm0_index", {{"id", row["id"]}}, {}, {{index_value["_field"], _index["id"]}}); return row.empty(); }()){ cout << __LINE__ << " ОШИБКА обновления идентификатора обученного морфа" << endl;
					}else{ cout << __LINE__ << " (Делегирование) обновление морфа " << row["id"] << endl;
					} return false;
				}()){ // Обновление итога
			}else if([&](){ TMs pos; string v; // Устновка в правильные положения морф по их позиции
					if(index_mate["calc_pos_id"] == ""){ cout << __LINE__ << " Парный морф не имеет позицию пропускаем расчет смены парных морфов << " << row["id"] << endl;
					}else if([&](){ pos = erb(BM0_CALC_POS, {{"id", _index["calc_pos_id"]}}); return pos.empty(); }()){ cout << __LINE__ << " Новый морф вернулся без позиции" << endl;// mpre(_index, __LINE__, "Расширяемый морф");
					}else if((index_value["_field"] == "index_id") && (pos["v0"] == pos["v1"])){ cout << __LINE__ << " Положение морфа у родиетля установлено правильно index_id v0==v1 << " << row["id"] << endl;// mpre(pos, __LINE__, "Позиция морфа");
					}else if((index_value["_field"] == "bm0-index") && (pos["v0"] != pos["v1"])){ cout << __LINE__ << " Положение морфа у родиетля установлено правильно bm0-index v0!=v1 << " << row["id"] << endl;// mpre(pos, __LINE__, "Позиция морфа");
					}else if([&](){ BM0_INDEX[row["id"]] = row = fk("mp_bm0_index", {{"id", row["id"]}}, {}, {{"index_id", row["bm0-index"]}, {"bm0-index", row["index_id"]}}); return row.empty(); }()){ cout << __LINE__ << "Ошибка смены полей index_id и bm0-index у морфа" << endl;

//					}else if(index["val"] >= bm0_index["val"]){ cout << __LINE__ << " Фаворит << " << row["id"] << endl;
//					}else if([&](){ index = erb(BM0_INDEX, {{"id", row["index_id"]}}); return index.empty(); }()){ cout << __LINE__ << " (Расширение) Основной морф пуст << " << row["id"] << endl;// mpre(row, __LINE__); mpre(index, __LINE__); data(BM0_INDEX);
//					}else if([&](){ bm0_index = erb(BM0_INDEX, {{"id", row["bm0-index"]}}); return bm0_index.empty(); }()){ cout << __LINE__ << " (Расширение) Дополнительный морф пуст << " << row["id"] << endl; mpre(row, __LINE__); mpre(bm0_index, __LINE__);
					}else{ cout << __LINE__ << " (Коррекция) Установка полярности << " << row["id"] << endl;
//						mpre(index_value, __LINE__, "Вернувшийся морф");
//						mpre(pos, __LINE__, "Парный морф");
					} return false;
				}()){ // Уведомление
			}else if([&](){ index = erb(BM0_INDEX, {{"id", row["index_id"]}}); return index.empty(); }()){ cout << __LINE__ << " (Расширение) Основной морф пуст << " << row["id"] << endl;// mpre(row, __LINE__); mpre(index, __LINE__); data(BM0_INDEX);
			}else if([&](){ bm0_index = erb(BM0_INDEX, {{"id", row["bm0-index"]}}); return bm0_index.empty(); }()){ cout << __LINE__ << " (Расширение) Дополнительный морф пуст << " << row["id"] << endl; mpre(row, __LINE__); mpre(bm0_index, __LINE__);
			}else if([&](){ bm0_calc_val = erb(BM0_CALC_VAL, {{"v0", index.at("val")}, {"v1", bm0_index.at("val")}}); return bm0_calc_val.empty(); }()){ cout << __LINE__ << "ОШИБКА расчета занчения расширяемого морфа" << endl;
			}else if([&](){ row["calc_val_id"] = bm0_calc_val["id"]; return false; }()){ // Установка значения
			}else if([&](){ row["val"] = (bm0_calc = erb(BM0_CALC, {{"calc_pos_id", row["calc_pos_id"]}, {"calc_val_id", row["calc_val_id"]}}))["val"]; return bm0_calc.empty(); }()){ cout << __LINE__ << " ОШИБКА определения расчета морфа << " << row["id"] << endl;// mpre(row, __LINE__);
			}else if(row["val"] == value){ cout << __LINE__ << " (Делегирование) Значение совпало не обновляем << " << row["id"] << endl;
			}else if([&](){ cout << __LINE__ << " Перевертыш << " << row["id"] << endl;
					TMs _bm0_calc_val, _bm0_calc_balance; string val, v0, v1;
					if(value == bm0_calc["val"]){ cout << __LINE__ << " (Балансировка) Значение совпадает с обучением." << endl;
					}else if([&](){ bm0_calc_pos = erb(BM0_CALC_POS, {{"id", bm0_calc_pos["calc_pos_id"]}}); return bm0_calc_pos.empty(); }()){ cout << __LINE__ << " ОШИБКА выборки зеркальной позиции морфа << " << row["id"] << endl;
					}else if([&](){ row["calc_pos_id"] = bm0_calc_pos["id"]; return false; }()){ cout << __LINE__ << " Зеркалим значение позиции" << endl;
					}else if([&](){ bm0_calc = erb(BM0_CALC, {{"calc_pos_id", row["calc_pos_id"]}, {"calc_val_id", row["calc_val_id"]}}); return bm0_calc.empty(); }()){ cout << __LINE__ << " ОШИБКА выборки расчета морфа << " << row["id"] << endl; mpre(row, __LINE__, "Морф");
					}else if([&](){ row["val"] = bm0_calc["val"]; return false; }()){ // Установка значения
					}else if(!mate.empty()){ cout << __LINE__ << " (Балансировка) У морфа есть партнер не балансируем значение << " << row["id"] << endl;
					}else{ cout << __LINE__ << " (Балансировка) Корневой морф балансируем значение < " << val << " << " << row["id"] << endl; // cout << __LINE__ << " Расчет позиции v1:" << index["val"] << " v0:" << bm0_index["val"] << " value:" << value << " << " << row["id"] << endl;
//						mpre(bm0_calc_val, __LINE__, "Старое значение");
//						mpre(_bm0_calc_val, __LINE__, "Обновленное значение");
//						mpre(_bm0_calc_balance, __LINE__, "Обновленная балансировака");
					} return bm0_calc_pos.empty();
				}()){ cout << __LINE__ << " ОШИБКА расчета обновленной позиции морфа" << endl;
			}else{// mpre(row, __LINE__, "Коррекция морфа"); mpre(mate, __LINE__, "Близкий морф");
			} return false;
		}()){ // Перебор всех выщестоящих элементов
	}else if([&](){ // Сохранение изменений морфа
			if(row["id"] == ""){ cout << __LINE__ << " Идентификатор записи меньше либо равен нулю" << endl;
			}else if([&](){ row["value"] = row["val"]; return false; }()){ // Установка расчетного значения
			}else if([&](){ row = fk("mp_bm0_index", {{"id", row["id"]}}, {}, row); return false; }()){ // Сохренение данных в список морфов
			}else{ BM0_INDEX[row["id"]] = row;
			} return false;
		}()){ // Если морф реальный - сохраняем в списке морфов
	}else{ return row; }
}

TMs calc(TMs &row, bool debug = true){// mpre(row, __LINE__);
	TMs index, bm0_index, top, lnk, dan;
	string id, index_id, bm0_index_id, top_id, field;

	if((row.find("_index_id") == row.end()) || (row.find("_bm0-index") == row.end())){ cout << "|"; return row; // Прерывание расчета на недосчитанных морфах
	}else if([&](){ cout << "."; return false; }()){ // Информация о расчете
	}else if([&](){// mpre(row, __LINE__); // Свойства текущего морфа
			index_id = (string) row.find("index_id")->second;
			bm0_index_id = (string) row.find("bm0-index")->second;
			return false;
		}()){ cout << __LINE__ << " Расчета значений морфа" << endl;
	}else if([&](){ string calc_id, v; int v1, v0; TMs bm0_calc_pos, bm0_calc_val, bm0_calc; TMMs::iterator itr;
			if([&](){ index = erb(BM0_INDEX, {{"id", row["index_id"]}}); return false; }()){ cout << __LINE__ << " ОШИБКА расчета старшего морфа" << endl;
			}else if([&](){ bm0_index = erb(BM0_INDEX, {{"id", row["bm0-index"]}}); return false; }()){ cout << __LINE__ << " ОШИБКА расчета младшего морфа" << endl;
			}else if((index.size() == 0) || (bm0_index.size() == 0)){ return false; cout << __LINE__ << " Значения нижестоящих морфов не заданы" << endl;
			}else if([&](){ bm0_calc_pos = erb(BM0_CALC_POS, {{"id", row["calc_pos_id"]}}); return false; }()){ // Получение позиции записи
			}else if([&](){ bm0_calc_val = erb(BM0_CALC_VAL, {{"v1", index["val"]}, {"v0", bm0_index["val"]}}); return false; }()){ // Поиск значений
			}else if([&](){ row["calc_val_id"] = bm0_calc_val["id"]; return false; }()){ // Установка занчения морфа
			}else if([&](){ bm0_calc = erb(BM0_CALC, {{"calc_pos_id", bm0_calc_pos["id"]}, {"calc_val_id", bm0_calc_val["id"]}}); return false; }()){ // Расчет калькуляции
			}else if(bm0_calc.empty()){ // Ветка без веток
			}else if([&](){ TMs orient;
					if([&](){ orient = erb(BM0_CALC_ORIENT, {{"calc_pos_id", index["calc_pos_id"]}, {"bm0-calc_pos", bm0_index["calc_pos_id"]}}); return orient.empty(); }()){// cout << __LINE__ << " ОШИБКА расчета ориентации структуры морфа << " << row["id"] << endl; mpre(index, __LINE__, "Основной морф"); mpre(bm0_index, __LINE__, "Вторичный морф");
					}else if(row["calc_val_id"] != orient["calc_val_id"]){ // ДОпустимое значение
					}else{ cout << ">" << row["id"] << "<"; // cout << __LINE__ << " Исключение структуры << " << row["id"] << endl;
					} return false;
				}()){ cout << __LINE__ << " ОШИБКА Исключение структуры" << endl;
			}else{// mpre(row, __LINE__); // cout << __LINE__ << " Идентификатор расчета << " << id << " = " << calc_id << endl;// mpre(calc, id);
				row["val"] = bm0_calc["val"];
			} return false;
		}()){ cout << __LINE__ << " Расчета значения морфа << " << id << endl;
	}else if([&](){ TMs itog;// mpre(row, __LINE__);
			if([&](){ itog = erb(BM0_ITOG, {{"index_id", row["id"]}}); return itog.empty(); }()){// cout << __LINE__ << " Морф не связан с итогом << " << row["id"] << endl;
			}else if([&](){ BM0_ITOG[itog["id"]] = itog = fk("mp_bm0_itog", {{"id", itog["id"]}}, {}, {{"val", row["val"]}}); return false; }()){ // Сохранение данных в базе
			}else{ cout << row["id"] << "$(" << row["val"] << ")";// mpre(itog, __LINE__);
			} return false;
		}()){ cout << __LINE__ << " Проверка связи с итогом" << endl;
	}else if([&](){ TMMs INDEX, _INDEX; // Получение списка ссылающихся морфов
			if([&](){ INDEX = rb(BM0_INDEX, {{"index_id", row["id"]}}); return false; }()){ // Получение списка морфов по старшей связи
			}else if([&](){ _INDEX = rb(BM0_INDEX, {{"bm0-index", row["id"]}}); return false; }()){ // Получение списка морфов младшей связи
			}else{// data(INDEX, __LINE__); data(_INDEX, __LINE__);
				for(auto itr = INDEX.begin(); itr != INDEX.end(); itr++){
					TMs index = itr->second;// mpre(index);
					lnk.insert(make_pair(index["id"], "index_id"));
				} for(auto itr = _INDEX.begin(); itr != _INDEX.end(); itr++){
					TMs _index = itr->second;// mpre(_index);
					lnk.insert(make_pair(_index["id"], "bm0-index"));
				}// mpre(lnk, __LINE__);
			} return false;
		}()){ return row; cout << __LINE__ << " Получения списка ссылок" << endl;
	}else if([&](){ string changeling; // Переход к ссылающимся элементам
			for(TMs::iterator itr = lnk.begin(); itr != lnk.end(); itr++){
				if([&](){ top_id = itr->first; return false; }()){ cout << __LINE__ << " Идентификатор вышестоящего морфа не найден" << endl;
				}else if([&](){ field = itr->second; return false; }()){ cout << __LINE__ << " ОШИБКА получения поля по которому ссылается вышестоящий элемент";
				}else if([&](){ top = erb(BM0_INDEX, {{"id", top_id}}); return false; }()){ cout << __LINE__ << " Вышестоящего морфа" << endl;
				}else if([&](){ BM0_INDEX.find(top["id"])->second["_"+field] = top["_"+field] = "1"; return false; }()){ cout << __LINE__ << " Отметка о прохождении морфа" << endl;
				}else if([&](){ BM0_INDEX[row["id"]] = row; return false; }()){ // Сохранение расчетов текущего морфа
				}else{// mpre(top, __LINE__);
					calc(top);
				}
			} return false;
		}()){ cout << __LINE__ << " Перебор списка ссылающихся элементов" << endl;
	}else if([&](){ string shift; // Расчет изменений морфа
			if([&](){ shift = (row["value"] == row["val"] ? row["shift"] : bm0_clump["shift"]); return false; }()){ // Расчет значения изменения
			}else{ row["shift"] = shift;
			} return row.empty();
		}()){ cout << __LINE__ << " ОШИБКА обновления изменения морфа" << endl;
	}else if([&](){ row["value"] = row["val"]; return false; }()){ // Установка обучающего сигнала
	}else if([&](){ BM0_INDEX[row["id"]] = fk("mp_bm0_index", {{"id", row["id"]}}, {}, row); return false; }()){ // Сохренение данных в список морфов
	}else{ return row; }
}

TMMs dataset(TMMs TAB, string name, string table, string comment, bool check = true){
	TMMs _TAB; string sql;
	if([&](){ sql = "SELECT * FROM `"+ table+ "`"; return false; }()){ // Запрос на выбору данных
	}else if([&](){ _TAB = tab(sql); return false; }()){ cout << __LINE__ << " ОШИБКА выборки таблицы из базы данных" << endl;
	}else if(!check){ return _TAB;
	}else if(_TAB.size() != TAB.size()){ cout << __LINE__ << " Не совпадение расмера данных в справочниках" << endl; data(_TAB, __LINE__, name, comment);
	}else if([&](){ bool check = false; TMs index, index_, row;
			for(auto itr = _TAB.begin(); itr != _TAB.end(); itr++){
				TMs _index = itr->second;
				if([&](){ index = erb(TAB, _index); row = erb(TAB, {{"id", _index["id"]}}); return index.empty(); }()){ cout << __LINE__ << " Данные не совпадают" << endl; mpre(row, __LINE__); mpre(_index, __LINE__); check = true;
				}else{// acout << __LINE__ << " Данные совпали << " << index["id"] << endl;
//					mpre(index, __LINE__); mpre(_index, __LINE__);
				}
			} return check;
		}()){ cout << __LINE__ << " Даныне в справочниках не совпадают" << endl; data(_TAB, __LINE__, name, comment);
	}else{// cout << __LINE__ << " (Выборка данных) Запрос: " << sql << endl;
	} return TAB;
}

int main(int argc, char **argv){
	string *err = 0;
	TMs ARGV; // Массив обучающих сигналов
	if([&](){ string dbname;
			if([&](){ dbname = "../../.htdb"; return false; }()){ cout << __LINE__ << " Путь до файла базы данных";
			}else if(SQLITE_OK != sqlite3_open(dbname.c_str(), &db)){ cout << __LINE__ << " ОШИБКА открытия базы данных << " << dbname << endl;
			}else if(qw("PRAGMA journal_mode=OFF;")){ cout << __LINE__ << " ОШИБКА установки режима работы БД" << endl;
			}else if(qw("PRAGMA locking_mode=NORMAL;")){ cout << __LINE__ << " ОШИБКА установки режима блокирования БД" << endl;
			}else{ cout << __LINE__ << " (Соединение с БД) Успешное соединение" << endl;
			} return false;
		}()){ // Установка соединения с базой данных
	}else if([&](){ BM0_CALC = dataset(BM0_CALC, "BM0_CALC", "mp_bm0_calc", "Расчеты"); return false; }()){
	}else if([&](){ BM0_CALC_AMEND = dataset(BM0_CALC_AMEND, "BM0_CALC_AMEND", "mp_bm0_calc_amend", "Изменения"); return false; }()){
	}else if([&](){ BM0_CALC_POS = dataset(BM0_CALC_POS, "BM0_CALC_POS", "mp_bm0_calc_pos", "Позиции"); return false; }()){
	}else if([&](){ BM0_CALC_SEQUENCE = dataset(BM0_CALC_SEQUENCE, "BM0_CALC_SEQUENCE", "mp_bm0_calc_sequence", "Распиновка"); return false; }()){
	}else if([&](){ BM0_CALC_BALANCE = dataset(BM0_CALC_BALANCE, "BM0_CALC_BALANCE", "mp_bm0_calc_balance", "Балансировка"); return false; }()){
	}else if([&](){ BM0_CALC_ORIENT = dataset(BM0_CALC_ORIENT, "BM0_CALC_ORIENT", "mp_bm0_calc_orient", "Ориентация"); return false; }()){
	}else if([&](){ BM0_CALC_VAL = dataset(BM0_CALC_VAL, "BM0_CALC_VAL", "mp_bm0_calc_val", "Значения"); return false; }()){
//	}else if([&](){ BM0_CALC_TRANSIT = dataset(BM0_CALC_TRANSIT, "BM0_CALC_TRANSIT", "mp_bm0_calc_transit", "Транзиты"); return false; }()){
/*	}else if([&](){
			for(auto it = BM0_CALC_BALANCE.begin(); it != BM0_CALC_BALANCE.end(); it++){
				TMs balance = it->second; mpre(balance, __LINE__, "Баланс");
				balance = fk("mp_bm0_calc_balance", {{"id", balance["id"]}}, balance, {});
			} return false;
		}()){ // Восстановление таблицы*/
	}else if([&](){ TMs val; string v0, v1, id;
			for(TMMs::iterator itr = BM0_CALC_VAL.begin(); itr != BM0_CALC_VAL.end(); itr++){
				val = itr->second;
				VAL[ atoi(val["v1"].c_str()) ][ atoi(val["v0"].c_str()) ] = val["id"];
			} return false;
		}()){ cout << __LINE__ << " Выборки списка значений" << endl;
	}else if([&](){
			for(TMMs::iterator itr = BM0_CALC.begin(); itr != BM0_CALC.end(); itr++){
				TMs calc = itr->second;
				int calc_pos_id = atoi(calc["calc_pos_id"].c_str());
				int calc_val_id = atoi(calc["calc_val_id"].c_str());
				CALC[ calc_pos_id ][ calc_val_id ] = calc["id"];
			} return false;
		}()){ cout << __LINE__ << " Массив поиска расчета" << endl;
	}else if([&](){ string hide; TMs clump;
			BM0_CLUMP = tab("SELECT * FROM `mp_bm0_clump`");
			for(TMMs::iterator it = BM0_CLUMP.begin(); it != BM0_CLUMP.end(); it++){
				if([&](){ (clump = it->second); return false; }()){ cout << __LINE__ << " ОШИБКА выборки записи созвездия" << endl;
				}else if(clump["hide"] == "1"){// cout << __LINE__ << " Созвездие скрыто << " << row["name"] << endl;
				}else if([&](){ clump = fk("mp_bm0_clump", {{"id", clump["id"]}}, {}, {}); return false; }()){ // Обновление повтора
				}else{ cout << __LINE__ << " Видимое созвездие << " << clump["id"] << " (" << clump["name"] << ")" << endl;
					bm0_clump = clump;
				}
			} return false;
		}()){ cout << __LINE__ << " Скопление" << endl; mpre(bm0_clump, __LINE__);
	}else if([&](){ string sql;
			if([&](){ sql = "DELETE FROM `mp_bm0_itog`;"; qw(sql); return false; }()){// Удаление итога
			}else{ cout << __LINE__ << " Обнуление итогов" << endl;
			} return false;
		}()){ // Обнуление итогов
	}else if([&](){
			BM0_ITOG = tab("SELECT * FROM `mp_bm0_itog` WHERE `clump_id`="+ bm0_clump["id"]);
			for(TMMs::iterator itr = BM0_ITOG.begin(); itr != BM0_ITOG.end(); itr++){
				TMs itog = itr->second;
				INDEX_ITOG.insert(std::make_pair(itog["index_id"], itog));
			} return false;
		}()){ cout << __LINE__ << " Массив поиска итогов" << endl;
	}else if([&](){ string sql;
			if([&](){ sql = "DELETE FROM `mp_bm0_dano`;"; qw(sql); return false; }()){// Удаление итога
			}else{ cout << __LINE__ << " Обнуление дано" << endl;
			} return false;
		}()){ // Обнуление входных значений
	}else if([&](){
			BM0_DANO = tab("SELECT * FROM `mp_bm0_dano` WHERE `clump_id`="+ bm0_clump["id"]);
//			cout << __LINE__ << " Расчет количество входящих дано для обработки: " << BM0_DANO.size() << endl;
			for(TMMs::iterator it = BM0_DANO.begin(); it != BM0_DANO.end(); ++it){
				TMs dano = it->second;// mpre(dano, dano["id"]);
				if(dano["val"] == dano["value"]){ cout << __LINE__ << " Значения дано равны << " << dano["id"] << endl;
				}else if(!qw("UPDATE `mp_bm0_index` SET val='"+ dano["val"]+ "' WHERE dano_id="+ dano["id"])){ cout << __LINE__ << " Обновление морфов итога" << endl;
				}else{ cout << __LINE__ << " Обновление висящих морфов << " << dano["id"] << endl; }
			} return false;
		}()){ cout << __LINE__ << " Обновление значений морфов" << endl;
	}else if([&](){ string sql;
			if([&](){ sql = "DELETE FROM `mp_bm0_index`;"; qw(sql); return false; }()){// Удаление итога
			}else{ cout << __LINE__ << " Обнуление морфов" << endl;
			} return false;
		}()){ // Обнуление входных значений
	}else if([&](){ BM0_INDEX = tab("SELECT * FROM `mp_bm0_index` WHERE clump_id="+ bm0_clump["id"]); return false; }()){ cout << __LINE__ << " Подготовка вспомогательных данных" << endl;
	}else if([&](){ TMs dano; string v, val = "", value = "";
			if(argv[1] != NULL){ cout << __LINE__ << " Расчетные значения argv[1]=" << argv[1] << endl;
			}else{ cout << endl << __LINE__ << " Генерация расчетных значений dano << " << bm0_clump["dano"] << endl;
				for(int i = 0; i < pow(2, atoi(bm0_clump["dano"].c_str())); i++){
					string val = dec2bin(i, atoi(bm0_clump["dano"].c_str()));
					string value = "";
					int count = std::count(val.begin(), val.end(), '1');
					//for(int v = 0; v <= val.size(); v++){
					//	value += (count == v ? "1" : "0");
					//} value += (("01" == val) || ("10" == val) || ("111" == val) || ("0" == val) ? "1" : "0");
					srand(time(NULL)+i);
					value += to_string(rand()%2);
//					value += val.substr(1, 1);
					ARGV.insert(make_pair(val, value));
					cout << __LINE__ << " Перевод значений " << i << " в расчет " << val << " обучение " << value << endl;
				} mpre(ARGV, __LINE__); cin.get();
			} return false;
		}()){ cout << __LINE__ << " Установка обучающих данных в итоги"; exit(0);
	}else if([&](){ qw("DELETE FROM `mp_bm0_clump_shot`"); return false; }()){ // Очистка истории тестов
	}else if([&](){ //Расчет
			TMs index, bm0_calc, bm0_calc_orient, _index; int tchange, change;
			do{ int nn = 0; tchange = 0;

		 		if([&](){ BM0_ITOG = tab("SELECT * FROM `mp_bm0_itog` WHERE `clump_id`="+ bm0_clump["id"]); return false; }()){ // Итоги
				}else if([&](){ BM0_DANO = tab("SELECT * FROM `mp_bm0_dano` WHERE `clump_id`="+ bm0_clump["id"]); return false; }()){ // Дано
				}else if([&](){ BM0_INDEX = tab("SELECT * FROM `mp_bm0_index` WHERE clump_id="+ bm0_clump["id"]); return false; }()){ // Морфы
				}else if([&](){ ARGV = {};
						for(int i = 0; i < pow(2, atoi(bm0_clump["dano"].c_str())); i++){
							string value = "";
							string val = dec2bin(i, atoi(bm0_clump["dano"].c_str()));
							srand(time(NULL)+i);
							value += to_string(rand()%2);
							ARGV.insert(make_pair(val, value));
						} return false;
					}()){ cout << __LINE__ << " ОШИБКА переформатирования обучающих данных" << endl;
				}else{// mpre(ARGV, __LINE__, "Переформатирование данных"); cin.get();
				}

				do{ change = 0;
					TMs SORT = {}, TMP = ARGV;// mpre(TEMP, __LINE__);
					if(true){ //Расчет//Прямая последовательность
						for(int i = 0; i < ARGV.size(); i++){
							auto itr = ARGV.begin();
							advance(itr, i);
							
							string val = itr->first;
							string value = itr->second;

							SORT.insert(make_pair(dec2bin(i, atoi(bm0_clump["dano"].c_str())), val));
							TMP.erase(val);
						} cout << __LINE__ << " Сортирвока: прямая" << endl;
					}else{ //Расчет//Случайная последовательность
						for(int i = 0; i < ARGV.size(); i++){
							srand(time(NULL)+i);
							int offset = rand()%TMP.size();
	
							auto itr = TMP.begin();
							advance(itr, offset);
					
							string val = itr->first;
							string value = itr->second;
						
							SORT.insert(make_pair(dec2bin(i, atoi(bm0_clump["dano"].c_str())), val));
							TMP.erase(val);
						} cout << __LINE__ << " Сортирвока: случайная" << endl;
					}// mpre(SORT, __LINE__, "Сортировка"); mpre(ARGV, __LINE__, "Список аргументов");

					for(auto iter = SORT.begin(); iter != SORT.end(); iter++){
						string key = iter->first;
						string val = iter->second;
						string value = ARGV[val];

						if([&](){ cout << endl << endl <<  __LINE__ << " n:" << nn << " val: " << val << " value: " << value << endl; return false; }()){ // Уведомление
//						}else if(qw("BEGIN;")){ qw("ROLLBACK;"); /*change = true;*/ cout << __LINE__ << " (Транзакция) ОШИБКА запуска" << endl; sleep(1); 
//						}else if([&](){ cout << __LINE__ << " Уведомление val:" << val << " value:" << value << endl; return false; }()){ // Уведомление
						}else if([&](){ string shift; // Обновления значения обучения у скопления
								if([&](){ shift = to_string(atoi(bm0_clump["shift"].c_str())+1); return false; }()){ cout << __LINE__ << " ОШИБКА расчета значения изменения";
								}else if([&](){ bm0_clump = fk("mp_bm0_clump", {{"id", bm0_clump["id"]}}, {}, {{"shift", shift}}); return false; }()){ // Сохранение инкремента в базу
								}else{ cout << __LINE__ << " Увеличение значения изменения" << endl;
								} return false;
							}()){ // Инкремент номера обучения
						}else if([&](){ TMs dano; string shift, c; // Создание необзходимых дано если нет и установка значения обучения и признака изменения
								for(int i = 0; i < val.length(); i++){
									if([&](){ c = string(1, val.at(i)); return false; }()){ // Получения значения
									}else if([&](){ dano = fk("mp_bm0_dano", {{"clump_id", bm0_clump["id"]}, {"name", to_string(i)}}, {{"time", to_string(time(NULL))}, {"shift", bm0_clump["shift"]}}, {}); return dano.empty(); }()){ cout << __LINE__ << " ОШИБКА выборки дано по значению" << endl;
									}else if([&](){ shift = (dano["val"] == c ? dano["shift"] : bm0_clump["shift"]); return false; }()){ // Расчет значения изменения
									}else if([&](){ BM0_DANO[dano["id"]] = dano = fk("mp_bm0_dano", {{"id", dano["id"]}}, {}, {{"val", c}, {"shift", shift}}); return dano.empty(); }()){ cout << __LINE__ << " ОШИБКА установки значений дано" << endl;
									}else{ cout << __LINE__ << " (Дано) Установка значений " << c << " << " << dano["id"] << endl;
									}
								} return false;
							}()){ // Установка обучающих значений
						}else if([&](){ TMs itog; string shift, c;
								for(int i = 0; i < value.length(); i++){
									if([&](){ c = string(1, value.at(i)); return false; }()){
									}else if([&](){ itog = fk("mp_bm0_itog", {{"clump_id", bm0_clump["id"]}, {"name", to_string(i)}}, {{"time", to_string(time(NULL))}}, {}); return itog.empty(); }()){ cout << __LINE__ << " ОШИБКА выборки итога" << endl;
									}else if([&](){ shift = (itog["value"] == c ? itog["shift"] : bm0_clump["shift"]); return false; }()){ // Расчет значения изменения
									}else if([&](){ BM0_ITOG[itog["id"]] = itog = fk("mp_bm0_itog", {{"id", itog["id"]}}, {}, {{"value", c}, {"shift", shift}}); return itog.empty(); }()){ cout << __LINE__ << " ОШИБКА обновления итогового значения" << endl;
									}else{ cout << __LINE__ << " (Итоги) Установка значений " << c << " << " << itog["name"] << endl;// mpre(itog, __LINE__);// mpre(bm0_clump, __LINE__);
									}
								} return false;
							}()){ // Установка итоговых значений
						}else if([&](){ TMMs INDEX; TMs _index;
								for(TMMs::iterator it = BM0_DANO.begin(); it != BM0_DANO.end(); it++){ // Перебераем изменившиеся морфы
									TMs dano = it->second;
									cout << endl << __LINE__ << " Расчет дано << " << dano["id"] << " скопление:" << bm0_clump["id"] << endl;// data(_INDEX, __LINE__);

									if([&](){ INDEX = rb(BM0_INDEX, {{"clump_id", bm0_clump["id"]}, {"dano_id", dano["id"]}, {"index_id", ""}, {"bm0-index", ""}}); return !INDEX.empty(); }()){// cout << __LINE__ << " Добавление к дано << " << dano["id"] << " первоначального морфа" << endl;
									}else if([&](){ index = fk("mp_bm0_index", {{"clump_id", bm0_clump["id"]}, {"dano_id", dano["id"]}}, {{"time", to_string(time(NULL))}, {"form", "{"+ dano["id"]+ "}"}, {"val", dano["val"]}}, {{"val", dano["val"]}}); return index.empty(); }()){ cout << __LINE__ << " ОШИБКА добавления нового элемента к дано" << endl;
									}else if([&](){ INDEX.insert(make_pair(index["id"], index)); return INDEX.empty(); }()){ cout << __LINE__ << " ОШИБКА добавления элемента к списку обработки" << endl;
									}else if([&](){ BM0_INDEX.insert(make_pair(index["id"], index)); return BM0_INDEX.empty(); }()){ cout << __LINE__ << " ОШИБКА добавления элемента к общему списку" << endl;
									}else{ cout << __LINE__ << " Создан новый морф связанный с дано << " << index["id"] << endl;
//										mpre(INDEX, __LINE__, "Список морфов дано");
									}

									for(auto it = INDEX.begin(); it != INDEX.end(); it++){
										TMs index = it->second;// mpre(index, __LINE__);
										if([&](){ BM0_INDEX[index["id"]] = index = fk("mp_bm0_index", {{"id", index["id"]}}, {}, {{"val", dano["val"]}}); return false; }()){ // Установка значения морфу
										}else if([&](){ index["_index_id"] = index["_bm0-index"] = "1"; return false; }()){ // Значения для перебора
//										}else if([&](){ mpre(index, __LINE__, "Обучаемый"); return false; }()){
										}else if([&](){ _index = calc(index); return false; }()){ // Расчет
										}else if([&](){ BM0_INDEX[_index["id"]] = _index; return _index.empty(); }()){ cout << __LINE__ << " ОШИБКА результат функции расчета" << endl;
										}else if([&](){ BM0_DANO[dano["id"]] = dano = fk("mp_bm0_dano", {{"id", dano["id"]}}, {}, {{"value", _index["val"]}}); return dano.empty(); }()){ cout << __LINE__ << " ОШИБКА установки расчетных значений дано";
										}else{// cout << __LINE__ << " (Расчет) значений " << _index["id"] << endl;
//											mpre(_index, __LINE__, "Зна")
//											mpre(BM0_INDEX, __LINE__, "Список морфов"); cin.get();
										}
									}// qw("UPDATE mp_bm0_dano SET value=val WHERE id="+ dano["id"]);
								} return false;
							}()){ // Расчет значения морфов
						}else if([&](){
								cout << endl << __LINE__ << " Список расчетов: ";
								for(auto itr = BM0_ITOG.begin(); itr != BM0_ITOG.end(); itr++){ // Перебераем изменившиеся морфы
									TMs itog = itr->second;
									cout << itog["val"];
								} cout << endl;
								return false;
							}()){ cin.get();// Отображение списка расчетов
						}else if([&](){
								for(TMMs::iterator it = BM0_ITOG.begin(); it != BM0_ITOG.end(); it++){
									bm0_itog = it->second;

									if([&](){ dano_index = erb(BM0_INDEX, {{"id", bm0_itog["index_id"]}}); return false; }()){ cout << __LINE__ << " Выборка итогового морфа" << endl;
									}else if([&](){ TMMs INDEX;// mpre(dano_index, __LINE__); mpre(bm0_itog, __LINE__);
											if(!dano_index.empty()){// cout << __LINE__ << " Ссылка морфа не пустая" << endl;
											}else if([&](){ INDEX = rb(BM0_INDEX, {{"index_id", ""}, {"bm0-index", ""}}); return INDEX.empty(); }()){ cout << __LINE__ << " (Расширение) ОШИБКА выборка массива подходящих морфов" << endl;
											}else if([&](){// mpre(INDEX, __LINE__);
													for(auto itr = INDEX.rbegin(); itr != INDEX.rend(); itr++){
														TMs _index = itr->second;// mpre(_index, __LINE__);
														if(dano_index.empty()){ dano_index = _index;
//														}else if(atoi(dano_index["retry"].c_str()) < atoi(_index["retry"].c_str())){ dano_index = _index; // Текущий морф меньше выборанного
														}else if(value != _index["val"]){ // Значения не совпадают
														}else{ dano_index = _index; }
													} cout << __LINE__ << " (Расширение) Результат расчета подходящих value:" << value << " << " << dano_index["id"] << endl;// mpre(bm0_index, __LINE__);// cin.get();
													return dano_index.empty();
												}()){ cout << __LINE__ << " (Связь с итогом) ОШИБКА выборки подходящего морфа" << endl; exit(0);
//											}else if([&](){ index = BM0_INDEX.begin()->second; return false; }()){ // Первый подходящий
											}else if([&](){ BM0_ITOG[bm0_itog["id"]] = bm0_itog = fk("mp_bm0_itog", {{"id", bm0_itog["id"]}}, {}, {{"index_id", dano_index["id"]}, {"val", dano_index["val"]}}); return false; }()){ // Обновление морфа итога
											}else{// mpre(bm0_itog, __LINE__); mpre(dano_index, __LINE__); data(INDEX);
											} return false;
										}()){ // Если связанный морф пуст - добавляем
									}else if([&](){
											if(bm0_itog["value"] == dano_index["val"]){ cout << __LINE__ << " (Предсказания) верны << " << bm0_itog["id"] << endl;
											}else{ cout << __LINE__ << " (Предсказания) не верны << " << bm0_itog["id"] << endl;
												++change; ++tchange;
											} return false;
										}()){ // Сохранение попадания расчета
									}else if([&](){ _index = learn(dano_index, bm0_itog["value"], {}, {}); return _index.empty(); }()){ cout << __LINE__ << " ОШИБКА результата расчета морфа" << endl;
									}else if(_index.empty()){ cout << __LINE__ << " Обучение вернуло пустой морф" << endl;
									}else if([&](){// TMs select, insert, update;// mpre(_index, __LINE__);
											if([&](){ BM0_ITOG[bm0_itog["id"]] = bm0_itog = fk("mp_bm0_itog", {{"id", bm0_itog["id"]}}, {}, {{"index_id", _index["id"]}, {"val", _index["val"]}}); return false; }()){ cout << __LINE__ << " Обновление значение итога у морфа" << endl;
											}else{ cout << __LINE__ << " Обновляем значение морфа `index_id` у итога << " << bm0_itog["id"] << endl;
											} return false;
										}()){ cout << __LINE__ << " Установка морфа итога если вернут другой";
									}else if([&](){// mpre(ARGV, __LINE__);
											cout << endl << __LINE__  << " Список обучения value:" << value << " << ";
											for(auto itr = BM0_ITOG.begin(); itr != BM0_ITOG.end(); itr++){ // Перебераем изменившиеся морфы
												TMs row = itr->second;
												cout << row["val"];
											} cout << endl;
											return false;
										}()){ cout << __LINE__ << " Отображение значений итогов" << endl;
									}else{ // mpre(index, __LINE__); mpre(_index, __LINE__);
									}
								} return false;
							}()){
//						}else if(qw("COMMIT;")){ qw("ROLLBACK;"); change += 1; cout << __LINE__ << " (Транзакция) ОШИБКА закрытия" << endl; sleep(1);
//							if([&](){ BM0_INDEX = tab("SELECT * FROM `mp_bm0_index` WHERE clump_id="+ bm0_clump["id"]); return BM0_INDEX.empty(); }()){ cout << __LINE__ << " ОШИБКА загрузки списка морфов из базы" << endl;
//							}else if([&](){ BM0_DANO = tab("SELECT * FROM `mp_bm0_dano` WHERE `clump_id`="+ bm0_clump["id"]); return BM0_DANO.empty(); }()){ cout << __LINE__ << " ОШИБКА загрузки исходных данных из базы" << endl;
//							}else if([&](){ BM0_ITOG = tab("SELECT * FROM `mp_bm0_itog` WHERE `clump_id`="+ bm0_clump["id"]); return BM0_ITOG.empty(); }()){ cout << __LINE__ << " ОШИБКА загрузки итоговых данных из базы" << endl;
//							}else{ cout << __LINE__ << " Обновляем данные из базы данных"; }
						}else if([&](){ TMs shot, SORT; // Сохранение результата проверки в базуa
//								mpre(ARGV, __LINE__, "Список аргументов");
								if(true){ // Пропускаем
								}else if([&](){ TMs bm0_clump_history, SORT;
										if([&](){ bm0_clump_history = fk("mp_bm0_clump_history", {{"clump_id", bm0_clump["id"]}, {"dano", val}}, {{"dano", val}, {"learn", ""}}, {{"dano", val}, {"learn", ""}}); return bm0_clump_history.empty(); }()){cout << __LINE__ << " ОШИБКА сохранения в базу текущего обучения" << endl;
										}else if([&](){ bm0_clump = fk("mp_bm0_clump", {{"id", bm0_clump["id"]}}, {}, {{"clump_history_id", bm0_clump_history["id"]}}); return bm0_clump.empty(); }()){ cout << __LINE__ << " ОШИБКА сохранения ситории в скоплении" << endl;
										}else{
										} return bm0_clump_history.empty();
									}()){ cout << __LINE__ << " ОШИБКА сохранение в скопление информации о текущем обучении";
								}else if([&](){ int i; // Список проверочных сигналов
										for(i = 0; i < ARGV.size(); i++){
											auto it = ARGV.begin();
											advance(it, i);
											string key = it->first;
											SORT.insert(make_pair(dec2bin(i, atoi(bm0_clump["dano"].c_str())), key));
										} SORT.insert(make_pair("_", val));

//										mpre(SORT, __LINE__, "Сортировка списка"); mpre(ARGV, __LINE__, "Список аргументов");
										for(auto it = SORT.begin(); it != SORT.end(); it++){
											string sort = it->second;
//											cout << __LINE__ << " Ключ сортировки:" << sort << endl;
											string val2 = ARGV.find(sort)->first;
											string value2 = ARGV.find(sort)->second;
											if([&](){
													for(int i = 0; i < val2.length(); i++){
														string c = string(1, val2.at(i)); TMs dano;
														if([&](){ dano = erb(BM0_DANO, {{"clump_id", bm0_clump["id"]}, {"name", to_string(i)}}); return false; }()){ // Получение истодных данных
														}else if([&](){ dano = fk("mp_bm0_dano", {{"clump_id", bm0_clump["id"]}, {"name", to_string(i)}}, {{"time", to_string(time(NULL))}, {"val", c}}, {{"val", c}}); return false; }()){ // Обновление морфа
														}else{// cout << __LINE__ << " (Дано) Установка значений " << c << " << " << dano["id"] << endl;
															BM0_DANO[dano["id"]] = dano;
														}
													} return false;
												}()){ // Установка итоговых значений
											}else if([&](){ TMMs INDEX; TMs _index;
													for(TMMs::iterator it = BM0_DANO.begin(); it != BM0_DANO.end(); it++){ // Перебераем изменившиеся морфы
															TMs dano = it->second;
//														cout << endl << __LINE__  << " dano["<< dano["name"] << "]=" << dano["val"] << " => ";
	
														if([&](){ INDEX = rb(BM0_INDEX, {{"clump_id", bm0_clump["id"]}, {"dano_id", dano["id"]}, {"index_id", ""}, {"bm0-index", ""}}); return !INDEX.empty(); }()){// cout << __LINE__ << " Добавление к дано << " << dano["id"] << " первоначального морфа" << endl;
														}else if([&](){ index = fk("mp_bm0_index", {{"clump_id", bm0_clump["id"]}, {"dano_id", dano["id"]}}, {{"time", to_string(time(NULL))}, {"form", "{"+ dano["id"]+ "}"}, {"val", dano["val"]}}, {{"val", dano["val"]}}); return index.empty(); }()){ cout << __LINE__ << " ОШИБКА добавления нового элемента к дано" << endl;
														}else if([&](){ INDEX.insert(make_pair(index["id"], index)); return INDEX.empty(); }()){ cout << __LINE__ << " ОШИБКА добавления элемента к списку обработки" << endl;
														}else if([&](){ BM0_INDEX.insert(make_pair(index["id"], index)); return BM0_INDEX.empty(); }()){ cout << __LINE__ << " ОШИБКА добавления элемента к общему списку" << endl;
														}else{ cout << __LINE__ << " Сохдан новый морф связанный с дано << " << index["id"] << endl;
														}
	
														for(TMMs::iterator it = INDEX.begin(); it != INDEX.end(); it++){
															TMs index = it->second;// mpre(index, __LINE__);
															if([&](){ index = fk("mp_bm0_index", {{"id", index["id"]}}, {}, {{"val", dano["val"]}}); return false; }()){ // Установка значения морфу
															}else if([&](){ BM0_INDEX[index["id"]] = index; return false; }()){ // Сохранение значения в список
															}else if([&](){ index["_index_id"] = index["_bm0-index"] = "1"; return false; }()){ // Значения для перебора
															}else if([&](){ _index = calc(index); return false; }()){ // Расчет
															}else if([&](){ BM0_DANO[dano["id"]] = dano = fk("mp_bm0_dano", {{"id", dano["id"]}}, {}, {{"value", _index["val"]}}); return dano.empty(); }()){ cout << __LINE__ << " ОШИБКА установки расчетных значений дано";
															}else{// cout << __LINE__ << " (Расчет) значений " << _index["id"] << endl;
															}
														}// qw("UPDATE mp_bm0_dano SET value=val WHERE id="+ dano["id"]);
													} return false;
												}()){ // Расчет значения морфов
											}else if([&](){ string result;
//													cout << endl << __LINE__ << " Список расчетов: ";
													for(TMMs::iterator itr = BM0_ITOG.begin(); itr != BM0_ITOG.end(); itr++){ // Перебераем изменившиеся морфы
														TMs itog = itr->second;
														result += itog["val"];
//														HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
														itog = fk("mp_bm0_itog", {{"id", itog["id"]}}, {}, {{"value", itog["val"]}});
														string color = (ARGV[val2] == itog["value"] ? "\033[1;32m" : "\033[1;31m");
														string color2 = (val2 == val ? "\033[1;33m" : "");
														cout << __LINE__ << " [" << color2 << val2 << "\033[0m]=" << ARGV[val2] << " " << color << itog["value"] << "\033[0m" << endl;
//														cout << itog["val"];
													}// cout << endl;
													TMs bm0_clump_history = fk("mp_bm0_clump_history", {{"clump_id", bm0_clump["id"]}, {"dano", val}}, {{"calc", value}, {"val", result}}, {{"calc", value}, {"val", result}});
													return bm0_clump_history.empty();
												}()){ cout << __LINE__ << " Ошибка сохранения результатов проверки расчета в базу" << endl;
											}else{
											}
										} return false;
									}()){ // Проверка всех значений
								}else if([&](){ BM0_CALC = dataset(BM0_CALC, "BM0_CALC", "mp_bm0_calc", "Расчеты"); return false; }()){
								}else if([&](){ srand(time(NULL)); return true; }()){ // Инициализация счетчика
/*								}else if([&](){ // Тестирование
										for(auto itr = BM0_CALC.begin(); itr != BM0_CALC.end(); itr++){
											TMs bm0_calc = itr->second;
											shot = fk("mp_bm0_clump_shot", {}, {{"time", to_string(time(NULL))}, {"clump_id", bm0_clump["id"]}, {"calc_id", bm0_calc["id"]}, {"val", to_string(change)}, {"bm0-calc_orient", bm0_calc["bm0-calc_orient"]}, {"size", to_string(change)}, {"length", to_string(BM0_INDEX.size())}}, {});
										}
										for(auto itr = BM0_CALC.begin(); itr != BM0_CALC.end(); itr++){
											TMs bm0_calc = itr->second;
											int bm0_calc_orient_id = rand()%2*2+2;
											bm0_calc_orient = erb(BM0_CALC_ORIENT, {{"id", to_string(bm0_calc_orient_id)}});
											BM0_CALC[bm0_calc["id"]]["bm0-calc_orient"] = bm0_calc_orient["id"];
										} return false;
									}()){ // Сохранение результатов*/
								}else{
								} return false;
							}()){ cout << __LINE__ << " ОШИБКА сохранения результатов тестов";
/*						}else if([&](){
								for(auto it = BM0_INDEX.begin(); it != BM0_INDEX.end(); it++){
									TMs index = it->second;
									mpre(index, __LINE__, "Морф");
								} cin.get(); return false;
							}()){*/
/*						}else if([&](){ TMs pos;
								if([&](){ pos = erb(BM0_CALC_POS, {{"calc_id", bm0_calc["id"]}}); return pos.empty(); }()){// cout << __LINE__ << " Не исключительная позиция" << endl;// mpre(bm0_calc, __LINE__, "Позиция морфа"); 
									string n = (row["_calc"].length() ? row["_calc"] : "0"); row["_calc"] = to_string(atoi(n.c_str())-1);
								}else{// cout << __LINE__ << " Исключительная позиция" << endl; mpre(bm0_calc, __LINE__, "Позиция морфа");
									string n = (row["_calc"].length() ? row["_calc"] : "0"); row["_calc"] = to_string(atoi(n.c_str())+1);
								} BM0_INDEX[row["id"]] = row; mpre(row, __LINE__, "Расчет исключительности"); cin.get(); return false;
							}()){ // Анализ исключительности*/
						}else if([&](){
								if(false){ // Задержка при выполнении программы
								}else if(true){ return false; // Выход без ожидаения нажатия клавиши
								}else if(cin.get()){ // Ожидаем нажатия клавиши
								} return false;
							}()){ // Задержка
						}else{// cout << __LINE__ << " Автоинкремент скопления retry:" << retry << " << " << bm0_clump["id"] << endl;
						}
					} // Полный перебор списка обучающей выборки
//				}while(true);
				}while((++nn < 20) && (change > 0));

				TMs shot = fk("mp_bm0_clump_shot", {}, {{"clump_id", bm0_clump["id"]}, {"nn", to_string(nn)}, {"size", to_string(BM0_INDEX.size())}, {"change", to_string(tchange)}}, {});
				cout << __LINE__ << " Обучение закончено. Повторов:" << nn << " Изменений:" << tchange << endl;
				mpre(ARGV, __LINE__, "Список сигналов");
				sleep(1);
			}while(qw("DELETE FROM `mp_bm0_index`") || true);

			return false;
		}()){ cout << __LINE__ << " Перебор итоговых значений" << endl;
	}else{
		sqlite3_finalize(stmt);
		sqlite3_close(db);
	} return 0;
}
