typedef std::map<string, string> TMs;
typedef std::map<string, TMs> TMMs;

/*bool data(string tab, TMMs TAB){
	string sepor;
	cout << __LINE__ << " " << tab << " = ";
	cout << "{" << endl;
	for(TMMs::iterator itr = TAB.begin(); itr != TAB.end(); itr++){
		string id = itr->first;
		TMs row = itr->second;
		cout << "\t{\""+ id+ "\", {";
		for(TMs::iterator it = row.begin(); it != row.end(); it++){
			string key = it->first;
			string val = it->second;
			sepor = (it == row.begin() ? "" : ", ");
			cout << sepor+ "{\""+ key+ "\", \""+ val+ "\"}";
		} cout << "}}," << endl;
	} cout << "}" <<  endl;
}*/


TMMs BM0_CALC = { // Расчеты
	{"1", {{"bm0-calc_orient", "2"}, {"calc_amend_id", "4"}, {"calc_orient_id", "4"}, {"calc_pos_id", "1"}, {"calc_val_id", "1"}, {"id", "1"}, {"name", "0000"}, {"time", "1504896122"}, {"val", "0"}, {"value", "0"}}},
	{"10", {{"bm0-calc_orient", "2"}, {"calc_amend_id", "2"}, {"calc_orient_id", "2"}, {"calc_pos_id", "3"}, {"calc_val_id", "2"}, {"id", "10"}, {"name", "1001"}, {"time", "1504896321"}, {"val", "0"}, {"value", "9"}}},
	{"11", {{"bm0-calc_orient", "2"}, {"calc_amend_id", "3"}, {"calc_orient_id", "3"}, {"calc_pos_id", "3"}, {"calc_val_id", "3"}, {"id", "11"}, {"name", "1010"}, {"time", "1504896341"}, {"val", "0"}, {"value", "10"}}},
	{"12", {{"bm0-calc_orient", "2"}, {"calc_amend_id", "4"}, {"calc_orient_id", "2"}, {"calc_pos_id", "3"}, {"calc_val_id", "4"}, {"id", "12"}, {"name", "1011"}, {"time", "1504896357"}, {"val", "0"}, {"value", "11"}}},
	{"13", {{"bm0-calc_orient", "4"}, {"calc_amend_id", "4"}, {"calc_orient_id", "3"}, {"calc_pos_id", "4"}, {"calc_val_id", "1"}, {"id", "13"}, {"name", "1100"}, {"time", "1504896372"}, {"val", "1"}, {"value", "12"}}},
	{"14", {{"bm0-calc_orient", "4"}, {"calc_amend_id", "3"}, {"calc_orient_id", "2"}, {"calc_pos_id", "4"}, {"calc_val_id", "2"}, {"id", "14"}, {"name", "1101"}, {"time", "1504896390"}, {"val", "1"}, {"value", "13"}}},
	{"15", {{"bm0-calc_orient", "4"}, {"calc_amend_id", "2"}, {"calc_orient_id", "2"}, {"calc_pos_id", "4"}, {"calc_val_id", "3"}, {"id", "15"}, {"name", "1110"}, {"time", "1504896405"}, {"val", "1"}, {"value", "14"}}},
	{"16", {{"bm0-calc_orient", "4"}, {"calc_amend_id", "3"}, {"calc_orient_id", "2"}, {"calc_pos_id", "4"}, {"calc_val_id", "4"}, {"id", "16"}, {"name", "1111"}, {"time", "1504896422"}, {"val", "0"}, {"value", "15"}}},
	{"2", {{"bm0-calc_orient", "2"}, {"calc_amend_id", "3"}, {"calc_orient_id", "4"}, {"calc_pos_id", "1"}, {"calc_val_id", "2"}, {"id", "2"}, {"name", "0001"}, {"time", "1504896150"}, {"val", "0"}, {"value", "1"}}},
	{"3", {{"bm0-calc_orient", "2"}, {"calc_amend_id", "2"}, {"calc_orient_id", "4"}, {"calc_pos_id", "1"}, {"calc_val_id", "3"}, {"id", "3"}, {"name", "0010"}, {"time", "1504896160"}, {"val", "0"}, {"value", "2"}}},
	{"4", {{"bm0-calc_orient", "2"}, {"calc_amend_id", "3"}, {"calc_orient_id", "1"}, {"calc_pos_id", "1"}, {"calc_val_id", "4"}, {"id", "4"}, {"name", "0011"}, {"time", "1504896221"}, {"val", "1"}, {"value", "3"}}},
	{"5", {{"bm0-calc_orient", "4"}, {"calc_amend_id", "3"}, {"calc_orient_id", "4"}, {"calc_pos_id", "2"}, {"calc_val_id", "1"}, {"id", "5"}, {"name", "0100"}, {"time", "1504896240"}, {"val", "0"}, {"value", "4"}}},
	{"6", {{"bm0-calc_orient", "4"}, {"calc_amend_id", "2"}, {"calc_orient_id", "1"}, {"calc_pos_id", "2"}, {"calc_val_id", "2"}, {"id", "6"}, {"name", "0101"}, {"time", "1504896261"}, {"val", "1"}, {"value", "5"}}},
	{"7", {{"bm0-calc_orient", "4"}, {"calc_amend_id", "3"}, {"calc_orient_id", "4"}, {"calc_pos_id", "2"}, {"calc_val_id", "3"}, {"id", "7"}, {"name", "0110"}, {"time", "1504896277"}, {"val", "1"}, {"value", "6"}}},
	{"8", {{"bm0-calc_orient", "4"}, {"calc_amend_id", "4"}, {"calc_orient_id", "2"}, {"calc_pos_id", "2"}, {"calc_val_id", "4"}, {"id", "8"}, {"name", "0111"}, {"time", "1504896292"}, {"val", "1"}, {"value", "7"}}},
	{"9", {{"bm0-calc_orient", "2"}, {"calc_amend_id", "3"}, {"calc_orient_id", "4"}, {"calc_pos_id", "3"}, {"calc_val_id", "1"}, {"id", "9"}, {"name", "1000"}, {"time", "1504896307"}, {"val", "1"}, {"value", "8"}}},
};

/*TMMs BM0_CALC_TRANSIT = { // Транзиты
	{"1", {{"bm0-calc_orient", "4"}, {"calc_orient_id", "4"}, {"calc_val_id", "1"}, {"id", "1"}, {"time", "1517521131"}, {"val", "1"}}},
	{"2", {{"bm0-calc_orient", "2"}, {"calc_orient_id", "4"}, {"calc_val_id", "4"}, {"id", "2"}, {"time", "1517521164"}, {"val", "1"}}},
	{"3", {{"bm0-calc_orient", "2"}, {"calc_orient_id", "2"}, {"calc_val_id", "4"}, {"id", "3"}, {"time", "1517521656"}, {"val", "0"}}},
	{"4", {{"bm0-calc_orient", "4"}, {"calc_orient_id", "2"}, {"calc_val_id", "1"}, {"id", "4"}, {"time", "1517521766"}, {"val", "0"}}},
};*/

/*TMMs BM0_CALC_TRANSIT = { // Переключения
	{"1", {{"bm0-calc_orient", "2"}, {"calc_orient_id", "1"}, {"calc_val_id", "1"}, {"id", "1"}, {"time", "1517276705"}}},
	{"2", {{"bm0-calc_orient", "4"}, {"calc_orient_id", "1"}, {"calc_val_id", "4"}, {"id", "2"}, {"time", "1517276738"}}},
	{"3", {{"bm0-calc_orient", "2"}, {"calc_orient_id", "4"}, {"calc_val_id", "2"}, {"id", "3"}, {"time", "1517276789"}}},
	{"4", {{"bm0-calc_orient", "4"}, {"calc_orient_id", "4"}, {"calc_val_id", "3"}, {"id", "4"}, {"time", "1517276813"}}},
	{"5", {{"bm0-calc_orient", "4"}, {"calc_orient_id", "2"}, {"calc_val_id", "3"}, {"id", "5"}, {"time", "1517276833"}}},
	{"6", {{"bm0-calc_orient", "2"}, {"calc_orient_id", "2"}, {"calc_val_id", "2"}, {"id", "6"}, {"time", "1517276855"}}},
	{"7", {{"bm0-calc_orient", "2"}, {"calc_orient_id", "3"}, {"calc_val_id", "4"}, {"id", "7"}, {"time", "1517276870"}}},
	{"8", {{"bm0-calc_orient", "4"}, {"calc_orient_id", "3"}, {"calc_val_id", "1"}, {"id", "8"}, {"time", "1517276890"}}},
};*/

TMMs BM0_CALC_POS = { // Позиции
	{"1", {{"bm0-calc", "4"}, {"bm0-calc_orient", "1"}, {"calc_id", "2"}, {"calc_orient_id", "4"}, {"calc_pos_id", "4"}, {"description", "И"}, {"form", "({v1}&{v0})"}, {"id", "1"}, {"name", "00"}, {"time", "1504459453"}, {"v0", "0"}, {"v1", "0"}, {"value", "0"}}},
	{"2", {{"bm0-calc", "5"}, {"bm0-calc_orient", "1"}, {"calc_id", "8"}, {"calc_orient_id", "2"}, {"calc_pos_id", "3"}, {"description", "ИЛИ"}, {"form", "({v1}|{v0})"}, {"id", "2"}, {"name", "01"}, {"time", "1504459481"}, {"v0", "1"}, {"v1", "0"}, {"value", "1"}}},
	{"3", {{"bm0-calc", "9"}, {"bm0-calc_orient", "3"}, {"calc_id", "9"}, {"calc_orient_id", "4"}, {"calc_pos_id", "2"}, {"description", "НЕ ИЛИ"}, {"form", "!({v1}|{v0})"}, {"id", "3"}, {"name", "10"}, {"time", "1504459493"}, {"v0", "0"}, {"v1", "1"}, {"value", "2"}}},
	{"4", {{"bm0-calc", "16"}, {"bm0-calc_orient", "3"}, {"calc_id", "15"}, {"calc_orient_id", "2"}, {"calc_pos_id", "1"}, {"description", "НЕ И"}, {"form", "!({v1}&{v0})"}, {"id", "4"}, {"name", "11"}, {"time", "1504459501"}, {"v0", "1"}, {"v1", "1"}, {"value", "3"}}},
};

TMMs BM0_CALC_VAL = { // Значения
	{"1", {{"id", "1"}, {"name", "00"}, {"time", "1504459633"}, {"v0", "0"}, {"v1", "0"}, {"value", "0"}}},
	{"2", {{"id", "2"}, {"name", "01"}, {"time", "1504459642"}, {"v0", "1"}, {"v1", "0"}, {"value", "1"}}},
	{"3", {{"id", "3"}, {"name", "10"}, {"time", "1504459648"}, {"v0", "0"}, {"v1", "1"}, {"value", "2"}}},
	{"4", {{"id", "4"}, {"name", "11"}, {"time", "1504459656"}, {"v0", "1"}, {"v1", "1"}, {"value", "3"}}},
};

TMMs BM0_CALC_ORIENT = { // Ориентация
	{"1", {{"bm0-calc_balance", "8"}, {"bm0-calc_pos", "2"}, {"calc_balance_id", "1"}, {"calc_pos_id", "1"}, {"calc_val_id", "3"}, {"description", "Прямая"}, {"id", "1"}, {"name", "Верх"}, {"time", "1514131564"}}},
	{"2", {{"bm0-calc_balance", "8"}, {"bm0-calc_pos", "2"}, {"calc_balance_id", "5"}, {"calc_pos_id", "4"}, {"calc_val_id", "1"}, {"description", "Положительная"}, {"id", "2"}, {"name", "Право"}, {"time", "1514131572"}}},
	{"3", {{"bm0-calc_balance", "4"}, {"bm0-calc_pos", "3"}, {"calc_balance_id", "5"}, {"calc_pos_id", "4"}, {"calc_val_id", "2"}, {"description", "Обратная"}, {"id", "3"}, {"name", "Низ"}, {"time", "1514131576"}}},
	{"4", {{"bm0-calc_balance", "4"}, {"bm0-calc_pos", "3"}, {"calc_balance_id", "1"}, {"calc_pos_id", "1"}, {"calc_val_id", "4"}, {"description", "Отрицательная"}, {"id", "4"}, {"name", "Лево"}, {"time", "1514131580"}}},
};


TMMs BM0_CALC_BALANCE = { // Балансировка
	{"1", {{"bm0-calc_pos", "1"}, {"calc_orient_id", "2"}, {"calc_pos_id", "2"}, {"calc_val_id", "1"}, {"id", "1"}, {"name", "00=>0"}, {"val", "0"}}},
	{"2", {{"bm0-calc_pos", "3"}, {"calc_orient_id", "2"}, {"calc_pos_id", "1"}, {"calc_val_id", "2"}, {"id", "2"}, {"name", "01=>0"}, {"val", "0"}}},
	{"3", {{"bm0-calc_pos", "1"}, {"calc_orient_id", "2"}, {"calc_pos_id", "3"}, {"calc_val_id", "3"}, {"id", "3"}, {"name", "10=>0"}, {"val", "0"}}},
	{"4", {{"bm0-calc_pos", "3"}, {"calc_orient_id", "2"}, {"calc_pos_id", "4"}, {"calc_val_id", "4"}, {"id", "4"}, {"name", "11=>0"}, {"val", "0"}}},
	{"5", {{"bm0-calc_pos", "4"}, {"calc_orient_id", "4"}, {"calc_pos_id", "3"}, {"calc_val_id", "1"}, {"id", "5"}, {"name", "00=>1"}, {"val", "1"}}},
	{"6", {{"bm0-calc_pos", "4"}, {"calc_orient_id", "4"}, {"calc_pos_id", "2"}, {"calc_val_id", "2"}, {"id", "6"}, {"name", "01=>1"}, {"val", "1"}}},
	{"7", {{"bm0-calc_pos", "2"}, {"calc_orient_id", "4"}, {"calc_pos_id", "4"}, {"calc_val_id", "3"}, {"id", "7"}, {"name", "10=>1"}, {"val", "1"}}},
	{"8", {{"bm0-calc_pos", "2"}, {"calc_orient_id", "4"}, {"calc_pos_id", "1"}, {"calc_val_id", "4"}, {"id", "8"}, {"name", "11=>1"}, {"val", "1"}}},
};

TMMs BM0_CALC_AMEND = { // Изменения
	{"1", {{"id", "1"}, {"name", "Любой"}, {"time", "1504460121"}, {"v0", "1"}, {"v1", "1"}, {"value", "3"}}},
	{"2", {{"id", "2"}, {"name", "Младший"}, {"time", "1504460131"}, {"v0", "1"}, {"v1", "0"}, {"value", "1"}}},
	{"3", {{"id", "3"}, {"name", "Старший"}, {"time", "1504460136"}, {"v0", "0"}, {"v1", "1"}, {"value", "2"}}},
	{"4", {{"id", "4"}, {"name", "Оба"}, {"time", "1504460139"}, {"v0", "0"}, {"v1", "0"}, {"value", "0"}}},
};

TMMs BM0_CALC_SEQUENCE = { // Распиновка
	{"1", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "1"}, {"name", "0xxx"}, {"v0", ""}, {"v1", ""}, {"v2", ""}, {"v3", "0"}}},
	{"10", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "10"}, {"name", "010x"}, {"v0", ""}, {"v1", "0"}, {"v2", "1"}, {"v3", "0"}}},
	{"11", {{"calc_id", "5"}, {"calc_orient_id", "4"}, {"hide", "0"}, {"id", "11"}, {"name", "0100"}, {"v0", "0"}, {"v1", "0"}, {"v2", "1"}, {"v3", "0"}}},
	{"12", {{"calc_id", "6"}, {"calc_orient_id", "1"}, {"hide", "0"}, {"id", "12"}, {"name", "0101"}, {"v0", "1"}, {"v1", "0"}, {"v2", "1"}, {"v3", "0"}}},
	{"13", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "13"}, {"name", "011x"}, {"v0", ""}, {"v1", "1"}, {"v2", "1"}, {"v3", "0"}}},
	{"14", {{"calc_id", "7"}, {"calc_orient_id", "4"}, {"hide", "0"}, {"id", "14"}, {"name", "0110"}, {"v0", "0"}, {"v1", "1"}, {"v2", "1"}, {"v3", "0"}}},
	{"15", {{"calc_id", "8"}, {"calc_orient_id", "2"}, {"hide", "0"}, {"id", "15"}, {"name", "0111"}, {"v0", "1"}, {"v1", "1"}, {"v2", "1"}, {"v3", "0"}}},
	{"16", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "16"}, {"name", "1xxx"}, {"v0", ""}, {"v1", ""}, {"v2", ""}, {"v3", "1"}}},
	{"17", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "17"}, {"name", "10xx"}, {"v0", ""}, {"v1", ""}, {"v2", "0"}, {"v3", "1"}}},
	{"18", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "18"}, {"name", "100x"}, {"v0", ""}, {"v1", "0"}, {"v2", "0"}, {"v3", "1"}}},
	{"19", {{"calc_id", "9"}, {"calc_orient_id", "4"}, {"hide", "0"}, {"id", "19"}, {"name", "1000"}, {"v0", "0"}, {"v1", "0"}, {"v2", "0"}, {"v3", "1"}}},
	{"2", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "2"}, {"name", "00xx"}, {"v0", ""}, {"v1", ""}, {"v2", "0"}, {"v3", "0"}}},
	{"20", {{"calc_id", "10"}, {"calc_orient_id", "2"}, {"hide", "0"}, {"id", "20"}, {"name", "1001"}, {"v0", "1"}, {"v1", "0"}, {"v2", "0"}, {"v3", "1"}}},
	{"21", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "21"}, {"name", "101x"}, {"v0", ""}, {"v1", "1"}, {"v2", "0"}, {"v3", "1"}}},
	{"22", {{"calc_id", "11"}, {"calc_orient_id", "3"}, {"hide", "0"}, {"id", "22"}, {"name", "1010"}, {"v0", "0"}, {"v1", "1"}, {"v2", "0"}, {"v3", "1"}}},
	{"23", {{"calc_id", "12"}, {"calc_orient_id", "2"}, {"hide", "0"}, {"id", "23"}, {"name", "1011"}, {"v0", "1"}, {"v1", "1"}, {"v2", "0"}, {"v3", "1"}}},
	{"24", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "24"}, {"name", "11xx"}, {"v0", ""}, {"v1", ""}, {"v2", "1"}, {"v3", "1"}}},
	{"25", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "25"}, {"name", "110x"}, {"v0", ""}, {"v1", "0"}, {"v2", "1"}, {"v3", "1"}}},
	{"26", {{"calc_id", "13"}, {"calc_orient_id", "3"}, {"hide", "0"}, {"id", "26"}, {"name", "1100"}, {"v0", "0"}, {"v1", "0"}, {"v2", "1"}, {"v3", "1"}}},
	{"27", {{"calc_id", "14"}, {"calc_orient_id", "2"}, {"hide", "0"}, {"id", "27"}, {"name", "1101"}, {"v0", "1"}, {"v1", "0"}, {"v2", "1"}, {"v3", "1"}}},
	{"28", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "28"}, {"name", "111x"}, {"v0", ""}, {"v1", "1"}, {"v2", "1"}, {"v3", "1"}}},
	{"29", {{"calc_id", "15"}, {"calc_orient_id", "2"}, {"hide", "0"}, {"id", "29"}, {"name", "1110"}, {"v0", "0"}, {"v1", "1"}, {"v2", "1"}, {"v3", "1"}}},
	{"3", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "3"}, {"name", "000x"}, {"v0", ""}, {"v1", "0"}, {"v2", "0"}, {"v3", "0"}}},
	{"30", {{"calc_id", "16"}, {"calc_orient_id", "2"}, {"hide", "0"}, {"id", "30"}, {"name", "1111"}, {"v0", "1"}, {"v1", "1"}, {"v2", "1"}, {"v3", "1"}}},
	{"31", {{"calc_id", ""}, {"calc_orient_id", "4"}, {"hide", "0"}, {"id", "31"}, {"name", "00x0"}, {"v0", "0"}, {"v1", ""}, {"v2", "0"}, {"v3", "0"}}},
	{"32", {{"calc_id", ""}, {"calc_orient_id", "1"}, {"hide", "0"}, {"id", "32"}, {"name", "00x1"}, {"v0", "1"}, {"v1", ""}, {"v2", "0"}, {"v3", "0"}}},
	{"33", {{"calc_id", ""}, {"calc_orient_id", "4"}, {"hide", "0"}, {"id", "33"}, {"name", "01x0"}, {"v0", "0"}, {"v1", ""}, {"v2", "1"}, {"v3", "0"}}},
	{"34", {{"calc_id", ""}, {"calc_orient_id", "1"}, {"hide", "0"}, {"id", "34"}, {"name", "01x1"}, {"v0", "1"}, {"v1", ""}, {"v2", "1"}, {"v3", "0"}}},
	{"35", {{"calc_id", ""}, {"calc_orient_id", "3"}, {"hide", "0"}, {"id", "35"}, {"name", "10x0"}, {"v0", "0"}, {"v1", ""}, {"v2", "0"}, {"v3", "1"}}},
	{"36", {{"calc_id", ""}, {"calc_orient_id", "2"}, {"hide", "0"}, {"id", "36"}, {"name", "10x1"}, {"v0", "1"}, {"v1", ""}, {"v2", "0"}, {"v3", "1"}}},
	{"37", {{"calc_id", ""}, {"calc_orient_id", "3"}, {"hide", "0"}, {"id", "37"}, {"name", "11x0"}, {"v0", "0"}, {"v1", ""}, {"v2", "1"}, {"v3", "1"}}},
	{"38", {{"calc_id", ""}, {"calc_orient_id", "2"}, {"hide", "0"}, {"id", "38"}, {"name", "11x1"}, {"v0", "1"}, {"v1", ""}, {"v2", "1"}, {"v3", "1"}}},
	{"39", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "39"}, {"name", "0x0x"}, {"v0", ""}, {"v1", "0"}, {"v2", ""}, {"v3", "0"}}},
	{"4", {{"calc_id", "1"}, {"calc_orient_id", "4"}, {"hide", "0"}, {"id", "4"}, {"name", "0000"}, {"v0", "0"}, {"v1", "0"}, {"v2", "0"}, {"v3", "0"}}},
	{"40", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "40"}, {"name", "0x1x"}, {"v0", ""}, {"v1", "1"}, {"v2", ""}, {"v3", "0"}}},
	{"41", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "41"}, {"name", "1x0x"}, {"v0", ""}, {"v1", "0"}, {"v2", ""}, {"v3", "1"}}},
	{"42", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "42"}, {"name", "1x1x"}, {"v0", ""}, {"v1", "1"}, {"v2", ""}, {"v3", "1"}}},
	{"43", {{"calc_id", ""}, {"calc_orient_id", "4"}, {"hide", "0"}, {"id", "43"}, {"name", "0xx0"}, {"v0", "0"}, {"v1", ""}, {"v2", ""}, {"v3", "0"}}},
	{"44", {{"calc_id", ""}, {"calc_orient_id", "1"}, {"hide", "0"}, {"id", "44"}, {"name", "0xx1"}, {"v0", "1"}, {"v1", ""}, {"v2", ""}, {"v3", "0"}}},
	{"45", {{"calc_id", ""}, {"calc_orient_id", "3"}, {"hide", "0"}, {"id", "45"}, {"name", "1xx0"}, {"v0", "0"}, {"v1", ""}, {"v2", ""}, {"v3", "1"}}},
	{"46", {{"calc_id", ""}, {"calc_orient_id", "2"}, {"hide", "0"}, {"id", "46"}, {"name", "1xx1"}, {"v0", "1"}, {"v1", ""}, {"v2", ""}, {"v3", "1"}}},
	{"47", {{"calc_id", ""}, {"calc_orient_id", "4"}, {"hide", "0"}, {"id", "47"}, {"name", "0x00"}, {"v0", "0"}, {"v1", "0"}, {"v2", ""}, {"v3", "0"}}},
	{"48", {{"calc_id", ""}, {"calc_orient_id", "1"}, {"hide", "0"}, {"id", "48"}, {"name", "0x01"}, {"v0", "1"}, {"v1", "0"}, {"v2", ""}, {"v3", "0"}}},
	{"49", {{"calc_id", ""}, {"calc_orient_id", "4"}, {"hide", "0"}, {"id", "49"}, {"name", "0x10"}, {"v0", "0"}, {"v1", "1"}, {"v2", ""}, {"v3", "0"}}},
	{"5", {{"calc_id", "2"}, {"calc_orient_id", "4"}, {"hide", "0"}, {"id", "5"}, {"name", "0001"}, {"v0", "1"}, {"v1", "0"}, {"v2", "0"}, {"v3", "0"}}},
	{"50", {{"calc_id", ""}, {"calc_orient_id", "1"}, {"hide", "0"}, {"id", "50"}, {"name", "0x11"}, {"v0", "1"}, {"v1", "1"}, {"v2", ""}, {"v3", "0"}}},
	{"51", {{"calc_id", ""}, {"calc_orient_id", "3"}, {"hide", "0"}, {"id", "51"}, {"name", "1x00"}, {"v0", "0"}, {"v1", "0"}, {"v2", ""}, {"v3", "1"}}},
	{"52", {{"calc_id", ""}, {"calc_orient_id", "2"}, {"hide", "0"}, {"id", "52"}, {"name", "1x01"}, {"v0", "1"}, {"v1", "0"}, {"v2", ""}, {"v3", "1"}}},
	{"53", {{"calc_id", ""}, {"calc_orient_id", "3"}, {"hide", "0"}, {"id", "53"}, {"name", "1x10"}, {"v0", "0"}, {"v1", "1"}, {"v2", ""}, {"v3", "1"}}},
	{"54", {{"calc_id", ""}, {"calc_orient_id", "2"}, {"hide", "0"}, {"id", "54"}, {"name", "1x11"}, {"v0", "1"}, {"v1", "1"}, {"v2", ""}, {"v3", "1"}}},
	{"55", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "55"}, {"name", "x0xx"}, {"v0", ""}, {"v1", ""}, {"v2", "0"}, {"v3", ""}}},
	{"56", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "56"}, {"name", "x1xx"}, {"v0", ""}, {"v1", ""}, {"v2", "1"}, {"v3", ""}}},
	{"57", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "57"}, {"name", "xx0x"}, {"v0", ""}, {"v1", "0"}, {"v2", ""}, {"v3", ""}}},
	{"58", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "58"}, {"name", "xx1x"}, {"v0", ""}, {"v1", "1"}, {"v2", ""}, {"v3", ""}}},
	{"59", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "59"}, {"name", "xxx0"}, {"v0", "0"}, {"v1", ""}, {"v2", ""}, {"v3", ""}}},
	{"6", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "6"}, {"name", "001x"}, {"v0", ""}, {"v1", "1"}, {"v2", "0"}, {"v3", "0"}}},
	{"60", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "60"}, {"name", "xxx1"}, {"v0", "1"}, {"v1", ""}, {"v2", ""}, {"v3", ""}}},
	{"61", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "61"}, {"name", "x00x"}, {"v0", ""}, {"v1", "0"}, {"v2", "0"}, {"v3", ""}}},
	{"62", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "62"}, {"name", "x01x"}, {"v0", ""}, {"v1", "1"}, {"v2", "0"}, {"v3", ""}}},
	{"63", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "63"}, {"name", "x10x"}, {"v0", ""}, {"v1", "0"}, {"v2", "1"}, {"v3", ""}}},
	{"64", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "64"}, {"name", "x11x"}, {"v0", ""}, {"v1", "1"}, {"v2", "1"}, {"v3", ""}}},
	{"65", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "65"}, {"name", "x0x0"}, {"v0", "0"}, {"v1", ""}, {"v2", "0"}, {"v3", ""}}},
	{"66", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "66"}, {"name", "x0x1"}, {"v0", "1"}, {"v1", ""}, {"v2", "0"}, {"v3", ""}}},
	{"67", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "67"}, {"name", "x1x0"}, {"v0", "0"}, {"v1", ""}, {"v2", "1"}, {"v3", ""}}},
	{"68", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "68"}, {"name", "x1x1"}, {"v0", "1"}, {"v1", ""}, {"v2", "1"}, {"v3", ""}}},
	{"69", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "69"}, {"name", "xx00"}, {"v0", "0"}, {"v1", "0"}, {"v2", ""}, {"v3", ""}}},
	{"7", {{"calc_id", "3"}, {"calc_orient_id", "4"}, {"hide", "0"}, {"id", "7"}, {"name", "0010"}, {"v0", "0"}, {"v1", "1"}, {"v2", "0"}, {"v3", "0"}}},
	{"70", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "70"}, {"name", "xx01"}, {"v0", "1"}, {"v1", "0"}, {"v2", ""}, {"v3", ""}}},
	{"71", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "71"}, {"name", "xx10"}, {"v0", "0"}, {"v1", "1"}, {"v2", ""}, {"v3", ""}}},
	{"72", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "72"}, {"name", "xx11"}, {"v0", "1"}, {"v1", "1"}, {"v2", ""}, {"v3", ""}}},
	{"73", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "73"}, {"name", "x000"}, {"v0", "0"}, {"v1", "0"}, {"v2", "0"}, {"v3", ""}}},
	{"74", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "74"}, {"name", "x001"}, {"v0", "1"}, {"v1", "0"}, {"v2", "0"}, {"v3", ""}}},
	{"75", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "75"}, {"name", "x010"}, {"v0", "0"}, {"v1", "1"}, {"v2", "0"}, {"v3", ""}}},
	{"76", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "76"}, {"name", "x011"}, {"v0", "1"}, {"v1", "1"}, {"v2", "0"}, {"v3", ""}}},
	{"77", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "77"}, {"name", "x100"}, {"v0", "0"}, {"v1", "0"}, {"v2", "1"}, {"v3", ""}}},
	{"78", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "78"}, {"name", "x101"}, {"v0", "1"}, {"v1", "0"}, {"v2", "1"}, {"v3", ""}}},
	{"79", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "79"}, {"name", "x110"}, {"v0", "0"}, {"v1", "1"}, {"v2", "1"}, {"v3", ""}}},
	{"8", {{"calc_id", "4"}, {"calc_orient_id", "1"}, {"hide", "0"}, {"id", "8"}, {"name", "0011"}, {"v0", "1"}, {"v1", "1"}, {"v2", "0"}, {"v3", "0"}}},
	{"80", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "80"}, {"name", "x111"}, {"v0", "1"}, {"v1", "1"}, {"v2", "1"}, {"v3", ""}}},
	{"81", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "81"}, {"name", "xxxx"}, {"v0", ""}, {"v1", ""}, {"v2", ""}, {"v3", ""}}},
	{"9", {{"calc_id", ""}, {"calc_orient_id", ""}, {"hide", "1"}, {"id", "9"}, {"name", "01xx"}, {"v0", ""}, {"v1", ""}, {"v2", "1"}, {"v3", "0"}}},
};


